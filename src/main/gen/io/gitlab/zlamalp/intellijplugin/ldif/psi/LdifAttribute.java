// This is a generated file. Not intended for manual editing.
package io.gitlab.zlamalp.intellijplugin.ldif.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface LdifAttribute extends PsiElement {

  @NotNull
  LdifAttributeKey getAttributeKey();

  @Nullable
  LdifValueBase64 getValueBase64();

  @Nullable
  LdifValueNormal getValueNormal();

  @Nullable
  LdifValueUrl getValueUrl();

}
