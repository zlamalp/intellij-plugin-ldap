// This is a generated file. Not intended for manual editing.
package io.gitlab.zlamalp.intellijplugin.ldif.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;
import com.intellij.navigation.ItemPresentation;

public interface LdifDn extends LdifNamedElement {

  @Nullable
  LdifValueBase64 getValueBase64();

  @Nullable
  LdifValueNormal getValueNormal();

  PsiElement getValue();

  String getName();

  PsiElement setName(String newName);

  ItemPresentation getPresentation();

  PsiElement getNameIdentifier();

}
