// This is a generated file. Not intended for manual editing.
package io.gitlab.zlamalp.intellijplugin.ldif.psi;

import com.intellij.psi.tree.IElementType;
import com.intellij.psi.PsiElement;
import com.intellij.lang.ASTNode;
import io.gitlab.zlamalp.intellijplugin.ldif.psi.impl.*;

public interface LdifTypes {

  IElementType ATTRIBUTE = new LdifElementType("ATTRIBUTE");
  IElementType ATTRIBUTE_KEY = new LdifElementType("ATTRIBUTE_KEY");
  IElementType CHANGETYPE = new LdifElementType("CHANGETYPE");
  IElementType CHANGE_ADD = new LdifElementType("CHANGE_ADD");
  IElementType CHANGE_DELETE = new LdifElementType("CHANGE_DELETE");
  IElementType CHANGE_DELOLDRDN = new LdifElementType("CHANGE_DELOLDRDN");
  IElementType CHANGE_MODDN = new LdifElementType("CHANGE_MODDN");
  IElementType CHANGE_MODIFY = new LdifElementType("CHANGE_MODIFY");
  IElementType CHANGE_MODIFY_ADD = new LdifElementType("CHANGE_MODIFY_ADD");
  IElementType CHANGE_MODIFY_DELETE = new LdifElementType("CHANGE_MODIFY_DELETE");
  IElementType CHANGE_MODIFY_REPLACE = new LdifElementType("CHANGE_MODIFY_REPLACE");
  IElementType CHANGE_NEWRDN = new LdifElementType("CHANGE_NEWRDN");
  IElementType CHANGE_NEWSUP = new LdifElementType("CHANGE_NEWSUP");
  IElementType CONTROL = new LdifElementType("CONTROL");
  IElementType DN = new LdifElementType("DN");
  IElementType ENTRY = new LdifElementType("ENTRY");
  IElementType OPTION = new LdifElementType("OPTION");
  IElementType VALUE_BASE_64 = new LdifElementType("VALUE_BASE_64");
  IElementType VALUE_NORMAL = new LdifElementType("VALUE_NORMAL");
  IElementType VALUE_URL = new LdifElementType("VALUE_URL");
  IElementType VERSION = new LdifElementType("VERSION");

  IElementType ADD = new LdifTokenType("ADD");
  IElementType COMMENT = new LdifTokenType("COMMENT");
  IElementType CRITICALITY = new LdifTokenType("CRITICALITY");
  IElementType CRLF = new LdifTokenType("CRLF");
  IElementType DASH = new LdifTokenType("DASH");
  IElementType DELETE = new LdifTokenType("DELETE");
  IElementType DELOLDRDN = new LdifTokenType("DELOLDRDN");
  IElementType KEY = new LdifTokenType("KEY");
  IElementType KEY_ADD = new LdifTokenType("KEY_ADD");
  IElementType KEY_CHANGE = new LdifTokenType("KEY_CHANGE");
  IElementType KEY_CONTROL = new LdifTokenType("KEY_CONTROL");
  IElementType KEY_DELETE = new LdifTokenType("KEY_DELETE");
  IElementType KEY_DN = new LdifTokenType("KEY_DN");
  IElementType KEY_REPLACE = new LdifTokenType("KEY_REPLACE");
  IElementType KEY_VERSION = new LdifTokenType("KEY_VERSION");
  IElementType MODDN = new LdifTokenType("MODDN");
  IElementType MODIFY = new LdifTokenType("MODIFY");
  IElementType MODRDN = new LdifTokenType("MODRDN");
  IElementType NEWRDN = new LdifTokenType("NEWRDN");
  IElementType NEWSUP = new LdifTokenType("NEWSUP");
  IElementType OPT = new LdifTokenType("OPT");
  IElementType OPT_SEP = new LdifTokenType("OPT_SEP");
  IElementType SEPARATOR = new LdifTokenType("SEPARATOR");
  IElementType SEPARATOR_64 = new LdifTokenType("SEPARATOR_64");
  IElementType SEPARATOR_FILE = new LdifTokenType("SEPARATOR_FILE");
  IElementType VALUE = new LdifTokenType("VALUE");
  IElementType VALUE_64 = new LdifTokenType("VALUE_64");
  IElementType VALUE_DIGIT = new LdifTokenType("VALUE_DIGIT");
  IElementType VALUE_FILE = new LdifTokenType("VALUE_FILE");
  IElementType VALUE_OID = new LdifTokenType("VALUE_OID");
  IElementType WHITE_SPACE = new LdifTokenType("WHITE_SPACE");

  class Factory {
    public static PsiElement createElement(ASTNode node) {
      IElementType type = node.getElementType();
      if (type == ATTRIBUTE) {
        return new LdifAttributeImpl(node);
      }
      else if (type == ATTRIBUTE_KEY) {
        return new LdifAttributeKeyImpl(node);
      }
      else if (type == CHANGETYPE) {
        return new LdifChangetypeImpl(node);
      }
      else if (type == CHANGE_ADD) {
        return new LdifChangeAddImpl(node);
      }
      else if (type == CHANGE_DELETE) {
        return new LdifChangeDeleteImpl(node);
      }
      else if (type == CHANGE_DELOLDRDN) {
        return new LdifChangeDeloldrdnImpl(node);
      }
      else if (type == CHANGE_MODDN) {
        return new LdifChangeModdnImpl(node);
      }
      else if (type == CHANGE_MODIFY) {
        return new LdifChangeModifyImpl(node);
      }
      else if (type == CHANGE_MODIFY_ADD) {
        return new LdifChangeModifyAddImpl(node);
      }
      else if (type == CHANGE_MODIFY_DELETE) {
        return new LdifChangeModifyDeleteImpl(node);
      }
      else if (type == CHANGE_MODIFY_REPLACE) {
        return new LdifChangeModifyReplaceImpl(node);
      }
      else if (type == CHANGE_NEWRDN) {
        return new LdifChangeNewrdnImpl(node);
      }
      else if (type == CHANGE_NEWSUP) {
        return new LdifChangeNewsupImpl(node);
      }
      else if (type == CONTROL) {
        return new LdifControlImpl(node);
      }
      else if (type == DN) {
        return new LdifDnImpl(node);
      }
      else if (type == ENTRY) {
        return new LdifEntryImpl(node);
      }
      else if (type == OPTION) {
        return new LdifOptionImpl(node);
      }
      else if (type == VALUE_BASE_64) {
        return new LdifValueBase64Impl(node);
      }
      else if (type == VALUE_NORMAL) {
        return new LdifValueNormalImpl(node);
      }
      else if (type == VALUE_URL) {
        return new LdifValueUrlImpl(node);
      }
      else if (type == VERSION) {
        return new LdifVersionImpl(node);
      }
      throw new AssertionError("Unknown element type: " + type);
    }
  }
}
