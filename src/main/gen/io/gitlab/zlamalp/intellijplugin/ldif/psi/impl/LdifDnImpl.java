// This is a generated file. Not intended for manual editing.
package io.gitlab.zlamalp.intellijplugin.ldif.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static io.gitlab.zlamalp.intellijplugin.ldif.psi.LdifTypes.*;
import io.gitlab.zlamalp.intellijplugin.ldif.psi.LdifNamedElementImpl;
import io.gitlab.zlamalp.intellijplugin.ldif.psi.*;
import com.intellij.navigation.ItemPresentation;

public class LdifDnImpl extends LdifNamedElementImpl implements LdifDn {

  public LdifDnImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull LdifVisitor visitor) {
    visitor.visitDn(this);
  }

  @Override
  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof LdifVisitor) accept((LdifVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public LdifValueBase64 getValueBase64() {
    return findChildByClass(LdifValueBase64.class);
  }

  @Override
  @Nullable
  public LdifValueNormal getValueNormal() {
    return findChildByClass(LdifValueNormal.class);
  }

  @Override
  public PsiElement getValue() {
    return LdifPsiUtil.getValue(this);
  }

  @Override
  public String getName() {
    return LdifPsiUtil.getName(this);
  }

  @Override
  public PsiElement setName(String newName) {
    return LdifPsiUtil.setName(this, newName);
  }

  @Override
  public ItemPresentation getPresentation() {
    return LdifPsiUtil.getPresentation(this);
  }

  @Override
  public PsiElement getNameIdentifier() {
    return LdifPsiUtil.getNameIdentifier(this);
  }

}
