// This is a generated file. Not intended for manual editing.
package io.gitlab.zlamalp.intellijplugin.ldif.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface LdifChangeModdn extends PsiElement {

  @Nullable
  LdifChangeDeloldrdn getChangeDeloldrdn();

  @Nullable
  LdifChangeNewrdn getChangeNewrdn();

  @Nullable
  LdifChangeNewsup getChangeNewsup();

}
