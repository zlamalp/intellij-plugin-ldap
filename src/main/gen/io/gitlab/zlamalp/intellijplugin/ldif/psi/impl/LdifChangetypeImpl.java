// This is a generated file. Not intended for manual editing.
package io.gitlab.zlamalp.intellijplugin.ldif.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static io.gitlab.zlamalp.intellijplugin.ldif.psi.LdifTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import io.gitlab.zlamalp.intellijplugin.ldif.psi.*;

public class LdifChangetypeImpl extends ASTWrapperPsiElement implements LdifChangetype {

  public LdifChangetypeImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull LdifVisitor visitor) {
    visitor.visitChangetype(this);
  }

  @Override
  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof LdifVisitor) accept((LdifVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public LdifChangeAdd getChangeAdd() {
    return findChildByClass(LdifChangeAdd.class);
  }

  @Override
  @Nullable
  public LdifChangeDelete getChangeDelete() {
    return findChildByClass(LdifChangeDelete.class);
  }

  @Override
  @Nullable
  public LdifChangeModdn getChangeModdn() {
    return findChildByClass(LdifChangeModdn.class);
  }

  @Override
  @Nullable
  public LdifChangeModify getChangeModify() {
    return findChildByClass(LdifChangeModify.class);
  }

}
