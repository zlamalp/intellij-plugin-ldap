// This is a generated file. Not intended for manual editing.
package io.gitlab.zlamalp.intellijplugin.ldif.psi.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static io.gitlab.zlamalp.intellijplugin.ldif.psi.LdifTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import io.gitlab.zlamalp.intellijplugin.ldif.psi.*;

public class LdifAttributeImpl extends ASTWrapperPsiElement implements LdifAttribute {

  public LdifAttributeImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull LdifVisitor visitor) {
    visitor.visitAttribute(this);
  }

  @Override
  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof LdifVisitor) accept((LdifVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public LdifAttributeKey getAttributeKey() {
    return findNotNullChildByClass(LdifAttributeKey.class);
  }

  @Override
  @Nullable
  public LdifValueBase64 getValueBase64() {
    return findChildByClass(LdifValueBase64.class);
  }

  @Override
  @Nullable
  public LdifValueNormal getValueNormal() {
    return findChildByClass(LdifValueNormal.class);
  }

  @Override
  @Nullable
  public LdifValueUrl getValueUrl() {
    return findChildByClass(LdifValueUrl.class);
  }

}
