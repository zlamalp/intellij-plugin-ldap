package io.gitlab.zlamalp.intellijplugin.ldap.editor;

import com.intellij.icons.AllIcons;
import com.intellij.LdapBundle;
import com.intellij.openapi.ui.ComponentValidator;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.ui.ValidationInfo;
import com.intellij.ui.components.fields.ExtendableTextComponent;
import com.intellij.ui.components.fields.ExtendableTextField;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapIcons;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.AttributeValueEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.schema.SchemaUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.schema.SchemaSettings;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import org.apache.commons.lang.StringUtils;
import org.apache.directory.api.ldap.model.entry.Value;
import org.apache.directory.api.ldap.model.exception.LdapInvalidAttributeValueException;
import org.apache.directory.api.ldap.model.message.ResultCodeEnum;
import org.apache.directory.api.ldap.model.schema.AttributeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableModel;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EventObject;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;

/**
 * CellEditor component used to edit Cell in JBTable representing LDAP Entry Attribute values.
 *
 * @see AttributeTableModel
 * @see AttributeModelItem
 *
 * @author Attila Majoros
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class AttributeTableCellEditor implements TableCellEditor {

	private static Logger log = LoggerFactory.getLogger(AttributeTableCellEditor.class);

	private LdapEntryEditor editor;
	private LdapEntryTreeNode ldapNode;
	private AttributeTable table;
	private List<CellEditorListener> listeners;
	private Value newEditorValue;
	private Value currentEditorValue;
	private boolean editingStoppedFromInside = false;
	private Integer previousRowHeight = null;
	private Integer previousRowIndex = null;

	private volatile static boolean isValidValue = true;

	public AttributeTableCellEditor(LdapEntryEditor editor) {
		this.editor = editor;
		this.ldapNode = editor.getLdapNode();
		this.table = editor.getTable();
		this.listeners = new ArrayList<>();
	}

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {

		previousRowHeight = table.getRowHeight(row);
		previousRowIndex = row;

		AttributeModelItem selectedItem = getSelectedItem();
		if (selectedItem != null) {

			// attribute is not not editable for users
			if (!selectedItem.isUserModifiable()) return null;

			// fill editor with current value from model
			currentEditorValue = (Value)((AttributeModelItem)value).getRawValue();

			// reset the editing status if UI reuses the widget
			editingStoppedFromInside = false;

			if (LdapUtils.binaryAttributeDetector.isBinary(selectedItem.getAttribute().getId()) ||
					(selectedItem.getRawValue() != null && !selectedItem.getRawValue().isHumanReadable()) ||
					(selectedItem.getStringValue() != null && selectedItem.getStringValue().contains("\n"))) {
				// popup editing for binary or non-human-readable or multi-line attributes
				openValueEditor(selectedItem, null);
			} else {
				// inline editing for human-readable or non-binary -> String attributes
				Component comp =  createTextFieldForEditing(selectedItem, (Value)((AttributeModelItem)value).getRawValue());
				table.setRowHeight(row, (int)comp.getPreferredSize().getHeight());
				return comp;
			}
		}
		// no row/cell selected
		return null;
	}

	@Override
	public Object getCellEditorValue() {
		return currentEditorValue;
	}

	@Override
	public boolean isCellEditable(EventObject anEvent) {
		return isDoubleClick(anEvent);
	}

	@Override
	public boolean shouldSelectCell(EventObject anEvent) {
		return true;
	}

	@Override
	public boolean stopCellEditing() {

		// cancel stop editing, since new value is not valid
		if (!isValidValue) return false;

		currentEditorValue = newEditorValue;
		fireEditingStopped(createChangeEvent(this));
		if (previousRowIndex != null && previousRowIndex >= 0 && previousRowHeight != null) {
			table.setRowHeight(previousRowIndex, previousRowHeight);
		}
		return true;
	}

	@Override
	public void cancelCellEditing() {
		fireEditingCanceled(createChangeEvent(this));
		if (previousRowIndex != null && previousRowIndex >= 0 && previousRowHeight != null) {
			table.setRowHeight(previousRowIndex, previousRowHeight);
		}
	}

	@Override
	public void addCellEditorListener(CellEditorListener l) {
		listeners.add(l);
	}

	@Override
	public void removeCellEditorListener(CellEditorListener l) {
		listeners.remove(l);
	}

	private AttributeTableModel getModel() {
		TableModel model = table.getModel();
		if (model instanceof AttributeTableModel) {
			return (AttributeTableModel) model;
		}
		return null;
	}

	private AttributeModelItem getSelectedItem() {
		int selectedRow = table.getSelectedRow();
		AttributeTableModel model = getModel();
		return model != null ? selectedRow != -1 ? model.getItems().get(table.convertRowIndexToModel(selectedRow)) : null : null;
	}

	private boolean isDoubleClick(EventObject eventObject) {
		return (eventObject instanceof MouseEvent) && ((MouseEvent) eventObject).getClickCount() > 1;
	}

	private ChangeEvent createChangeEvent(Object source) {
		return new ChangeEvent(source);
	}

	private void fireEditingStopped(ChangeEvent changeEvent) {
		List<CellEditorListener> listenersCopy = new ArrayList<>(listeners);
		Collections.reverse(listenersCopy);
		for (CellEditorListener listener : listenersCopy) {
			listener.editingStopped(changeEvent);
		}
	}

	private void fireEditingCanceled(ChangeEvent changeEvent) {
		List<CellEditorListener> listenersCopy = new ArrayList<>(listeners);
		Collections.reverse(listenersCopy);
		for (CellEditorListener listener : listenersCopy) {
			listener.editingCanceled(changeEvent);
		}
	}


	/**
	 * Generate CellEditor component used to edit table cell
	 *
	 * @param value Value from Cell
	 * @return CellEditor component
	 */
	private Component createTextFieldForEditing(AttributeModelItem selectedItem, Value value) {

		// TODO - would be nice to be expandable, but we must somehow disable default parser/joiner, otherwise spaces are converted to newlines on expansion.
		ExtendableTextField textField = new ExtendableTextField();
		textField.addExtension(new ExtendableTextComponent.Extension() {
			@Override
			public Icon getIcon(boolean hovered) {
				return AllIcons.General.ExpandComponent;
			}

			@Override
			public Runnable getActionOnClick() {
				return new Runnable() {
					@Override
					public void run() {
						openValueEditor(selectedItem, textField);
					}
				};
			}

			@Override
			public boolean isIconBeforeText() {
				return true;
			}

			@Override
			public String getTooltip() {
				return LdapBundle.message("ldap.editor.value.inline.tooltip.text");
			}

		});

		// input validation
		new ComponentValidator(editor).withValidator(new Supplier<ValidationInfo>() {
			@Override
			public ValidationInfo get() {

				SchemaSettings.SchemaProvider schemaProvider = LdapUtils.getPreferredLdapSettings(selectedItem.getLdapNode().getLdapServer().getSettings()).getSchemaSettings().getSchemaProvider();

				// we are supposed to add new value if previous was null or empty
				boolean isAddingNewValue = (currentEditorValue == null || currentEditorValue.isNull());

				if (Objects.equals(SchemaSettings.SchemaProvider.NONE, schemaProvider)) {

					// no schema validation, just basic (not)empty state
					isValidValue = true;
					if (StringUtils.isEmpty(textField.getText())) {
						return new ValidationInfo(LdapBundle.message("ldap.editor.value.validator.isEmptyNoSchema.text"), textField).asWarning().withOKEnabled();
					} else {
						return null;
					}

				} else {

					// use schema validation
					LdapInvalidAttributeValueException wrongSyntax = null;
					long maxLength = 0;
					AttributeType type = SchemaUtils.getAttributeType(selectedItem.getLdapNode(), selectedItem.getAttribute());
					try {
						if (type != null) {
							maxLength = type.getSyntaxLength();
							new Value(type, textField.getText());
						}
					} catch (LdapInvalidAttributeValueException e) {
						wrongSyntax = e;
					} catch (IllegalArgumentException ex) {
						// thrown when value can't be normalized
						wrongSyntax = new LdapInvalidAttributeValueException(ResultCodeEnum.INVALID_ATTRIBUTE_SYNTAX, ex.getMessage());
					}

					if ((SchemaUtils.isMustAttribute(selectedItem.getLdapNode(), selectedItem.getAttribute().getId()) && selectedItem.getAttribute().size() <= 1)
							&& StringUtils.isBlank(textField.getText())) {
						isValidValue = false;
						return new ValidationInfo(LdapBundle.message("ldap.editor.value.validator.cantBeEmptyRequired.text", selectedItem.getAttribute().getUpId()), textField);
					} else if (maxLength > 0 && textField.getText().length() > maxLength) {
						isValidValue = false;
						return new ValidationInfo(LdapBundle.message("ldap.editor.value.validator.toLong.text", maxLength), textField);
					} else if (wrongSyntax != null && !isAddingNewValue) {
						if (StringUtils.isNotBlank(textField.getText())) {
							isValidValue = false;
							if (type.getSyntax() != null) {
								return new ValidationInfo(LdapBundle.message("ldap.editor.value.validator.invalidSyntaxExpected.text", type.getSyntax().getDescription()), textField);
							} else {
								return new ValidationInfo(LdapBundle.message("ldap.editor.value.validator.invalidSyntax.text", textField));
							}
						} else {
							isValidValue = true;
							return new ValidationInfo(LdapBundle.message("ldap.editor.value.validator.isEmpty.text"), textField).asWarning().withOKEnabled();
						}
					} else if (wrongSyntax != null) {
						// case of adding new value
						isValidValue = false;
						return new ValidationInfo(LdapBundle.message("ldap.editor.value.validator.cantBeEmpty.text"), textField);
					} else if (isAddingNewValue) {
						if (StringUtils.isBlank(textField.getText())) {
							// case of adding new value
							isValidValue = false;
							return new ValidationInfo(LdapBundle.message("ldap.editor.value.validator.cantBeEmpty.text"), textField);
						} else {
							isValidValue = true;
							return null;
						}
					} else {
						isValidValue = true;
						return null;
					}

				}

			}
		}).installOn(textField).andRegisterOnDocumentListener(textField);

		// set value to text field
		newEditorValue = value;
		textField.setText(selectedItem.getValueProvider().getStringValue());

		// handle value submit on ENTER
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {

				if (e.getKeyCode() == KeyEvent.VK_ENTER && isValidValue) {

					e.consume();

					SchemaSettings.SchemaProvider schemaProvider = LdapUtils.getPreferredLdapSettings(selectedItem.getLdapNode().getLdapServer().getSettings()).getSchemaSettings().getSchemaProvider();
					boolean noSchemaValidation = Objects.equals(SchemaSettings.SchemaProvider.NONE, schemaProvider);
					boolean removeEmpty = true;

					try {
						newEditorValue = selectedItem.getValueProvider().constructNewValue(textField.getText());
					} catch (LdapInvalidAttributeValueException | IllegalArgumentException ex) {
						log.error("Can't create new editor value.", ex);
						newEditorValue = null;
					}

					// value not changed
					if (Objects.equals(currentEditorValue, newEditorValue)) {
						cancelCellEditing();
						return;
					}

					if (noSchemaValidation) {

						int result = askWhatOnEmpty(removeEmpty, noSchemaValidation);
						if (result == Messages.CANCEL) {
							//cancelCellEditing();
						} else if (result == Messages.OK) {
							// remove empty value
							editingStoppedFromInside = true;
							newEditorValue = null;
							stopCellEditing();
						} else if (result == Messages.NO) {
							// set empty
							editingStoppedFromInside = true;
							stopCellEditing();
						}

					} else {

						AtomicReference<ValidationInfo> info = new AtomicReference<>();
						ComponentValidator.getInstance(textField).ifPresent(componentValidator -> {
							info.set(componentValidator.getValidationInfo());
						});

						if (info.get() == null) {
							// valid input, submit value as it is
							editingStoppedFromInside = true;
							stopCellEditing();
						} else if (info.get().okEnabled) {
							// warned about removal, converting result value to null
							newEditorValue = null;
							editingStoppedFromInside = true;
							stopCellEditing();
						} else {
							//cancelCellEditing();
						}

					}

				} else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					e.consume();
					cancelCellEditing();
				}
			}

			private int askWhatOnEmpty(boolean removeEmpty, boolean noSchemaValidation) {

				int result = removeEmpty ? Messages.OK : Messages.NO; // REMOVE by DEFAULT
				if (noSchemaValidation && StringUtils.isBlank(textField.getText())) {

					result = Messages.showYesNoCancelDialog(textField,
							LdapBundle.message("ldap.editor.value.askRemove.message.text" ),
							LdapBundle.message("ldap.editor.value.askRemove.title.text"),
							LdapBundle.message("ldap.editor.value.askRemove.button.remove.text"),
							LdapBundle.message("ldap.editor.value.askRemove.button.setEmpty.text"),
							LdapBundle.message("ldap.editor.value.askRemove.button.cancel.text"), LdapIcons.REMOVE);
					if (result == Messages.OK) {
						removeEmpty = true;
						return result;
					} else if (result == Messages.NO) {
						removeEmpty = false;
						return result;
					} else {
						return result;
					}

				}
				return result;

			}


		});

		// force re-validate on display if not adding new
		boolean isAddingNewValue = (currentEditorValue == null || currentEditorValue.isNull());
		if (!isAddingNewValue) {
			ComponentValidator.getInstance(textField).ifPresent(ComponentValidator::revalidate);
			// TODO - we will need placeholder display on empty but focused value
			textField.getEmptyText().setText(LdapBundle.message("ldap.editor.value.editor.text"));
		}

		return textField;
	}

	/**
	 * Open external editor for attribute value of selected item.
	 * External editor handles all input validation.
	 *
	 * @param selectedItem selected item to edit value for
	 * @param textField inline editor (if not null, focus is returned on cancellation)
	 */
	private void openValueEditor(AttributeModelItem selectedItem, JTextComponent textField) {

		// if source attribute value is empty, then we are adding new value
		boolean isAddingNewValue = (currentEditorValue == null || currentEditorValue.isNull());

		AttributeValueEditor valueEditorDialog = selectedItem.getValueProvider().getValueEditor(table, isAddingNewValue);
		// FIXME / TODO - we might want to pass already modified value from one editor into another, but it must not modify Value in Attribute !!
		if (valueEditorDialog.getDialogWrapper().showAndGet()) {

			newEditorValue = valueEditorDialog.getNewValue();
			editingStoppedFromInside = true;
			stopCellEditing();

		} else {
			// external editor cancelled - get back focus on inline editor
			if (textField != null) {
				textField.requestFocusInWindow();
			}
		}

	}

	/**
	 * Check if it was internal action from value editor, which stopped editing or external action.
	 * Since call stopCellEditing() is shared in logic we must distinguish, if editing was stopped by
	 * submitting new value or by focus loss (canceling value change).
	 *
	 * @return TRUE if user intentionally submitted change / FALSE = stuff like focus loss
	 */
	public boolean isEditingStoppedFromInside() {
		return editingStoppedFromInside;
	}

}
