package io.gitlab.zlamalp.intellijplugin.ldap.editor;

import com.intellij.LdapBundle;
import com.intellij.icons.AllIcons;
import com.intellij.ide.ui.UISettings;
import com.intellij.ide.ui.UISettingsListener;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.ui.ColorHexUtil;
import com.intellij.ui.ColoredTableCellRenderer;
import com.intellij.ui.JBColor;
import com.intellij.ui.SimpleTextAttributes;
import com.intellij.ui.components.JBLabel;
import com.intellij.ui.components.JBPanel;
import com.intellij.ui.table.JBTable;
import com.intellij.util.ui.JBDimension;
import com.intellij.util.ui.JBUI;
import com.intellij.util.ui.UIUtil;
import com.intellij.util.ui.components.BorderLayoutPanel;
import com.intellij.vcs.log.ui.render.SimpleColoredComponentLinkMouseListener;
import org.apache.directory.api.ldap.model.entry.Entry;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.table.TableStringConverter;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeVirtualFile;

import static com.intellij.ui.SimpleTextAttributes.GRAYED_ITALIC_ATTRIBUTES;
import static com.intellij.ui.SimpleTextAttributes.REGULAR_ITALIC_ATTRIBUTES;

/**
 * Attribute table is main editor component for {@link LdapEntryEditor}, which is used
 * to edit {@link LdapNodeVirtualFile} (it represents single LDAP {@link Entry}).
 *
 * This class extend standard IntelliJ's {@link JBTable} and provides it with self
 * configured defaults. We define/use:
 *
 * - define table size, model, border, colors
 * - define cell renderers for all columns
 * - use single row selection // todo: multi-row selection
 * - define row sorter
 * - define table header renderer (+sort on click)
 * - define link listener (allow actions when cell contains clickable link)
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class AttributeTable extends JBTable {

	private LdapEntryEditor editor;

	public AttributeTable(@NotNull LdapEntryEditor editor) {
		super(new AttributeTableModel(editor.getLdapNode()));
		this.editor = editor;
		configure();
	}

	@NotNull
	public LdapEntryEditor getEditor() {
		return editor;
	}

	private void configure() {

		setAutoCreateColumnsFromModel(true);
		setShowColumns(true);
		setAutoResizeMode(JBTable.AUTO_RESIZE_LAST_COLUMN);

		setRowSelectionAllowed(true);
		//setCellSelectionEnabled(true);
		//setColumnSelectionAllowed(true);
		getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		setEnableAntialiasing(true);
		setGridColor(JBColor.border());
		setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

		setMinimumSize(new JBDimension(350, 300));

		// support column sorting
		setRowSorter(createTableRowSorter((AttributeTableModel)getModel()));

		// install listener to handle clickable links in table cells
		// TODO - use its logic in renderer for DN/Name attributes to click trough to different Entry.
		new SimpleColoredComponentLinkMouseListener().installOn(this);

		// custom table header
		JTableHeader header = getTableHeader();
		header.setDefaultRenderer(new AttributeTableHeaderRenderer());
		header.setReorderingAllowed(false);
		// support sort change when header is clicked
		header.addMouseListener(new MouseAdapter() {
			private SortOrder   currentOrder    = SortOrder.UNSORTED;
			private int         lastCol         = -1;

			@Override
			public void mouseClicked(MouseEvent e) {
				int column = getTableHeader().columnAtPoint(e.getPoint());
				column = convertColumnIndexToModel(column);
				if (column != lastCol) {
					currentOrder = SortOrder.UNSORTED;
					lastCol = column;
				}
				RowSorter<?> sorter = getRowSorter();
				List<RowSorter.SortKey> sortKeys = new ArrayList<>();
				if (e.getButton() == MouseEvent.BUTTON1) {
					switch (currentOrder) {
						case UNSORTED:
							sortKeys.add(new RowSorter.SortKey(column, currentOrder = SortOrder.ASCENDING));
							break;
						case ASCENDING:
							sortKeys.add(new RowSorter.SortKey(column, currentOrder = SortOrder.DESCENDING));
							break;
						case DESCENDING:
							sortKeys.add(new RowSorter.SortKey(column, currentOrder = SortOrder.UNSORTED));
							break;
					}
					sorter.setSortKeys(sortKeys);
				}
			}
		});

		TableColumn expandColumn = getColumn(AttributeTableModel.COLUMN_NAMES[0]);
		expandColumn.setResizable(false);
		expandColumn.setWidth(33);
		expandColumn.setPreferredWidth(33);
		expandColumn.setMinWidth(33);
		expandColumn.setMaxWidth(33);
		expandColumn.setCellRenderer(new ColoredTableCellRenderer() {
			@Override
			protected void customizeCellRenderer(JTable table, @Nullable Object value, boolean selected, boolean hasFocus, int row, int column) {

				AttributeModelItem modelItem = ((AttributeTable)table).getItem(row, column);
				if (modelItem == null) return;

				if (getSelectedRow() == row) {
					// default caret line color
					setBackground(new JBColor(ColorHexUtil.fromHexOrNull("fcfaed"), ColorHexUtil.fromHexOrNull("474c5b")));
				} else {
					setBackground(UIUtil.getPanelBackground());
				}

				setIconOnTheRight(true);
				setTransparentIconBackground(true);
				setBorder(null);

				if (modelItem.isCollapsible()) {
					if (modelItem.isCollapsed()) {
						setIcon(AllIcons.General.ArrowRight);
					} else {
						setIcon(AllIcons.General.ArrowDown);
					}
				}

			}
		});

		// optimize attribute name column
		TableColumn attributeColumn = getColumn(AttributeTableModel.COLUMN_NAMES[1]);
		attributeColumn.setResizable(true);
		attributeColumn.setPreferredWidth(200);
		attributeColumn.setMinWidth(120);
		attributeColumn.setMaxWidth(400);
		attributeColumn.setCellRenderer(new ColoredTableCellRenderer() {
			@Override
			protected void customizeCellRenderer(JTable table, @Nullable Object value, boolean selected, boolean hasFocus, int row, int column) {

				AttributeModelItem modelItem = ((AttributeTable)table).getItem(row, column);
				if (modelItem == null) return;

				SimpleTextAttributes style = SimpleTextAttributes.REGULAR_ATTRIBUTES;
				if (modelItem.isOperationAttribute()) style = REGULAR_ITALIC_ATTRIBUTES;
				if (modelItem.isMustAttribute()) style = SimpleTextAttributes.REGULAR_BOLD_ATTRIBUTES;

				append(modelItem.getAttribute().getUpId().split(";")[0], style, true);
				if (modelItem.getAttribute().getUpId().split(";", 2).length == 2) {
					append(";"+modelItem.getAttribute().getUpId().split(";", 2)[1], style.derive(REGULAR_ITALIC_ATTRIBUTES.getStyle(), null, null, null), false);
				}

				if (modelItem.isCollapsible() && modelItem.isCollapsed()) {
					append(" ("+modelItem.getAttribute().size()+")", GRAYED_ITALIC_ATTRIBUTES, 1, SwingConstants.RIGHT);
				}
				setBorder(JBUI.Borders.empty(1, 8));

			}
		});

		// optimize attribute value column
		TableColumn valueColumn = getColumn(AttributeTableModel.COLUMN_NAMES[2]);
		valueColumn.setResizable(true);
		valueColumn.setCellRenderer(new ColoredTableCellRenderer() {
			@Override
			protected void customizeCellRenderer(JTable table, @Nullable Object value, boolean selected, boolean hasFocus, int row, int column) {

				AttributeModelItem modelItem = ((AttributeTable)table).getItem(row, column);
				if (modelItem == null) return;

				SimpleTextAttributes style = SimpleTextAttributes.REGULAR_ATTRIBUTES;
				if (modelItem.isOperationAttribute()) style = REGULAR_ITALIC_ATTRIBUTES;
				if (modelItem.isMustAttribute()) style = SimpleTextAttributes.REGULAR_BOLD_ATTRIBUTES;

				if (modelItem.getRawValue() == null || modelItem.getRawValue().isNull()) {
					append(LdapBundle.message("ldap.editor.value.editor.text"), GRAYED_ITALIC_ATTRIBUTES, true);
				} else {
					modelItem.renderDisplayValue(this, style, !getEditor().isShowDecoratedValues());
				}
				setBorder(JBUI.Borders.empty(1, 8));

			}
		});


		// set proper row height
		if (!getEditor().isUseCompactTable()) {
			setRowHeight((int) (UIUtil.getLabelFont().getSize()*1.8));
		}
		ApplicationManager.getApplication().getMessageBus().connect(editor).subscribe(UISettingsListener.TOPIC, new UISettingsListener() {
			@Override
			public void uiSettingsChanged(UISettings uiSettings) {
				if (!getEditor().isUseCompactTable()) {
					setRowHeight((int)(uiSettings.getFontSize()*1.8));
				}
			}
		});

		getEmptyText().setText("");
		/*
		getEmptyText().setText("Entry has no attributes or they are not loaded yet.").appendSecondaryText("Fetch attributes", SimpleTextAttributes.LINK_ATTRIBUTES, e -> {
			LdapUtils.refresh(editor.getLdapNode());
		});
		*/

	}

	@Nullable
	public AttributeModelItem getItem(int row, int column) {

		if (row >= 0 && column >= 0) {
			try {
				int modelRow = convertRowIndexToModel(row);
				int modelColumn = convertColumnIndexToModel(column);
				return (AttributeModelItem)getModel().getValueAt(modelRow, modelColumn);
			} catch (IndexOutOfBoundsException ex) {
				// probably race condition
			}
		}
		return null;

	}

	@Nullable
	public AttributeModelItem getSelectedItem() {

		int row = getSelectedRow();
		if (row > -1) {
			try {
				int convertedRow = convertRowIndexToModel(row);
				return ((AttributeTableModel) getModel()).getItems().get(convertedRow);
			} catch (IndexOutOfBoundsException ex) {
				// probably race condition
				return null;
			}
		}
		return null;

	}

	public List<AttributeModelItem> getSelectedItems() {

		List<AttributeModelItem> items = new ArrayList<>();
		int[] rows = getSelectedRows();
		if (rows != null && rows.length > 0) {
			for (int row : rows) {
				if (row > -1) {
					try {
						int convertedRow = convertRowIndexToModel(row);
						AttributeModelItem item = ((AttributeTableModel) getModel()).getItems().get(convertedRow);
						items.add(item);
					} catch (IndexOutOfBoundsException ex) {
						// probably race condition, skip adding item
					}
				}
			}
		}
		return items;

	}

	/**
	 * Return TableSorter which provides proper string value and sort keys to sorting logic.
	 * TODO - we could allow sort by both columns (0,1).
	 *
	 * @param model model to sort
	 * @return TableSorter
	 */
	private TableRowSorter<AttributeTableModel> createTableRowSorter(AttributeTableModel model) {

		TableRowSorter<AttributeTableModel> sorter = new TableRowSorter<>(model);
		sorter.setStringConverter(new TableStringConverter() {
			@Override
			public String toString(TableModel model, int row, int column) {

				AttributeModelItem item = (AttributeModelItem)model.getValueAt(row, column);
				if (item == null) return "";
				if (column == 0) {
					return "";
				} else if (column == 1) {
					return item.getAttribute().getId();
				} else if (column == 2) {
					return item.getStringValue();
					/* FXIME - decorated values contains HTML and sorts badly
					if (showDecoratedValues) {
						return item.getDisplayValue();
					} else {
						return item.getStringValue();
					}
					*/
				}
				return "";

			}
		});
		sorter.isSortable(0);
		sorter.isSortable(1);
		sorter.setSortsOnUpdates(true);

		return sorter;

	}

	/**
	 * Attributes Table Header renderer. Draw header in Intellij IDEA L&F
	 * and respect column sorting indication.
	 */
	public static class AttributeTableHeaderRenderer implements TableCellRenderer {

		public AttributeTableHeaderRenderer() {}

		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

			int modelIndex = table.convertColumnIndexToModel(column);

			// column name
			JBLabel label = new JBLabel();
			label.setText((String)value);
			label.setHorizontalTextPosition(SwingConstants.LEFT);
			UIUtil.addBorder(label, JBUI.Borders.empty(1, 8));
			label.setFontColor(UIUtil.FontColor.NORMAL);

			// wrap in a box with border
			JBPanel<BorderLayoutPanel> box = new JBPanel<>(new BorderLayout());
			box.add(label, BorderLayout.WEST);

			box.setBorder(new MatteBorder(0,0,1,1,JBColor.border()));

			if (modelIndex > 0) {

				// construct sorting state label
				JBLabel l = new JBLabel("");
				l.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));
				l.setIcon(AllIcons.General.ArrowSplitCenterV);

				for (RowSorter.SortKey sk : table.getRowSorter().getSortKeys()) {
					if (sk.getColumn() == modelIndex) {

						if (sk.getSortOrder() == SortOrder.ASCENDING) {
							l.setIcon(AllIcons.General.ArrowUp);
						} else if (sk.getSortOrder() == SortOrder.DESCENDING) {
							l.setIcon(AllIcons.General.ArrowDown);
						} else {
							l.setIcon(AllIcons.General.ArrowSplitCenterV);
						}
					}
				}
				box.add(l, BorderLayout.EAST);

			}

			return box;
		}
	}

}
