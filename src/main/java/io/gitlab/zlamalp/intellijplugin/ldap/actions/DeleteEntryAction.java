package io.gitlab.zlamalp.intellijplugin.ldap.actions;

import com.intellij.LdapBundle;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapActionUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapIcons;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.LdapTreePanel;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapUIUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeType;
import io.gitlab.zlamalp.intellijplugin.ldap.ui.LdapNotificationHandler;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.util.Objects;

/**
 * Delete selected LdapTreeNode from LdapTreePanel meaning LDAP Entry deletion.
 *
 * @see LdapTreePanel
 * @see LdapEntryTreeNode
 *
 * @author Pavel Zlámal
 */
public class DeleteEntryAction extends AnAction {

	public static final String ID = "io.gitlab.zlamalp.intellijplugin.ldap.delete";
	private static Logger log = LoggerFactory.getLogger(DeleteEntryAction.class);

	public DeleteEntryAction() {
	}

	public DeleteEntryAction(@Nls(capitalization = Nls.Capitalization.Title) @Nullable String text, @Nls(capitalization = Nls.Capitalization.Sentence) @Nullable String description, @Nullable Icon icon) {
		super(text, description, icon);
	}

	@Override
	public void actionPerformed(@NotNull AnActionEvent e) {

		if (!isActionActive(e)) return;

		if (LdapActionUtils.isFromTree(e)) {
			LdapTreePanel treePanel = LdapActionUtils.getLdapTreePanel(e);
			LdapEntryTreeNode[] selectedTreeNodes = treePanel.getTree().getSelectedNodes(LdapEntryTreeNode.class, null);
			for (LdapEntryTreeNode treeNode : selectedTreeNodes) {
				if (nodeTypeCheck(treeNode)) {
					performAction(e.getProject(), treePanel, treeNode);
				}
			}
		} else if (LdapActionUtils.isFromEditor(e)) {
			LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
			if (editor != null) {
				LdapEntryTreeNode node = editor.getLdapNode();
				if (nodeTypeCheck(node)) {
					performAction(e.getProject(), editor.getComponent(), node);
				}
			}
		}

	}

	@Override
	public void update(AnActionEvent e) {
		e.getPresentation().setEnabled(isActionActive(e));
	}


	/**
	 * Return TRUE if DeleteEntry action should be active.
	 *
	 * @param e triggered event
	 * @return TRUE if action should be active
	 */
	private boolean isActionActive(AnActionEvent e) {

		if (!LdapActionUtils.isProjectActive(e)) return false;
		if (LdapActionUtils.isFromTree(e)) {
			LdapTreePanel treePanel = LdapActionUtils.getLdapTreePanel(e);
			LdapEntryTreeNode[] selectedTreeNodes = treePanel.getTree().getSelectedNodes(LdapEntryTreeNode.class, null);
			for (LdapEntryTreeNode treeNode : selectedTreeNodes) {
				if (nodeTypeCheck(treeNode)) {
					return true;
				}
			}
		} else if (LdapActionUtils.isFromEditor(e)) {
			LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
			if (editor != null && !editor.isLoading()) {
				return nodeTypeCheck(editor.getLdapNode());
			}
		}
		return false;

	}

	/**
	 * Check whether LdapNode is allowed type and connection isn't read only
	 *
	 * @return TRUE if correct node
	 */
	private boolean nodeTypeCheck(LdapEntryTreeNode node) {
		return ((Objects.equals(LdapNodeType.NODE, node.getNodeType()) ||
				Objects.equals(LdapNodeType.BASE, node.getNodeType()))
				&& ((node.getEditor() != null) ?
				!node.getEditor().isLocallyReadOnly() :
				!node.getLdapServer().isReadOnly()));
	}

	/**
	 * Delete selected entry
	 *
	 * @param parentComponent parent source component
	 * @param node LdapNode to delete
	 */
	private void performAction(@NotNull Project project, @NotNull Component parentComponent, @NotNull LdapEntryTreeNode node) {

		int result = Messages.showOkCancelDialog(parentComponent,
				"Really delete whole Entry \"" + node.getDn() + "\" ? This action cannot be undone!",
				"Delete Entry", "Delete", "Cancel",
				LdapIcons.DELETE);

		if (result == Messages.OK) {

			Task task = new Task.Modal(project,
					LdapBundle.message("ldap.update.modal.deleteEntry", node.getDn()),
					true) {

				Exception exception = null;
				LdapConnection connection = null;

				@Override
				public void run(@NotNull ProgressIndicator indicator) {

					try {

						indicator.setIndeterminate(false);
						indicator.setText(LdapBundle.message("ldap.update.modal.connecting"));
						indicator.setFraction(0.1);
						LdapUIUtils.waitIfUIDebug();
						indicator.checkCanceled();
						connection = node.getLdapServer().getConnection();

						indicator.setFraction(0.5);
						indicator.setText(LdapBundle.message("ldap.update.modal.deletingEntry"));
						LdapUIUtils.waitIfUIDebug();
						indicator.checkCanceled();
						log.info("Deleting {}", node.getDn());
						connection.delete(node.getDn());
						indicator.setFraction(1.0);

					} catch (LdapException ex) {
						exception = ex;
					}

				}

				@Override
				public void onFinished() {

					// close connection even if user canceled operation
					if (connection != null) {
						try {
							node.getLdapServer().releaseConnection(connection);
						} catch (LdapException e) {
							LdapNotificationHandler.handleError(e, node.getLdapServer().getName(), "Can't release connection.");
						}
					}

				}

				@Override
				public void onSuccess() {

					if (exception != null) {
						LdapNotificationHandler.handleError(exception, node.getLdapServer().getName(), "Entry "+ node.getDn() +" was not deleted.");
					} else {
						// update model locally, no need to refresh whole subtree
						LdapTreePanel.getInstance(project).getModel().removeNodeFromParent(node);
					}

				}

			};
			ProgressManager.getInstance().run(task);

		}

	}

}
