package io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors;

import com.intellij.LdapBundle;
import com.intellij.openapi.ui.ComponentValidator;
import com.intellij.openapi.ui.ValidationInfo;
import com.intellij.ui.EditorTextField;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers.IA5StringValueProvider;
import io.gitlab.zlamalp.intellijplugin.ldap.schema.SchemaUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.schema.SchemaSettings;
import org.apache.commons.lang.StringUtils;
import org.apache.directory.api.ldap.model.entry.Value;
import org.apache.directory.api.ldap.model.exception.LdapInvalidAttributeValueException;
import org.apache.directory.api.ldap.model.message.ResultCodeEnum;
import org.apache.directory.api.ldap.model.schema.AttributeType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * LDAP Attribute Value editor for IA5String syntax.
 *
 * It provides visual UI for editing attribute value and all necessary data (related attribute / new value)
 * when editing is done. It provides value validation (syntax check).
 *
 * This editor allows empty string values.
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class IA5StringValueEditor extends DefaultValueEditor {

	private static Logger log = LoggerFactory.getLogger(IA5StringValueEditor.class);

	/**
	 * Create default Attribute value editor with specified action ADD / EDIT.
	 *
	 * @param parent Parent Component for modal dialog
	 * @param valueProvider Value provider for edited Attribute and its Value
	 * @param isAddingNewValue TRUE if dialog should add new value for same attribute instead of using the one from value provider
	 */
	public IA5StringValueEditor(@NotNull Component parent, @NotNull IA5StringValueProvider valueProvider, boolean isAddingNewValue) {
		super(parent, valueProvider, isAddingNewValue);
		this.removeEmpty = false;
		init();
	}

	@NotNull
	@Override
	public ComponentValidator getComponentValidator() {

		if (this.validator != null) {
			return this.validator;
		}

		this.validator = new ComponentValidator(getDisposable()).withValidator(new Supplier<ValidationInfo>() {
			@Override
			public ValidationInfo get() {

				EditorTextField editor = (EditorTextField)getEditorComponent();
				if (editor == null) {
					return null;
				}

				SchemaSettings.SchemaProvider schemaProvider = settings.getSchemaSettings().getSchemaProvider();

				if (Objects.equals(SchemaSettings.SchemaProvider.NONE, schemaProvider)) {

					// no schema validation, just basic (not)empty state
					if (StringUtils.isEmpty(editor.getText())) {
						return new ValidationInfo(LdapBundle.message("ldap.editor.value.validator.isEmpty.text"), editor).asWarning().withOKEnabled();
					} else {
						return null;
					}

				} else {

					LdapInvalidAttributeValueException wrongSyntax = null;
					long maxLength = 0;
					AttributeType type = SchemaUtils.getAttributeType(valueProvider.getEntry(), valueProvider.getAttribute());
					try {
						if (type != null) {
							maxLength = type.getSyntaxLength();
							new Value(type, editor.getText());
						}
					} catch (LdapInvalidAttributeValueException e) {
						wrongSyntax = e;
					} catch (IllegalArgumentException ex) {
						// thrown when value can't be normalized
						wrongSyntax = new LdapInvalidAttributeValueException(ResultCodeEnum.INVALID_ATTRIBUTE_SYNTAX, ex.getMessage());
					}

					if ((SchemaUtils.isMustAttribute(valueProvider.getEntry(), valueProvider.getAttribute().getId()) && valueProvider.getAttribute().size() <= 1)
							&& StringUtils.isBlank(editor.getText())) {
						return new ValidationInfo(LdapBundle.message("ldap.editor.value.validator.cantBeEmptyRequired.text", valueProvider.getAttribute().getUpId()), editor);
					} else if (maxLength > 0 && editor.getText().length() > maxLength) {
						return new ValidationInfo(LdapBundle.message("ldap.editor.value.validator.toLong.text", maxLength), editor);
					} else if (wrongSyntax != null) {
						if (type.getSyntax() != null) {
							return new ValidationInfo(LdapBundle.message("ldap.editor.value.validator.invalidSyntaxExpected.text", type.getSyntax().getDescription()), editor);
						} else {
							return new ValidationInfo(LdapBundle.message("ldap.editor.value.validator.invalidSyntax.text", editor));
						}
					} else {
						return null;
					}

				}

			}
		});

		return this.validator;

	}

	@Nullable
	@Override
	protected String getDimensionServiceKey() {
		return DefaultValueEditor.class.getCanonicalName();
	}

}
