package io.gitlab.zlamalp.intellijplugin.ldap.actions.node;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapActionUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.AttributeTable;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.AttributeTableModel;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.AttributeModelItem;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeVirtualFile;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.datatransfer.*;
import java.awt.Toolkit;

/**
 * Copy value of selected Attribute in table of opened LdapNode file (representing single LDAP entry).
 *
 * @see LdapNodeVirtualFile
 * @see LdapEntryEditor
 * @see AttributeTable
 * @see AttributeTableModel
 *
 * @author Attila Majoros
 * @author Pavel Zlámal
 */
public class CopyAttributeValueAction extends AnAction {

	public static final String ID = "io.gitlab.zlamalp.intellijplugin.ldap.copyAttributeValue";
	private static Logger log = LoggerFactory.getLogger(CopyAttributeValueAction.class);

	public CopyAttributeValueAction() {
	}

	public CopyAttributeValueAction(@Nls(capitalization = Nls.Capitalization.Title) @Nullable String text, @Nls(capitalization = Nls.Capitalization.Sentence) @Nullable String description, @Nullable Icon icon) {
		super(text, description, icon);
	}

	@Override
	public void actionPerformed(AnActionEvent e) {

		// Cancel if attribute value can't be copied
		if (!isActionActive(e)) return;

		LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
		AttributeModelItem selectedItem = editor.getModel().getItems().get(editor.getTable().convertRowIndexToModel(editor.getTable().getSelectedRow()));

		StringSelection stringSelection = new StringSelection(selectedItem.getAttribute().getUpId() +": "+selectedItem.getStringValue());
		Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
		clpbrd.setContents(stringSelection, null);

	}

	@Override
	public void update(AnActionEvent e) {
		e.getPresentation().setEnabled(isActionActive(e));
	}

	/**
	 * Return TRUE if CopyAttributeValue action is allowed for selected Attribute and Value.
	 *
	 * @param e Event triggering this
	 * @return TRUE if Action is active or FALSE
	 */
	private boolean isActionActive(AnActionEvent e) {

		if (!LdapActionUtils.isProjectActive(e)) return false;
		if (LdapActionUtils.isFromEditor(e)) {
			LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
			if (editor != null && !editor.isLoading()) {
				int selectedRow = editor.getTable().getSelectedRow();
				if (selectedRow != -1) {
					AttributeModelItem selectedItem = editor.getModel().getItems().get(editor.getTable().convertRowIndexToModel(selectedRow));
					// we can't copy passwords !!!
					return !selectedItem.isCellUserPassword();
				} else {
					// no row selected
					return false;
				}
			}
		}
		return false;

	}

}


