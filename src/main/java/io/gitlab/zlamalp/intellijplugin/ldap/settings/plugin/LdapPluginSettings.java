package io.gitlab.zlamalp.intellijplugin.ldap.settings.plugin;

import io.gitlab.zlamalp.intellijplugin.ldap.settings.LdapPluginSettingsService;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.LdapServersStore;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.LdapSettings;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.plugin.LdapPluginConfigurable;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.plugin.LdapPluginSettingsForm;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.schema.SchemaSettings;
import org.apache.directory.api.ldap.model.message.AliasDerefMode;

import java.util.Set;
import java.util.TreeSet;

/**
 * IDE wide LDAP plugin settings template. Which holds template or current configuration.
 * Its persisted in LdapPlguinSettingsService and managed by LdapPluginSettingsForm UI.
 * UI is included in global IDE Settings using LdapPluginConfigurable component.
 *
 * @see LdapPluginSettingsService
 * @see LdapPluginSettingsForm
 * @see LdapPluginConfigurable
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdapPluginSettings extends LdapServersStore implements LdapSettings {

	// connection
	private int maxConnections = 3;
	private int minIdleConnections = 1;
	private int connectionTimeout = 5;

	// Fetching entries in browser
	private int pageSize = 1000; // 0 to disable
	private int wrapChildrenBy = 1000;  // 10-10.000
	private int requestTimeout = 60;  // seconds to wait for server response 10-3.600
	private int requestLimit = 0; // limit number of returned entries 0 to disable (LDAP server can have server side limit !!)
	private AliasDerefMode aliasDerefMode = AliasDerefMode.DEREF_ALWAYS;
	private boolean fetchUserAttributes = true;
	private boolean fetchOperationAttributes = false;
	private Set<String> fetchOnlyAttributes = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
	private boolean fetchAllAttributeValues = false; // retrieve all values of ranged attributes (like in AD "member;range=1-1499")

	// LDAP Entry/Value editor
	private boolean updateLdap = true; // immediately update value in LDAP on change.
	private boolean useCompactTable = true;
	private boolean showDecoratedValues = true; // show eg. "SHA hashed password" instead of "{SHA}Md93MCMksadmoKDW"
	private boolean sortObjectClassFirst = true;
	private boolean sortMustAttributeFirst = true;
	private boolean sortByAttributeType = true;
	private boolean sortAscending = true;
	private boolean sortOperationalAttributesLast = true;
	private int wrapAttributesBy = 10; // attributes with 10+ values will be wrapped by default, set to 0 do disable wrapping.

	// LDIF export settings
	private int wrapLdifLines = 80; // by RFC should be 80 chars per line
	private boolean allowExportUserPassword = true;
	private boolean sortLdifAttributes = true;

	private SchemaSettings schemaSettings = new SchemaSettings();

	@Override
	public int getWrapLdifLines() {
		return wrapLdifLines;
	}

	@Override
	public void setWrapLdifLines(int wrapLinesOnColumn) {
		this.wrapLdifLines = wrapLinesOnColumn;
	}

	@Override
	public boolean isAllowExportUserPassword() {
		return allowExportUserPassword;
	}

	@Override
	public void setAllowExportUserPassword(boolean allowExportUserPassword) {
		this.allowExportUserPassword = allowExportUserPassword;
	}

	@Override
	public boolean isSortLdifAttributes() {
		return sortLdifAttributes;
	}

	@Override
	public void setSortLdifAttributes(boolean sortAttributes) {
		this.sortLdifAttributes = sortAttributes;
	}

	@Override
	public int getPageSize() {
		return pageSize;
	}

	@Override
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	@Override
	public int getWrapChildrenBy() {
		return wrapChildrenBy;
	}

	@Override
	public void setWrapChildrenBy(int wrapChildrenBy) {
		this.wrapChildrenBy = wrapChildrenBy;
	}

	@Override
	public int getRequestTimeout() {
		return requestTimeout;
	}

	@Override
	public void setRequestTimeout(int requestTimeout) {
		this.requestTimeout = requestTimeout;
	}

	@Override
	public boolean isFetchUserAttributes() {
		return fetchUserAttributes;
	}

	@Override
	public void setFetchUserAttributes(boolean fetchUserAttributes) {
		this.fetchUserAttributes = fetchUserAttributes;
	}

	@Override
	public boolean isFetchOperationAttributes() {
		return fetchOperationAttributes;
	}

	@Override
	public void setFetchOperationAttributes(boolean fetchOperationAttributes) {
		this.fetchOperationAttributes = fetchOperationAttributes;
	}

	@Override
	public Set<String> getFetchOnlyAttributes() {
		return fetchOnlyAttributes;
	}

	@Override
	public void setFetchOnlyAttributes(Set<String> fetchOnlyAttributes) {
		this.fetchOnlyAttributes = fetchOnlyAttributes;
	}

	@Override
	public boolean isFetchAllAttributeValues() {
		return fetchAllAttributeValues;
	}

	@Override
	public void setFetchAllAttributeValues(boolean fetchAllAttributeValues) {
		this.fetchAllAttributeValues = fetchAllAttributeValues;
	}

	@Override
	public boolean isUpdateLdap() {
		return updateLdap;
	}

	@Override
	public void setUpdateLdap(boolean updateLdap) {
		this.updateLdap = updateLdap;
	}

	@Override
	public boolean isUseCompactTable() {
		return useCompactTable;
	}

	@Override
	public void setUseCompactTable(boolean useCompactTable) {
		this.useCompactTable = useCompactTable;
	}

	@Override
	public boolean isShowDecoratedValues() {
		return showDecoratedValues;
	}

	@Override
	public void setShowDecoratedValues(boolean showDecoratedValues) {
		this.showDecoratedValues = showDecoratedValues;
	}

	@Override
	public boolean isSortObjectClassFirst() {
		return sortObjectClassFirst;
	}

	@Override
	public void setSortObjectClassFirst(boolean sortObjectClassFirst) {
		this.sortObjectClassFirst = sortObjectClassFirst;
	}

	@Override
	public boolean isSortMustAttributeFirst() {
		return sortMustAttributeFirst;
	}

	@Override
	public void setSortMustAttributeFirst(boolean sortMustAttributeFirst) {
		this.sortMustAttributeFirst = sortMustAttributeFirst;
	}

	@Override
	public boolean isSortByAttributeType() {
		return sortByAttributeType;
	}

	@Override
	public void setSortByAttributeType(boolean sortByAttributeType) {
		this.sortByAttributeType = sortByAttributeType;
	}

	@Override
	public boolean isSortAscending() {
		return sortAscending;
	}

	@Override
	public void setSortAscending(boolean sortAscending) {
		this.sortAscending = sortAscending;
	}

	@Override
	public int getWrapAttributesBy() {
		return wrapAttributesBy;
	}

	@Override
	public void setWrapAttributesBy(int wrapAttributesBy) {
		this.wrapAttributesBy = wrapAttributesBy;
	}

	@Override
	public boolean isSortOperationalAttributesLast() {
		return sortOperationalAttributesLast;
	}

	@Override
	public void setSortOperationalAttributesLast(boolean sortOperationalAttributesLast) {
		this.sortOperationalAttributesLast = sortOperationalAttributesLast;
	}

	@Override
	public int getRequestLimit() {
		return requestLimit;
	}

	@Override
	public void setRequestLimit(int requestLimit) {
		this.requestLimit = requestLimit;
	}

	@Override
	public AliasDerefMode getAliasDerefMode() {
		return aliasDerefMode;
	}

	@Override
	public void setAliasDerefMode(AliasDerefMode aliasDerefMode) {
		this.aliasDerefMode = aliasDerefMode;
	}

	@Override
	public int getConnectionTimeout() {
		return connectionTimeout;
	}

	@Override
	public void setConnectionTimeout(int timeout) {
		this.connectionTimeout = timeout;
	}

	@Override
	public int getMaxConnections() {
		return maxConnections;
	}

	@Override
	public void setMaxConnections(int maxConnections) {
		this.maxConnections = maxConnections;
	}

	@Override
	public int getMinIdleConnections() {
		return minIdleConnections;
	}

	@Override
	public void setMinIdleConnections(int minIdle) {
		this.minIdleConnections = minIdle;
	}

	@Override
	public SchemaSettings getSchemaSettings() {
		return schemaSettings;
	}

	@Override
	public void setSchemaSettings(SchemaSettings schemaSettings) {
		this.schemaSettings = schemaSettings;
	}

}
