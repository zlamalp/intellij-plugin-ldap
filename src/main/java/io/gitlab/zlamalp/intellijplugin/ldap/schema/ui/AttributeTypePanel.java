package io.gitlab.zlamalp.intellijplugin.ldap.schema.ui;

import com.intellij.ui.components.JBLabel;
import com.intellij.ui.components.labels.BoldLabel;
import com.intellij.util.ui.JBDimension;
import org.apache.commons.lang.StringUtils;
import org.apache.directory.api.ldap.model.schema.AttributeType;

import javax.swing.*;
import java.util.stream.Collectors;

public class AttributeTypePanel {

	private JPanel wrapper;
	private JBLabel names;
	private JBLabel attributeTypeUsage;
	private BoldLabel heading;
	private JBLabel oids;
	private JBLabel description;
	private JBLabel superior;
	private JBLabel userModifiable;
	private JBLabel singleValued;
	private JBLabel humanReadable;
	private JBLabel obsolete;
	private JBLabel syntax;
	private JBLabel equality;
	private JBLabel substring;
	private JBLabel ordering;
	private JBLabel collective;
	private JTextPane definition;

	public AttributeTypePanel(AttributeType attributeType) {

		// 20 px smaller (10 for each side border)
		wrapper.setMinimumSize(new JBDimension(530, 580));
		wrapper.setPreferredSize(new JBDimension(530, 580));

		heading.setText("<html>Attribute Type: "+attributeType.getName()+"</html>");

		names.setCopyable(true);
		oids.setCopyable(true);
		description.setCopyable(true);
		superior.setCopyable(true);
		syntax.setCopyable(true);
		equality.setCopyable(true);
		substring.setCopyable(true);
		ordering.setCopyable(true);

		names.setText("<html>"+StringUtils.join(attributeType.getNames().stream()
				.sorted().collect(Collectors.toList()), "<br/>")+"</html>");
		oids.setText(attributeType.getOid());

		description.setText(attributeType.getDescription() != null ? attributeType.getDescription() : "");
		attributeTypeUsage.setText(attributeType.getUsage().name());

		if (!StringUtils.isBlank(attributeType.getSuperiorName())) {
			superior.setText("<html>"+attributeType.getSuperiorName()+"</html>");
		}

		userModifiable.setText(attributeType.isUserModifiable() ? "yes" : "no");
		singleValued.setText(attributeType.isSingleValued() ? "yes" : "no");
		humanReadable.setText(attributeType.isHR() ? "yes" : "no");
		obsolete.setText(attributeType.isObsolete() ? "yes" : "no");
		collective.setText(attributeType.isCollective() ? "yes" : "no");

		if (attributeType.getSyntaxName() != null) syntax.setText(attributeType.getSyntaxName() + ((attributeType.getSyntaxLength() != 0) ? " (length: "+attributeType.getSyntaxLength()+")" : ""));
		if (attributeType.getEqualityName() != null) equality.setText(attributeType.getEqualityName());
		if (attributeType.getSubstringName() != null) substring.setText(attributeType.getSubstringName());
		if (attributeType.getOrderingName() != null) ordering.setText(attributeType.getOrderingName());

		//definition.setMaximumSize(new JBDimension(400, -1));
		// make sure that content font is respected
		definition.setFont(names.getFont());
		definition.setBackground(null);
		definition.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		definition.putClientProperty(JEditorPane.HONOR_DISPLAY_PROPERTIES, true);
		if (attributeType.toString() != null) {
			definition.setText("<html>"+attributeType.toString()+"</html>");
		}

		attributeType.getExtensions();

	}

	public JComponent getComponent() {
		return wrapper;
	}

}
