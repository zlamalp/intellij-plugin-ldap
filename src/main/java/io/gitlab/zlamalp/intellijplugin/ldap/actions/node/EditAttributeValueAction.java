package io.gitlab.zlamalp.intellijplugin.ldap.actions.node;

import com.intellij.LdapBundle;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapActionUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapUIUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.AttributeModelItem;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.AttributeValueEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeVirtualFile;
import io.gitlab.zlamalp.intellijplugin.ldap.schema.SchemaUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.ui.LdapNotificationHandler;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.DefaultModification;
import org.apache.directory.api.ldap.model.entry.Entry;
import org.apache.directory.api.ldap.model.entry.Modification;
import org.apache.directory.api.ldap.model.entry.ModificationOperation;
import org.apache.directory.api.ldap.model.entry.Value;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.api.ldap.model.exception.LdapInvalidAttributeValueException;
import org.apache.directory.api.ldap.model.schema.AttributeType;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Edit selected attribute in LdapValueEditorDialog
 *
 * @see AttributeValueEditor
 * @see LdapEntryTreeNode
 * @see LdapEntryEditor
 * @see LdapNodeVirtualFile
 *
 * @author Pavel Zlámal
 */
public class EditAttributeValueAction extends AnAction {

	public static final String ID = "io.gitlab.zlamalp.intellijplugin.ldap.editAttribute";
	private static Logger log = LoggerFactory.getLogger(EditAttributeValueAction.class);

	public EditAttributeValueAction() {
	}

	public EditAttributeValueAction(@Nls(capitalization = Nls.Capitalization.Title) @Nullable String text, @Nls(capitalization = Nls.Capitalization.Sentence) @Nullable String description, @Nullable Icon icon) {
		super(text, description, icon);
	}

	@Override
	public void actionPerformed(@NotNull AnActionEvent e) {

		// Cancel if attribute value can't be edited
		if (!isActionActive(e)) return;

		LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
		LdapEntryTreeNode node = editor.getLdapNode();
		AttributeModelItem selectedItem = editor.getModel().getItems().get(editor.getTable().convertRowIndexToModel(editor.getTable().getSelectedRow()));

		boolean isAddingNew = selectedItem.getRawValue() == null || selectedItem.getRawValue().isNull();
		AttributeValueEditor valueEditorDialog = selectedItem.getValueProvider().getValueEditor(editor.getTable(), isAddingNew);

		if (valueEditorDialog.getDialogWrapper().showAndGet()) {

			Value newValue = ((AttributeValueEditor) valueEditorDialog).getNewValue();
			Value oldValue = selectedItem.getRawValue();
			Attribute attribute = selectedItem.getAttribute();

			// We must check, that attribute is present in model and wasn't discarded -> newly created by value provider
			// since somebody could change our model during edit action by mistake
			if (node.getAttributeByName(attribute.getId()) == null) {
				log.error("Local representation of '{}' doesn't have Attribute we want to edit: {}", node.getDn(), attribute);
				return;
			}

			// test value change by client schema validation on "clone" of source attribute
			Attribute clonedAttribute = attribute.clone();
			Entry clonedEntry = node.getEntry().clone();
			if (checkAndChangeAttribute(node, clonedEntry, clonedAttribute, oldValue, newValue)) {

				if (editor.isDirectEdit()) {

					// MODIFY LDAP DIRECTLY AND RELOAD ENTRY
					AttributeType type = SchemaUtils.getAttributeType(node, clonedAttribute);
					// TODO - we probably need to find transitive equality ??
					final boolean hasEquality = (type != null && type.getEquality() != null) && !valueEditorDialog.isPreferReplace();

					Task task = new Task.Modal(e.getProject(), LdapBundle.message("ldap.update.modal.modifyEntry", node.getDn()), true) {

						Exception exception = null;
						LdapConnection connection = null;

						@Override
						public void run(@NotNull ProgressIndicator indicator) {

							indicator.setIndeterminate(false);
							try {

								indicator.setText(LdapBundle.message("ldap.update.modal.connecting"));
								indicator.setFraction(0.1);
								LdapUIUtils.waitIfUIDebug();
								indicator.checkCanceled();
								connection = node.getLdapServer().getConnection();

								List<Modification> mods = new ArrayList<>();
								String realModText = LdapBundle.message("ldap.update.modal.changingAttributeValue", clonedAttribute.getUpId());

								if (hasEquality) {
									mods.add(new DefaultModification(ModificationOperation.REMOVE_ATTRIBUTE, clonedAttribute.getUpId(), oldValue));
									if (newValue != null) {
										mods.add(new DefaultModification(ModificationOperation.ADD_ATTRIBUTE, clonedAttribute.getUpId(), newValue));
									} else {
										realModText = LdapBundle.message("ldap.update.modal.removingAttributeValue", clonedAttribute.getUpId());
									}
								} else {
									mods.add(new DefaultModification(ModificationOperation.REPLACE_ATTRIBUTE, clonedAttribute));
									realModText = LdapBundle.message("ldap.update.modal.changingAttributeValueUsingReplace", clonedAttribute.getUpId());
								}

								indicator.setFraction(0.5);
								indicator.setText(realModText);
								LdapUIUtils.waitIfUIDebug();
								log.info("Updating attribute: {}", clonedAttribute.getUpId());
								indicator.checkCanceled();
								connection.modify(node.getDn(), mods.toArray(new Modification[0]));

								indicator.setFraction(1.0);

							} catch (LdapException ex) {
								exception = ex;
							}

						}

						@Override
						public void onFinished() {

							// close connection even if user canceled operation
							if (connection != null) {
								try {
									node.getLdapServer().releaseConnection(connection);
								} catch (LdapException e) {
									LdapNotificationHandler.handleError(e, node.getLdapServer().getName(), "Can't release connection.");
								}
							}

						}

						@Override
						public void onSuccess() {

							if (exception != null) {

								// local model was not modified, so just show error
								LdapNotificationHandler.handleError(exception, node.getLdapServer().getName(), "Entry "+ node.getDn() +" was not updated.");

							} else {

								// update local model representation
								checkAndChangeAttribute(node, node.getEntry(), attribute, oldValue, newValue);
								// TODO - we should set last state
								// we modified LDAP, make current state the last known
								//editor.setLastState(node.getEntry());

							}

							editor.getModel().refresh();

						}

					};

					ProgressManager.getInstance().run(task);

				} else {

					// MODIFY MODEL AND REFRESH VIEW

					// we don't have to update value provider, since model was changed by modifying source attribute
					// and it will disappeared with this dialog and after refreshing model
					//selectedItem.getValueProvider().setRawValue(valueEditorDialog.getValue());

					// actually change local attribute
					checkAndChangeAttribute(node, node.getEntry(), attribute, oldValue, newValue);

					// we had to modify local model before actually deciding "directEdit", so now just refresh
					editor.getModel().refresh();

				}


			}

			// values were equals -> don't modify model or LDAP.

		}

	}

	@Override
	public void update(AnActionEvent e) {
		e.getPresentation().setEnabled(isActionActive(e));
	}

	/**
	 * Return TRUE if EditAttribute action is allowed for selected Attribute.
	 *
	 * @param e Event triggering this
	 * @return TRUE if Action is active or FALSE
	 */
	private boolean isActionActive(AnActionEvent e) {

		if (!LdapActionUtils.isProjectActive(e)) return false;
		if (LdapActionUtils.isFromEditor(e)) {
			LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
			if (editor != null && !editor.isLoading()) {
				int selectedRow = editor.getTable().getSelectedRow();
				if (selectedRow != -1) {
					return editor.getModel().isRowEditable(editor.getTable().convertRowIndexToModel(selectedRow));
				} else {
					// no row selected
					return false;
				}
			}
		}
		return false;

	}

	/**
	 * Return TRUE if newValue is valid for the attribute and is different than oldValue.
	 * During the process, sourced Attribute object values are modified.
	 *
	 * Return FALSE if either values are equals (sourcing Attribute is not modified)
	 * or when modification of Attribute with newValue fails (sourcing Attribute might by modified).
	 *
	 * If old value is NULL (was empty attribute), then it is not removed.
	 * If new value is NULL, then it is not set.
	 * If attribute is MUST and resulting values are empty, then FALSE is returned.
	 * If attribute is empty, then it is removed from Entry.
	 *
	 * If you wish to check the change first, you can always pass cloned Attribute object.
	 *
	 * @see Entry#clone()
	 * @see Attribute#clone()
	 *
	 * @param ldap LDAP this Attribute belongs to
	 * @param entry Sourcing Entry containing changed attribute
	 * @param attribute Sourcing Attribute containing all values to be modified
	 * @param oldValue Old value to be removed
	 * @param newValue New value to be added
	 * @return TRUE if new value is different than old and valid and attribute was locally modified / FALSE otherwise
	 */
	private boolean checkAndChangeAttribute(LdapEntryTreeNode ldap, Entry entry, Attribute attribute, Value oldValue, Value newValue) {

		// check equality
		if (!Objects.equals(oldValue, newValue)) {

			try {
				// NULL value means empty user input -> we will be removing attribute
				if (newValue != null) attribute.add(newValue);
			} catch (LdapInvalidAttributeValueException e1) {
				LdapNotificationHandler.handleError(e1, ldap.getLdapServer().getName(), "Unable to change value of Attribute '" + attribute.getUpId() + "'.");
				return false;
			}
			// remove old value if was not empty too
			if (oldValue != null) attribute.remove(oldValue);

			AttributeType type = SchemaUtils.getAttributeType(ldap, attribute);
			if (type != null && type.isSingleValued() && attribute.size() > 1) {
				LdapNotificationHandler.handleError(null,ldap.getLdapServer().getName(), "Attribute '" + attribute.getUpId() + "' is single valued!");
				return false;
			}

			if (!entry.get(attribute.getId()).iterator().hasNext()) {
				if (SchemaUtils.isMustAttribute(ldap, attribute.getId())){
					LdapNotificationHandler.handleError(null,ldap.getLdapServer().getName(), "Attribute '" + attribute.getUpId() + "' is MUST and must have at least one value.");
					return false;
				}
				// it was a last value of attribute !!
				try {
					entry.remove(attribute);
				} catch (LdapException e) {
					LdapNotificationHandler.handleError(e, ldap.getLdapServer().getName(), "Unable to remove empty Attribute '"+attribute.getUpId()+"'.");
				}
			}

			return true;

		}
		return false;

	}

}
