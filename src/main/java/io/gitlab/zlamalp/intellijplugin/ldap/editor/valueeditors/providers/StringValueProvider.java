package io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers;

import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.AttributeValueEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.StringValueEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.Value;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;

/**
 * Basic String (Directory String) value editor for LDAP Entry attribute values.
 * Only HumanReadable (non-binary) attributes can be used with this editor.
 *
 * FIXME - it is currently used as fallback for all human readable attrbutes !!!
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class StringValueProvider extends AbstractValueProvider {

	private static Logger log = LoggerFactory.getLogger(StringValueProvider.class);

	public StringValueProvider(@NotNull LdapEntryTreeNode node, @NotNull Attribute attribute, Value value) {
		super(node, attribute, value);
		if (!attribute.isHumanReadable() && (value != null && !value.isHumanReadable())) {
			throw new IllegalArgumentException(attribute.getUpId() + " is not HumanReadable. You can't use StringValueProvider to edit its value.");
		}
	}

	@Override
	public AttributeValueEditor getValueEditor(Component component, boolean addingNewValue) {
		return new StringValueEditor(component,this, addingNewValue);
	}

}
