package io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers;

import com.intellij.openapi.editor.colors.EditorColorsManager;
import com.intellij.openapi.editor.colors.EditorColorsScheme;
import com.intellij.ui.ColoredTableCellRenderer;
import com.intellij.ui.JBColor;
import com.intellij.ui.SimpleTextAttributes;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.AttributeValueEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.StringValueEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.schema.SchemaUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import org.apache.directory.api.ldap.model.constants.SchemaConstants;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.Value;
import org.apache.directory.api.ldap.model.schema.AttributeType;
import org.apache.directory.api.util.GeneralizedTime;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.text.ParseException;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

/**
 * This value provider allows displaying time in current users timezone instead of server time zone
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class GeneralizedTimeValueProvider extends AbstractValueProvider {

	private static Logger log = LoggerFactory.getLogger(GeneralizedTimeValueProvider.class);
	private static final DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime( FormatStyle.LONG ).withLocale(Locale.getDefault()).withZone(ZoneId.systemDefault());

	public GeneralizedTimeValueProvider(@NotNull LdapEntryTreeNode node, @NotNull Attribute attribute, Value value) {
		super(node, attribute, value);
		if (!attribute.isHumanReadable() && (value != null && !value.isHumanReadable())) {
			throw new IllegalArgumentException(attribute.getUpId() + " is not HumanReadable. You can't use GeneralizedTimeValueProvider to edit its value.");
		}

		AttributeType type = SchemaUtils.getAttributeType(node, attribute);
		if (type != null) {
			if (!SchemaConstants.GENERALIZED_TIME_SYNTAX.equals(type.getSyntaxOid())) {
				throw new IllegalArgumentException(attribute.getUpId() + " doesn't have GeneralizedTime syntax. You can't use GeneralizedTimeValueProvider to edit its value.");
			}
		}

	}

	@Override
	public void renderDisplayValue(ColoredTableCellRenderer renderer, SimpleTextAttributes style, boolean raw) {

		if (getRawValue() == null ||getRawValue().isNull()) {
			renderer.append("", style, true);
		} else {
			if (raw) {
				super.renderDisplayValue(renderer, style, raw);
			} else {
				try {
					// TODO - do we want colored "enchanted" values or not ?
					//EditorColorsScheme scheme = EditorColorsManager.getInstance().getGlobalScheme();
					//renderer.setFont(new Font(scheme.getEditorFontName(), Font.PLAIN, scheme.getEditorFontSize()));
					//style = style.derive(style.getStyle() | SimpleTextAttributes.STYLE_ITALIC, JBColor.darkGray, new JBColor(0xeffae7, 0x49544a), null);
					GeneralizedTime time = new GeneralizedTime(getStringValue());
					renderer.append( formatter.format(time.getCalendar().toInstant()), style, true);
				} catch (ParseException e) {
					// fallback to standard renderer
					super.renderDisplayValue(renderer, style, raw);
				}
			}

		}

	}

	@Override
	public AttributeValueEditor getValueEditor(Component component, boolean addingNewValue) {
		// TODO/FIXME - create custom timestamp editor
		return new StringValueEditor(component,this, addingNewValue);
	}

}
