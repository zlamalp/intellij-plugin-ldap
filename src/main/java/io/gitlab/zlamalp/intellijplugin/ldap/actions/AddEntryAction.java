package io.gitlab.zlamalp.intellijplugin.ldap.actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.project.Project;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapActionUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapServer;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.LdapTreePanel;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapTreeNode;
/* FIXME - uncomment once its fully implemented
import io.gitlab.zlamalp.intellijplugin.ldap.ui.addEntry.SelectObjectClassStep;
import io.gitlab.zlamalp.intellijplugin.ldap.ui.addEntry.SelectSourceStep;
import io.gitlab.zlamalp.intellijplugin.ldap.ui.addEntry.SetAttributesStep;
import io.gitlab.zlamalp.intellijplugin.ldap.ui.addEntry.SetEntryNameStep;
*/
import io.gitlab.zlamalp.intellijplugin.ldap.ui.components.WizardDialog;
import io.gitlab.zlamalp.intellijplugin.ldap.ui.components.WizardModel;
import io.gitlab.zlamalp.intellijplugin.ldap.ui.components.WizardStep;
import org.apache.directory.api.ldap.model.schema.ObjectClass;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;

import java.util.List;

/**
 * Add new LDAP Entry
 *
 * @see LdapTreePanel
 * @see LdapEntryTreeNode
 *
 * @author Pavel Zlámal
 */
public class AddEntryAction extends AnAction {

	public static final String ID = "io.gitlab.zlamalp.intellijplugin.ldap.add";
	private static Logger log = LoggerFactory.getLogger(AddEntryAction.class);

	public AddEntryAction() {
	}

	public AddEntryAction(@Nls(capitalization = Nls.Capitalization.Title) @Nullable String text, @Nls(capitalization = Nls.Capitalization.Sentence) @Nullable String description, @Nullable Icon icon) {
		super(text, description, icon);
	}

	@Override
	public void actionPerformed(@NotNull AnActionEvent e) {

		if (!isActionActive(e)) return;

		WizardDialog<CreateEntryModel> wizard = null;
		CreateEntryModel model = new CreateEntryModel("Add New Entry");
		/* FIXME - uncomment once its fully implemented
		model.addStep(new SelectSourceStep());
		model.addStep(new SelectObjectClassStep());
		model.addStep(new SetEntryNameStep(e.getProject()));
		model.addStep(new SetAttributesStep());
		*/
		if (LdapActionUtils.isFromTree(e)) {
			LdapTreePanel treePanel = LdapActionUtils.getLdapTreePanel(e);
			LdapTreeNode[] selectedTreeNodes = treePanel.getTree().getSelectedNodes(LdapTreeNode.class, null);
			if (selectedTreeNodes.length == 1) {
				LdapServer ldapServer = selectedTreeNodes[0].getLdapServer();
				if (ldapServer.connect()) {
					model.setLdapServer(ldapServer);
					model.setProject(treePanel.getProject());
					wizard = new WizardDialog<CreateEntryModel>(e.getProject(), true, model);
				}
			}
		} else if (LdapActionUtils.isFromEditor(e)) {
			LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
			if (editor != null) {
				LdapServer ldapServer = editor.getLdapNode().getLdapServer();
				if (ldapServer.connect()) {
					model.setLdapServer(ldapServer);
					model.setProject(e.getProject());
					wizard = new WizardDialog<CreateEntryModel>(e.getProject(), true, model);
				}
			}
		}

		if (wizard != null && wizard.showAndGet()) {
			// TODO - create entry here or from within wizard ?
		}

	}

	@Override
	public void update(AnActionEvent e) {
		e.getPresentation().setEnabled(isActionActive(e));
	}

	/**
	 * Return TRUE if AddEntry action should be active.
	 *
	 * @param e triggered event
	 * @return TRUE if action should be active
	 */
	private boolean isActionActive(AnActionEvent e) {

		if (!LdapActionUtils.isProjectActive(e)) return false;

		// adding new Entry is enabled only for "R/W connections", editor is irrelevant !!

		if (LdapActionUtils.isFromTree(e)) {
			LdapTreePanel treePanel = LdapTreePanel.getInstance(e.getProject());
			LdapTreeNode[] selectedTreeNodes = treePanel.getTree().getSelectedNodes(LdapTreeNode.class, null);
			return selectedTreeNodes.length == 1 && !selectedTreeNodes[0].getLdapServer().isReadOnly();
		} else if (LdapActionUtils.isFromEditor(e)) {
			LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
			if (editor != null && !editor.isLoading()) {
				return !editor.getLdapNode().getLdapServer().isReadOnly();
			}
		}
		return false;

	}

	/**
	 * Model / DataHolder for our CreateEntry wizard
	 */
	public static class CreateEntryModel extends WizardModel {

		private LdapServer ldapServer = null;
		private Project project = null;
		private List<ObjectClass> selectedObjectClasses;
		private LdapEntryTreeNode node;

		public CreateEntryModel(String title) {
			super(title);
		}

		public CreateEntryModel(String title, List<WizardStep> steps) {
			super(title, steps);
		}

		public LdapServer getLdapServer() {
			return ldapServer;
		}

		public void setLdapServer(LdapServer ldapServer) {
			this.ldapServer = ldapServer;
		}

		public Project getProject() {
			return project;
		}

		public void setProject(Project project) {
			this.project = project;
		}

		public List<ObjectClass> getSelectedObjectClasses() {
			return selectedObjectClasses;
		}

		public void setSelectedObjectClasses(List<ObjectClass> selectedObjectClasses) {
			this.selectedObjectClasses = selectedObjectClasses;
		}

		public LdapEntryTreeNode getNode() {
			return node;
		}

		public void setNode(LdapEntryTreeNode node) {
			this.node = node;
		}
	}

}
