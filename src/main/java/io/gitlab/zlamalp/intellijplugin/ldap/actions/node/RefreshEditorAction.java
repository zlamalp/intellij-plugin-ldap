package io.gitlab.zlamalp.intellijplugin.ldap.actions.node;

import com.intellij.LdapBundle;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.ui.Messages;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapActionUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapActions;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.AttributeTable;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.LdapTreePanel;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.AttributeTableModel;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeType;
import io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeVirtualFile;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.ui.LdapNotificationHandler;
import org.apache.directory.api.ldap.model.entry.Entry;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;

import static com.intellij.openapi.progress.PerformInBackgroundOption.ALWAYS_BACKGROUND;

/**
 * Refresh all Attributes of LdapNode in table of opened LdapNode file (representing single LDAP entry).
 *
 * @see LdapNodeVirtualFile
 * @see LdapEntryEditor
 * @see AttributeTable
 * @see AttributeTableModel
 *
 * @author Attila Majoros
 * @author Pavel Zlámal
 */
public class RefreshEditorAction extends AnAction {

	public static final String ID = "io.gitlab.zlamalp.intellijplugin.ldap.refreshEditor";
	private static Logger log = LoggerFactory.getLogger(RefreshEditorAction.class);

	public RefreshEditorAction() {
	}

	public RefreshEditorAction(@Nls(capitalization = Nls.Capitalization.Title) @Nullable String text, @Nls(capitalization = Nls.Capitalization.Sentence) @Nullable String description, @Nullable Icon icon) {
		super(text, description, icon);
	}

	@Override
	public void actionPerformed(@NotNull AnActionEvent e) {
		actionPerformed(e, true);
	}

	public void actionPerformed(@NotNull AnActionEvent e, boolean askToSaveChanges) {

		if (!isActionActive(e)) return;
		LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
		LdapEntryTreeNode node = editor.getLdapNode();
		LdapTreePanel treePanel = LdapActionUtils.getLdapTreePanel(e);

		if (askToSaveChanges && editor.isModified() && !editor.isDirectEdit()) {
			int result = Messages.showYesNoCancelDialog(editor.getComponent(),
					LdapBundle.message("action.ldap.RefreshEditorAction.askOnChanges.text"),
					LdapBundle.message("action.ldap.RefreshEditorAction.askOnChanges.title"),
					LdapBundle.message("action.ldap.RefreshEditorAction.askOnChanges.save"),
					LdapBundle.message("action.ldap.RefreshEditorAction.askOnChanges.discard"),
					LdapBundle.message("action.ldap.RefreshEditorAction.askOnChanges.cancel"),
					Messages.getQuestionIcon());
			if (result == Messages.OK) {
				LdapActions.getWrappedAction(SaveEditorChangesAction.ID).actionPerformed(e);
			} else if (result == Messages.CANCEL) {
				return;
			}
		}

		// clear selection otherwise UI refresh race-condition fuck-up the IDE
		if (editor.getTable().isEditing()) editor.getTable().getCellEditor().cancelCellEditing();
		editor.getTable().getSelectionModel().clearSelection();

		// clear table and update UI
		editor.getModel().clearItems();
		editor.getTable().getRowSorter().allRowsChanged();
		editor.setLoading(true);

		Task task = new Task.Backgroundable(treePanel.getProject(), LdapBundle.message("ldap.update.modal.fetchEntry", node.getDn()), true, ALWAYS_BACKGROUND) {

			LdapConnection connection;
			LdapException exception;
			Entry entry;

			@Override
			public void run(@NotNull ProgressIndicator indicator) {

				try {

					indicator.setIndeterminate(false);
					indicator.setText(LdapBundle.message("ldap.update.modal.connecting"));
					indicator.setFraction(0.1);
					//LdapUIUtils.waitIfUIDebug();
					indicator.checkCanceled();
					connection = node.getLdapServer().getConnection();

					indicator.setFraction(0.5);
					indicator.setText(LdapBundle.message("ldap.update.modal.fetchingEntry"));
					//LdapUIUtils.waitIfUIDebug();
					indicator.checkCanceled();
					entry = LdapUtils.getEntry(editor.getLdapNode(), null, editor.getLocallyRequestedAttributes());

					indicator.checkCanceled();

				} catch (LdapException ex) {
					exception = ex;
				}


			}

			@Override
			public void onSuccess() {

				if (exception != null) {

					if (LdapNodeType.ROOT_DSE.equals(node.getNodeType())) {
						LdapNotificationHandler.handleError(exception, node.getLdapServer().getName(), "Can't retrieve 'ROOT_DSE' from LDAP.");
					} else {
						LdapNotificationHandler.handleError(exception, node.getLdapServer().getName(), "Can't retrieve '"+node.getDn()+"' from LDAP.");
					}

				} else if (entry != null) {

					editor.getLdapNode().setEntry(entry);

					// make sure tree is updated since setting Entry might fail to update the node
					treePanel.getModel().nodeChanged(node);

				} else {

					if (node.getNodeType().equals(LdapNodeType.ROOT_DSE)) {
						LdapNotificationHandler.handleError(null, node.getLdapServer().getName(), "Entry 'ROOT_DSE' doesn't exists!");
					} else {
						LdapNotificationHandler.handleError(null, node.getLdapServer().getName(), "Entry '"+node.getDn()+"' doesn't exists!");
					}

					// Entry not found by DN
					if (node.getParent() instanceof LdapEntryTreeNode) {
						// parent is entry, refresh it
						LdapUtils.refresh((LdapEntryTreeNode)node.getParent(), LdapUtils.RefreshType.WITH_CHILDREN);
						treePanel.getModel().nodeStructureChanged(node.getParent());
					} else {
						// parent is LdapServerTreeNode or something else
						treePanel.getModel().removeNodeFromParent(node);
					}

				}

			}

			@Override
			public void onFinished() {

				// close connection even if user canceled operation
				if (connection != null) {
					try {
						node.getLdapServer().releaseConnection(connection);
					} catch (LdapException e) {
						LdapNotificationHandler.handleError(e, node.getLdapServer().getName(), "Can't release connection.");
					}
				}

				editor.setLastState(entry);
				editor.getModel().refresh();
				editor.setLoading(false);

			}

		};
		ProgressManager.getInstance().run(task);

	}

	@Override
	public void update(AnActionEvent e) {
		e.getPresentation().setEnabled(isActionActive(e));
	}

	/**
	 * Return TRUE if RefreshEditor action is allowed for Entry
	 *
	 * @param e Event triggering this
	 * @return TRUE if Action is active or FALSE
	 */
	private boolean isActionActive(AnActionEvent e) {

		if (!LdapActionUtils.isProjectActive(e)) return false;
		if (LdapActionUtils.isFromEditor(e)) {
			LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
			return editor != null && !editor.isLoading();
		}
		return false;

	}

}
