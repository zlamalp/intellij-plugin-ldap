package io.gitlab.zlamalp.intellijplugin.ldap.settings.schema;

import com.intellij.ide.ui.UISettings;
import com.intellij.ide.ui.UISettingsListener;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.ui.ComboBox;
import com.intellij.openapi.ui.ComboBoxTableRenderer;
import com.intellij.ui.BooleanTableCellEditor;
import com.intellij.ui.BooleanTableCellRenderer;
import com.intellij.ui.CollectionComboBoxModel;
import com.intellij.ui.JBColor;
import com.intellij.ui.SimpleListCellRenderer;
import com.intellij.ui.TitledSeparator;
import com.intellij.ui.components.JBCheckBox;
import com.intellij.ui.components.JBLabel;
import com.intellij.ui.components.JBScrollPane;
import com.intellij.ui.table.JBTable;
import com.intellij.util.ui.JBDimension;
import com.intellij.util.ui.UIUtil;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.plugin.LdapPluginSettings;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.api.ldap.model.schema.SchemaManager;
import org.apache.directory.api.ldap.model.schema.registries.Schema;
import org.apache.directory.api.ldap.model.schema.registries.SchemaLoader;
import org.apache.directory.api.ldap.schema.loader.JarLdifSchemaLoader;
import org.codehaus.plexus.util.StringUtils;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * UI form to edit Schema related settings of LDAP plugin.
 *
 * @author Pavel Zlámal
 */
public class SchemaSettingsForm {

	private ComboBox<SchemaSettings.SchemaProvider> schemaValidationBox;
	private JBLabel helpLabel;
	private JPanel wrapper;
	private JBTable table;
	private JBCheckBox strict;
	private TitledSeparator availableSchemasSeparator;
	private JBScrollPane scroll;

	public SchemaSettingsForm(LdapPluginSettings state) {

		schemaValidationBox.setModel(new CollectionComboBoxModel<SchemaSettings.SchemaProvider>(Arrays.asList(SchemaSettings.SchemaProvider.values())));
		//helpLabel.setIcon(AllIcons.General.ContextHelp);
		helpLabel.setFont(UIUtil.getLabelFont(UIUtil.FontSize.SMALL));
		helpLabel.setForeground(UIUtil.getContextHelpForeground());

		// FIXME - temporary disable
		scroll.setVisible(false);
		table.setVisible(false);
		availableSchemasSeparator.setVisible(false);

		schemaValidationBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (Objects.equals(SchemaSettings.SchemaProvider.NONE, schemaValidationBox.getSelectedItem())) {
					strict.setEnabled(false);
					strict.setSelected(false);
				} else if (Objects.equals(SchemaSettings.SchemaProvider.PLUGIN, schemaValidationBox.getSelectedItem())) {
					strict.setEnabled(true);
				} else if (Objects.equals(SchemaSettings.SchemaProvider.APACHE_DIRECTORY_API, schemaValidationBox.getSelectedItem())) {
					strict.setEnabled(true);
				}
				updateHelpLabelText();
			}
		});
		schemaValidationBox.setRenderer(new SimpleListCellRenderer<SchemaSettings.SchemaProvider>() {
			@Override
			public void customize(@NotNull JList<? extends SchemaSettings.SchemaProvider> list, SchemaSettings.SchemaProvider value, int index, boolean selected, boolean hasFocus) {
				setText(value.toString());
			}
		});

		List<SchemaModelItem> modelList = new ArrayList<>();
		try {
			SchemaLoader loader = new JarLdifSchemaLoader();
			for (Schema schema : loader.getAllSchemas()) {
				modelList.add(new SchemaModelItem(schema.getSchemaName(), false, schema.isEnabled(), true, Arrays.asList(schema.getDependencies())));
			}
		} catch (LdapException | IOException e) {
			e.printStackTrace();
		}

		SchemaModel model = new SchemaModel(modelList);

		table.setAutoResizeMode(JBTable.AUTO_RESIZE_LAST_COLUMN);
		table.setAutoCreateColumnsFromModel(true);
		table.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setRowSelectionAllowed(true);

		// set proper row height
		table.setRowHeight((int) (UIUtil.getLabelFont().getSize()*1.8));

		ApplicationManager.getApplication().getMessageBus().connect().subscribe(UISettingsListener.TOPIC, new UISettingsListener() {
			@Override
			public void uiSettingsChanged(UISettings uiSettings) {
				table.setRowHeight((int)(uiSettings.getFontSize()*1.8));
			}
		});

		table.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

		table.getEmptyText().setText("Loading attributes...");
		table.setShowColumns(true);
		table.setEnableAntialiasing(true);
		table.setGridColor(JBColor.border());
		table.setMinimumSize(new JBDimension(350, 300));

		// TODO
		//table.setModel(model);
		table.setModel(new SchemaModel(new ArrayList<>()));
		table.getEmptyText().setText("TODO - There will be configurable list of known schemas for LDAP plugin.");

		table.getTableHeader().setReorderingAllowed(false);
		table.setAutoCreateRowSorter(true);

		TableColumn column2 = table.getColumnModel().getColumn(0);
		column2.setCellRenderer(new BooleanTableCellRenderer());
		column2.setCellEditor(new BooleanTableCellEditor());
		column2.setMaxWidth(33);
		column2.setPreferredWidth(33);
		column2.setWidth(33);
		column2.setMinWidth(33);

		TableColumn column0 = table.getColumnModel().getColumn(1);
		column0.setCellRenderer(new DefaultTableCellRenderer());

		TableColumn column = table.getColumnModel().getColumn(2);
		ComboBoxTableRenderer<SchemaModelItem.Mode> rend = new ComboBoxTableRenderer<>(SchemaModelItem.Mode.values());
		rend.withClickCount(1);
		column.setCellRenderer(new DefaultTableCellRenderer());
		column.setCellEditor(rend);

		TableColumn column3 = table.getColumnModel().getColumn(3);
		column3.setCellRenderer(new DefaultTableCellRenderer());

		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				int row = table.rowAtPoint(e.getPoint());
				int col = table.columnAtPoint(e.getPoint());
				if (row != -1 && col != -1) {
					if (!((SchemaModel)table.getModel()).getBackingObjectAt(row).isBuiltIn()) {

					}
				}
				super.mouseClicked(e);
			}
		});

	}

	public void loadSettings(LdapPluginSettings settings) {

		SchemaSettings schemaSettings = settings.getSchemaSettings();

		schemaValidationBox.setSelectedItem(schemaSettings.getSchemaProvider());
		updateHelpLabelText();
		if (SchemaSettings.SchemaProvider.NONE.equals(schemaSettings.getSchemaProvider())) {
			strict.setEnabled(false);
		}
		strict.setSelected(schemaSettings.isStrictSchemaValidation());

		/*
		Map<String, SchemaModelItem> defaultKnownSchemasSettings = schemaSettings.getDefaultKnownSchemasSettings();
		if (defaultKnownSchemasSettings.isEmpty()) {
			for (SchemaModelItem item : ((SchemaModel)table.getModel()).getItems()) {
				defaultKnownSchemasSettings.put(item.getName(), item);
			}
		} else {
			table.setModel(new SchemaModel(new ArrayList<>(defaultKnownSchemasSettings.values())));
			table.repaint();
		}
		*/

	}

	public void saveSettings(LdapPluginSettings settings) {

		SchemaSettings schemaSettings = settings.getSchemaSettings();

		schemaSettings.setSchemaProvider((SchemaSettings.SchemaProvider) schemaValidationBox.getSelectedItem());
		if (Objects.equals(SchemaSettings.SchemaProvider.NONE, schemaSettings.getSchemaProvider())) {
			schemaSettings.setStrictSchemaValidation(false);
		} else {
			schemaSettings.setStrictSchemaValidation(strict.isSelected());
		}
		/*
		List<SchemaModelItem> tableSchemas = new ArrayList<>(((SchemaModel)table.getModel()).getItems());
		schemaSettings.getDefaultKnownSchemasSettings().clear();
		for (SchemaModelItem item : tableSchemas) {
			schemaSettings.getDefaultKnownSchemasSettings().put(item.getName(), item);
		}
		 */

		settings.setSchemaSettings(schemaSettings);

	}

	public boolean isModified(LdapPluginSettings settings) {

		SchemaSettings schemaSettings = settings.getSchemaSettings();

		if (!Objects.equals(schemaSettings.getSchemaProvider(), schemaValidationBox.getSelectedItem())) return true;
		if (!Objects.equals(schemaSettings.isStrictSchemaValidation(), strict.isSelected())) return true;
		/*
		Map<String, SchemaModelItem> defaultKnownSchemasSettings = schemaSettings.getDefaultKnownSchemasSettings();
		List<SchemaModelItem> tableSchemas = new ArrayList<>(((SchemaModel)table.getModel()).getItems());
		List<SchemaModelItem> storedSchemas = new ArrayList<>(defaultKnownSchemasSettings.values());
		if (!Objects.equals(tableSchemas, storedSchemas)) return true;
		*/

		return false;

	}

	public JComponent getContent() {
		return wrapper;
	}

	private void updateHelpLabelText() {
		if (Objects.equals(SchemaSettings.SchemaProvider.NONE, schemaValidationBox.getSelectedItem())) {
			helpLabel.setText("<html>Schema is not retrieved from LDAP.<br/>Plugin provides no input/syntax validation, no recognition on MUST/MAY attributes etc.<br/>Best effort is done to determine (non)binary attributes for editing.</html>");
		} else if (Objects.equals(SchemaSettings.SchemaProvider.PLUGIN, schemaValidationBox.getSelectedItem())) {
			helpLabel.setText("<html>This is the default. Schema is retrieved from LDAP and ApacheDirectory API schemas are used as fallback.<br/>Plugin provides best effort support on input/syntax validation, provides specific editors based on attribute syntax,<br/>recognize MUST/MAY attributes and single/multi valued attributes.</html>");
		} else if (Objects.equals(SchemaSettings.SchemaProvider.APACHE_DIRECTORY_API, schemaValidationBox.getSelectedItem())) {
			helpLabel.setText("<html>All schemas retrieved from LDAP are expected to be in RFC valid syntax, otherwise they are not loaded.<br/>Plugin provides support on input/syntax validation, provides specific editors based on attribute syntax,<br/>recognize MUST/MAY attributes and single/multi valued attributes.</html>");
		}
	}

	public static class SchemaModelItem implements Comparable<SchemaModelItem> {

		public enum Mode {
			STRICT,
			RELAXED
		}

		String name;
		boolean isRelaxed = SchemaManager.STRICT;
		boolean enabled = false;
		boolean builtIn = false;
		List<String> dependencies = new ArrayList<>();
		private String content;

		public SchemaModelItem(String name, boolean isRelaxed, boolean enabled, boolean builtIn, List<String> dependencies) {
			this.name = name;
			this.isRelaxed = isRelaxed;
			this.enabled = enabled;
			this.builtIn = builtIn;
			this.dependencies = dependencies;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public boolean isRelaxed() {
			return isRelaxed;
		}

		public void setRelaxed(boolean relaxed) {
			isRelaxed = relaxed;
		}

		public boolean isEnabled() {
			return enabled;
		}

		public void setEnabled(boolean enabled) {
			this.enabled = enabled;
		}

		public boolean isBuiltIn() {
			return builtIn;
		}

		public void setBuiltIn(boolean builtIn) {
			this.builtIn = builtIn;
		}

		public List<String> getDependencies() {
			return dependencies;
		}

		public void setDependencies(List<String> dependencies) {
			this.dependencies = dependencies;
		}

		public String getContent() {
			return content;
		}

		public void setContent(String content) {
			this.content = content;
		}

		@Override
		public int compareTo(@NotNull SchemaModelItem o) {
			return this.name.compareTo(o.getName());
		}

	}

	public static class SchemaModel implements TableModel {

		List<TableModelListener> listeners = new ArrayList<>();

		public final String[] COLUMN_NAMES = new String[] {"", "Name", "Mode", "Dependencies"};
		public final Class<?>[] COLUMN_CLASSES = new Class<?>[] {Boolean.class, String.class, SchemaModelItem.Mode.class, String.class};

		List<SchemaModelItem> items = new ArrayList<>();

		public SchemaModel(List<SchemaModelItem> items) {
			this.items = items;
		}

		@Override
		public int getRowCount() {
			return items.size();
		}

		@Override
		public int getColumnCount() {
			return COLUMN_NAMES.length;
		}

		@Override
		public String getColumnName(int columnIndex) {
			return COLUMN_NAMES[columnIndex];
		}

		@Override
		public Class<?> getColumnClass(int columnIndex) {
			return COLUMN_CLASSES[columnIndex];
		}

		@Override
		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return (columnIndex == 0 || columnIndex == 2);
			//(columnIndex == 0 || columnIndex == 2) || (columnIndex == 1 && !items.get(rowIndex).isBuiltIn());
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			SchemaModelItem item = items.get(rowIndex);
			if (columnIndex == 0) return item.isEnabled();
			if (columnIndex == 1) return item.getName() + ((item.isBuiltIn()) ? " (built-in)" : "");
			if (columnIndex == 2) {
				return ((item.isRelaxed()) ? SchemaModelItem.Mode.RELAXED : SchemaModelItem.Mode.STRICT);
			}
			if (columnIndex == 3) return StringUtils.join(item.getDependencies().iterator(), ", ");
			return null;
		}

		public SchemaModelItem getBackingObjectAt(int rowIndex) {
			if (rowIndex == -1) return null;
			return items.get(rowIndex);
		}

		@Override
		public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
			if (columnIndex == 2) {
				items.get(rowIndex).setRelaxed((Objects.equals(aValue, SchemaModelItem.Mode.RELAXED)));
			}
			if (columnIndex == 0) {
				items.get(rowIndex).setEnabled((Boolean)aValue);
			}
			// fire event changed
			for (TableModelListener listener : listeners) {
				listener.tableChanged(new TableModelEvent(this, rowIndex));
			}
			System.out.println("set value");
		}

		@Override
		public void addTableModelListener(TableModelListener l) {
			listeners.add(l);
		}

		@Override
		public void removeTableModelListener(TableModelListener l) {
			listeners.remove(l);
		}

		public List<SchemaModelItem> getItems() {
			return Collections.unmodifiableList(items);
		}

	}

}
