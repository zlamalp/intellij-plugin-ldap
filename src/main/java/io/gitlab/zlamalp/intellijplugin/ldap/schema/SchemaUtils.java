package io.gitlab.zlamalp.intellijplugin.ldap.schema;

import io.gitlab.zlamalp.intellijplugin.ldap.LdapServer;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.schema.SchemaSettings;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.api.ldap.model.schema.AttributeType;
import org.apache.directory.api.ldap.model.schema.LdapSyntax;
import org.apache.directory.api.ldap.model.schema.ObjectClass;
import org.apache.directory.api.ldap.model.schema.SchemaManager;
import org.apache.directory.api.ldap.model.schema.UsageEnum;
import org.apache.directory.api.ldap.model.schema.registries.Schema;
import org.apache.directory.api.ldap.model.schema.registries.SchemaLoader;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.DirectoryStringSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.IntegerSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.OctetStringSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.PrintableStringSyntaxChecker;
import org.apache.directory.api.ldap.schema.loader.JarLdifSchemaLoader;
import org.apache.directory.api.ldap.schema.manager.impl.DefaultSchemaManager;
import org.apache.directory.ldap.client.api.DefaultSchemaLoader;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * This is class with utility methods which works with our representation of LDAP schema.
 * Whenever schema is updated from LDAP, we should init static caches:
 *
 * @see #updateCache(LdapSchema)
 *
 * Its also lazy initialized once first used to get may/must attributes etc.
 *
 * TODO - Move isMust/isMay etc methods from LdapUtils to this class
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class SchemaUtils {

	private static transient Logger log = LoggerFactory.getLogger(SchemaUtils.class);

	/**
	 * Cache of attribute names, which have OPERATIONAL use by Schema
	 */
	private static Map<LdapSchema, Set<String>> operationalAttributesCache = new HashMap<>();

	/**
	 * Cache of attribute names which are MUST by some objectClasses of Schema
	 */
	private static Map<LdapSchema, Map<String, Set<String>>> mustAttributesCache = new HashMap<>();

	/**
	 * Cache of attribute names which are MAY by some objectClasses
	 */
	private static Map<LdapSchema, Map<String, Set<String>>> mayAttributesCache = new HashMap<>();


	// OPEN LDAP
	public static final List<String> openLDAPdefaultSchemas = Arrays.asList("system","core","apache","apachemeta","cosine","nis","inetorgperson");
	// MS AD - without "system" is missing syntaxes, with "system" fails on some objectclasses and attributes
	public static final List<String> msADdefaultSchemas = Arrays.asList("system");

	/**
	 * Update / Initialize caches for passed schema
	 *
	 * @param schema Update caches with this schema
	 */
	public static synchronized void updateCache(LdapSchema schema) {

		Set<String> opAttrs = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
		if (operationalAttributesCache.containsKey(schema)) {
			opAttrs = operationalAttributesCache.get(schema);
			opAttrs.clear();
		}

		for (AttributeType attribute : schema.getAttributeTypes().values()) {
			if (UsageEnum.DIRECTORY_OPERATION.equals(attribute.getUsage())) {
				opAttrs.addAll(attribute.getNames());
			}
		}

		operationalAttributesCache.put(schema, opAttrs);

		// clear MUST/MAY caches - they are rebuild at runtime
		mustAttributesCache.remove(schema);
		mayAttributesCache.remove(schema);

	}

	/**
	 * Return TRUE if attribute defined by name is considered as operational (USAGE = directoryOperation).
	 * If state is unknown or USAGE has different value, FALSE is returned.
	 *
	 * @param schema Schema to check with
	 * @param attribute Attribute name to check with
	 * @return TRUE if is operation / FALSE if not or unknown
	 */
	public static boolean isOperationalAttribute(@NotNull LdapSchema schema, @NotNull String attribute) {

		if (operationalAttributesCache.containsKey(schema)) {
			return operationalAttributesCache.get(schema).contains(attribute);
		}
		return false;

	}

	/**
	 * Check if attribute is MUST (required by schema defined by entry objectClasses)
	 *
	 * @param ldapNode LDAP entry to check schema on
	 * @param attrID attribute name
	 * @return TRUE if is MUST by schema (required)
	 */
	public static boolean isMustAttribute(@NotNull LdapEntryTreeNode ldapNode, String attrID) {

		LdapSchema schema = ldapNode.getLdapServer().getSchema();
		List<String> objectClassNames = ldapNode.getObjectClasses();

		Set<String> mustAttributes = getMustAttributes(schema, objectClassNames);
		mustAttributes = mustAttributes.stream().map(String::toLowerCase).collect(Collectors.toSet());

		return mustAttributes.contains(attrID.toLowerCase()) || mustAttributes.contains(attrID);

	}

	/**
	 * Return set of all MUST attribute names normalized to LOWER_CASE,
	 * which are required by schema hierarchy for specified objectClasses and schema.
	 *
	 * @param schema Schema to check against
	 * @param objectClassNames objectClass names to get MUST attributes for
	 * @return set of MUST attribute names
	 */
	public static Set<String> getMustAttributes(@NotNull LdapSchema schema, List<String> objectClassNames) {

		if (mustAttributesCache.containsKey(schema)) {
			Collections.sort(objectClassNames);
			if (mustAttributesCache.get(schema).containsKey(objectClassNames.toString())) {
				return Collections.unmodifiableSet(mustAttributesCache.get(schema).get(objectClassNames.toString()));
			}
		} else {
			mustAttributesCache.putIfAbsent(schema, new HashMap<>());
		}

		Set<String> mustAttributes = new HashSet<>();
		for (String objectClassName : objectClassNames) {

			ObjectClass objectClass = schema.getObjectClasses().get(objectClassName);
			if (objectClass != null) {
				mustAttributes.addAll(objectClass.getMustAttributeTypes().stream().map(AttributeType::getName).collect(Collectors.toSet()));
				List<String> superiorNames = objectClass.getSuperiors().stream().map(ObjectClass::getName).collect(Collectors.toList());
				if (!superiorNames.isEmpty()) {
					mustAttributes.addAll(getMustAttributes(schema, superiorNames)); // TODO - fixme check if correctly use superior names
				}
			} else {
				log.warn("ObjectClass '{}' is not present in LDAP schema!", objectClassName);
			}

		}
		mustAttributesCache.get(schema).put(objectClassNames.toString(), mustAttributes);
		return Collections.unmodifiableSet(mustAttributes);

	}

	/**
	 * Return set of all MUST attribute names normalized to LOWER_CASE,
	 * which are required by schema hierarchy for specified objectClasses and schema.
	 *
	 * @param schema Schema to check against
	 * @param objectClassNames objectClass names to get MUST attributes for
	 * @return set of MUST attribute names
	 */
	public static Set<String> getMustAttributes(SchemaManager schema, List<String> objectClassNames) {

		Set<String> mustAttributes = new HashSet<>();
		for (String objectClassName : objectClassNames) {

			ObjectClass objectClass = null;
			try {
				objectClass = schema.getObjectClassRegistry().lookup(objectClassName);
			} catch (LdapException e) {
				e.printStackTrace();
			}
			if (objectClass != null) {
				mustAttributes.addAll(objectClass.getMustAttributeTypeOids()); //.stream().map(String::toLowerCase).collect( Collectors.toSet()));
				mustAttributes.addAll(getMustAttributes(schema, objectClass.getSuperiorOids()));
			} else {
				log.warn("ObjectClass '{}' is not present in LDAP schema!", objectClassName);
			}

		}
		return Collections.unmodifiableSet(mustAttributes);

	}

	/**
	 * Return set of all MAY attribute names normalized to LOWER_CASE,
	 * which are required by schema hierarchy for specified objectClasses and schema.
	 *
	 * @param schema Schema to check against
	 * @param objectClassNames objectClass names to get MAY attributes for
	 * @return set of MAY attribute names
	 */
	public static Set<String> getMayAttributes(SchemaManager schema, List<String> objectClassNames) {

		Set<String> mayAttributes = new HashSet<>();
		for (String objectClassName : objectClassNames) {

			ObjectClass objectClass = null;
			try {
				objectClass = schema.getObjectClassRegistry().lookup(objectClassName);
			} catch (LdapException e) {
				e.printStackTrace();
			}
			if (objectClass != null) {
				mayAttributes.addAll(objectClass.getMayAttributeTypeOids()); //.stream().map(String::toLowerCase).collect( Collectors.toSet()));
				mayAttributes.addAll(getMayAttributes(schema, objectClass.getSuperiorOids()));
			} else {
				log.warn("ObjectClass '{}' is not present in LDAP schema!", objectClassName);
			}

		}
		return Collections.unmodifiableSet(mayAttributes);

	}

	/**
	 * Return set of all MAY attribute names normalized to LOWER_CASE,
	 * which are required by schema hierarchy for specified objectClasses and schema.
	 *
	 * @param schema Schema to check against
	 * @param objectClassNames objectClass names to get MAY attributes for
	 * @return set of MAY attribute names
	 */
	public static Set<String> getMayAttributes(LdapSchema schema, List<String> objectClassNames) {

		if (mayAttributesCache.containsKey(schema)) {
			Collections.sort(objectClassNames);
			if (mayAttributesCache.get(schema).containsKey(objectClassNames.toString())) {
				return Collections.unmodifiableSet(mayAttributesCache.get(schema).get(objectClassNames.toString()));
			}
		} else {
			mayAttributesCache.putIfAbsent(schema, new HashMap<>());
		}

		Set<String> mayAttributes = new HashSet<>();
		for (String objectClassName : objectClassNames) {

			ObjectClass objectClass = schema.getObjectClasses().get(objectClassName);
			if (objectClass != null) {
				mayAttributes.addAll(objectClass.getMayAttributeTypes().stream().map(AttributeType::getName).collect(Collectors.toSet()));
				List<String> superiorNames = objectClass.getSuperiors().stream().map(ObjectClass::getName).collect(Collectors.toList());
				if (!superiorNames.isEmpty()) {
					mayAttributes.addAll(getMayAttributes(schema, superiorNames)); // TODO - fixme check if correctly use superior names
				}
			} else {
				log.warn("ObjectClass '{}' is not present in LDAP schema!", objectClassName);
			}

		}
		mayAttributesCache.get(schema).put(objectClassNames.toString(), mayAttributes);
		return Collections.unmodifiableSet(mayAttributes);

	}

	/**
	 * Fills passed SchemaLoader with Schemas read from LDAP server connection
	 *
	 * @param server LDAP server to get schemas from
	 * @param loader SchemaLoader to fill schema for
	 * @param sourceConnection LDAP connection to participate on
	 * @throws LdapException When loading schema fails
	 */
	private static void fillSchemasFromLdap(LdapServer server, SchemaLoader loader, LdapConnection sourceConnection) throws LdapException {

		// TODO - we should load schema in relaxed/strict mode based on user settings for such schema

		LdapConnection ldapConnection = null;
		try {
			ldapConnection = (sourceConnection != null) ? sourceConnection : server.getConnection();
			for (Schema schema : new DefaultSchemaLoader(ldapConnection, true).getAllSchemas()) {
				loader.addSchema(schema);
			}
		} finally {
			// close locally created connection
			if (sourceConnection == null && ldapConnection != null) {
				server.releaseConnection(ldapConnection);
			}
		}

	}

	/**
	 * Provides SchemaManager instance with appropriate schemas loaded from Jar and LDAP connection
	 * based on preferences set by user in schema settings.
	 *
	 * @param server LDAP server to get schema manager for
	 * @param sourceConnection LDAP connection to participate on
	 * @return Loader with loaded schemas
	 * @throws LdapException When loading schema fails
	 */
	public static SchemaManager buildSchemaManager(LdapServer server, LdapConnection sourceConnection) throws LdapException {

		SchemaSettings schemaSettings = LdapUtils.getPreferredLdapSettings(server.getSettings()).getSchemaSettings();
		if (SchemaSettings.SchemaProvider.NONE.equals(schemaSettings.getSchemaProvider())) {
			return new DefaultSchemaManager();
		}

		List<String> requestedSchemas = new ArrayList<>();

		if (Objects.equals(server.getLdapType(), LdapServer.LdapType.OPEN_LDAP)) {
			requestedSchemas.addAll(openLDAPdefaultSchemas);
		} else if (Objects.equals(server.getLdapType(), LdapServer.LdapType.ACTIVE_DIRECTORY)) {
			requestedSchemas.addAll(msADdefaultSchemas);
		} else if (Objects.equals(server.getLdapType(), LdapServer.LdapType.APACHE_DIRECTORY)) {
			// TODO - is reading from connection enough ??
		} else {
			// FIXME - fallback to open LDAP ??
			requestedSchemas.addAll(openLDAPdefaultSchemas);
		}

		// make sure "other" schema read from connection is last
		requestedSchemas.add("other");

		if (SchemaSettings.SchemaProvider.APACHE_DIRECTORY_API.equals(schemaSettings.getSchemaProvider())) {

			// TODO - support strict vs. relaxed
			try {
				SchemaLoader loader = new JarLdifSchemaLoader();
				if (requestedSchemas.contains("other")) {
					fillSchemasFromLdap(server, loader, sourceConnection);
				}
				for (Schema schema : loader.getAllSchemas()) {
					if (requestedSchemas.contains(schema.getSchemaName())) {
						schema.enable();
					} else {
						schema.disable();
					}
					log.info("Found schema: " + schema.getSchemaName() + " | enabled: " + schema.isEnabled());
				}
				SchemaManager schemaManager = new DefaultSchemaManager(loader);
				for (Schema schema : loader.getAllSchemas()) {
					if (Objects.equals("other", schema.getSchemaName())) {
						// FIXME - load the one from ldap as relaxed, otherwise it is not usable for openLDAP
						schemaManager.loadWithDepsRelaxed(schema);
					} else {
						schemaManager.loadWithDeps(schema);
					}
				}

				return schemaManager;
			} catch (Exception e) {
				log.error("Unable to load Apache Directory API schemas.",e);
				return null;
			}

		} else {

			// our best-effort plugin version with fallback to apache directory api
			SchemaLoader loader;
			try {
				loader = new JarLdifSchemaLoader();
				if (requestedSchemas.contains("other")) {
					fillSchemasFromLdap(server, loader, sourceConnection);
				}
			} catch (IOException ex) {
				LdapConnection ldapConnection = null;
				try {
					ldapConnection = (sourceConnection != null) ? sourceConnection : server.getConnection();
					loader = new DefaultSchemaLoader(ldapConnection, true);
				} finally {
					if (sourceConnection == null && ldapConnection != null) {
						server.releaseConnection(ldapConnection);
					}
				}
			}

			// enable/disable wanted schemas
			for (Schema schema : loader.getAllSchemas()) {
				if (requestedSchemas.contains(schema.getSchemaName())) {
					schema.enable();
				} else {
					schema.disable();
				}
				log.info("Found schema: " + schema.getSchemaName() + " | enabled: " + schema.isEnabled());
			}

			SchemaManager schemaManager = new DefaultSchemaManager(loader);
			for (String schemaName : requestedSchemas) {

				Schema schema = loader.getSchema(schemaName);
				log.info("Loading schema: " + schema.getSchemaName());
				boolean result;
				// should be last
				if (schemaName.equalsIgnoreCase("other")) {

					// load additional fixes
					if (LdapServer.LdapType.OPEN_LDAP.equals(server.getLdapType())) {
						appendMissingOpenLdapElements(schemaManager);
					} else if (LdapServer.LdapType.ACTIVE_DIRECTORY.equals(server.getLdapType())) {
						appendMissingMSADElements(schemaManager);
					}

					result = schemaManager.loadWithDepsRelaxed(schema);

				} else {
					result = schemaManager.loadWithDeps(schema);
				}
				log.info("Loading " + ((result) ? "finished." : "failed."));
			}

			return schemaManager;

		}

	}

	/**
	 * Append missing schema elements for OpenLDAP into SchemaManager
	 *
	 * @param schemaManager SchemaManager associated with LDAP connection
	 * @throws LdapException When we can't update schema manager
	 */
	private static void appendMissingOpenLdapElements(SchemaManager schemaManager) throws LdapException {

		// Append missing OpenLDAP schema element
		AttributeType configContext = new AttributeType("1.3.6.1.4.1.4203.666.11.1.1");
		configContext.setNames("configContext");
		configContext.setDescription("config context");
		configContext.setSyntaxOid("1.3.6.1.4.1.1466.115.121.1.12");
		configContext.setUsage(UsageEnum.DSA_OPERATION);
		configContext.setSingleValued(true);
		configContext.setUserModifiable(false);
		schemaManager.getRegistries().add(configContext, true);

		// TODO - we should remove any former attributes and replace them with the ones provided by LDAP itself ??

	}

	/**
	 * Append missing schema elements for MS ActiveDirectory into SchemaManager
	 *
	 * @param schemaManager SchemaManager associated with LDAP/AD connection
	 * @throws LdapException When we can't update schema manager
	 */
	private static void appendMissingMSADElements(SchemaManager schemaManager) throws LdapException {

		// remove Labeled Ref URI, since it collides with MS AD "middleName" (from DirectoryAPI "system" schema)
		schemaManager.getRegistries().delete(schemaManager.getRegistries().getObjectClassRegistry().get("2.16.840.1.113730.3.2.6"));
		schemaManager.getRegistries().delete(schemaManager.getRegistries().getAttributeTypeRegistry().get("2.16.840.1.113730.3.1.34"));


		// Append missing MS AD elements


		// Encoded as 1.3.6.1.4.1.1466.115.121.1.27 but guaranteed to support 64-bit numbers.
		LdapSyntax msLargeInteger = new LdapSyntax("1.2.840.113556.1.4.906", "Large integer (a.k.a. INTEGER8)", true);
		msLargeInteger.setSyntaxChecker(IntegerSyntaxChecker.INSTANCE);
		schemaManager.getRegistries().add(msLargeInteger, true);

		LdapSyntax msCaseIgnoreString = new LdapSyntax("1.2.840.113556.1.4.905", "Case-ignore string (teletex)", true);
		msCaseIgnoreString.setSyntaxChecker(PrintableStringSyntaxChecker.INSTANCE);
		schemaManager.getRegistries().add(msCaseIgnoreString, true);

		LdapSyntax msCaseExactString = new LdapSyntax("1.2.840.113556.1.4.1362", "Case-sensitive string (a.k.a. case-exact string)", true);
		msCaseExactString.setSyntaxChecker(DirectoryStringSyntaxChecker.INSTANCE);
		schemaManager.getRegistries().add(msCaseExactString, true);

		LdapSyntax msSecurityDescriptor = new LdapSyntax("1.2.840.113556.1.4.907", "NT security descriptor", true);
		msSecurityDescriptor.setSyntaxChecker(PrintableStringSyntaxChecker.INSTANCE);
		schemaManager.getRegistries().add(msSecurityDescriptor, true);

		LdapSyntax msDnWithBinary = new LdapSyntax("1.2.840.113556.1.4.903", "DN with binary (a.k.a. DN with octet string)", false);
		// TODO - has complex syntax
		schemaManager.getRegistries().add(msDnWithBinary, true);

		LdapSyntax msDnWithUnicode = new LdapSyntax("1.2.840.113556.1.4.904", "DN with Unicode string", false);
		// TODO - has complex syntax
		schemaManager.getRegistries().add(msDnWithUnicode, true);

		LdapSyntax msOrName = new LdapSyntax("1.2.840.113556.1.4.1221", "OR name", false);
		// TODO - has more complex syntax
		msCaseExactString.setSyntaxChecker(DirectoryStringSyntaxChecker.INSTANCE);
		schemaManager.getRegistries().add(msOrName, true);

		// not in RFC
		LdapSyntax msOctetString = new LdapSyntax("OctetString", "OctetString (not in RFC), same as '1.3.6.1.4.1.1466.115.121.1.40'", false);
		msCaseExactString.setSyntaxChecker(OctetStringSyntaxChecker.INSTANCE);
		schemaManager.getRegistries().add(msOctetString, true);


		// Append missing attribute types used in Root DSE


		AttributeType rootdomainnamingcontext = new AttributeType("3.1.1.3.2.16");
		rootdomainnamingcontext.setNames("rootDomainNamingContext");
		rootdomainnamingcontext.setSyntaxOid("1.3.6.1.4.1.1466.115.121.1.12"); // refer to generic syntax of DN
		rootdomainnamingcontext.setUsage(UsageEnum.DSA_OPERATION);
		rootdomainnamingcontext.setSingleValued(true);
		rootdomainnamingcontext.setUserModifiable(false);
		schemaManager.getRegistries().add(rootdomainnamingcontext, true);

		AttributeType currenttime = new AttributeType("3.1.1.3.2.2");
		currenttime.setNames("currentTime");
		currenttime.setSyntaxOid("1.3.6.1.4.1.1466.115.121.1.24"); // refer to generic syntax of GeneralizedTime
		currenttime.setUsage(UsageEnum.DSA_OPERATION);
		currenttime.setSingleValued(true);
		currenttime.setUserModifiable(false);
		schemaManager.getRegistries().add(currenttime, true);

		// TODO - supportedLdapPolicies

	}

	/**
	 * Return proper AttributeType for passed Attribute.
	 *
	 * @param node LdapEntryTreeNode to get schema from to search for AttributeType
	 * @param attribute Attribute to check and get AttributeType for
	 * @return AttributeType for Attribute if known or NULL
	 */
	public static @Nullable AttributeType getAttributeType(@NotNull LdapEntryTreeNode node, @NotNull Attribute attribute) {

		LdapSchema schema = node.getLdapServer().getSchema();
		SchemaSettings.SchemaProvider schemaProvider = LdapUtils.getPreferredLdapSettings(node.getLdapServer().getSettings()).getSchemaSettings().getSchemaProvider();

		// no type information support
		if (SchemaSettings.SchemaProvider.NONE.equals(schemaProvider)) {
			return null;
		}

		// strip any "options" from attribute name
		String attrName = attribute.getId();
		int optPos = attrName.indexOf( ';' );
		if (optPos != -1) {
			attrName = attrName.substring(0, optPos);
		}

		// We have NONE or RELAXED schema support
		SchemaManager schemaManager = schema.getSchemaManager();
		AttributeType type = null;
		try {
			String oid = schemaManager.getAttributeTypeRegistry().getOidByName(attrName);
			type = schemaManager.getAttributeType(oid);
		} catch (LdapException ex) {
			// silent ?
			log.warn("We couldn't find AttributeType for {} in SchemaManager.", attrName);
		}

		// Apache directory api
		if (SchemaSettings.SchemaProvider.APACHE_DIRECTORY_API.equals(schemaProvider)) {
			// we have STRICT schema validation, type is a part of the Attribute object
			if (attribute.getAttributeType() != null) {
				type = attribute.getAttributeType();
			}
			// not strict validation
			return type;
		}

		// Plugin provider - we prefer our LdapSchema, if not found, we fallback to ApacheDirectory API SchemaManager

		// we have STRICT schema validation, type is a part of the Attribute object
		// FIXME - schema manager applied to connection differs from our schema, we can't provide value from attribute itself, but we should
		//if (attribute.getAttributeType() != null) {
		//	type = attribute.getAttributeType();
		//}
		return schema.getAttributeTypes().getOrDefault(attrName, type);

	}

}
