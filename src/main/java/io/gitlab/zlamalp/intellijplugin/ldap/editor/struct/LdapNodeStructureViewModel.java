package io.gitlab.zlamalp.intellijplugin.ldap.editor.struct;

import com.intellij.ide.structureView.FileEditorPositionListener;
import com.intellij.ide.structureView.ModelListener;
import com.intellij.ide.structureView.StructureViewModel;
import com.intellij.ide.structureView.StructureViewTreeElement;
import com.intellij.ide.util.treeView.smartTree.Filter;
import com.intellij.ide.util.treeView.smartTree.Grouper;
import com.intellij.ide.util.treeView.smartTree.NodeProvider;
import com.intellij.ide.util.treeView.smartTree.ProvidingTreeModel;
import com.intellij.ide.util.treeView.smartTree.Sorter;
import com.intellij.util.containers.ContainerUtil;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.AttributeModelItem;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeVirtualFile;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import org.jetbrains.annotations.NotNull;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Implementation of StructureViewModel for LdapEntryEditor.
 *
 * @see LdapEntryEditor
 * @see LdapNodeStructureViewTreeElement
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdapNodeStructureViewModel implements StructureViewModel, ProvidingTreeModel, StructureViewModel.ElementInfoProvider {

	private LdapNodeVirtualFile file;
	private LdapNodeStructureViewTreeElement root;
	private LdapEntryEditor editor;
	private final List<ModelListener> myModelListeners = ContainerUtil.createLockFreeCopyOnWriteList();
	private final List<FileEditorPositionListener> myListeners = ContainerUtil.createLockFreeCopyOnWriteList();

	public LdapNodeStructureViewModel(LdapEntryEditor editor, LdapNodeVirtualFile file) {
		this.editor = editor;
		this.file = file;

		// handle scroll from source
		editor.getTable().getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				for (FileEditorPositionListener listener : myListeners) {
					listener.onCurrentElementChanged();
				}
			}
		});

	}

	/**
	 * Return OBJECT currently selected in editor (in our case model items Attribute).
	 * It must match getValue() of TreeElement (tree model item), since if they are equals,
	 * item is selected when "scroll from source" is enabled.
	 *
	 * @return Attribute or NULL of selected table model item.
	 */
	@Override
	public Object getCurrentEditorElement() {

		AttributeModelItem item = editor.getTable().getSelectedItem();
		if (item != null) return item.getAttribute();
		return null;

	}

	@Override
	public void addEditorPositionListener(@NotNull FileEditorPositionListener listener) {
		myListeners.add(listener);
	}

	@Override
	public void removeEditorPositionListener(@NotNull FileEditorPositionListener listener) {
		myListeners.remove(listener);
	}

	@Override
	public void addModelListener(@NotNull ModelListener modelListener) {
		myModelListeners.add(modelListener);
	}

	@Override
	public void removeModelListener(@NotNull ModelListener modelListener) {
		myModelListeners.remove(modelListener);
	}

	@NotNull
	@Override
	public StructureViewTreeElement getRoot() {
		if (root != null) return root;
		LdapEntryTreeNode node = (LdapEntryTreeNode) file.getLdapTreeNode();
		root = new LdapNodeStructureViewTreeElement(editor, node, null);
		return root;
	}

	@Override
	public void dispose() {
		myModelListeners.clear();
	}

	@Override
	@NotNull
	public Grouper[] getGroupers() {
		return Grouper.EMPTY_ARRAY;
	}

	@Override
	@NotNull
	public Sorter[] getSorters() {
		return new Sorter[] {Sorter.ALPHA_SORTER};
	}

	@Override
	@NotNull
	public Filter[] getFilters() {
		return Filter.EMPTY_ARRAY;
	}


	@Override
	public boolean shouldEnterElement(Object element) {
		return false;
	}

	/**
	 * Manually invalidate value of root node in case model changed
	 */
	public void invalidate() {
		root.setInvalidated(true);
	}

	@NotNull
	@Override
	public Collection<NodeProvider<?>> getNodeProviders() {
		return Collections.emptyList();
	}

	@Override
	public boolean isEnabled(@NotNull NodeProvider provider) {
		return false;
	}

	@Override
	public boolean isAlwaysShowsPlus(StructureViewTreeElement element) {
		return false;
	}

	@Override
	public boolean isAlwaysLeaf(StructureViewTreeElement element) {
		return false;
	}
}
