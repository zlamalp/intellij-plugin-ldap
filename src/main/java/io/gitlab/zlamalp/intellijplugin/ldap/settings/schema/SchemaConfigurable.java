package io.gitlab.zlamalp.intellijplugin.ldap.settings.schema;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.options.Configurable;
import com.intellij.openapi.options.ConfigurationException;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.plugin.LdapPluginSettings;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.LdapPluginSettingsService;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

/**
 * IDE configurable for Schema settings of LDAP plugin
 *
 * @author Pavel Zlámal
 */
public class SchemaConfigurable implements Configurable {

	private SchemaSettingsForm form;

	@Nls
	public String getDisplayName() {
		return "Schema";
	}

	@Nullable
	public Icon getIcon() {
		return null;
	}

	@Nullable
	@NonNls
	public String getHelpTopic() {
		return null;
	}

	public JComponent createComponent() {
		if (form == null) {
			LdapPluginSettingsService instance = LdapPluginSettingsService.getInstance();
			LdapPluginSettings state = instance.getState();
			form = new SchemaSettingsForm(state);
		}
		return form.getContent();
	}

	public boolean isModified() {
		if (form != null) {
			return form.isModified(LdapPluginSettingsService.getInstance().getState());
		}
		return false;
	}

	public void apply() throws ConfigurationException {
		LdapPluginSettings settings = LdapPluginSettingsService.getInstance().getState();
		if (form != null) {
			form.saveSettings(settings);
			// notify listeners about schema settings changes
			ApplicationManager.getApplication().getMessageBus().syncPublisher(SchemaSettingsListener.TOPIC).schemaSettingsChanged(settings.getSchemaSettings());
		}
	}

	public void reset() {
		LdapPluginSettings settings = LdapPluginSettingsService.getInstance().getState();
		if (form != null) {
			form.loadSettings(settings);
		}
	}

	public void disposeUIResources() {
		form = null;
	}

}
