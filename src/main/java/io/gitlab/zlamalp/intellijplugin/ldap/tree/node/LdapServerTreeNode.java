package io.gitlab.zlamalp.intellijplugin.ldap.tree.node;

import io.gitlab.zlamalp.intellijplugin.ldap.LdapIcons;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapServer;
import io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeType;
import io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeVirtualFile;
import org.apache.directory.api.ldap.model.name.Dn;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 * TreeNode representing LDAP server connection. Provides child -> ROOT_DSE TreeNode for browsing the LDAP tree
 * and Custom Searches TreeNode for aggregating results of custom searches.
 *
 * @author Attila Majoros
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdapServerTreeNode extends DefaultMutableTreeNode implements LdapTreeNode {

	private static Logger log = LoggerFactory.getLogger(LdapServerTreeNode.class);

	private boolean initialized = false;
	private LdapEntryTreeNode rootDSE;
	private LdapSearchTreeNode customSearches;
	private LdapNodeVirtualFile file;

	/**
	 * TreeNode representing single LdapServer
	 *
	 * @param ldapServer LdapServer to represent
	 */
	public LdapServerTreeNode(@NotNull LdapServer ldapServer) {
		super(ldapServer, true);
		this.rootDSE = new LdapEntryTreeNode(getLdapServer(), LdapNodeType.ROOT_DSE, Dn.ROOT_DSE, null);
		this.customSearches = new LdapSearchTreeNode(getLdapServer());
		this.file = new LdapNodeVirtualFile(this);
		init();
	}

	public LdapEntryTreeNode getRootDSE() {
		return rootDSE;
	}

	public LdapSearchTreeNode getCustomSearches() {
		return customSearches;
	}

	public void init() {
		if (!initialized) {
			removeAllChildren();
			add(getRootDSE());
			add(getCustomSearches());
			initialized = true;
		}
	}

	/**
	 * Get LdapServerConnection related to this TreeNode
	 *
	 * @return LdapServerConnection related to this TreeNode
	 */
	public @NotNull
	LdapServer getLdapServer() {
		return (LdapServer)this.getUserObject();
	}

	@NotNull
	@Override
	public String getTreeNodePresentation() {
		return toString();
	}

	@Nullable
	@Override
	public Icon getIcon() {

		if (LdapServer.LdapType.ACTIVE_DIRECTORY.equals(getLdapServer().getLdapType())) {
			return LdapIcons.LDAP_MSAD;
		} else if (LdapServer.LdapType.APACHE_DIRECTORY.equals(getLdapServer().getLdapType())) {
			return LdapIcons.LDAP_APACHE;
		} else {
			return LdapIcons.LDAP;
		}

	}

	@Nullable
	@Override
	public LdapNodeVirtualFile getVirtualFile() {
		return file;
	}

}
