package io.gitlab.zlamalp.intellijplugin.ldap.ui;

import com.intellij.LdapBundle;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.progress.PerformInBackgroundOption;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.testFramework.LightVirtualFile;
import com.intellij.ui.components.JBCheckBox;
import com.intellij.ui.components.JBRadioButton;
import com.intellij.ui.components.fields.IntegerField;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapUIUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.AttributeModelItem;
import io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeType;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.LdapSettings;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldif.LdifFileType;
import org.apache.directory.api.ldap.model.constants.SchemaConstants;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.Entry;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.api.ldap.model.ldif.LdifUtils;
import org.apache.directory.api.util.Base64;
import org.apache.directory.api.util.Strings;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

/**
 * Dialog for exporting Entry or its Attributes to LDIF where user can modify depth and preferred way of export.
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class ExportToLdifDialog extends DialogWrapper {

	private static Logger log = LoggerFactory.getLogger(ExportToLdifDialog.class);

	private JPanel wrapper;
	private JBRadioButton singleEntry;
	private JBRadioButton branchOneLevel;
	private JBRadioButton branchAll;
	private JBCheckBox sortAttributes;
	private JBCheckBox allowExportUserPassword;
	private IntegerField wrapLines;
	private JRadioButton attributeSingleValueRadioButton;
	private JRadioButton attributeAllValuesRadioButton;
	private LdapEntryTreeNode ldapNode;
	private boolean initialized = false;
	private Project project;
	private AttributeModelItem modelItem;


	public ExportToLdifDialog(Project project, boolean canBeParent, @NotNull LdapEntryTreeNode ldapNode, AttributeModelItem modelItem) {
		super(project, canBeParent);
		this.ldapNode = ldapNode;
		this.project = project;
		this.modelItem = modelItem;
		init();
	}

	private void initialize() {

		setTitle("Export to LDIF: " + ((Objects.equals(LdapNodeType.ROOT_DSE , ldapNode.getNodeType())) ? ("Root DSE ("+ldapNode.getLdapServer().getName()+")") : ldapNode.getDn()));

		setOKButtonText("Export");

		LdapSettings settings = LdapUtils.getPreferredLdapSettings(ldapNode.getLdapServer().getSettings());

		sortAttributes.setSelected(settings.isSortLdifAttributes());
		allowExportUserPassword.setSelected(settings.isAllowExportUserPassword());
		wrapLines.setValue(settings.getWrapLdifLines());

		if (!ldapNode.getAllowsChildren()) {
			branchOneLevel.setEnabled(false);
			branchAll.setEnabled(false);
		}

		if (modelItem == null) {
			attributeAllValuesRadioButton.setEnabled(false);
			attributeSingleValueRadioButton.setEnabled(false);
		} else {
			// prevent exporting empty file when exporting password is disabled and user selected user password to export
			allowExportUserPassword.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {

					if (!allowExportUserPassword.isSelected() && modelItem != null &&
							SchemaConstants.USER_PASSWORD_AT.equalsIgnoreCase(modelItem.getAttribute().getId())) {
						attributeAllValuesRadioButton.setEnabled(false);
						attributeSingleValueRadioButton.setEnabled(false);

						if (attributeAllValuesRadioButton.isSelected() || attributeSingleValueRadioButton.isSelected()) {
							singleEntry.setSelected(true);
						}

					} else {
						attributeAllValuesRadioButton.setEnabled(true);
						attributeSingleValueRadioButton.setEnabled(true);
					}

				}
			});
		}

		initialized = true;

	}

	@Nullable
	@Override
	protected JComponent createCenterPanel() {
		if (!initialized) {
			initialize();
		}
		return wrapper;
	}

	@Nullable
	@Override
	public JComponent getPreferredFocusedComponent() {
		return singleEntry;
	}

	@Override
	protected void doOKAction() {

		// FIXME - we shouldn't iterate over the tree but get as much at once as possible !!
		// FIXME - we should omit any limit while doing export
		// FIXME - allow export to file on disk instead of virtual file

		Task exportTask = new Task.Backgroundable(project,
				LdapBundle.message("ldap.update.modal.exportLdif"),
				true,
				PerformInBackgroundOption.ALWAYS_BACKGROUND) {

			LightVirtualFile finalFile = null;
			Exception exception = null;
			LdapConnection connection = null;

			@Override
			public void run(@NotNull ProgressIndicator indicator) {

				try {

					indicator.setIndeterminate(false);
					indicator.setText(LdapBundle.message("ldap.update.modal.connecting"));
					indicator.setFraction(0.1);
					indicator.checkCanceled();
					connection = ldapNode.getLdapServer().getConnection();

					String fileName = (Objects.equals(LdapNodeType.ROOT_DSE, ldapNode.getNodeType())) ? ("Root DSE (" + ldapNode.getLdapServer().getName() + ")") : ldapNode.getDn().toString();
					LightVirtualFile vf = null;

					int length = wrapLines.getValue();
					if (length < 0) length = 80;
					if (length == 0) length = 100000; // do not wrap

					if (modelItem != null && attributeSingleValueRadioButton.isSelected()) {
						try {
							// fake single value attribute
							Attribute clonedAttribute = modelItem.getAttribute().clone();
							clonedAttribute.clear();
							clonedAttribute.add(modelItem.getRawValue());
							vf = new LightVirtualFile(fileName, LdifFileType.INSTANCE, LdifUtils.convertToLdif(clonedAttribute, length));
						} catch (LdapException ex) {
							log.error("Error formatting attributes for output.", ex);
						}
					} else if (modelItem != null && attributeAllValuesRadioButton.isSelected()) {
						vf = new LightVirtualFile(fileName, LdifFileType.INSTANCE, LdifUtils.convertToLdif(modelItem.getAttribute(), length));
					} else if (singleEntry.isSelected() || (!ldapNode.getAllowsChildren())) {
						indicator.setText(LdapBundle.message("ldap.update.modal.exportingLdif", ldapNode.getDn()));
						indicator.setFraction(0.5);
						LdapUIUtils.waitIfUIDebug();
						indicator.checkCanceled();
						vf = new LightVirtualFile(fileName, LdifFileType.INSTANCE, convertToLdif(LdapUtils.getEntry(ldapNode, null), length, sortAttributes.isSelected(), allowExportUserPassword.isSelected()) + "\n");
					} else if (branchOneLevel.isSelected()) {
						indicator.setText(LdapBundle.message("ldap.update.modal.exportingLdif", ldapNode.getDn()));
						indicator.setFraction(0.4);
						LdapUIUtils.waitIfUIDebug();
						indicator.checkCanceled();
						vf = new LightVirtualFile(fileName, LdifFileType.INSTANCE, getSubTreeLDIF(ldapNode, true, length, connection, indicator));
					} else if (branchAll.isSelected()) {
						indicator.setText(LdapBundle.message("ldap.update.modal.exportingLdif", ldapNode.getDn()));
						indicator.setFraction(0.2);
						LdapUIUtils.waitIfUIDebug();
						indicator.checkCanceled();
						vf = new LightVirtualFile(fileName, LdifFileType.INSTANCE, getSubTreeLDIF(ldapNode, false, length, connection, indicator));
					}

					indicator.setFraction(1.0);
					indicator.checkCanceled();

					finalFile = vf;

				} catch (LdapException ex) {
					exception = ex;
				}

			}

			@Override
			public void onFinished() {

				// close connection even if user canceled operation
				if (connection != null) {
					try {
						ldapNode.getLdapServer().releaseConnection(connection);
					} catch (LdapException e) {
						LdapNotificationHandler.handleError(e, ldapNode.getLdapServer().getName(), "Can't release connection.");
					}
				}

			}

			@Override
			public void onSuccess() {

				if (exception != null) {
					LdapNotificationHandler.handleError(exception, ldapNode.getLdapServer().getName(), "Can't export data to LDIF'.");
				} else if (finalFile != null) {
					FileEditorManager.getInstance(project).openFile(finalFile, true, true);
					LdapUIUtils.showEditorNotificationPanelForExport(project, finalFile);
				}

			}

			/**
			 * Recursively print LdapTreeNode as LDIF including all its children.
			 *
			 * @param node Ldap entry to be printed
			 * @return printed LDIF
			 */
			private String getSubTreeLDIF(LdapEntryTreeNode node, boolean oneLevel, int length, LdapConnection connection, ProgressIndicator indicator) throws LdapException {

				List<LdapEntryTreeNode> children = new ArrayList<>();
				if (!node.getAllowsChildren() || node.getEntry() == null) {
					node.setEntry(LdapUtils.getEntry(node, connection));
				}
				if (node.getAllowsChildren()) {
					children.addAll(LdapUtils.getChildren(node,connection));
				}

				indicator.setText(LdapBundle.message("ldap.update.modal.exportingLdif", node.getDn()));
				indicator.checkCanceled();

				StringBuilder LDIF = new StringBuilder(convertToLdif(node.getEntry(),length, sortAttributes.isSelected(), allowExportUserPassword.isSelected()) + "\n");

				double fraction = 0.1;
				if (children.size() > 0) {
					 fraction = 1.0 / children.size();
				}

				for (LdapEntryTreeNode child : children) {
					indicator.checkCanceled();
					if (oneLevel) {
						// single-level export, just print children
						child.setEntry(LdapUtils.getEntry(child, connection)); // only to get all attributes
						indicator.setText(LdapBundle.message("ldap.update.modal.exportingLdif", child.getDn()));
						if (indicator.getFraction()<1.0) {
							indicator.setFraction(indicator.getFraction()+fraction);
						}
						LDIF.append(convertToLdif(child.getEntry(), length, sortAttributes.isSelected(), allowExportUserPassword.isSelected()) + "\n");
					} else {
						// depth multi-level export
						if (indicator.getFraction() < 1.0) {
							indicator.setFraction(indicator.getFraction()+fraction);
						}
						LDIF.append(getSubTreeLDIF(child, false, length, connection, indicator));
					}
				}
				indicator.checkCanceled();
				return LDIF.toString();

			}

		};

		// perform export
		ProgressManager.getInstance().run(exportTask);

		super.doOKAction();

	}

	/**
	 * Convert an Entry to LDIF with specified preferences
	 *
	 * @param entry the Entry to convert
	 * @param length the expected line length
	 * @param sorted TRUE to sort exported attributes alphabetically
	 * @param allowUserPassword TRUE to allow 'userPassword' in export
	 * @return the corresponding LDIF code as a String
	 */
	public String convertToLdif(Entry entry, int length, boolean sorted, boolean allowUserPassword) {

		if (entry == null) return "\n";

		StringBuilder sb = new StringBuilder();

		if (entry.getDn() != null) {
			// First, dump the Dn
			if (LdifUtils.isLDIFSafe(entry.getDn().getName())) {
				sb.append(LdifUtils.stripLineToNChars("dn: " + entry.getDn().getName(), length));
			} else {
				// force encoding using UTF-8 charset, as required in RFC2849 note 7
				sb.append(LdifUtils.stripLineToNChars("dn:: " + new String(Base64.encode(Strings.getBytesUtf8(entry.getDn().getName()))), length));
			}

			sb.append('\n');
		}

		// TODO - support EDITOR like sorting (objectClassFirst, operationalLast etc.)

		// Then all the attributes
		if (sorted) {

			List<Attribute> sortedList = new ArrayList<>(entry.getAttributes());
			sortedList.sort(new Comparator<Attribute>() {
				@Override
				public int compare(Attribute o1, Attribute o2) {
					return o1.getUpId().compareTo(o2.getUpId());
				}
			});

			for (Attribute attribute : sortedList) {
				if ((SchemaConstants.USER_PASSWORD_AT.equalsIgnoreCase(attribute.getUpId()) ||
						SchemaConstants.USER_PASSWORD_AT_OID.equals(attribute.getUpId())) && !allowUserPassword) continue;
				sb.append(LdifUtils.convertToLdif(attribute, length));
			}

		} else {
			for (Attribute attribute : entry) {
				if ((SchemaConstants.USER_PASSWORD_AT.equalsIgnoreCase(attribute.getUpId()) ||
						SchemaConstants.USER_PASSWORD_AT_OID.equals(attribute.getUpId())) && !allowUserPassword) continue;
				sb.append(LdifUtils.convertToLdif(attribute, length));
			}
		}

		return sb.toString();

	}

}
