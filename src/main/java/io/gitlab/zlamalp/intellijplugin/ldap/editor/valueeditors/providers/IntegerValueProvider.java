package io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers;

import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.AttributeValueEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.NumberValueEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.schema.SchemaUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import org.apache.directory.api.ldap.model.constants.SchemaConstants;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.Value;
import org.apache.directory.api.ldap.model.exception.LdapInvalidAttributeValueException;
import org.apache.directory.api.ldap.model.message.ResultCodeEnum;
import org.apache.directory.api.ldap.model.schema.AttributeType;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.IntegerSyntaxChecker;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;

/**
 * Basic Integer value editor for LDAP Entry attribute values.
 * Only HumanReadable (non-binary) attributes can be used with this editor.
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class IntegerValueProvider extends AbstractValueProvider {

	private static Logger log = LoggerFactory.getLogger(IntegerValueProvider.class);

	public IntegerValueProvider(@NotNull LdapEntryTreeNode node, @NotNull Attribute attribute, Value value) {

		super(node, attribute, value);

		if (!attribute.isHumanReadable() && (value != null && !value.isHumanReadable())) {
			throw new IllegalArgumentException(attribute.getUpId() + " is not HumanReadable. You can't use IntegerValueProvider to edit its value.");
		}

		AttributeType type = SchemaUtils.getAttributeType(node, attribute);
		if (type != null) {
			if (!SchemaConstants.INTEGER_SYNTAX.equals(type.getSyntaxOid())) {
				throw new IllegalArgumentException(attribute.getUpId() + " doesn't have INTEGER syntax. You can't use IntegerValueProvider to edit its value.");
			}
		}

	}

	@Override
	public Value constructNewValue(String stringValue) throws LdapInvalidAttributeValueException {

		Value value = null;
		AttributeType type = SchemaUtils.getAttributeType(node, attribute);
		if (attribute.getAttributeType() != null && type != null) {
			value = new Value(type, stringValue);
		} else {
			if (!IntegerSyntaxChecker.INSTANCE.isValidSyntax(stringValue)) {
				log.error("Can't set String: '{}' as a value of Attribute: '{}'", stringValue, attribute.getUpId());
				throw new LdapInvalidAttributeValueException(ResultCodeEnum.INVALID_ATTRIBUTE_SYNTAX, "Is not a number.");
			}
			value = new Value(stringValue);
		}

		return value;

	}

	@Override
	public AttributeValueEditor getValueEditor(Component component, boolean addingNewValue) {
		return new NumberValueEditor(component, this, addingNewValue);
	}

}
