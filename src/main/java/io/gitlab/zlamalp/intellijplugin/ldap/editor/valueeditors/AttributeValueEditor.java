package io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors;

import com.intellij.openapi.ui.DialogWrapperDialog;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers.AttributeValueProvider;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.Value;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

/**
 * Interface for all Attribute Values editors. It defines API to retrieve
 * old and new attribute values as well as attribute itself so we can work with them
 * once editor (dialog window) is closed.
 *
 * Implementation of editor is not expected to change Attribute or Entry. It is expected to be done
 * further in implementation, once dialog window is closed.
 *
 * @see DefaultValueEditor
 *
 * @author Pavel Zlámal
 */
public interface AttributeValueEditor extends DialogWrapperDialog {

	/**
	 * Return previous Attribute Value or NULL if there wasn't any
	 * (we were adding new value to attribute). It is expected to be
	 * a shorthand for retrieving it from AttributeValueProvider.
	 *
	 * @see AttributeValueProvider#getRawValue()
	 *
	 * @return Value or NULL for empty input.
	 */
	@Nullable
	Value getOldValue();

	/**
	 * Return new Attribute Value constructed from editor UI based on attribute type (value provider).
	 * Based on schema knowledge it might return NULL or empty Value on empty user input
	 * (like IA5String syntax allows empty string, but DirectoryString syntax doesn't).
	 *
	 * If new Value is NULL, further processing of change from editor is expect to remove
	 * old value from the entry (if old value is also not null).
	 *
	 * If new Value is NOT NULL, further processing is expected to remove old value and replace it with
	 * new value within the attribute.
	 *
	 * @return Value or NULL for empty input.
	 */
	Value getNewValue();

	/**
	 * Return Attribute we are editing with this editor.
	 *
	 * If editor is editing existing value, than Attribute is already present within Entry.
	 * If editor is adding new Value, Attribute might be present within Entry or we are about
	 * to add new Attribute with first value. In such case arbitrary Attribute object is returned
	 * based on used AttributeValueProvider.
	 *
	 * @return Attribute
	 */
	@NotNull
	Attribute getAttribute();

	// TODO - methods to provide inline editor component or define OK/Cancel Actions

	/**
	 * Returns most inner editor component used to edit attribute value. It is usually the one, which
	 * takes focus on editor opening.
	 *
	 * Editor component is expected to be suitable for inline table editing (aka TableCellEditor) purpose.
	 * If not, NULL should be returned !!
	 *
	 * @return Most inner editor component or NULL
	 */
	@Nullable
	JComponent getEditorComponent();

	boolean isPreferReplace();

}
