package io.gitlab.zlamalp.intellijplugin.ldap.tree;

import com.intellij.ide.navigationToolbar.NavBarPanel;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.actionSystem.DataProvider;
import com.intellij.openapi.project.Project;
import com.intellij.ui.treeStructure.Tree;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapServer;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapTreeNode;
import org.jetbrains.annotations.NotNull;
import javax.swing.tree.TreePath;
import java.awt.*;

/**
 * We override IntelliJ custom {@link Tree} in order to allow "busy" icon
 * to be active even when tree is not focused.
 *
 * Our component is also {@link DataProvider} in order to support navigation in {@link NavBarPanel}
 * and we support "file colors" feature, to draw custom background for each connection.
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdapTree extends Tree implements DataProvider {

	private Project project;

	public LdapTree(Project project, LdapTreeModel model) {
		super(model);
		this.project = project;
	}

	@Override
	protected boolean shouldShowBusyIconIfNeeded() {
		// we want to see busy icon on non-focused tree
		return true;
	}

	@Override
	public Object getData(@NotNull String dataId) {
		if (CommonDataKeys.PROJECT.is(dataId)) return project;
		// Support for "focused" NavBar update
		if (CommonDataKeys.VIRTUAL_FILE.is(dataId)) {
			if (getLastSelectedPathComponent() instanceof LdapTreeNode) {
				return ((LdapTreeNode) getLastSelectedPathComponent()).getVirtualFile();
			}
		}
		return null;
	}

	/*
	 * "COLOR" support for LDAP connection
	 */

	@Override
	public boolean isFileColorsEnabled() {
		return true;
	}

	@Override
	public Color getFileColorForPath(TreePath path) {
		Object component = path.getLastPathComponent();
		if (component instanceof LdapTreeNode) {
			LdapServer ldap = ((LdapTreeNode) component).getLdapServer();
			if (ldap.getColor().getName().equals("No color")) return null;
			return ldap.getColor().getJBColor();
		}
		return null;
	}

}
