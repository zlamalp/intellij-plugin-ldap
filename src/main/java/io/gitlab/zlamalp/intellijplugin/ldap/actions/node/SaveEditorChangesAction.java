package io.gitlab.zlamalp.intellijplugin.ldap.actions.node;

import com.intellij.LdapBundle;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapActionUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapUIUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.ui.LdapNotificationHandler;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.DefaultModification;
import org.apache.directory.api.ldap.model.entry.Entry;
import org.apache.directory.api.ldap.model.entry.Modification;
import org.apache.directory.api.ldap.model.entry.ModificationOperation;
import org.apache.directory.api.ldap.model.entry.Value;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.api.ldap.model.message.ModifyRequest;
import org.apache.directory.api.ldap.model.message.ModifyRequestImpl;
import org.apache.directory.api.ldap.model.message.ModifyResponse;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.apache.directory.ldap.client.api.LdapConnectionWrapper;
import org.apache.directory.ldap.client.api.LdapNetworkConnection;
import org.apache.directory.ldap.client.api.future.ModifyFuture;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Save all changes in {@link LdapEntryEditor} done to the {@link Entry} and update LDAP with it.
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class SaveEditorChangesAction extends AnAction {

	public static final String ID = "io.gitlab.zlamalp.intellijplugin.ldap.saveEditorChangesAction";
	private static Logger log = LoggerFactory.getLogger(DiscardEditorChangesAction.class);

	public SaveEditorChangesAction() {
	}

	public SaveEditorChangesAction(@Nls(capitalization = Nls.Capitalization.Title) @Nullable String text, @Nls(capitalization = Nls.Capitalization.Sentence) @Nullable String description, @Nullable Icon icon) {
		super(text, description, icon);
	}

	@Override
	public void actionPerformed(@NotNull AnActionEvent e) {
		if (isActionActive(e)) {
			LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
			if (editor != null) {

				// FIXME - we must support case, when entry or last state is null if this action is triggered from createEntry dialog
				Entry currentState = editor.getLdapNode().getEntry();
				Entry previousState = editor.getLastState();

				editor.setLoading(true, LdapBundle.message("ldap.editor.saving"));

				Task task = new Task.Modal(e.getProject(), LdapBundle.message("ldap.update.modal.modifyEntry", currentState.getDn()), true) {

					Exception exception = null;
					LdapNetworkConnection networkConnection = null;
					LdapConnection connection = null;
					ModifyResponse response = null;

					@Override
					public void run(@NotNull ProgressIndicator indicator) {

						List<Modification> modifications = new ArrayList<>();

						modifications.addAll(getAddModifications(previousState, currentState));
						modifications.addAll(getRemoveModifications(previousState, currentState));
						//modifications.addAll(getReplaceModifications(previousState, currentState));
						Modification[] mods = modifications.toArray(new Modification[0]);

						indicator.setIndeterminate(false);
						indicator.setFraction(0);

						try {
							if (mods.length > 0) {

								indicator.setText(LdapBundle.message("ldap.update.modal.connecting"));
								indicator.setFraction(0.1);
								LdapUIUtils.waitIfUIDebug();
								indicator.checkCanceled();

								connection = editor.getLdapNode().getLdapServer().getConnection();
								networkConnection  = (LdapNetworkConnection)((LdapConnectionWrapper)connection).wrapped();

								indicator.setText(LdapBundle.message("ldap.update.modal.modifyingEntry"));
								indicator.setFraction(0.5);
								LdapUIUtils.waitIfUIDebug();
								indicator.checkCanceled();

								ModifyRequest request = new ModifyRequestImpl();
								request.setName(currentState.getDn());
								for (Modification mod : mods) {
									request.addModification(mod);
								}
								ModifyFuture future = networkConnection.modifyAsync(request);

								while (!future.isCancelled() && !future.isDone()) {
									try {
										if (indicator.isCanceled()) {
											future.cancel(true);
										} else {
											response = future.get(1, TimeUnit.SECONDS);
											if (response == null) {
												indicator.setFraction(Math.min(indicator.getFraction()+0.1, 1.0));
											} else {
												indicator.setFraction(1.0);
											}
											LdapUIUtils.waitIfUIDebug();
										}

									} catch (InterruptedException ex) {
										log.error("Interrupted while waiting for modify response.", ex);
										exception = ex;
									}
								}

							}
						} catch (LdapException ex) {
							log.error("Can't modify Entry.", ex);
							exception = ex;
						}

					}

					@Override
					public void onSuccess() {

						if (exception != null) {

							// local model was not modified, so just show error
							LdapNotificationHandler.handleError(exception, editor.getLdapNode().getLdapServer().getName(), "Unable to modify Entry '" + editor.getLdapNode().getDn() + "'.");

						} else {

							// get current modified entry and set it to last state
							editor.setLastState(currentState);
							// refreshing model
							editor.getModel().refresh();

						}

					}

					@Override
					public void onFinished() {

						// close connection even if user canceled operation
						if (connection != null) {
							try {
								editor.getLdapNode().getLdapServer().releaseConnection(connection);
							} catch (LdapException e) {
								LdapNotificationHandler.handleError(e, editor.getLdapNode().getLdapServer().getName(), "Can't release connection.");
							}
						}

						editor.setLoading(false);

					}
				};

				ProgressManager.getInstance().run(task);

			}
		}
	}

	@Override
	public void update(@NotNull AnActionEvent e) {
		e.getPresentation().setEnabled(isActionActive(e));
	}

	private boolean isActionActive(AnActionEvent e) {
		if (!LdapActionUtils.isProjectActive(e)) return false;
		if (LdapActionUtils.isFromEditor(e)) {
			LdapEntryEditor editor= LdapActionUtils.getEntryEditor(e);
			if (editor != null && !editor.isLoading()) {
				if (editor.isLocallyReadOnly()) return false;
				return !editor.isDirectEdit() && editor.isModified();
			}
		}
		return false;
	}

	private List<Modification> getAddModifications(Entry previousState, Entry newState) {

		List<Modification> modifications = new ArrayList<>();

		for (Attribute attribute : newState) {
			if (!previousState.contains(attribute)) {
				// completely new attribute - add it including all current values
				modifications.add(new DefaultModification(ModificationOperation.ADD_ATTRIBUTE, attribute));
			} else {
				// TODO - check if attribute can be added/removed or must be REPLACED
				for (Value value : attribute) {
					// for all new values
					if (!previousState.contains(attribute.getUpId(), value)) {
						// if new value is not between old values => was added
						modifications.add(new DefaultModification(ModificationOperation.ADD_ATTRIBUTE, attribute.getUpId(), value));
					}
				}
			}
		}

		log.trace("ADD: "+modifications + " ["+modifications.size()+"]");
		return modifications;

	}

	private List<Modification> getRemoveModifications(Entry previousState, Entry newState) {

		List<Modification> modifications = new ArrayList<>();

		for (Attribute attribute : previousState) {
			if (!newState.contains(attribute)) {
				// completely removed attribute - do not pass values
				modifications.add(new DefaultModification(ModificationOperation.REMOVE_ATTRIBUTE, attribute.getUpId()));
			} else {
				// find added values
				// TODO - check if attribute can be added/removed or must be REPLACED
				for (Value value : attribute) {
					// previous attribute doesn't contain value
					if (!newState.contains(attribute.getUpId(), value)) {
						// remove all values from old attribute, if they are not present in new attribute
						modifications.add(new DefaultModification(ModificationOperation.REMOVE_ATTRIBUTE, attribute.getUpId(), value));
					}
				}
			}
		}

		log.trace("REMOVE: "+modifications + " ["+modifications.size()+"]");
		return modifications;

	}

}
