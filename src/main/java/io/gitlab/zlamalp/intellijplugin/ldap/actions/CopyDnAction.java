package io.gitlab.zlamalp.intellijplugin.ldap.actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapActionUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.LdapTreePanel;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeType;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;

/**
 * Copy DN of single / first selected LdapTreeNode (representing single LDAP entry) in LdapTreePanel.
 *
 * @see LdapEntryTreeNode
 * @see LdapTreePanel
 *
 * @author Pavel Zlámal
 */
public class CopyDnAction extends AnAction {

	public static final String ID = "io.gitlab.zlamalp.intellijplugin.ldap.copyDn";
	private static Logger log = LoggerFactory.getLogger(CopyDnAction.class);

	public CopyDnAction() {
	}

	public CopyDnAction(@Nls(capitalization = Nls.Capitalization.Title) @Nullable String text, @Nls(capitalization = Nls.Capitalization.Sentence) @Nullable String description, @Nullable Icon icon) {
		super(text, description, icon);
	}

	@Override
	public void actionPerformed(@NotNull AnActionEvent e) {

		// Cancel if attribute value can't be copied
		if (!isActionActive(e)) return;

		LdapTreePanel treePanel = LdapActionUtils.getLdapTreePanel(e);
		if (LdapActionUtils.isFromTree(e)) {

			LdapEntryTreeNode[] selectedTreeNodes = treePanel.getTree().getSelectedNodes(LdapEntryTreeNode.class,
					node -> node.getNodeType().equals(LdapNodeType.NODE) || node.getNodeType().equals(LdapNodeType.BASE));
			if (selectedTreeNodes.length > 0) {

				StringSelection stringSelection = new StringSelection(selectedTreeNodes[0].getDn().toString());
				Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
				clpbrd.setContents(stringSelection, null);

			}

		} else if (LdapActionUtils.isFromEditor(e)) {

			LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
			if (editor != null) {
				LdapEntryTreeNode node = editor.getLdapNode();

				StringSelection stringSelection = new StringSelection(node.getDn().toString());
				Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
				clpbrd.setContents(stringSelection, null);

			}

		}

	}

	@Override
	public void update(AnActionEvent e) {
		e.getPresentation().setEnabled(isActionActive(e));
	}

	/**
	 * Return TRUE if CopyDn action is allowed for selected LdapTreeNode.
	 *
	 * @param e Event triggering this
	 * @return TRUE if Action is active or FALSE
	 */
	private boolean isActionActive(AnActionEvent e) {

		if (!LdapActionUtils.isProjectActive(e)) return false;
		if (LdapActionUtils.isFromTree(e)) {
			LdapTreePanel treePanel = LdapActionUtils.getLdapTreePanel(e);
			LdapEntryTreeNode[] selectedTreeNodes = treePanel.getTree().getSelectedNodes(LdapEntryTreeNode.class,
					node -> node.getNodeType().equals(LdapNodeType.NODE) || node.getNodeType().equals(LdapNodeType.BASE));
			return selectedTreeNodes.length > 0;
		} else if (LdapActionUtils.isFromEditor(e)) {
			LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
			return editor != null && !editor.isLoading();
		}
		return false;

	}

}


