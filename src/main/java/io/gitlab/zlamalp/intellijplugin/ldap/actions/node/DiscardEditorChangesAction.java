package io.gitlab.zlamalp.intellijplugin.ldap.actions.node;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapActionUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditor;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;

/**
 * Discards all changes in {@link LdapEntryEditor} by refreshing it.
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class DiscardEditorChangesAction extends AnAction {

	public static final String ID = "io.gitlab.zlamalp.intellijplugin.ldap.discardEditorChangesAction";
	private static Logger log = LoggerFactory.getLogger(DiscardEditorChangesAction.class);

	public DiscardEditorChangesAction() {
	}

	public DiscardEditorChangesAction(@Nls(capitalization = Nls.Capitalization.Title) @Nullable String text, @Nls(capitalization = Nls.Capitalization.Sentence) @Nullable String description, @Nullable Icon icon) {
		super(text, description, icon);
	}

	@Override
	public void actionPerformed(@NotNull AnActionEvent e) {
		if (isActionActive(e)) {
			LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
			if (editor != null) {
				LdapUtils.refresh(editor.getLdapNode(), LdapUtils.RefreshType.ENTRY);
			}
		}
	}

	@Override
	public void update(@NotNull AnActionEvent e) {
		e.getPresentation().setEnabled(isActionActive(e));
	}

	private boolean isActionActive(AnActionEvent e) {
		if (!LdapActionUtils.isProjectActive(e)) return false;
		if (LdapActionUtils.isFromEditor(e)) {
			LdapEntryEditor editor= LdapActionUtils.getEntryEditor(e);
			if (editor != null && !editor.isLoading()) {
				if (editor.isLocallyReadOnly()) return false;
				return !editor.isDirectEdit() && editor.isModified();
			}
		}
		return false;
	}

}
