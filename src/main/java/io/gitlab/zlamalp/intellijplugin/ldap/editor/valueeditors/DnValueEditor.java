package io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors;

import com.intellij.ui.TextFieldWithAutoCompletion;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers.DnValueProvider;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.awt.*;

public class DnValueEditor extends DefaultValueEditor implements AttributeValueEditor {

	/**
	 * Create default Attribute value editor with specified action ADD / EDIT.
	 *
	 * @param parent Parent Component for modal dialog
	 * @param valueProvider Value provider for edited Attribute and its Value
	 * @param addingNewValue TRUE if dialog should add new value for same attribute instead of using the one from value provider
	 */
	public DnValueEditor(@NotNull Component parent, DnValueProvider valueProvider, boolean addingNewValue) {
		super(parent, valueProvider, addingNewValue);
		textEditor = TextFieldWithAutoCompletion.<String>create(valueProvider.getEntry().getEditor().getProject(), valueProvider.getEntry().getLdapServer().getKnownNodes(), false, "");
		oneLiner = true;
		init();
	}

	@Nullable
	@Override
	protected String getDimensionServiceKey() {
		return DnValueEditor.class.getCanonicalName();
	}

}
