package io.gitlab.zlamalp.intellijplugin.ldap.editor.struct;

import com.intellij.ide.structureView.StructureView;
import com.intellij.ide.structureView.StructureViewBuilder;
import com.intellij.ide.structureView.StructureViewFactory;
import com.intellij.ide.structureView.StructureViewModel;
import com.intellij.ide.structureView.TreeBasedStructureViewBuilder;
import com.intellij.openapi.Disposable;
import com.intellij.openapi.fileEditor.FileEditor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.Disposer;
import com.intellij.openapi.util.Key;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.AttributeTableModel;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeVirtualFile;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

/**
 * Structure view builder for LdapNodeVirtualFiles provided by factory LdapNodeStructureViewBuilderProvider.
 * Implementation is taken and modified from TreeBasedStructureViewBuilder.
 *
 * @see TreeBasedStructureViewBuilder
 * @see LdapNodeVirtualFile
 * @see LdapNodeStructureViewBuilderProvider
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdapNodeStructureViewBuilder implements StructureViewBuilder {

	public static final Key<TableModelListener> EDITOR_STRUCTUREVIEW_LISTENER_KEY = Key.create("EDITOR_STRUCTUREVIEW_LISTENER_KEY");
	public final LdapNodeVirtualFile file;
	public final Project project;

	public LdapNodeStructureViewBuilder(@NotNull LdapNodeVirtualFile file, @NotNull Project project) {
		this.file = file;
		this.project = project;
	}

	public boolean isRootNodeShown() {
		return true;
	}

	@NotNull
	public StructureViewModel createStructureViewModel(@Nullable FileEditor editor) {

		// create model from editor
		final LdapNodeStructureViewModel model = new LdapNodeStructureViewModel((editor instanceof LdapEntryEditor) ? (LdapEntryEditor) editor : null, file);

		// assign editor change listeners
		if (editor != null) {
			final AttributeTableModel tableModel = ((LdapEntryEditor)editor).getModel();
			final TableModelListener listener = new TableModelListener() {
				@Override
				public void tableChanged(TableModelEvent e) {
					// invalidate treeview model in case of editor change
					model.invalidate();
				}
			};
			TableModelListener oldListener = editor.getUserData(EDITOR_STRUCTUREVIEW_LISTENER_KEY);
			if (oldListener != null) {
				tableModel.removeTableModelListener(oldListener);
			}
			editor.putUserData(EDITOR_STRUCTUREVIEW_LISTENER_KEY, listener);
			tableModel.addTableModelListener(listener);
		}

		return model;
	}

	@NotNull
	public StructureView createStructureView(FileEditor fileEditor, @NotNull Project project) {
		if (project == null) {
			throw new IllegalArgumentException("Project can't be null");
		}

		final StructureViewModel model = this.createStructureViewModel(fileEditor);
		StructureView view = StructureViewFactory.getInstance(project).createStructureView(fileEditor, model, project, this.isRootNodeShown());
		Disposer.register(view, new Disposable() {
			public void dispose() {
				model.dispose();
			}
		});
		if (view == null) {
			throw new IllegalArgumentException("View can't be null");
		}

		return view;
	}

}
