package io.gitlab.zlamalp.intellijplugin.ldap.ui;

import com.intellij.icons.AllIcons;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogEarthquakeShaker;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.ValidationInfo;
import com.intellij.openapi.util.registry.Registry;
import com.intellij.openapi.wm.IdeFocusManager;
import com.intellij.ui.DocumentAdapter;
import com.intellij.ui.components.JBLabel;
import com.intellij.ui.components.JBTextField;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import org.apache.directory.api.ldap.model.exception.LdapInvalidDnException;
import org.apache.directory.api.ldap.model.name.Dn;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * UI dialog to move Entry within the LDAP tree.
 *
 * @see LdapEntryTreeNode
 * @see io.gitlab.zlamalp.intellijplugin.ldap.actions.MoveEntryAction
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class MoveEntryDialog extends DialogWrapper {

	private JPanel wrapper;
	private JBLabel currentDn;
	private JBLabel newDn;
	private JBTextField newParentField;
	private JButton parentButton;
	private LdapEntryTreeNode node;
	private boolean initialized = false;
	private static Logger log = LoggerFactory.getLogger(MoveEntryDialog.class);

	public MoveEntryDialog(@Nullable Project project, boolean canBeParent, @NotNull LdapEntryTreeNode node) {
		super(project, canBeParent);
		this.node = node;
		init();
	}

	@Nullable
	@Override
	protected JComponent createCenterPanel() {
		if (!initialized) {
			initialize();
		}
		return wrapper;
	}

	private void initialize() {

		setTitle("Move '"+node.getDn()+"'");

		parentButton.setIcon(AllIcons.Nodes.UpFolder);
		parentButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				try {

					Dn dn = new Dn(newParentField.getText());
					newParentField.setText(dn.getAncestorOf(dn.getRdn().toString()).toString());

				} catch (LdapInvalidDnException ex) {

					try {
						newParentField.setText(node.getDn().getAncestorOf(node.getRdn()).toString());
					} catch (LdapInvalidDnException exc) {
						// failed
					}

				}

			}
		});

		currentDn.setText(node.getDn().toString());
		newDn.setText(node.getDn().toString());

		try {
			Dn dn = new Dn(node.getDn().toString());
			newParentField.setText(dn.getAncestorOf(node.getRdn()).toString());
		} catch (LdapInvalidDnException ex) {
			LdapNotificationHandler.handleError(ex, "LDAP: "+node.getDn(), "Invalid DN passed to Rename Entry dialog.");
		}
		newParentField.setColumns(newParentField.getText().length()+3);

		setOKButtonText("Move");

		newParentField.getDocument().addDocumentListener(new DocumentAdapter() {
			@Override
			protected void textChanged(DocumentEvent documentEvent) {
				triggerValidation();
				newDn.setText(node.getRdn()+","+newParentField.getText());
			}
		});

	}

	@Nullable
	@Override
	public JComponent getPreferredFocusedComponent() {
		return newParentField;
	}

	@Nullable
	@Override
	protected ValidationInfo doValidate() {

		if (newParentField.getText() == null || newParentField.getText().isEmpty()) {
			return new ValidationInfo("Invalid DN format!", newParentField);
		}
		if (newParentField.getText().matches("(.*)=$")) {
			return new ValidationInfo("Invalid DN format!", newParentField);
		}

		try {
			Dn dn = new Dn(node.getRdn()+","+newParentField.getText());
		} catch (LdapInvalidDnException e) {
			return new ValidationInfo("Invalid DN format!", newParentField);
		}
		return super.doValidate();

	}

	private void triggerValidation() {

		List<ValidationInfo> infoList = doValidateAll();
		if (!infoList.isEmpty()) {
			ValidationInfo info = infoList.get(0);
			if (info.component != null && info.component.isVisible()) {
				IdeFocusManager.getInstance(null).requestFocus(info.component, true);
			}

			if (!Registry.is("ide.inplace.validation.tooltip")) {
				DialogEarthquakeShaker.shake(getPeer().getWindow());
			}

			startTrackingValidation();
			if(infoList.stream().anyMatch(info1 -> !info1.okEnabled)) return;
		}

	}

	public String getNewParentDn() {
		return newParentField.getText();
	}

}
