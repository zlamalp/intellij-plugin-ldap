package io.gitlab.zlamalp.intellijplugin.ldap.actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.project.Project;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapActionUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapServer;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.LdapTreePanel;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.schema.ui.SchemaBrowserDialog;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapTreeNode;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;

/**
 * Open schema browser for selected LDAP connection
 *
 * @see LdapTreePanel
 * @see LdapEntryTreeNode
 * @see LdapServer
 * @see io.gitlab.zlamalp.intellijplugin.ldap.schema.LdapSchema
 *
 * @author Pavel Zlámal
 */
public class BrowseSchemaAction extends AnAction {

	public static final String ID = "io.gitlab.zlamalp.intellijplugin.ldap.browseSchema";
	private static Logger log = LoggerFactory.getLogger(BrowseSchemaAction.class);

	public BrowseSchemaAction() {
	}

	public BrowseSchemaAction(@Nls(capitalization = Nls.Capitalization.Title) @Nullable String text, @Nls(capitalization = Nls.Capitalization.Sentence) @Nullable String description, @Nullable Icon icon) {
		super(text, description, icon);
	}

	@Override
	public void actionPerformed(@NotNull AnActionEvent e) {

		if (!isActionActive(e)) return;

		if (LdapActionUtils.isFromTree(e)) {

			LdapTreePanel treePanel = LdapActionUtils.getLdapTreePanel(e);
			LdapTreeNode[] selectedNodes = treePanel.getTree().getSelectedNodes(LdapTreeNode.class, null);
			if (selectedNodes.length > 0)
			performAction(e.getProject(), selectedNodes[0].getLdapServer(), treePanel);

		} else if (LdapActionUtils.isFromEditor(e)) {

			LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
			if (editor != null) {
				performAction(e.getProject(), editor.getLdapNode().getLdapServer(), editor.getComponent());
			}

		}

	}

	@Override
	public void update(AnActionEvent e) {
		e.getPresentation().setEnabled(isActionActive(e));
	}

	/**
	 * Return TRUE if BrowseSchema action should be active.
	 *
	 * @param e triggered event
	 * @return TRUE if action should be active
	 */
	private boolean isActionActive(AnActionEvent e) {

		if (!LdapActionUtils.isProjectActive(e)) return false;
		if (LdapActionUtils.isFromTree(e)) {
			LdapTreePanel treePanel = LdapActionUtils.getLdapTreePanel(e);
			LdapTreeNode[] selectedTreeNodes = treePanel.getTree().getSelectedNodes(LdapTreeNode.class, null);
			return selectedTreeNodes.length > 0;
		} else if (LdapActionUtils.isFromEditor(e)) {
			LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
			return editor != null && !editor.isLoading();
		}
		return false;

	}

	/**
	 * Perform BrowseSchemaAction action on connection.
	 *
	 * @param project Project
	 * @param ldapServer Connection to browse schema for
	 * @param parent Parent component for dialog
	 */
	private void performAction(@NotNull Project project, @NotNull LdapServer ldapServer, Component parent) {

		SchemaBrowserDialog dialog = new SchemaBrowserDialog(project, parent, ldapServer);
		//dialog.show();

	}

}
