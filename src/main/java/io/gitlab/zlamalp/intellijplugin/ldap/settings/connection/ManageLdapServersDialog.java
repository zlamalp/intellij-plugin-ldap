package io.gitlab.zlamalp.intellijplugin.ldap.settings.connection;

import com.intellij.CommonBundle;
import com.intellij.openapi.actionSystem.ActionManager;
import com.intellij.openapi.actionSystem.ActionToolbar;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonShortcuts;
import com.intellij.openapi.actionSystem.DefaultActionGroup;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.ValidationInfo;
import com.intellij.openapi.ui.VerticalFlowLayout;
import com.intellij.openapi.util.SystemInfo;
import com.intellij.openapi.util.registry.Registry;
import com.intellij.ui.CollectionListModel;
import com.intellij.ui.ColoredListCellRenderer;
import com.intellij.ui.JBColor;
import com.intellij.ui.OnePixelSplitter;
import com.intellij.ui.ScrollPaneFactory;
import com.intellij.ui.SimpleTextAttributes;
import com.intellij.ui.components.JBLabel;
import com.intellij.ui.components.JBList;
import com.intellij.ui.components.JBPanel;
import com.intellij.ui.components.JBPanelWithEmptyText;
import com.intellij.util.PlatformIcons;
import com.intellij.util.messages.MessageBus;
import com.intellij.util.ui.JBDimension;
import com.intellij.util.ui.UIUtil;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapIcons;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapServer;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.LdapTreePanel;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.intellij.ui.SimpleTextAttributes.STYLE_WAVED;

/**
 * Dialog for managing all {@link LdapServer}s settings stored within IDE and {@link Project}.
 *
 * It resemble standard IntelliJ IDEA config dialog with list of connections on the left and
 * form for managing connection properties on the right. Right panel is {@link LdapServerConfigTabbedPane}
 * and contains other sub-tabs with specific configuration.
 *
 * When dialog is submitted using OK or Apply action, then {@link LdapUtils#storeLdapServers(Project, List)}
 * is called to actually save new settings to permanent storage. It also fires change event to {@link MessageBus}
 * with {@link LdapUtils.LdapConnectionsListener#TOPIC} to notify expected listeners, like {@link LdapTreePanel}.
 *
 * TODO - implement form validation with update to displayed list item
 * TODO - allow arrow keys movement between displayed connection lists
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class ManageLdapServersDialog extends DialogWrapper implements ChangeListener {

	private static Logger log = LoggerFactory.getLogger(ManageLdapServersDialog.class);

	private JPanel panel1;
	private OnePixelSplitter onePixelSplitter;
	private JBPanelWithEmptyText rightPanel;
	private Action applyAction = new AbstractAction(CommonBundle.getApplyButtonText()){
		@Override
		public void actionPerformed(ActionEvent event) {
			doApplyAction();
		}
		@Override
		public boolean isEnabled() {
			return !modifiedConnections.isEmpty() && doValidateAll().isEmpty();
		}
	};
	private boolean initialized = false;
	private ManageLdapServersDialog parent = this;

	private JBList<LdapServer> projectConnectionList;
	private CollectionListModel<LdapServer> projectConnectionListModel;
	private JBList<LdapServer> ideConnectionList;
	private CollectionListModel<LdapServer> ideConnectionListModel;
	JBLabel ideLabel = new JBLabel("<html><b>Global LDAP servers</b></html>");
	JBLabel projectLabel = new JBLabel("<html><b>Project LDAP servers</b></html>");

	private Map<LdapServer, LdapServerConfigTabbedPane> connectionToFormMap = new HashMap<>();
	private Set<LdapServerConfigTabbedPane> modifiedConnections = new HashSet<>();

	private Project project;
	private LdapServer preSelectedLdapServer;
	private int selectedSubTabIndex = 0;

	private Comparator<LdapServer> listComparator = (o1, o2) -> {

		// sort by current name or stored name
		LdapServerConfigTabbedPane form1 = connectionToFormMap.get(o1);
		LdapServerConfigTabbedPane form2 = connectionToFormMap.get(o2);

		String name1 = (form1 != null) ? StringUtils.trimToEmpty(form1.getDisplayName()) : o1.getName();
		String name2 = (form2 != null) ? StringUtils.trimToEmpty(form2.getDisplayName()) : o2.getName();

		return name1.compareTo(name2);

	};

	private AnAction addAction = new AnAction("Add","Add new connection", LdapIcons.ADD) {
		@Override
		public void actionPerformed(@NotNull AnActionEvent e) {
			JBList<LdapServer> connectionList = resolveConnectionList(e);
			addNewConnection(connectionList, new LdapServer());
		}
	};

	private AnAction removeAction = new AnAction("Remove","Remove (delete)", LdapIcons.REMOVE) {

		@Override
		public void actionPerformed(@NotNull AnActionEvent e) {

			JBList<LdapServer> connectionList = resolveConnectionList(e);
			CollectionListModel<LdapServer> collectionListModel = (CollectionListModel<LdapServer>) connectionList.getModel();

			LdapServer selectedValue = connectionList.getSelectedValue();
			if (selectedValue != null) {

				int index = connectionList.getSelectedIndex();

				// remove both connection and form
				collectionListModel.remove(selectedValue);

				LdapServerConfigTabbedPane form = connectionToFormMap.get(selectedValue);
				if (form != null && form.isNew()) {
					// removed "new" connections are not "modified"
					modifiedConnections.remove(form);
				} else {
					// removed existing connection is "modified"
					modifiedConnections.add(form);
					selectedValue.setToBeRemoved(true);
				}
				connectionToFormMap.remove(selectedValue);

				// refresh selection
				int size = connectionList.getModel().getSize();
				// select connection above deleted or none
				connectionList.setSelectedIndex(Math.min(index, size - 1));
				connectionList.requestFocusInWindow();
				// (de)activate apply button
				updateApplyActionState();
				showHideLists();

			}
		}

		@Override
		public void update(AnActionEvent e) {
			e.getPresentation().setEnabled(projectConnectionList.getSelectedValue() != null || ideConnectionList.getSelectedValue() != null);
		}

	};

	private AnAction copyAction = new AnAction("Duplicate", "Duplicate (copy)", LdapIcons.COPY) {

		@Override
		public void actionPerformed(@NotNull AnActionEvent e) {

			JBList<LdapServer> connectionList = resolveConnectionList(e);

			LdapServer selectedConn = connectionList.getSelectedValue();
			if (selectedConn != null) {

				// copy connection from current form
				LdapServerConfigTabbedPane toBeCopiedForm = connectionToFormMap.get(selectedConn);

				LdapServer conn = new LdapServer();
				if (toBeCopiedForm != null) {

					// update new connection from existing form
					toBeCopiedForm.saveConnection(conn);
					conn.setName(conn.getName() + " (copy)");

				} else {

					// no previous form available -> copy raw data
					conn.setName(selectedConn.getName() + "(copy)");
					conn.setDescription(selectedConn.getDescription());
					conn.setGlobal(selectedConn.isGlobal());
					conn.setProtocol(selectedConn.getProtocol());
					conn.setHostname(selectedConn.getHostname());
					conn.setPort(selectedConn.getPort());
					conn.setBase(selectedConn.getBase());
					conn.setFilter(selectedConn.getFilter());
					conn.setReadOnly(selectedConn.isReadOnly());
					conn.setRequireAuthz(selectedConn.isRequireAuthz());
					conn.setUserName(selectedConn.getUserName());
					conn.setPassword(selectedConn.getPassword());
					conn.setRememberPassword(selectedConn.isRememberPassword());

				}

				addNewConnection(connectionList, conn);

			}

		}

		@Override
		public void update(AnActionEvent e) {
			e.getPresentation().setEnabled(projectConnectionList.getSelectedValue() != null || ideConnectionList.getSelectedValue() != null);
		}

	};

	private AnAction makeAction = new AnAction("Make Global","Make connection Global", PlatformIcons.IMPORT_ICON) {
		@Override
		public void actionPerformed(@NotNull AnActionEvent e) {

			if (projectConnectionList.getSelectedValue() != null) {
				// source from project -> move to IDE
				LdapServer selected = projectConnectionList.getSelectedValue();
				if (selected != null) {
					projectConnectionListModel.remove(selected);
					ideConnectionListModel.add(selected);
					ideConnectionListModel.sort(listComparator);
					ideConnectionList.setSelectedValue(selected, true);
					LdapServerConfigTabbedPane config = connectionToFormMap.get(selected);
					if (config != null) {
						config.setIsGlobal(true);
					}
					showHideLists();
				}
			} else if (ideConnectionList.getSelectedValue() != null) {
				// source from IDE -> move to project
				LdapServer selected = ideConnectionList.getSelectedValue();
				if (selected != null) {
					ideConnectionListModel.remove(selected);
					projectConnectionListModel.add(selected);
					projectConnectionListModel.sort(listComparator);
					projectConnectionList.setSelectedValue(selected, true);
					LdapServerConfigTabbedPane config = connectionToFormMap.get(selected);
					if (config != null) {
						config.setIsGlobal(false);
					}
					showHideLists();
				}
			}

		}
		@Override
		public void update(AnActionEvent e) {
			e.getPresentation().setEnabledAndVisible(projectConnectionList.getSelectedValue() != null || ideConnectionList.getSelectedValue() != null);
			if (projectConnectionList.getSelectedValue() != null) {
				e.getPresentation().setText("Make Global");
				// move to IDE
			} else if (ideConnectionList.getSelectedValue() != null) {
				// move to project
				e.getPresentation().setText("Move to Project");
			}
		}

	};

	/**
	 * Create Settings dialog for managing LDAP connections
	 *
	 * @param project associated project
	 * @param parentComponent optional parent component (tree panel or ldap/ldif editor is expected) !!
	 * @param preSelected preSelected connection
	 */
	public ManageLdapServersDialog(@NotNull Project project, @NotNull Component parentComponent, LdapServer preSelected) {
		// force IDE modality since we are managing also IDE wide ldap connections
		super(project, parentComponent, true, IdeModalityType.IDE);
		this.preSelectedLdapServer = preSelected;
		this.project = project;
		setTitle("LDAP Connections");
		init();
		initValidation();
	}

	private void initialize() {

		List<LdapServer> tmpServers = LdapUtils.getLdapServers(project);
		LdapUtils.loadLdapServerCredentials(tmpServers);
		tmpServers.sort(listComparator);

		projectConnectionListModel = new CollectionListModel<>(tmpServers.stream().filter(ldapServer -> !ldapServer.isGlobal()).collect(Collectors.toList()));
		ideConnectionListModel = new CollectionListModel<>(tmpServers.stream().filter(LdapServer::isGlobal).collect(Collectors.toList()));

		projectConnectionList = new JBList<>(projectConnectionListModel);
		ideConnectionList = new JBList<>(ideConnectionListModel);

		// create connection list toolbar
		ActionToolbar actionToolbar = ActionManager.getInstance().createActionToolbar("ldapConnectionSettingsActionToolbar",
				new DefaultActionGroup(addAction, removeAction, copyAction, makeAction), false);
		actionToolbar.setOrientation(JToolBar.HORIZONTAL);
		actionToolbar.getComponent().setVisible(true);
		actionToolbar.getComponent().setBackground(UIUtil.SIDE_PANEL_BACKGROUND);
		Box toolbarBox = Box.createHorizontalBox();
		toolbarBox.add(actionToolbar.getComponent());
		toolbarBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		toolbarBox.setBackground(UIUtil.SIDE_PANEL_BACKGROUND);

		// arrange project/global lists in top-down layout
		VerticalFlowLayout verticalLayout = new VerticalFlowLayout(VerticalFlowLayout.TOP, 0, 5, true, false);
		JPanel flowPanel = new JBPanel<>(verticalLayout);
		flowPanel.setOpaque(false);
		flowPanel.setBackground(UIUtil.SIDE_PANEL_BACKGROUND);

		ideLabel.setBorder(BorderFactory.createEmptyBorder(5,10,5,5));
		projectLabel.setBorder(BorderFactory.createEmptyBorder(5,10,5,5));

		flowPanel.add(ideLabel);
		flowPanel.add(ideConnectionList);
		flowPanel.add(projectLabel);
		flowPanel.add(projectConnectionList);

		// update lists visibility based on emptiness
		showHideLists();

		// scrollable connection list
		JScrollPane myScroller = ScrollPaneFactory.createScrollPane(flowPanel, true);
		myScroller.setViewportBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		myScroller.setWheelScrollingEnabled(true);

		if (!Registry.is("ide.scroll.background.auto")) {
			myScroller.setBackground(UIUtil.SIDE_PANEL_BACKGROUND);
			myScroller.getViewport().setBackground(UIUtil.SIDE_PANEL_BACKGROUND);
			myScroller.getVerticalScrollBar().setBackground(UIUtil.SIDE_PANEL_BACKGROUND);
		}

		// whole left component wrapper
		JComponent left = new JPanel(new BorderLayout());
		left.setPreferredSize(new JBDimension(200, 600));
		left.setMinimumSize(new JBDimension(200, 400));
		if (SystemInfo.isMac) {
			left.add(BorderLayout.PAGE_END, toolbarBox);
			left.add(BorderLayout.CENTER, myScroller);
		} else {
			left.add(BorderLayout.NORTH, toolbarBox);
			left.add(BorderLayout.CENTER, myScroller);
		}

		// init empty right component
		rightPanel = new JBPanelWithEmptyText();
		rightPanel.withEmptyText("No LDAP connection selected.");
		rightPanel.setPreferredSize(new JBDimension(600, 400));
		rightPanel.setMinimumSize(new JBDimension(600, 400));

		// put whole content together
		onePixelSplitter.setProportion(0.2f);
		onePixelSplitter.setAndLoadSplitterProportionKey("io.gitlab.zlamalp.intellijplugin.ldap.ManageLdapConnectionsDialogSplitterProportion");
		onePixelSplitter.setFirstComponent(left);
		onePixelSplitter.setSecondComponent(rightPanel);
		onePixelSplitter.setHonorComponentsMinimumSize(true);
		onePixelSplitter.setPreferredSize(new JBDimension(800, 600));
		onePixelSplitter.setMinimumSize(new JBDimension(800, 600));

		panel1.setPreferredSize(new JBDimension(800, 600));

		// init renderers and listeners
		initConnectionList(ideConnectionList);
		initConnectionList(projectConnectionList);

		// default "project" list empty state and action
		projectConnectionList.getEmptyText().appendText("No LDAP connections added.").appendSecondaryText("Add connection", SimpleTextAttributes.LINK_ATTRIBUTES, e -> {
			addNewConnection(projectConnectionList, new LdapServer());
		});

		// select first or use "pre-selected" connection
		if (projectConnectionListModel.contains(preSelectedLdapServer)) {
			projectConnectionList.setSelectedValue(preSelectedLdapServer, true);
		} else if (ideConnectionListModel.contains(preSelectedLdapServer)) {
			ideConnectionList.setSelectedValue(preSelectedLdapServer, true);
		} else if (preSelectedLdapServer != null) {
			// we initialized form with "add new connection"
			addNewConnection(projectConnectionList, preSelectedLdapServer);
		} else {
			// not pre-selected, pick first (we prefer global opposite to rest of logic, since its at the top)
			if (!ideConnectionList.isEmpty()) {
				ideConnectionList.setSelectedValue(ideConnectionListModel.getElementAt(0), true);
			} else if (!projectConnectionList.isEmpty()) {
				projectConnectionList.setSelectedValue(projectConnectionListModel.getElementAt(0), true);
			}
		}

		// keyboard shortcuts
		addAction.registerCustomShortcutSet(CommonShortcuts.getNew(), projectConnectionList);
		addAction.registerCustomShortcutSet(CommonShortcuts.getNew(), ideConnectionList);
		removeAction.registerCustomShortcutSet(CommonShortcuts.getDelete(), projectConnectionList);
		removeAction.registerCustomShortcutSet(CommonShortcuts.getDelete(), ideConnectionList);
		copyAction.registerCustomShortcutSet(CommonShortcuts.getDuplicate(), projectConnectionList);
		copyAction.registerCustomShortcutSet(CommonShortcuts.getDuplicate(), ideConnectionList);

		// default "apply" state is not-changed
		//applyAction.setEnabled(false);
		// (de)activate apply button
		updateApplyActionState();

		// FIXME - Prevents NullPointer, not sure it target component is right
		actionToolbar.setTargetComponent(left);

		initialized = true;

	}

	/**
	 * Initialize shared logic for LDAP server connections list
	 * like renderer or click handler.
	 *
	 * @param connectionList List to initialize
	 */
	private void initConnectionList(JBList<LdapServer> connectionList) {

		connectionList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		connectionList.setBackground(UIUtil.SIDE_PANEL_BACKGROUND);
		connectionList.setBorder(javax.swing.BorderFactory.createEmptyBorder());

		connectionList.setCellRenderer(new ColoredListCellRenderer<LdapServer>() {
			@Override
			protected void customizeCellRenderer(@NotNull JList<? extends LdapServer> list, LdapServer value, int index, boolean selected, boolean hasFocus) {

				if (LdapServer.LdapType.ACTIVE_DIRECTORY.equals(value.getLdapType())) {
					setIcon(LdapIcons.LDAP_MSAD);
				} else if (LdapServer.LdapType.APACHE_DIRECTORY.equals(value.getLdapType())) {
					setIcon(LdapIcons.LDAP_APACHE);
				} else {
					setIcon(LdapIcons.LDAP);
				}
				setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 5));

				LdapServerConfigTabbedPane form = connectionToFormMap.get(value);
				SimpleTextAttributes style = SimpleTextAttributes.REGULAR_ATTRIBUTES;
				if (form != null && !form.isValid()) {
					style = style.derive(STYLE_WAVED, null, null, JBColor.red);
				}

				if (form != null && form.isNew()) {
					style = style.derive(style.getStyle(), JBColor.GREEN.darker(), null,null);
				} else if (form != null && form.isModified(value)) {
					style = style.derive(style.getStyle(), JBColor.BLUE, null,null);
				}
				append((form != null) ? StringUtils.trimToEmpty(form.getDisplayName()) : value.toString(), style);

			}
		});

		connectionList.addListSelectionListener(listSelectionEvent -> {
			boolean adjust = listSelectionEvent.getValueIsAdjusting();
			if (!adjust) {
				JBList<LdapServer> list = (JBList<LdapServer>) listSelectionEvent.getSource();
				if (list.getSelectedValue() != null) {
					selectConnection(list.getSelectedValue());
					// clear selection in opposite list
					if (list == projectConnectionList) ideConnectionList.clearSelection();
					if (list == ideConnectionList) projectConnectionList.clearSelection();
				}
			}
		});

		connectionList.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (SwingUtilities.isRightMouseButton(e)) {
					int index = connectionList.locationToIndex(e.getPoint());
					if (index >= 0) {
						ActionManager.getInstance().createActionPopupMenu("connectionMenu",
								new DefaultActionGroup(addAction, removeAction, copyAction, makeAction))
								.getComponent().show(connectionList, e.getPoint().x, e.getPoint().y);
					}
				}
			}
		});

	}


	@NotNull
	@Override
	protected DialogStyle getStyle() {
		return DialogStyle.COMPACT;
	}

	@Nullable
	@Override
	protected JComponent createCenterPanel() {
		if (!initialized) {
			initialize();
		}
		return panel1;
	}

	/**
	 * Switch displayed dialog content based on selected LdapServerConnection.
	 *
	 * @param ldapServer LdapServerConnection used to display dialog content
	 */
	private void selectConnection(LdapServer ldapServer) {

		if (ldapServer != null) {

			// store once opened forms in a map
			LdapServerConfigTabbedPane form;
			if (!connectionToFormMap.containsKey(ldapServer)) {
				LdapServerConfigTabbedPane newForm = new LdapServerConfigTabbedPane(project, ldapServer, getDisposable());
				newForm.addChangeListener(this);
				connectionToFormMap.put(ldapServer, newForm);
				newForm.loadConnection(ldapServer);
			}
			form = connectionToFormMap.get(ldapServer);
			// keep same "sub-tab" opened when switching connections
			form.setSelectedSubTabIndex(selectedSubTabIndex);
			onePixelSplitter.setSecondComponent(form.getContent());

		} else {
			onePixelSplitter.setSecondComponent(rightPanel);
		}

	}

	@Override
	public void doCancelAction() {
		connectionToFormMap.values().forEach(form -> form.removeChangeListener(this));
		super.doCancelAction();
	}

	@Override
	protected void doOKAction() {
		doApplyAction();
		connectionToFormMap.values().forEach(form -> form.removeChangeListener(this));
		super.doOKAction();
	}

	/**
	 * Update connections with form data and save them co Project/Plugin settings state.
	 */
	public void doApplyAction() {

		List<LdapServer> modifiedServers = new ArrayList<>();
		for (LdapServerConfigTabbedPane form : modifiedConnections) {
			// save content of forms to actual connection
			if (form.isModified(form.getLdapServer())) {
				form.saveConnection(form.getLdapServer());
				form.setNew(false);
				form.showReset(false);
			}
			modifiedServers.add(form.getLdapServer());
		}

		LdapUtils.storeLdapServers(project, modifiedServers);
		modifiedConnections.clear();

		// (de)activate apply button
		updateApplyActionState();
		projectConnectionList.updateUI();
		ideConnectionList.updateUI();

	}

	@NotNull
	protected Action[] createActions() {

		ArrayList<Action> actions = new ArrayList<>();
		actions.add(getOKAction());
		actions.add(getCancelAction());

		Action apply = getApplyAction();
		if (apply != null) {
			actions.add(getApplyAction());
		}
		String topic = getHelpTopic();
		if (topic != null) {
			actions.add(getHelpAction());
		}
		return actions.toArray(new Action[0]);

	}

	public String getHelpTopic() {
		// TODO - implement
		return null;
	}

	public Action getApplyAction() {
		return this.applyAction;
	}

	@Override
	public void stateChanged(ChangeEvent e) {

		Object source = e.getSource();
		if (source instanceof LdapServerConfigTabbedPane) {
			LdapServerConfigTabbedPane form = (LdapServerConfigTabbedPane)source;
			selectedSubTabIndex = form.tabbedPane.getSelectedIndex();
			if (form.isModified(form.getLdapServer())) {
				// mark modified
				form.showReset(true);
				modifiedConnections.add(form);
			} else {
				// mark not modified
				modifiedConnections.remove(form);
				form.showReset(false);

				// we could have re-set global/project state -> move to proper list
				if (projectConnectionListModel.contains(form.getLdapServer()) && form.getLdapServer().isGlobal()) {
					projectConnectionListModel.remove(form.getLdapServer());
					ideConnectionListModel.add(form.getLdapServer());
					ideConnectionListModel.sort(listComparator);
					ideConnectionList.setSelectedValue(form.getLdapServer(), true);
				}
				if (ideConnectionListModel.contains(form.getLdapServer()) && !form.getLdapServer().isGlobal()) {
					ideConnectionListModel.remove(form.getLdapServer());
					projectConnectionListModel.add(form.getLdapServer());
					projectConnectionListModel.sort(listComparator);
					projectConnectionList.setSelectedValue(form.getLdapServer(), true);
				}

			}
			showHideLists();
			ideConnectionList.updateUI();
			projectConnectionList.updateUI();
			form.setShowResetToIDEdefaults();
			updateApplyActionState();
		}

	}

	/**
	 * Resolves sourcing LDAP server connection list from the ActionEvent. If there is none, we default to Project.
	 *
	 * @param event An event to resolve
	 * @return Sourcing LDAP connection list
	 */
	private JBList<LdapServer> resolveConnectionList(AnActionEvent event) {
		Component sourceComponent = PlatformDataKeys.CONTEXT_COMPONENT.getData(event.getDataContext());
		if (sourceComponent instanceof JBList) {
			return (JBList<LdapServer>)sourceComponent;
		} else {
			return projectConnectionList;
		}
	}

	/**
	 * Adds "new" LDAP connection to the passed list.
	 * Connection is expected to be completely new (new UUID).
	 *
	 * @param list List to add connection to
	 * @param conn LDAP connection to be added
	 */
	private void addNewConnection(JBList<LdapServer> list, LdapServer conn) {

		CollectionListModel<LdapServer> collectionListModel = (CollectionListModel<LdapServer>) list.getModel();
		// we are adding to global list
		if (collectionListModel == ideConnectionListModel) {
			conn.setGlobal(true);
		} else {
			conn.setGlobal(false);
		}
		LdapServerConfigTabbedPane form = new LdapServerConfigTabbedPane(project, conn, getDisposable());
		form.setNew(true);
		form.addChangeListener(parent);
		form.loadConnection(conn);
		connectionToFormMap.put(conn, form);
		modifiedConnections.add(form);
		collectionListModel.add(conn);
		collectionListModel.sort(listComparator);
		list.setSelectedValue(conn, true);
		updateApplyActionState();
		showHideLists();

	}

	/**
	 * Update enabled/disabled state of "apply" button based on present changes in settings
	 */
	private void updateApplyActionState() {
		// we must use it for UI to get updated (disregarding custom implementation of isEnabled() in AnAction itself)
		applyAction.setEnabled(!modifiedConnections.isEmpty());
	}

	/**
	 * Shows/hides list of LDAP connections based on their emptiness.
	 */
	private void showHideLists() {

		ideLabel.setVisible(!ideConnectionList.isEmpty());
		ideConnectionList.setVisible(!ideConnectionList.isEmpty());
		// default to project list visible, since we want "empty" state message to be visible.
		projectLabel.setVisible(!projectConnectionList.isEmpty() || ideConnectionList.isEmpty());
		projectConnectionList.setVisible(!projectConnectionList.isEmpty() || ideConnectionList.isEmpty());

	}

	@NotNull
	@Override
	protected List<ValidationInfo> doValidateAll() {

		// for all existing forms
		List<ValidationInfo> result = new ArrayList<>();
		for (LdapServerConfigTabbedPane form : connectionToFormMap.values()) {
			result.addAll(form.doValidateAll());
		}

		// disable submit button if validation fails
		for (ValidationInfo info : result) {
			if (!info.okEnabled) {
				getApplyAction().setEnabled(false);
				getOKAction().setEnabled(false);
				return result;
			}
		}

		return result;

	}

}
