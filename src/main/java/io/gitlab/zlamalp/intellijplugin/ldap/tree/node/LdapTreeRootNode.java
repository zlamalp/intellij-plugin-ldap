package io.gitlab.zlamalp.intellijplugin.ldap.tree.node;

import io.gitlab.zlamalp.intellijplugin.ldap.LdapIcons;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapServer;
import io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeVirtualFile;
import org.jetbrains.annotations.ApiStatus;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.util.Comparator;

public class LdapTreeRootNode extends DefaultMutableTreeNode implements LdapTreeNode {

	LdapNodeVirtualFile virtualFile;

	public LdapTreeRootNode() {
		super("LDAP");
		virtualFile = new LdapNodeVirtualFile(this);
	}

	@NotNull
	@Override
	public String getTreeNodePresentation() {
		return "LDAP";
	}

	@Override
	public Icon getIcon() {
		return LdapIcons.LDAP_TOOL_WINDOW;
	}

	@NotNull
	@Override
	public LdapServer getLdapServer() {
		return null;
	}

	@Nullable
	@Override
	public LdapNodeVirtualFile getVirtualFile() {
		return virtualFile;
	}

	@ApiStatus.Experimental
	public void sort(@NotNull Comparator comparator) {
		if (this.children != null) {
			this.children.sort(comparator);
		}
	}

}
