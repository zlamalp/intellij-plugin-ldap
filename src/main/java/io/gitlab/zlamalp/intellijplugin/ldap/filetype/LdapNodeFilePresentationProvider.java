package io.gitlab.zlamalp.intellijplugin.ldap.filetype;

import com.intellij.ide.FileIconProvider;
import com.intellij.ide.IconProvider;
import com.intellij.ide.navigationToolbar.NavBarPanel;
import com.intellij.openapi.fileEditor.impl.EditorTabTitleProvider;
import com.intellij.openapi.project.DumbAware;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.Iconable;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiDirectory;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapTreeNode;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

/**
 * Tells IDE which icon and tab title should be associated with our {@link LdapNodeVirtualFile}.
 * Actual icon/tab title value is taken from inner {@link LdapTreeNode} associated with the file.
 *
 * Presentation is displayed in our {@link LdapEntryEditor} and IDE {@link NavBarPanel}.
 *
 * @author Attila Majoros
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdapNodeFilePresentationProvider extends IconProvider implements FileIconProvider, EditorTabTitleProvider, DumbAware {

	@Nullable
	@Override
	public Icon getIcon(@NotNull VirtualFile virtualFile, @Iconable.IconFlags int i, @Nullable Project project) {
		if (virtualFile instanceof LdapNodeVirtualFile) {
			return ((LdapNodeVirtualFile) virtualFile).getIcon();
		}
		return null;
	}

	@Nullable
	@Override
	public String getEditorTabTitle(@NotNull Project project, @NotNull VirtualFile virtualFile) {
		if (virtualFile instanceof LdapNodeVirtualFile) {
			LdapTreeNode ldapEntryTreeNode = ((LdapNodeVirtualFile) virtualFile).getLdapTreeNode();
			return ldapEntryTreeNode.toString();
		}
		return null;
	}

	@Nullable
	@Override
	public Icon getIcon(@NotNull PsiElement element, int flags) {

		if (element instanceof PsiDirectory) {
			if (((PsiDirectory) element).getVirtualFile() instanceof LdapNodeVirtualFile) {
				return ((LdapNodeVirtualFile) ((PsiDirectory) element).getVirtualFile()).getIcon();
			}
		}
		if (element instanceof PsiFile) {
			if (((PsiFile) element).getVirtualFile() instanceof LdapNodeVirtualFile) {
				return ((LdapNodeVirtualFile) ((PsiFile) element).getVirtualFile()).getIcon();
			}
		}

		return null;
	}

}
