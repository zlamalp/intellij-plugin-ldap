package io.gitlab.zlamalp.intellijplugin.ldap.actions;

import com.intellij.LdapBundle;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapActionUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.LdapTreePanel;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapServerTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapSearchResultTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.ui.CustomSearchDialog;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;

/**
 * Start Custom LDAP search dialog and performs it
 *
 * @see LdapTreePanel
 * @see LdapServerTreeNode
 * @see LdapEntryTreeNode
 *
 * @author Pavel Zlámal
 */
public class SearchAction extends AnAction {

	public static final String ID = "io.gitlab.zlamalp.intellijplugin.ldap.search";
	private static Logger log = LoggerFactory.getLogger(SearchAction.class);

	public SearchAction() {
	}

	public SearchAction(@Nls(capitalization = Nls.Capitalization.Title) @Nullable String text, @Nls(capitalization = Nls.Capitalization.Sentence) @Nullable String description, @Nullable Icon icon) {
		super(text, description, icon);
	}

	@Override
	public void actionPerformed(@NotNull AnActionEvent e) {

		if (!isActionActive(e)) return;

		if (LdapActionUtils.isFromTree(e)) {

			LdapTreePanel treePanel = LdapActionUtils.getLdapTreePanel(e);
			LdapTreeNode[] selectedTreeNodes = treePanel.getTree().getSelectedNodes(LdapTreeNode.class, null);

			CustomSearchDialog dialog;
			if (selectedTreeNodes.length > 0) {
				if (selectedTreeNodes[0] instanceof LdapSearchResultTreeNode) {
					// edit existing search
					dialog = new CustomSearchDialog(e.getProject(), selectedTreeNodes[0].getLdapServer(), (LdapSearchResultTreeNode) selectedTreeNodes[0]);
				} else {
					// open new search
					dialog = new CustomSearchDialog(e.getProject(), selectedTreeNodes[0].getLdapServer(), selectedTreeNodes[0]);
				}
				dialog.show();
			}

		} else if (LdapActionUtils.isFromEditor(e)) {

			LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
			if (editor != null) {
				CustomSearchDialog dialog = new CustomSearchDialog(e.getProject(), editor.getLdapNode().getLdapServer(), editor.getLdapNode());
				dialog.show();
			}

		}

	}

	@Override
	public void update(AnActionEvent e) {
		e.getPresentation().setEnabled(isActionActive(e));

		if (LdapActionUtils.isFromTree(e)) {
			LdapTreePanel treePanel = LdapActionUtils.getLdapTreePanel(e);
			LdapTreeNode[] selectedTreeNodes = treePanel.getTree().getSelectedNodes(LdapTreeNode.class, null);
			if (selectedTreeNodes.length > 0 && selectedTreeNodes[0] instanceof LdapSearchResultTreeNode) {
				e.getPresentation().setText(LdapBundle.message("action.ldap.SearchAction.alternative.text"));
			}
		} else if (LdapActionUtils.isFromEditor(e)) {
			// We don't have editor for "search result" nodes
		}

	}

	/**
	 * Return TRUE if Search action is allowed for selected LdapNodes.
	 *
	 * @param e Event triggering this action
	 * @return TRUE if Action is active or FALSE
	 */
	private boolean isActionActive(AnActionEvent e) {

		if (!LdapActionUtils.isProjectActive(e)) return false;
		if (LdapActionUtils.isFromTree(e)) {
			LdapTreePanel treePanel = LdapActionUtils.getLdapTreePanel(e);
			LdapTreeNode[] selectedTreeNodes = treePanel.getTree().getSelectedNodes(LdapTreeNode.class, null);
			return selectedTreeNodes.length > 0;
		} else if (LdapActionUtils.isFromEditor(e)) {
			LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
			return editor != null && !editor.isLoading();
		}

		return false;

	}

}
