package io.gitlab.zlamalp.intellijplugin.ldap.actions;

import com.intellij.LdapBundle;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.progress.PerformInBackgroundOption;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.project.Project;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapActionUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapServer;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.LdapTreePanel;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.ui.LdapNotificationHandler;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Synchronize LDAP server schema into its local representation LdapServerConnection.
 *
 * @see LdapTreePanel
 * @see LdapEntryTreeNode
 * @see LdapServer
 * @see io.gitlab.zlamalp.intellijplugin.ldap.schema.LdapSchema
 *
 * @author Pavel Zlámal
 */
public class SynchronizeSchemaAction extends AnAction {

	public static final String ID = "io.gitlab.zlamalp.intellijplugin.ldap.synchronizeSchema";
	private static Logger log = LoggerFactory.getLogger(SynchronizeSchemaAction.class);

	public SynchronizeSchemaAction() {
	}

	public SynchronizeSchemaAction(@Nls(capitalization = Nls.Capitalization.Title) @Nullable String text, @Nls(capitalization = Nls.Capitalization.Sentence) @Nullable String description, @Nullable Icon icon) {
		super(text, description, icon);
	}

	@Override
	public void actionPerformed(@NotNull AnActionEvent e) {

		if (!isActionActive(e)) return;

		if (LdapActionUtils.isFromTree(e)) {

			LdapTreePanel treePanel = LdapActionUtils.getLdapTreePanel(e);
			LdapTreeNode[] selectedServerTreeNodes = treePanel.getTree().getSelectedNodes(LdapTreeNode.class, null);
			Set<LdapServer> foundConnections = new HashSet<>();
			for (LdapTreeNode selectedNode : selectedServerTreeNodes) {
				foundConnections.add(selectedNode.getLdapServer());
			}
			for (LdapServer conn : foundConnections) {
				performAction(e.getProject(), conn);
			}

		} else if (LdapActionUtils.isFromEditor(e)) {

			LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
			if (editor != null) {
				performAction(e.getProject(), editor.getLdapNode().getLdapServer());
			}

		}

	}

	@Override
	public void update(AnActionEvent e) {
		e.getPresentation().setEnabled(isActionActive(e));
	}

	/**
	 * Return TRUE if SynchronizeSchema action should be active.
	 *
	 * @param e triggered event
	 * @return TRUE if action should be active
	 */
	private boolean isActionActive(AnActionEvent e) {

		if (!LdapActionUtils.isProjectActive(e)) return false;
		if (LdapActionUtils.isFromTree(e)) {
			LdapTreePanel treePanel = LdapActionUtils.getLdapTreePanel(e);
			LdapTreeNode[] selectedTreeNodes = treePanel.getTree().getSelectedNodes(LdapTreeNode.class, null);
			return selectedTreeNodes.length > 0;
		} else if (LdapActionUtils.isFromEditor(e)) {
			LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
			return editor != null && !editor.isLoading();
		}
		return false;
	}

	/**
	 * Perform SynchronizeSchema action on connection.
	 *
	 * @param project Project
	 * @param ldap LDAP to synchronize schema for
	 */
	private void performAction(@NotNull Project project, @NotNull LdapServer ldap) {

		Task task = new Task.Backgroundable(project,
				LdapBundle.message("ldap.update.modal.synchronizeSchema", ldap.getName()),
				true,
				PerformInBackgroundOption.ALWAYS_BACKGROUND) {

			LdapConnection connection = null;
			Exception exception = null;

			@Override
			public void run(@NotNull ProgressIndicator indicator) {

				try {

					indicator.setIndeterminate(false);
					indicator.setText(LdapBundle.message("ldap.update.modal.connecting"));
					indicator.setFraction(0.1);
					indicator.checkCanceled();
					connection = ldap.getConnection();

					indicator.checkCanceled();
					indicator.setFraction(0.5);
					indicator.setText(LdapBundle.message("ldap.update.modal.synchronizingSchema"));

					log.info("Synchronizing schema for {}.", ldap.getName());
					ldap.synchronizeSchema(connection);
					indicator.setFraction(1.0);

				} catch (LdapException ex) {

					exception = ex;

				}

			}

			@Override
			public void onSuccess() {

				if (exception != null) {
					LdapNotificationHandler.handleError(exception, ldap.getName(), "Schema was not synchronized!");
				} else {
					LdapNotificationHandler.notify(ldap.getName(), "Schema was synchronized.");
				}

			}

			@Override
			public void onFinished() {

				// close connection even if user canceled operation
				if (connection != null) {
					try {
						ldap.releaseConnection(connection);
					} catch (LdapException e) {
						LdapNotificationHandler.handleError(e, ldap.getName(), "Can't release connection.");
					}
				}

			}
		};
		ProgressManager.getInstance().run(task);

	}

}
