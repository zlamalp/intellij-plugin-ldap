package io.gitlab.zlamalp.intellijplugin.ldap.ui;

import com.intellij.notification.Notification;
import com.intellij.notification.NotificationType;
import com.intellij.notification.Notifications;
import com.intellij.openapi.ui.Messages;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Handle Notifications and Exceptions from callbacks to LDAP server and display them as IDEA nice popup notification.
 *
 * @author Attila Majoros
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public final class LdapNotificationHandler {

	private static final String NOTIFICATION_GROUP = "LDAP notifications";

	private LdapNotificationHandler() {
		throw new UnsupportedOperationException();
	}

	/**
	 * Show information notification like "Schema was synchronized".
	 *
	 * @param title Title to show (usually LdapServerConnection name)
	 * @param message Message to show
	 */
	public static void notify(String title, String message) {
		Notifications.Bus.notify(new Notification(NOTIFICATION_GROUP, (title != null && ! title.trim().isEmpty()) ? "LDAP: " + title : "LDAP", message, NotificationType.INFORMATION));
	}

	/**
	 * Show error notification like "Entry not exists".
	 *
	 * @param e Exception thrown by LDAP connector
	 * @param message Message to show.
	 */
	public static void handleError(Exception e, String message) {
		handleError(e, null, message);
	}

	/**
	 * Show error notification like "Entry not exists" with custom title (usually LdapServerConnection name).
	 *
	 * @param e Exception thrown by LDAP connector
	 * @param title Custom title (usually LdapServerConnection name)
	 * @param message Message to show.
	 */
	public static void handleError(Exception e, String title , String message) {
		if (e == null) {

			Notifications.Bus.notify(new Notification(NOTIFICATION_GROUP, (title != null && ! title.trim().isEmpty()) ? "LDAP: " + title : "LDAP", message, NotificationType.ERROR));

		} else {

			StringWriter stringWriter = new StringWriter();
			PrintWriter writer = new PrintWriter(stringWriter);
			e.printStackTrace(writer);
			writer.flush();
			final String stackTrace = stringWriter.toString();
			writer.close();

			Notifications.Bus.notify(new Notification(NOTIFICATION_GROUP, (title != null && ! title.trim().isEmpty()) ? "LDAP: " + title : "LDAP Exception", message + " (<a href=\"#showException\">show exception</a>)", NotificationType.ERROR, (notification, hyperlinkEvent) -> {
				if ("#showException".equals(hyperlinkEvent.getDescription())) {
					Messages.showErrorDialog(stackTrace, "LDAP Exception");
				}
			}));

		}
	}

}
