package io.gitlab.zlamalp.intellijplugin.ldap.actions;

import com.intellij.openapi.actionSystem.ActionPlaces;
import com.intellij.openapi.actionSystem.AnActionEvent;

/**
 * Provider of all IDE and Plugin ActionGroup names. Its used by each Action to determine own source context.
 * We can then have single Action implementation with different behavior based on source context.
 *
 * @see AnActionEvent#getPlace()
 */
public abstract class LdapPluginActionPlaces extends ActionPlaces {

	public static final String LDAP_TREE_TOOLBAR = "ldapTreeToolbar";
	public static final String LDAP_TREE_POPUP = "ldapTreePopup";
	public static final String LDAP_EDITOR_TOOLBAR = "ldapEntryEditorToolbar";
	public static final String LDAP_EDITOR_POPUP = "ldapEntryEditorPopup";
	public static final String LDAP_KEYBOARD_SHORTCUT = "keyboard shortcut";

}
