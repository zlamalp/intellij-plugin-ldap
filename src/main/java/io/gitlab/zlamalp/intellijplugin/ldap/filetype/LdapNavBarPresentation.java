package io.gitlab.zlamalp.intellijplugin.ldap.filetype;

import com.intellij.ide.navigationToolbar.AbstractNavBarModelExtension;
import com.intellij.ide.navigationToolbar.NavBarPanel;
import com.intellij.openapi.actionSystem.DataKey;
import com.intellij.openapi.actionSystem.DataProvider;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiDirectory;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.impl.PsiManagerImpl;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.LdapTree;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

/**
 * We override how IDE {@link NavBarPanel} displays LDAP tree nodes/files, since its PSI centric only
 * and displays file names with extensions and also it overrides presentation defined in our
 * {@link LdapNodeFilePresentationProvider} for directories.
 *
 * It fixes many issues (including NavBar navigation) by returning our custom {@link LdapNodePsiDirectory} in
 * {@link #adjustElement(PsiElement)} method. Since by default {@link LdapNodeVirtualFile} for directory case
 * is returned as runtime class PsiJavaDirectoryImpl and we can't override its {@link PsiDirectory#navigate(boolean)}
 * method.
 *
 * NOTE: Sadly there is no way to define custom {@link PsiDirectory} presentation / implementation and associated it
 * with a {@link FileType} as it is for files (there is no extension point for that). It would make usage of
 * {@link #adjustElement(PsiElement)}} obsolete.
 *
 * NOTE: Leaf item displayed in the {@link NavBarPanel} is taken from events and their context, like opened {@link LdapEntryEditor}
 * or more generally currently focused {@link JComponent}. In our case {@link LdapTree}, which must implement {@link DataProvider}
 * interface and contain {@link Project} and either {@link VirtualFile} or {@link PsiFile} under expected {@link DataKey}s.
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdapNavBarPresentation extends AbstractNavBarModelExtension {

	@Nullable
	@Override
	public String getPresentableText(Object object) {
		if (object instanceof LdapNodePsiFile) {
			if (((PsiFile) object).getVirtualFile() instanceof LdapNodeVirtualFile) {
				return ((PsiFile) object).getVirtualFile().getPresentableName();
			}
		} else if (object instanceof PsiDirectory) {
			if (((PsiDirectory) object).getVirtualFile() instanceof LdapNodeVirtualFile) {
				return ((PsiDirectory) object).getVirtualFile().getPresentableName();
			}
		}
		return null;
	}

	@Nullable
	@Override
	public Icon getIcon(Object object) {
		if (object instanceof LdapNodePsiFile) {
			if (((PsiFile) object).getVirtualFile() instanceof LdapNodeVirtualFile) {
				return ((LdapNodeVirtualFile) ((PsiFile) object).getVirtualFile()).getIcon();
			}
		} else if (object instanceof PsiDirectory) {
			if (((PsiDirectory) object).getVirtualFile() instanceof LdapNodeVirtualFile) {
				return ((LdapNodeVirtualFile) ((PsiDirectory) object).getVirtualFile()).getIcon();
			}
		}
		return null;
	}

	@Nullable
	@Override
	public PsiElement adjustElement(PsiElement psiElement) {

		// Convert our generic PSI elements to custom LdapNodePsiDirectory in order to override its "navigate()" methods
		if (psiElement instanceof LdapNodePsiDirectory) {
			if (((PsiDirectory) psiElement).getVirtualFile() instanceof LdapNodeVirtualFile) {
				if (((PsiDirectory) psiElement).getVirtualFile().isDirectory()) {
					// keep our mocked PsiManager in order to fix navigation
					return psiElement;
				}
			}
		} else if (psiElement instanceof PsiDirectory) {
			if (((PsiDirectory) psiElement).getVirtualFile() instanceof LdapNodeVirtualFile) {
				if (((PsiDirectory) psiElement).getVirtualFile().isDirectory()) {
					// convert runtime PsiJavaDirectoryImpl to our LdapNodePsiDirectory
					// this is called first since PsiJavaDirectoryImpl is created from LdapNodeVirtualFile by IDE logic
					return new LdapNodePsiDirectory((PsiManagerImpl) psiElement.getManager(), ((PsiDirectory) psiElement).getVirtualFile());
				}
			}
		}
		return super.adjustElement(psiElement);
	}

	@Nullable
	@Override
	public String getPopupMenuGroup(@NotNull DataProvider provider) {
		// suppress IDE "standard file" popup menu
		// TODO - our custom popup menu group
		return "";
	}

}
