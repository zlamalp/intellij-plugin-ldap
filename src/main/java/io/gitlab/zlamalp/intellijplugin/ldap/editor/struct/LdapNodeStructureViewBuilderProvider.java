package io.gitlab.zlamalp.intellijplugin.ldap.editor.struct;

import com.intellij.ide.structureView.StructureViewBuilder;
import com.intellij.ide.structureView.StructureViewBuilderProvider;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeFileType;
import io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeVirtualFile;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Structure view builder provider for LdapNodeFileType
 *
 * @see LdapNodeFileType
 * @see StructureViewBuilderProvider
 * @see LdapNodeStructureViewBuilder
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdapNodeStructureViewBuilderProvider implements StructureViewBuilderProvider {

	@Nullable
	@Override
	public StructureViewBuilder getStructureViewBuilder(@NotNull FileType fileType, @NotNull VirtualFile file, @NotNull Project project) {

		if ( fileType instanceof LdapNodeFileType ) {
			return new LdapNodeStructureViewBuilder((LdapNodeVirtualFile) file, project);
		}
		return null;

	}

}
