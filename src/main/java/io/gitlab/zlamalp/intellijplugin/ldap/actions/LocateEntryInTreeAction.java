package io.gitlab.zlamalp.intellijplugin.ldap.actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.fileEditor.FileEditor;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.ui.tree.TreePathUtil;
import com.intellij.util.ui.tree.TreeUtil;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapActionUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.LdapTreePanel;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditor;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.tree.TreePath;

/**
 * Locate and select Entry (LdapEntryTreeNode) in the LdapTreePanel for the currently selected LdapEntryEditor.
 *
 * @see LdapTreePanel
 * @see LdapEntryEditor
 *
 * @author Pavel Zlámal
 */
public class LocateEntryInTreeAction extends AnAction {

	public static final String ID = "io.gitlab.zlamalp.intellijplugin.ldap.locate";
	private static Logger log = LoggerFactory.getLogger(LocateEntryInTreeAction.class);

	public LocateEntryInTreeAction() {
	}

	public LocateEntryInTreeAction(@Nls(capitalization = Nls.Capitalization.Title) @Nullable String text, @Nls(capitalization = Nls.Capitalization.Sentence) @Nullable String description, @Nullable Icon icon) {
		super(text, description, icon);
	}

	@Override
	public void actionPerformed(@NotNull AnActionEvent e) {

		if (!isActionActive(e)) return;

		FileEditor editor = FileEditorManager.getInstance(e.getProject()).getSelectedEditor();
		if (editor instanceof LdapEntryEditor) {
			TreePath path = TreePathUtil.toTreePath(((LdapEntryEditor) editor).getLdapNode());
			if (path != null) {
				LdapTreePanel treePanel = LdapActionUtils.getLdapTreePanel(e);
				TreeUtil.selectPath(treePanel.getTree(), path, true);
			}
		}

	}

	@Override
	public void update(AnActionEvent e) {
		e.getPresentation().setEnabled(isActionActive(e));
	}

	/**
	 * Return TRUE if LocateEntryInTree action is allowed for currently selected editor.
	 *
	 * @param e Event triggering this action
	 * @return TRUE if Action is active or FALSE
	 */
	private boolean isActionActive(AnActionEvent e) {

		if (!LdapActionUtils.isProjectActive(e)) return false;

		FileEditor editor = FileEditorManager.getInstance(e.getProject()).getSelectedEditor();
		return editor instanceof LdapEntryEditor && !((LdapEntryEditor)editor).isLoading();

	}

}
