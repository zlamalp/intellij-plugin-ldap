package io.gitlab.zlamalp.intellijplugin.ldap;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.ui.JBColor;
import com.intellij.util.xmlb.annotations.Transient;
import io.gitlab.zlamalp.intellijplugin.ldap.schema.LdapSchema;
import io.gitlab.zlamalp.intellijplugin.ldap.schema.SchemaUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.LdapSettings;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.schema.SchemaSettings;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.schema.SchemaSettingsListener;
import io.gitlab.zlamalp.intellijplugin.ldap.ui.LdapNotificationHandler;
import io.gitlab.zlamalp.intellijplugin.ldap.ui.components.ColorNamePair;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.apache.directory.api.ldap.model.entry.Entry;
import org.apache.directory.api.ldap.model.exception.LdapAuthenticationException;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.ldap.client.api.DefaultLdapConnectionFactory;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.apache.directory.ldap.client.api.LdapConnectionConfig;
import org.apache.directory.ldap.client.api.LdapConnectionPool;
import org.apache.directory.ldap.client.api.ValidatingPoolableLdapConnectionFactory;
import org.apache.mina.util.ConcurrentHashSet;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Serializable;
import java.time.Duration;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Configuration of single connection to LDAP server.
 *
 * This is storable object, associated with project, similar to DB connections in IDEA.
 *
 * @author Pavel Zlámal
 */
public class LdapServer implements Serializable {

	/**
	 * Types of LDAP (directory servers) supported by plugin.
	 * Using them allows us to apply specific fixes and behavior
	 */
	public enum LdapType {

		OPEN_LDAP("OpenLDAP"),
		ACTIVE_DIRECTORY("ActiveDirectory"),
		APACHE_DIRECTORY("ApacheDirectory"),
		OTHER("Other");

		private String name;
		private static final Map<String, LdapType> lookup = new HashMap<>();

		static {
			for (LdapType type : EnumSet.allOf(LdapType.class)) {
				lookup.put(type.name, type);
			}
		}

		LdapType(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}

		public static LdapType getLdapType(String name) {
			return lookup.get(name);
		}

	}

	private static transient Logger log = LoggerFactory.getLogger(LdapServer.class);

	private String uuid;
	private String name = "New LDAP connection";
	private String description;
	private String protocol = "ldap://";
	private String hostname = "localhost";
	private int port = 389;
	private String userName;
	@Transient private String password;
	private String base = "";
	private String filter;
	private boolean readOnly = false;
	private boolean requireAuthz = false;
	private boolean isGlobal = false;
	private boolean rememberPassword = false;
	private LdapServerSettings settings;
	private LdapType ldapType = LdapType.OPEN_LDAP;
	private ColorNamePair color = null;

	private transient LdapSchema schema; // schema is loaded from connection
	private transient AtomicBoolean connected = new AtomicBoolean(false);
	private transient LdapConnectionPool pool = null;
	private transient boolean toBeRemoved = false;

	// FIXME - Used for DN auto-completion, very raw, just proof of concept
	private transient ConcurrentHashSet<String> knownNodes = new ConcurrentHashSet<String>();
	public ConcurrentHashSet<String> getKnownNodes() {
		return this.knownNodes;
	}

	// FIXME - prevent concurrent connect() and disconnect() and getLdapServer(), what about releaseConnection()? It doesn't handle the case, when we hit pool max. limit.
	// FIXME   then we still wait and have lock taken!!
	/**
	 * This is an exclusive lock used to prevent two threads from modifying connection pool at once.
	 * All actions with pool are locked by this.
	 *
	 * @see #connect()
	 * @see #disconnect()
	 * @see #getConnection()
	 * @see #releaseConnection(LdapConnection)
	 */
	private transient Semaphore lock = new Semaphore(1, true);

	public LdapServer() {
		// generate some UUID on object creation
		uuid = Long.toHexString(Double.doubleToLongBits(Math.random()));

		ApplicationManager.getApplication().getMessageBus().connect().subscribe(SchemaSettingsListener.TOPIC, new SchemaSettingsListener() {
			@Override
			public void schemaSettingsChanged(SchemaSettings schemaSettings) {
				if (isConnected()) {
					try {
						synchronizeSchema(null);
					} catch (LdapException e) {
						log.error("Can't synchronize schema on schema preferences change!");
					}
				}
			}
		});

	}

	@NotNull
	public String getName() {
		return name;
	}

	public void setName(@NotNull String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Transient
	public String getPassword() {
		return password;
	}

	@Transient
	public void setPassword(String password) {
		this.password = password;
	}

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}

	public boolean isReadOnly() {
		return readOnly;
	}

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	@Transient
	public boolean isConnected() {
		return this.connected.get();
	}

	@Transient
	private void setConnected(boolean connected) {
		this.connected.set(connected);
	}

	public boolean isRequireAuthz() {
		return requireAuthz;
	}

	public void setRequireAuthz(boolean requireAuthz) {
		this.requireAuthz = requireAuthz;
	}

	@Transient
	public LdapSchema getSchema() {
		return schema;
	}

	@Transient
	public void setSchema(LdapSchema schema) {
		this.schema = schema;
		schema.setUuid(getUuid());
		SchemaUtils.updateCache(schema);
	}

	@NotNull
	public String getUuid() {
		return uuid;
	}

	public void setUuid(@NotNull String uuid) {
		this.uuid = uuid;
	}

	public boolean isGlobal() {
		return isGlobal;
	}

	public void setGlobal(boolean global) {
		isGlobal = global;
	}

	public boolean isRememberPassword() {
		return rememberPassword;
	}

	public void setRememberPassword(boolean rememberPassword) {
		this.rememberPassword = rememberPassword;
	}

	public LdapType getLdapType() {
		return ldapType;
	}

	public void setLdapType(LdapType ldapType) {
		this.ldapType = ldapType;
	}

	public LdapServerSettings getSettings() {
		return settings;
	}

	public void setSettings(LdapServerSettings settings) {
		this.settings = settings;
	}

	public ColorNamePair getColor() {
		if (this.color == null) return new ColorNamePair("No color", JBColor.WHITE);
		return this.color;
	}
	public void setColor(ColorNamePair color) {
		this.color = color;
	}

	@Transient
	public boolean isToBeRemoved() {
		return toBeRemoved;
	}

	@Transient
	public void setToBeRemoved(boolean toBeRemoved) {
		this.toBeRemoved = toBeRemoved;
	}

	/**
	 * Return LDAP server connection Url constructed from this connection properties
	 *
	 * @return URL constructed from protocol, hostname and port
	 */
	public String getUrl() {
		return protocol + hostname + ":" + port;
	}

	/**
	 * Get connected and authenticated LDAP connection to run queries on.
	 * If connection pool was not initialized by calling connect() then
	 * if does it first.
	 *
	 * @see #connect()
	 *
	 * Connections or IDE settings of "request timeout" is applied.
	 *
	 * @return LdapNetworkConnection to LDAP server
	 */
	@Transient
	public LdapConnection getConnection() throws LdapException {

		// if pool is not initialized -> try to reconnect
		if (!isConnected()) {
			if (!connect()) {
				throw new LdapException("Can't init connection pool on getting connection.");
			}
		}

		lock.acquireUninterruptibly();

		LdapSettings settings = LdapUtils.getPreferredLdapSettings(getSettings());

		try {

			LdapConnection conn = pool.getConnection();

			if (schema != null && schema.getSchemaManager() != null
					&& !settings.getSchemaSettings().getSchemaProvider().equals(SchemaSettings.SchemaProvider.NONE)
					&& settings.getSchemaSettings().isStrictSchemaValidation()) {
				// enforce validation on actual connection
				conn.setSchemaManager(schema.getSchemaManager());
			}

			lock.release();
			return conn;

		} catch (LdapAuthenticationException ex) {

			// try to re-connect, since AUTHZ failed
			if (LdapUIUtils.askForPasswordLoop(this, settings.getConnectionTimeout()*1000, ex.getResultCode().getMessage())) {
				return getConnection();
			}

			log.error("Unable to connect to LDAP: {}, {}", ex.getResultCode().getResultCode(), ex.getResultCode().getMessage(), ex);
			LdapNotificationHandler.handleError(ex, getName(), "Can't authenticate: " + ex.getResultCode().getMessage());

			// user canceled askForPasswordLoop
			throw ex;

		} catch (LdapException ex) {
			lock.release();
			disconnect();
			throw ex;
		}

	}

	@Transient
	public void releaseConnection(@NotNull LdapConnection connection) throws LdapException {

		// FIXME - should there be some lock too ?

		// pool is initialized and working
		if (isConnected() && pool != null && !pool.isClosed()) {
			pool.releaseConnection(connection);
			return;
		}

		// pool is not initialized, try to release passed connection manually
		if (connection.isConnected() && connection.isAuthenticated()) {
			try {
				connection.unBind();
				connection.close();
			} catch (IOException ex) {
				log.error("Unable to disconnect.", ex);
			}
		}

	}

	/**
	 * Initialize whole connection pool based on stored configuration.
	 * If previous pool exists, then its discarded and new pool is created.
	 *
	 * @return TRUE = pool exists and is connected / FALSE = pool was not initialized
	 */
	public boolean connect() {
		LdapSettings settings = LdapUtils.getPreferredLdapSettings(getSettings());
		return connect(settings.getConnectionTimeout()*1000);
	}

	/**
	 * Initialize whole connection pool based on stored configuration.
	 * If previous pool exists, then its discarded and new pool is created.
	 *
	 * @param timeout Timeout to wait for connection in milliseconds
	 *
	 * @return TRUE = pool exists and is connected / FALSE = pool was not initialized
	 */
	public boolean connect(int timeout) {
		try {
			return connect(timeout, false);
		} catch (LdapException e) {
			log.error("Connecting to LDAP thrown Exception while it shouldn't", e);
			return false;
		}
	}

	/**
	 * Initialize whole connection pool based on stored configuration.
	 * If previous pool exists, then its discarded and new pool is created.
	 *
	 * @param timeout Timeout to wait for connection in milliseconds
	 * @param throwException Whether to throw Exception outside to let caller know what went wrong.
	 *
	 * @return TRUE = pool exists and is connected / FALSE = pool was not initialized
	 */
	public boolean connect(int timeout, boolean throwException) throws LdapException {

		// kill previous pool
		if (isConnected()) {
			disconnect();
		}

		// actually load password for this LDAP server from the credentials store
		LdapUtils.loadLdapServerCredentials(Collections.singletonList(this));

		// if password is required but not provided, ask for it
		if (isRequireAuthz() && !rememberPassword && password == null) {
			return LdapUIUtils.askForPasswordLoop(this, timeout, null);
		}

		lock.acquireUninterruptibly();

		// initialize config
		LdapConnectionConfig config = new LdapConnectionConfig();
		config.setLdapHost(hostname);
		config.setLdapPort(port);
		if (isRequireAuthz()) {
			config.setName(userName);
			config.setCredentials(password);
		}
		config.setUseSsl(protocol.equalsIgnoreCase("ldaps://"));

		LdapSettings settings = LdapUtils.getPreferredLdapSettings(getSettings());

		DefaultLdapConnectionFactory factory = new DefaultLdapConnectionFactory(config);
		factory.setTimeOut(timeout);

		// pool configurations
		GenericObjectPoolConfig<?> poolConfig = new GenericObjectPoolConfig<>();
		poolConfig.setLifo(true);
		// TODO - we will revisit default
		poolConfig.setMaxTotal(settings.getMaxConnections()); // was 3 / default 8
		poolConfig.setMaxIdle(settings.getMinIdleConnections());  // was 2 / default 8
		poolConfig.setMinIdle(settings.getMinIdleConnections()); // was 0 / default 0??
		poolConfig.setMaxWait(Duration.ofMillis(-1L));
		poolConfig.setMinEvictableIdleTime(Duration.ofMillis(1000L * 60L * 30L));
		poolConfig.setNumTestsPerEvictionRun(3);
		poolConfig.setSoftMinEvictableIdleTime(Duration.ofMillis(-1L));
		poolConfig.setTestOnBorrow(false);
		poolConfig.setTestOnReturn(false);
		poolConfig.setTestWhileIdle(false);
		poolConfig.setTimeBetweenEvictionRuns(Duration.ofMillis(-1L));
		poolConfig.setBlockWhenExhausted(true);

		pool = new LdapConnectionPool(new ValidatingPoolableLdapConnectionFactory(factory), poolConfig);

		// sync schema using connection from the pool !!
		LdapConnection localConnection = null;
		try {

			localConnection = pool.getConnection();

			// synchronize schema on first connection
			if (schema == null) {
				synchronizeSchema(localConnection);
			}

			pool.releaseConnection(localConnection);

			// mark as connected
			setConnected(true);

		} catch (LdapAuthenticationException ex) {

			lock.release();
			if (throwException) throw ex;
			// restart the whole connect process and ask for password
			return LdapUIUtils.askForPasswordLoop(this, settings.getConnectionTimeout()*1000, ex.getResultCode().getMessage());

		} catch (LdapException ex) {


			lock.release();
			// failed to connect - report it
			disconnect();
			if (throwException) throw ex;
			return false;

		} finally {

			if (localConnection != null &&
					localConnection.isConnected() &&
					localConnection.isAuthenticated()) {
				try {
					localConnection.unBind();
					localConnection.close();
				} catch (IOException | LdapException ex) {
					log.error("Unable to disconnect.", ex);
				}
			}

		}

		lock.release();
		// successfully connected and schema synced
		return true;

	}

	/**
	 * Disconnect from LDAP by destroying the pool of connections.
	 * Connection state is updated in the tree.
	 */
	public void disconnect() {

		lock.acquireUninterruptibly();
		if (isConnected()) {
			pool.close();
			pool = null;
		}
		setConnected(false);
		lock.release();

	}

	/**
	 * Synchronize schema of LDAP server. Schema is synced from Server to local objects and stored within LdapServerConnection itself.
	 *
	 * GUI performs various UX helping actions based on known schema.
	 * If schema is not present, UX is plain.
	 *
	 * @param connection Connection to use for getting Schema for this LDAP
	 *
	 * @throws LdapException When synchronizing schema fails
	 */
	public void synchronizeSchema(LdapConnection connection) throws LdapException {
		Entry schemaContent = null;
		if (!Objects.equals(SchemaSettings.SchemaProvider.NONE, LdapUtils.getPreferredLdapSettings(getSettings()).getSchemaSettings().getSchemaProvider())) {
			schemaContent = LdapUtils.exportSchema(this, connection);
		}
		schema = new LdapSchema(getUuid(), SchemaUtils.buildSchemaManager(this, connection), schemaContent);
		SchemaUtils.updateCache(schema);
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		LdapServer that = (LdapServer) o;
		return Objects.equals(uuid, that.uuid);
	}

	@Override
	public int hashCode() {
		return Objects.hash(uuid);
	}

}
