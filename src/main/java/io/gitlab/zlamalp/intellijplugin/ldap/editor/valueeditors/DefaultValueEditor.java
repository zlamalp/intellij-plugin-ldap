package io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors;

import com.intellij.LdapBundle;
import com.intellij.ide.ui.laf.darcula.ui.DarculaEditorTextFieldBorder;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.editor.event.DocumentEvent;
import com.intellij.openapi.editor.event.DocumentListener;
import com.intellij.openapi.editor.ex.EditorEx;
import com.intellij.openapi.ui.ComponentValidator;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.ui.OptionAction;
import com.intellij.openapi.ui.ValidationInfo;
import com.intellij.ui.EditorSettingsProvider;
import com.intellij.ui.EditorTextField;
import com.intellij.ui.components.JBLabel;
import com.intellij.ui.components.JBPanel;
import com.intellij.util.ui.JBDimension;
import com.intellij.util.ui.StartupUiUtil;
import com.intellij.util.ui.UIUtil;
import com.intellij.util.ui.components.BorderLayoutPanel;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapIcons;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers.AttributeValueProvider;
import io.gitlab.zlamalp.intellijplugin.ldap.schema.SchemaUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.LdapSettings;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.schema.SchemaSettings;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.schema.SchemaSettingsListener;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import org.apache.commons.lang.StringUtils;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.Value;
import org.apache.directory.api.ldap.model.exception.LdapInvalidAttributeValueException;
import org.apache.directory.api.ldap.model.message.ResultCodeEnum;
import org.apache.directory.api.ldap.model.schema.AttributeType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * UI Editor for editing single Attribute Value. All references to edited attribute and its value
 * are taken from passed AttributeValueProvider. It also specifies whole Entry (LdapEntryTreeNode)
 * and connection to LDAP.
 *
 * This is very basic editor which provides single "text area like" component
 * in order to edit any type of attribute value. It can behave differently when we are editing existing value
 * or when we are adding new value.
 * If schema information is available and enabled within the plugin, it provides basic input validation.
 *
 * It supports both binary and string / human readable values. Binary values are converted to base64 string
 * for the purpose of editing and converted back to byte[] on save.
 *
 * You can get proper AttributeValueEditor instance for Attribute from within proper AttributeValueProvider
 * and you can get proper AttributeValueProvider for Attribute from LdapUtils#getLdapValueProvider().
 * It performs best guess based on provided attribute name/oid and available schema information.
 *
 * This class is expected to be extended and its methods overridden if attribute syntax requires more sophisticated
 * components to edit its value in use friendly manner.
 * You must override at least initUI() and getNewValue() in order to support custom UI and correct value retrieval.
 * Overriding #getComponentValidator() is not necessary, but might be useful if you wish to introduce custom
 * syntax/logic checks.
 *
 * It is NOT up to editor to actually modify any value within attribute/entry or update LDAP itself.
 * It is just UI and new/old value provider, so that the rest of logic can be done once editor is closed.
 *
 * @see Attribute
 * @see Value
 * @see LdapEntryTreeNode
 * @see AttributeValueProvider
 * @see io.gitlab.zlamalp.intellijplugin.ldap.LdapUtils#getLdapValueProvider(LdapEntryTreeNode,String)
 */
public class DefaultValueEditor extends DialogWrapper implements AttributeValueEditor {

	protected static Logger log = LoggerFactory.getLogger(AttributeValueEditor.class);

	protected AttributeValueProvider valueProvider;
	protected LdapSettings settings = null;
	protected boolean isAddingNewValue;
	protected boolean removeEmpty = true; 	// by default we remove empty values
	protected boolean oneLiner = false; // by default we have multiline values

	protected JBPanel<BorderLayoutPanel> content = new JBPanel<>(new BorderLayout());
	protected EditorTextField textEditor = new EditorTextField();
	protected ComponentValidator validator = null;
	protected JBLabel lengthLabel = new JBLabel();
	protected JBLabel valueLabel = new JBLabel(LdapBundle.message("ldap.editor.value.value.text"));
	protected Action option = new MyOptionAction(LdapBundle.message("ldap.editor.value.buttons.save.text"));
	private boolean preferReplace = false;

	protected boolean validationInitialized = false;

	/**
	 * Create new UI editor for editing attribute value.
	 *
	 * @param dialogParent parent component
	 * @param valueProvider value provider (specifies Entry, Attribute and which Value to edit)
	 */
	protected DefaultValueEditor(@NotNull Component dialogParent,
	                          @NotNull AttributeValueProvider valueProvider) {
		this(dialogParent, valueProvider, false);
	}

	/**
	 * Create new UI editor for editing attribute value.
	 *
	 * @param dialogParent parent component
	 * @param valueProvider value provider (contains Entry, Attribute and which Value to edit)
	 * @param isAddingNewValue Whether we are adding new value (or whole attribute) or editing existing value
	 */
	protected DefaultValueEditor(@NotNull Component dialogParent,
	                          @NotNull AttributeValueProvider valueProvider,
	                          boolean isAddingNewValue) {

		super(dialogParent, true);
		this.isAddingNewValue = isAddingNewValue;
		this.valueProvider = valueProvider;
		this.settings = LdapUtils.getPreferredLdapSettings(valueProvider.getEntry().getLdapServer().getSettings());

		ApplicationManager.getApplication().getMessageBus().connect().subscribe(SchemaSettingsListener.TOPIC, new SchemaSettingsListener() {
			@Override
			public void schemaSettingsChanged(SchemaSettings schemaSettings) {
				settings = LdapUtils.getPreferredLdapSettings(valueProvider.getEntry().getLdapServer().getSettings());
			}
		});

		setTitle(LdapBundle.message("ldap.editor.value.title.text"));
		setCancelButtonText(LdapBundle.message("ldap.editor.value.buttons.cancel.text"));

		log.info("Opening {} value in {}", valueProvider.getAttribute().getUpId(), getClass().getSimpleName());

	}

	@Override
	protected void init() {

		super.init();
		initUI();

		if (!isAddingNewValue) {
			initValidation();
			validationInitialized = true;
			// when editing existing value, then we expect values are same at start
			// FIXME - handle this when we pass modified value to editor
			option.setEnabled(false);
		}

		getRootPane().setDefaultButton(getButton(option));

	}

	@Override
	public Value getOldValue() {
		return this.valueProvider.getRawValue();
	}

	@Override
	public Value getNewValue() {
		if (StringUtils.isBlank(textEditor.getText()) && removeEmpty) {
			return null;
		}
		try {
			return this.valueProvider.constructNewValue(StringUtils.trimToEmpty(textEditor.getText()));
		} catch (LdapInvalidAttributeValueException e) {
			return null;
		}
	}

	@NotNull
	@Override
	public Attribute getAttribute() {
		return this.valueProvider.getAttribute();
	}

	@Override
	public boolean isPreferReplace() {
		return preferReplace;
	}

	/**
	 * Method which initialize any UI of this editor including input validation.
	 * It is expected to be overridden by more complex editor implementations.
	 */
	public void initUI() {

		// configure editor
		textEditor.setOneLineMode(oneLiner);
		textEditor.setMinimumSize(new JBDimension(270, oneLiner ? 34 : 55));
		textEditor.setMaximumSize(new JBDimension(600, -1));
		textEditor.setPreferredSize(new JBDimension(270, oneLiner ? 34 : 150));
		textEditor.addSettingsProvider(new EditorSettingsProvider() {
			@Override
			public void customizeSettings(EditorEx editor) {

				editor.getSettings().setUseSoftWraps(true);
				editor.setVerticalScrollbarVisible(true);
				editor.setEmbeddedIntoDialogWrapper(true);
				if (isAddingNewValue) {
					editor.setShowPlaceholderWhenFocused(true);
					editor.setPlaceholder(LdapBundle.message("ldap.editor.value.editor.text"));
				}

				// ensure borders like on single input fields
				if (StartupUiUtil.isUnderDarcula() || UIUtil.isUnderIntelliJLaF()) {
					editor.setBorder(new DarculaEditorTextFieldBorder(textEditor, editor));
				} else {
					editor.setBorder(BorderFactory.createCompoundBorder(UIUtil.getTextFieldBorder(), BorderFactory.createEmptyBorder(2, 2, 2, 2)));
				}

				// TODO - decide, whether we want to show line numbers ?
				// ensure borders like on single input fields but without whitespace around editor
				// editor.getSettings().setLineNumbersShown(true);
				// new DarculaEditorTextFieldBorder(textEditor, editor) {
				//						@Override
				//						public Insets getBorderInsets(Component c) {
				//							return new JBInsets(3, 3, 3, 3).asUIResource();
				//						}
				//					}
				// editor.setBorder(BorderFactory.createCompoundBorder(UIUtil.getTextFieldBorder(), BorderFactory.createEmptyBorder(3, 3, 3, 3)));

			}
		});

		// fill initial value
		if (!isAddingNewValue) {
			textEditor.setText(valueProvider.getStringValue());
		}

		// input validation
		getComponentValidator().installOn(textEditor);
		textEditor.addDocumentListener(new DocumentListener() {
			@Override
			public void documentChanged(@NotNull DocumentEvent event) {
				if (!validationInitialized) {
					// when we are adding new value, start first validation on typing
					initValidation();
					validationInitialized = true;
				} else {
					// we edit current value
					doValidateAll();
				}
				setLength(event.getDocument().getTextLength());
			}
		});

		// init length widget
		lengthLabel.setFont(UIUtil.getLabelFont(UIUtil.FontSize.SMALL));
		lengthLabel.setForeground(UIUtil.getContextHelpForeground());
		setLength(textEditor.getText().length());

		// create UI
		content.add(valueLabel, BorderLayout.NORTH);
		content.add(textEditor, BorderLayout.CENTER);
		JBPanel<BorderLayoutPanel> box = new JBPanel<>(new BorderLayout());
		box.add(lengthLabel, BorderLayout.EAST);
		content.add(box, BorderLayout.SOUTH);

	}

	/**
	 * Return validator instance used in this editor.
	 *
	 * Validator provides explanatory decorations for editor it is installed on by {@see #initUI} implementation.
	 * It also prevents editor new value submission on invalid input (disable buttons in {@see #doValidateAll} implementation).
	 *
	 * Default implementation supports both schema validation modes.
	 *
	 * 1. No schema provider
	 * - User is only warned about possible problems with empty string values (most ldap syntaxes doesn't allows them).
	 * - On such value submission user is asked to pick action (remove value or set it empty).
	 * TODO - what about submission of "new" empty value ??
	 *
	 * 2. Schema provider
	 * - Input is checked on arbitrary value created with editor value and attribute type to properly check syntax
	 *   (even if strict validation is off).
	 * - When editing current value, empty new value means current value removal (user is warned).
	 * - Empty new value is not allowed, when attribute is MUST and we are editing its only/last value.
	 * - Empty new value is not allowed, when editor is created with isAddingNewValue==TRUE
	 *
	 * @return validator used in editor component
	 */
	@NotNull
	public ComponentValidator getComponentValidator() {

		if (this.validator != null) {
			return this.validator;
		}

		this.validator = new ComponentValidator(getDisposable()).withValidator(new Supplier<ValidationInfo>() {
			@Override
			public ValidationInfo get() {

				EditorTextField editor = (EditorTextField)getEditorComponent();
				if (editor == null) {
					return null;
				}

				SchemaSettings.SchemaProvider schemaProvider = settings.getSchemaSettings().getSchemaProvider();

				if (Objects.equals(SchemaSettings.SchemaProvider.NONE, schemaProvider)) {

					// no schema validation, just basic (not)empty state
					if (StringUtils.isEmpty(textEditor.getText())) {
						if (isAddingNewValue) {
							return new ValidationInfo(LdapBundle.message("ldap.editor.value.validator.cantBeEmpty.text"), textEditor);
						} else {
							return new ValidationInfo(LdapBundle.message("ldap.editor.value.validator.isEmptyNoSchema.text"), textEditor).asWarning().withOKEnabled();
						}
					} else {
						return null;
					}

				} else {

					// use schema validation
					LdapInvalidAttributeValueException wrongSyntax = null;
					long maxLength = 0;
					AttributeType type = SchemaUtils.getAttributeType(valueProvider.getEntry(), valueProvider.getAttribute());
					try {
						if (type != null) {
							maxLength = type.getSyntaxLength();
							if (!type.isHR() || LdapUtils.binaryAttributeDetector.isBinary(getAttribute().getUpId())) {
								// binary value
								new Value(type, valueProvider.convertToBytes(editor.getText()));
							} else {
								// HR / String value
								new Value(type, editor.getText());
							}
						}
					} catch (LdapInvalidAttributeValueException e) {
						wrongSyntax = e;
					} catch (IllegalArgumentException ex) {
						// thrown when value can't be normalized
						wrongSyntax = new LdapInvalidAttributeValueException(ResultCodeEnum.INVALID_ATTRIBUTE_SYNTAX, ex.getMessage());
					}

					if (isAddingNewValue) {

						if (StringUtils.isBlank(textEditor.getText())) {
							// empty input is never allowed on adding
							return new ValidationInfo(LdapBundle.message("ldap.editor.value.validator.cantBeEmpty.text"), textEditor);
						} else {
							// length limit might not be checked by syntax -> manual re-check
							if (maxLength > 0 && textEditor.getText().length() > maxLength) {
								return new ValidationInfo(LdapBundle.message("ldap.editor.value.validator.toLong.text", maxLength), textEditor);
							}
							// wrong value syntax
							if (wrongSyntax != null) {
								if (type.getSyntax() != null) {
									return new ValidationInfo(LdapBundle.message("ldap.editor.value.validator.invalidSyntaxExpected.text", type.getSyntax().getDescription()), textEditor);
								} else {
									return new ValidationInfo(LdapBundle.message("ldap.editor.value.validator.invalidSyntax.text", textEditor));
								}
							}
							return null;
						}

					} else {

						// case editing existing value
						if ((SchemaUtils.isMustAttribute(valueProvider.getEntry(), valueProvider.getAttribute().getId()) && valueProvider.getAttribute().size() <= 1)
								&& StringUtils.isBlank(textEditor.getText())) {
							return new ValidationInfo(LdapBundle.message("ldap.editor.value.validator.cantBeEmptyRequired.text", valueProvider.getAttribute().getUpId()), textEditor);
						}

						// length limit might not be checked by syntax -> manual re-check
						if (maxLength > 0 && textEditor.getText().length() > maxLength) {
							return new ValidationInfo(LdapBundle.message("ldap.editor.value.validator.toLong.text", maxLength), textEditor);
						}

						// wrong value syntax
						if (wrongSyntax != null) {
							if (StringUtils.isNotBlank(textEditor.getText())) {
								if (type.getSyntax() != null) {
									return new ValidationInfo(LdapBundle.message("ldap.editor.value.validator.invalidSyntaxExpected.text", type.getSyntax().getDescription()), textEditor);
								} else {
									return new ValidationInfo(LdapBundle.message("ldap.editor.value.validator.invalidSyntax.text", textEditor));
								}
							} else {
								// empty not allowed, will be removed
								return new ValidationInfo(LdapBundle.message("ldap.editor.value.validator.isEmpty.text"), textEditor).asWarning().withOKEnabled();
							}
						}
						return null;

					}

				}

			}
		});

		return this.validator;

	}


	/**
	 * Sets current editor value length to displayed length-label
	 *
	 * @param length current length
	 */
	private void setLength(int length) {
		AttributeType type = SchemaUtils.getAttributeType(valueProvider.getEntry(), valueProvider.getAttribute());
		if (type != null && type.getSyntaxLength() > 0) {
			lengthLabel.setText(LdapBundle.message("ldap.editor.value.length.text", length, type.getSyntaxLength()));
		} else {
			lengthLabel.setText(LdapBundle.message("ldap.editor.value.lengthUnlimited.text", length));
		}
	}

	/**
	 * Decide, whether editor should remove edited value on empty input or submit it as empty value.
	 * If no schema provider is set, then we ask user for decision.
	 *
	 * @return 0 = remove / 1 = submit empty / 2 = user canceled the request
	 */
	private int askWhatOnEmpty() {

		boolean noSchemaValidation = Objects.equals(SchemaSettings.SchemaProvider.NONE, settings.getSchemaSettings().getSchemaProvider());

		int result = removeEmpty ? Messages.OK : Messages.NO; // REMOVE by DEFAULT
		if (noSchemaValidation && StringUtils.isBlank(textEditor.getText())) {

			result = Messages.showYesNoCancelDialog(textEditor,
					LdapBundle.message("ldap.editor.value.askRemove.message.text" ),
					LdapBundle.message("ldap.editor.value.askRemove.title.text"),
					LdapBundle.message("ldap.editor.value.askRemove.button.remove.text"),
					LdapBundle.message("ldap.editor.value.askRemove.button.setEmpty.text"),
					LdapBundle.message("ldap.editor.value.askRemove.button.cancel.text"), LdapIcons.REMOVE);
			if (result == Messages.OK) {
				removeEmpty = true;
				return result;
			} else if (result == Messages.NO) {
				removeEmpty = false;
				return result;
			} else {
				return result;
			}

		}
		return result;

	}

	/**
	 * Our implementation of dialogs OK/Submit action.
	 * It validates input before starting dialog OK action.
	 * User might pick his preference, whether to use REPLACE or ADD/REMOVE for value update operation in LDAP.
	 */
	private class MyOptionAction extends DialogWrapperAction implements OptionAction {

		/**
		 * The constructor
		 *
		 * @param name the action name (see {@link Action#NAME})
		 */
		protected MyOptionAction(@NotNull String name) {
			super(name);
		}

		@NotNull
		@Override
		public Action[] getOptions() {
			return new Action[]{new DialogWrapperAction(LdapBundle.message("ldap.editor.value.buttons.save.opt1.text")) {
				@Override
				protected void doAction(ActionEvent e) {
					doValidateAll();
					if (option.isEnabled()) {
						if (askWhatOnEmpty() == Messages.CANCEL) return;
						preferReplace = false;
						doOKAction();
					}
				}
			}, new DialogWrapperAction(LdapBundle.message("ldap.editor.value.buttons.save.opt2.text")) {
				@Override
				protected void doAction(ActionEvent e) {
					doValidateAll();
					if (option.isEnabled()) {
						if (askWhatOnEmpty() == Messages.CANCEL) return;
						preferReplace = true;
						doOKAction();
					}
				}
			}};
		}

		@Override
		protected void doAction(ActionEvent e) {
			doValidateAll();
			if (option.isEnabled()) {
				if (askWhatOnEmpty() == Messages.CANCEL) return;
				doOKAction();
			}
		}

	}

	// Default implementation of DialogWrapper methods

	@Nullable
	@Override
	protected String getDimensionServiceKey() {
		return this.getClass().getCanonicalName();
	}

	@Override
	public DialogWrapper getDialogWrapper() {
		return this;
	}

	@Nullable
	@Override
	protected JComponent createCenterPanel() {
		return content;
	}

	@NotNull
	@Override
	protected List<ValidationInfo> doValidateAll() {

		// gather validation results
		List<ValidationInfo> result = new ArrayList<>();
		ComponentValidator.getInstance(textEditor).ifPresent(componentValidator -> {
			componentValidator.revalidate();
			ValidationInfo info = componentValidator.getValidationInfo();
			if (info != null) {
				result.add(info);
			}
		});

		// disable submit button if validation fails
		for (ValidationInfo info : result) {
			if (!info.okEnabled) {
				option.setEnabled(false);
				return result;
			}
		}

		if (isAddingNewValue) {
			// never submit new value as NULL !!
			option.setEnabled(Objects.nonNull(getNewValue()));
		} else {
			// Do not submit unchaged values !!
			option.setEnabled(!Objects.equals(getOldValue(), getNewValue()));
		}

		return result;
	}

	@Nullable
	@Override
	public JComponent getPreferredFocusedComponent() {
		return textEditor;
	}

	@Nullable
	@Override
	public JComponent getEditorComponent() {
		return textEditor;
	}

	@NotNull
	@Override
	protected Action getOKAction() {
		return option;
	}

}
