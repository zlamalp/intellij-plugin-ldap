package io.gitlab.zlamalp.intellijplugin.ldap.ui.components;

import com.intellij.ui.Gray;
import com.intellij.util.ui.ColorIcon;
import org.jetbrains.annotations.NotNull;

import java.awt.*;

import static java.lang.Math.ceil;

/**
 * We override {@link ColorIcon} in order to draw it in a circle, instead of square.
 *
 * @author Pavel Zlámal
 */
public class CircleColorIcon extends ColorIcon {

	private int myColorSize;
	private boolean myBorder;

	public CircleColorIcon(int size, int colorSize, @NotNull Color color, boolean border) {
		super(size, colorSize, color, border);
		this.myColorSize = colorSize;
		this.myBorder = border;
	}

	public CircleColorIcon(int size, @NotNull Color color, boolean border) {
		super(size, color, border);
		this.myColorSize = size;
		this.myBorder = border;
	}

	public CircleColorIcon(int size, @NotNull Color color) {
		super(size, color);
		this.myColorSize = size;
		this.myBorder = false;
	}

	protected CircleColorIcon(CircleColorIcon icon) {
		super(icon);
	}

	@Override
	public void paintIcon(final Component component, final Graphics g, final int i, final int j) {
		final int iconWidth = getIconWidth();
		final int iconHeight = getIconHeight();
		g.setColor(getIconColor());

		final int size = (int)ceil(scaleVal(myColorSize));
		final int x = i + (iconWidth - size) / 2;
		final int y = j + (iconHeight - size) / 2;

		g.fillOval(x, y, size, size);

		if (myBorder) {
			g.setColor(Gray.x00.withAlpha(40));
			g.drawOval(x, y, size, size);
		}
	}

}
