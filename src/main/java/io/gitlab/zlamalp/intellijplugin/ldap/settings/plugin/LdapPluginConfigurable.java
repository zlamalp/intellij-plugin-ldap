package io.gitlab.zlamalp.intellijplugin.ldap.settings.plugin;

import com.intellij.openapi.options.Configurable;
import com.intellij.openapi.options.ConfigurationException;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.LdapPluginSettingsService;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

/**
 * This class represents single configuration item bundled within Intellij IDEA own SettingsDialog.
 * It provides means to
 *
 * @see com.intellij.openapi.options.newEditor.SettingsDialog
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdapPluginConfigurable implements Configurable {

	// TODO - Configuration change event by měla aktualizovat UI ?? Nebo to brát jako že se aplikuje na "nová okna".

	private LdapPluginSettingsForm form;

	@Nls
	public String getDisplayName() {
		return "LDAP support";
	}

	@Nullable
	public Icon getIcon() {
		return null;
	}

	@Nullable
	@NonNls
	public String getHelpTopic() {
		return null;
	}

	public JComponent createComponent() {
		if (form == null) {
			LdapPluginSettingsService instance = LdapPluginSettingsService.getInstance();
			LdapPluginSettings state = instance.getState();
			form = new LdapPluginSettingsForm(state);
		}
		return form.getContent();
	}

	public boolean isModified() {
		if (form != null) {
			return form.isModified(LdapPluginSettingsService.getInstance().getState());
		}
		return false;
	}

	public void apply() throws ConfigurationException {
		LdapPluginSettings settings = LdapPluginSettingsService.getInstance().getState();
		if (form != null) {
			form.saveSettings(settings);
		}
	}

	public void reset() {
		LdapPluginSettings settings = LdapPluginSettingsService.getInstance().getState();
		if (form != null) {
			form.loadSettings(settings);
		}
	}

	public void disposeUIResources() {
		form = null;
	}

}
