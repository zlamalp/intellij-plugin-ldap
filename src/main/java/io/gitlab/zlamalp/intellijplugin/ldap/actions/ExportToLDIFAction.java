package io.gitlab.zlamalp.intellijplugin.ldap.actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapActionUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.LdapTreePanel;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.AttributeModelItem;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.ui.ExportToLdifDialog;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

/**
 * Export selected LdapNode (from LdapTreePanel or LdapEntryEditor) as LDIF and open it in LdifVirtualFile.
 *
 * @see LdapTreePanel
 * @see LdapEntryEditor
 *
 * @author Pavel Zlámal
 */
public class ExportToLDIFAction extends AnAction {

	public static final String ID = "io.gitlab.zlamalp.intellijplugin.ldap.exportToLDIF";

	public ExportToLDIFAction() {
	}

	public ExportToLDIFAction(@Nls(capitalization = Nls.Capitalization.Title) @Nullable String text, @Nls(capitalization = Nls.Capitalization.Sentence) @Nullable String description, @Nullable Icon icon) {
		super(text, description, icon);
	}

	@Override
	public void actionPerformed(@NotNull AnActionEvent e) {

		if (!isActionActive(e)) return;

		// FIXME - limit printed attributes "to requested only"

		// TODO - allow export to File on filesystem ?

		if (LdapActionUtils.isFromTree(e)) {

			// triggered from tree
			LdapTreePanel treePanel = LdapActionUtils.getLdapTreePanel(e);
			LdapEntryTreeNode[] selectedTreeNodes = treePanel.getTree().getSelectedNodes(LdapEntryTreeNode.class, null);
			if (selectedTreeNodes.length > 0) {
				for (LdapEntryTreeNode node : selectedTreeNodes) {
					new ExportToLdifDialog(treePanel.getProject(), true, node, null).show();
				}
			}

		} else if (LdapActionUtils.isFromEditor(e)) {

			// triggered from editor
			LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
			if (editor != null) {
				AttributeModelItem item = editor.getTable().getSelectedItem();
				if (item != null) {
					new ExportToLdifDialog(e.getProject(), true, editor.getLdapNode(), item).show();
				}
			}

		}

	}

	@Override
	public void update(AnActionEvent e) {
		e.getPresentation().setEnabled(isActionActive(e));
	}

	/**
	 * Return TRUE if ExportToLDIF action is allowed for selected LdapNode.
	 *
	 * @param e Event triggering this action
	 * @return TRUE if Action is active or FALSE
	 */
	private boolean isActionActive(AnActionEvent e) {

		if (!LdapActionUtils.isProjectActive(e)) return false;
		if (LdapActionUtils.isFromTree(e)) {
			LdapTreePanel treePanel = LdapActionUtils.getLdapTreePanel(e);
			LdapEntryTreeNode[] selectedTreeNodes = treePanel.getTree().getSelectedNodes(LdapEntryTreeNode.class, null);
			return selectedTreeNodes.length > 0;
		} else if (LdapActionUtils.isFromEditor(e)) {
			LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
			return editor != null && !editor.isLoading();
		}
		return false;

	}

}
