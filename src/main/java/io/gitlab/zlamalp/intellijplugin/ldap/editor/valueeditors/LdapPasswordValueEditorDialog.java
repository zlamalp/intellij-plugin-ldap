package io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors;

import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.ValidationInfo;
import com.intellij.ui.ComboboxSpeedSearch;
import com.intellij.ui.DocumentAdapter;
import com.intellij.ui.JBColor;
import com.intellij.ui.MutableCollectionComboBoxModel;
import com.intellij.ui.components.JBCheckBox;
import com.intellij.ui.components.JBLabel;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.node.AddAttributeValueAction;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.AttributeTable;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.AttributeTableModel;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers.PasswordValueProvider;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import org.apache.directory.api.ldap.model.constants.LdapSecurityConstants;
import org.apache.directory.api.ldap.model.constants.SchemaConstants;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.DefaultAttribute;
import org.apache.directory.api.ldap.model.entry.Value;
import org.apache.directory.api.ldap.model.exception.LdapInvalidAttributeValueException;
import org.apache.directory.api.ldap.model.password.PasswordUtil;
import org.apache.directory.api.util.Strings;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Dialog to ADD or EDIT single "userPassword" Attribute value. Based on context it can either:
 *
 * - ADD value to existing attribute
 * - ADD 1st value to non-existing attribute -> means adding new Attribute to Entry
 * - EDIT value of existing attribute
 *
 * In case of ADD action, free selection of encryption algorithm is allowed.
 *
 * When editing, changing encryption algorithm is not allowed.
 *
 * @see io.gitlab.zlamalp.intellijplugin.ldap.actions.node.AddAttributeAction
 * @see AddAttributeValueAction
 * @see AttributeTable
 * @see AttributeTableModel
 *
 * @author Attila Majoros
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdapPasswordValueEditorDialog extends DialogWrapper implements AttributeValueEditor {

	private JPanel content;
	private JComboBox<String> algorithms;
	private JLabel displayedDN;
	private JPasswordField newPasswordField1;
	private JPasswordField newPasswordField2;
	private JPasswordField checkCurrentPassword;
	private JButton checkCurrentPasswordButton;
	private com.intellij.ui.TitledSeparator separator;
	private com.intellij.ui.TitledSeparator topSeparator;
	private JButton bindButton;
	private JBLabel currentPasswordTextField;
	private JBLabel currentAlgorithmLabel;
	private JBLabel currentSaltLabel;
	private JLabel currentPasswordLabel;
	private JLabel algorithmLabel;
	private JLabel saltLabel;
	private JLabel verifyLabel;
	private JBCheckBox showCurrentPasswordDetails;
	private JBLabel currentPasswordHexTextField;
	private JLabel currentPasswordHexLabel;
	private JBLabel verifyAndBindStatus;

	private boolean showPassword = false;
	private boolean initialized;
	private LdapEntryTreeNode node;
	private PasswordValueProvider valueProvider;
	private boolean addingNewValue = false;

	private static Logger log = LoggerFactory.getLogger(LdapPasswordValueEditorDialog.class);

	private void createUIComponents() {
		// TODO: place custom component creation code here
	}

	/**
	 * Create default Attribute value editor with specified action ADD / EDIT.
	 *
	 * @param parent Parent Component for modal dialog
	 * @param node LdapNode (Entry) to edit
	 * @param valueProvider Value provider for edited Attribute and its Value
	 * @param addingNewValue TRUE if dialog should add new value for same attribute instead of using the one from value provider
	 */
	public LdapPasswordValueEditorDialog(@NotNull Component parent, @NotNull LdapEntryTreeNode node, @NotNull PasswordValueProvider valueProvider, boolean addingNewValue) {

		super(parent, true);
		this.initialized = false;
		this.valueProvider = valueProvider;
		this.node = node;
		this.addingNewValue = addingNewValue;

		if (addingNewValue) {
			setTitle("Add New Password");
			this.setOKButtonText("Add new password");
		} else {
			setTitle("Password Editor");
			this.setOKButtonText("Change password");
		}
		init();
		validate();

	}

	private boolean verifyPassword() {
		String toBeCheckedPassword = new String(checkCurrentPassword.getPassword());
		if (valueProvider != null) {
			if (valueProvider.isArgon2value()) {
				Argon2PasswordEncoder arg2SpringSecurity = new Argon2PasswordEncoder(16, 32, 1, 4096, 3);
				// argon2 matching implementation ignores prefix (before first $), that's why passing "{ARGON2}value" works
				return arg2SpringSecurity.matches(toBeCheckedPassword, valueProvider.getRawValue().getString());
			} else {
				return PasswordUtil.compareCredentials(toBeCheckedPassword.getBytes(), valueProvider.getRawValue().getBytes());
			}
		}
		return false;
	}

	private void initialize(){

		if (!initialized) {

			currentPasswordTextField.setCopyable(true);
			currentSaltLabel.setCopyable(true);
			currentPasswordHexTextField.setCopyable(true);
			currentSaltLabel.setCopyable(true);
			currentAlgorithmLabel.setCopyable(true);

			verifyAndBindStatus.setVisible(false);

			checkCurrentPasswordButton.setEnabled(false);
			bindButton.setEnabled(false);

			checkCurrentPassword.getDocument().addDocumentListener(new DocumentAdapter() {
				@Override
				protected void textChanged(DocumentEvent e) {
					switchCurrentPasswordCheck(checkCurrentPassword.getPassword().length > 0);
				}
			});

			if (addingNewValue) {

				content.setPreferredSize(new Dimension(400, 150));

				// add new value => hide current password widgets
				showCurrentPasswordDetails.setVisible(false);
				checkCurrentPassword.setVisible(false);
				checkCurrentPasswordButton.setVisible(false);
				currentPasswordLabel.setVisible(false);
				currentPasswordTextField.setVisible(false);
				currentAlgorithmLabel.setVisible(false);
				currentSaltLabel.setVisible(false);
				topSeparator.setVisible(false);
				bindButton.setVisible(false);
				saltLabel.setVisible(false);
				algorithmLabel.setVisible(false);
				verifyLabel.setVisible(false);
				currentPasswordHexTextField.setVisible(false);
				currentPasswordHexLabel.setVisible(false);
				separator.setVisible(false);

			} else {

				content.setPreferredSize(new Dimension(400, 400));

				checkCurrentPasswordButton.addActionListener(e -> {
					verifyAndBindStatus.setVisible(true);
					if (verifyPassword()) {
						verifyAndBindStatus.setText("Password match!");
						verifyAndBindStatus.setForeground(JBColor.GREEN.darker());
					} else {
						verifyAndBindStatus.setText("Password mismatch!");
						verifyAndBindStatus.setForeground(JBColor.RED);
					}
				});

				bindButton.addActionListener(e -> {
					verifyAndBindStatus.setVisible(true);
					verifyAndBindStatus.setText("Not implemented yet!");
					verifyAndBindStatus.setForeground(JBColor.foreground());
				});

				showCurrentPasswordDetails.addChangeListener(e -> {
					showHideCurrentPasswordDetails(showCurrentPasswordDetails.isSelected());
				});

			}

			displayedDN.setText(node.getDn().toString());

			List<String> algorithmNames = new ArrayList<>();
			algorithmNames.add("PLAIN");
			for (LdapSecurityConstants constant : LdapSecurityConstants.values()) {
				algorithmNames.add(constant.getName());
			}
			algorithmNames.add("SASL");
			algorithmNames.add("ARGON2");

			algorithms.setModel(new MutableCollectionComboBoxModel<>(algorithmNames));
			ComboboxSpeedSearch.installOn(algorithms);

			// value provider set by selected attribute
			LdapSecurityConstants usedAlgorithm = valueProvider.getAlgorithm();
			if (usedAlgorithm != null) {
				algorithms.setSelectedItem(usedAlgorithm.getName());
				currentAlgorithmLabel.setText(usedAlgorithm.getName());
			} else {
				if (valueProvider.isSASLvalue()) {
					algorithms.setSelectedItem("SASL");
					currentAlgorithmLabel.setText("SASL");
					checkCurrentPasswordButton.setEnabled(false);
					checkCurrentPasswordButton.setToolTipText("SASL passwords can't be verified locally. Please test password using Bind.");
				} else if (valueProvider.isArgon2value()) {
					algorithms.setSelectedItem("ARGON2");
					//currentAlgorithmLabel.setText("ARGON2");
					//checkCurrentPasswordButton.setEnabled(false);
					//checkCurrentPasswordButton.setToolTipText("ARGON2 passwords can't be verified locally. Please test password using Bind.");
				} else {
					algorithms.setSelectedItem("PLAIN");
					currentAlgorithmLabel.setText("Plain text");
				}
			}

			showHideCurrentPasswordDetails(showCurrentPasswordDetails.isSelected());
			switchCurrentPasswordCheck(checkCurrentPassword.getPassword().length > 0);

			initialized = true;
		}
	}

	/**
	 * Build LdapAttributeValueProvider from selected Attribute item and current editor value.
	 *
	 * USE ONLY FOR "ADD" action
	 */
	private void buildValueProvider() {

		// init value provider
		String attributeName = SchemaConstants.USER_PASSWORD_AT;

		// try to construct attribute
		Attribute attribute;
		if (node != null && node.getAttributeByName(attributeName) != null) {
			// we have node and actual attribute for "selected" attribute in editor -> use it
			attribute = node.getAttributeByName(attributeName);
		} else {
			attribute = new DefaultAttribute(attributeName);
		}

		LdapSecurityConstants algorithm = LdapSecurityConstants.getAlgorithm((String) algorithms.getSelectedItem());

		this.valueProvider = new PasswordValueProvider(node, attribute, null, algorithm);


		// set back "textual value" of editor to valueProvider value
		// this is safe as far as we call this only when valueProvider was NULL on editor creation
		// and has no relation to actual Attributes Value
		this.valueProvider.setRawValue(getNewValue());

	}

	@Nullable
	@Override
	protected JComponent createCenterPanel() {
		initialize();
		return content;
	}

	@Nullable
	@Override
	public JComponent getPreferredFocusedComponent() {
		return newPasswordField1;
	}

	@Nullable
	@Override
	protected JComponent createTitlePane() {
		return super.createTitlePane();
	}

	@Nullable
	@Override
	protected ValidationInfo doValidate() {
		if (newPasswordField1.getPassword().length == 0) {
			return new ValidationInfo("Value cannot be empty, in such case please use Remove action.", newPasswordField1);
		}
		if (!Arrays.equals(newPasswordField1.getPassword(), newPasswordField2.getPassword())) {
			return new ValidationInfo("New and Confirm passwords doesn't match.", newPasswordField2);
		}
		return null;
	}

	@Nullable
	@Override
	public Value getOldValue() {
		return valueProvider.getRawValue();
	}

	@Override
	public Value getNewValue() {

		// LDAP can't store "empty" value !!
		if (newPasswordField1.getPassword().length == 0) {
			return null;
		} else {
			// set current algorithm
			valueProvider.setAlgorithm(LdapSecurityConstants.getAlgorithm((String) algorithms.getSelectedItem()));

			// create new instance of Value<?> based on value provider rules and new editor value
			String val = new String(newPasswordField1.getPassword());
			if (Objects.equals(algorithms.getSelectedItem(), "SASL")) {
				val = "{SASL}" + new String(newPasswordField1.getPassword());
			}
			if (Objects.equals(algorithms.getSelectedItem(), "ARGON2")) {
				val = "{ARGON2}" + valueProvider.stringToArgon2(new String(newPasswordField1.getPassword()));
			}

			try {
				return valueProvider.constructNewValue(val);
			} catch (LdapInvalidAttributeValueException e) {
				return null;
			}

		}

	}

	@NotNull
	@Override
	public Attribute getAttribute() {
		if (this.valueProvider != null) {
			return this.valueProvider.getAttribute();
		}
		// in case value provider is not initialized yet, return manually created object
		return new DefaultAttribute(SchemaConstants.USER_PASSWORD_AT);
	}

	@Nullable
	@Override
	public JComponent getEditorComponent() {
		return newPasswordField1;
	}

	@Override
	public boolean isPreferReplace() {
		return false;
	}

	private void showHideCurrentPasswordDetails(boolean show) {

		if (show) {
			currentPasswordTextField.setText(valueProvider.getStringValue());
			if (valueProvider != null) {
				currentSaltLabel.setText((valueProvider.getSalt() != null) ? Strings.toHexString(valueProvider.getSalt()) : "-");
				currentPasswordHexTextField.setText((valueProvider.getRawValue() != null) ?
						Strings.toHexString(PasswordUtil.splitCredentials(valueProvider.getRawValue().getBytes()).getPassword())
						: "-");
			}
		} else {
			currentPasswordTextField.setText("********");
			currentPasswordHexTextField.setText("********");
			currentSaltLabel.setText("-");
		}

	}

	private void switchCurrentPasswordCheck(boolean enable) {

		if (valueProvider.isSASLvalue()) {
			checkCurrentPasswordButton.setEnabled(false);
			bindButton.setEnabled(enable);
		} else {
			bindButton.setEnabled(enable);
			checkCurrentPasswordButton.setEnabled(enable);
		}
	}

	@Override
	public DialogWrapper getDialogWrapper() {
		return this;
	}

}
