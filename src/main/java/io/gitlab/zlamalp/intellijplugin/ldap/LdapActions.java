package io.gitlab.zlamalp.intellijplugin.ldap;

import com.intellij.LdapBundle;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.EmptyAction;
import com.intellij.openapi.application.ApplicationManager;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.AddConnectionAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.AddEntryAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.BrowseSchemaAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.ChangeObjectClassAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.ConnectAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.ConnectionSettingsAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.CopyDnAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.DeleteEntryAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.DisconnectAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.EditEntryAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.ExportSchemaAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.ExportToLDIFAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.ImportFromLDIFAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.LocateEntryInTreeAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.MoveEntryAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.RefreshEntryAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.RenameEntryAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.SearchAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.SetFilterAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.SynchronizeSchemaAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.node.AddAttributeAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.node.AddAttributeValueAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.node.CollapseAllValuesAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.node.CopyAttributeValueAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.node.DiscardEditorChangesAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.node.EditAttributeValueAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.node.ExpandAllValuesAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.node.RefreshEditorAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.node.RemoveAttributeAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.node.RemoveAttributeValueAction;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.node.SaveEditorChangesAction;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.LdapTreePanel;

import java.util.concurrent.ConcurrentHashMap;

/**
 * ActionConfigurationCustomizer class used to initialize almost all LDAP plugin Actions
 *
 * @see com.intellij.openapi.actionSystem.AnAction
 * @see LdapTreePanel
 * @see LdapEntryEditor
 *
 * @author Pavel Zlámal
 */
public class LdapActions {

	private static final ConcurrentHashMap<String, AnAction> ldapActions = new ConcurrentHashMap<>();

	public LdapActions() {
	}

	public static LdapActions getInstance() {
		return ApplicationManager.getApplication().getService(LdapActions.class);
	}

	public static AnAction getAction(String actionId) {
		return ldapActions.get(actionId);
	}

	public static AnAction getWrappedAction(String actionId) {
		return EmptyAction.wrap(ldapActions.get(actionId));
	}

	static {

		// register LDAP / Entry specific actions

		ldapActions.put(ConnectionSettingsAction.ID, new ConnectionSettingsAction(
				LdapBundle.message("action.ldap.ConnectionSettingsAction.text"),
				LdapBundle.message("action.ldap.ConnectionSettingsAction.description"),
				LdapIcons.CONNECTION_SETTINGS
		));

		ldapActions.put(AddConnectionAction.ID, new AddConnectionAction(
				LdapBundle.message("action.ldap.AddConnectionAction.text"),
				LdapBundle.message("action.ldap.AddConnectionAction.description"),
				LdapIcons.ADD_CONNECTION
		));

		ldapActions.put(ConnectAction.ID, new ConnectAction(
				LdapBundle.message("action.ldap.ConnectAction.text"),
				LdapBundle.message("action.ldap.ConnectAction.description"),
				LdapIcons.CONNECT
		));

		ldapActions.put(DisconnectAction.ID, new DisconnectAction(
				LdapBundle.message("action.ldap.DisconnectAction.text"),
				LdapBundle.message("action.ldap.DisconnectAction.description"),
				LdapIcons.DISCONNECT
		));

		ldapActions.put(RefreshEntryAction.ID, new RefreshEntryAction(
				LdapBundle.message("action.ldap.RefreshEntryAction.text"),
				LdapBundle.message("action.ldap.RefreshEntryAction.description"),
				LdapIcons.REFRESH
		));

		ldapActions.put(ExportToLDIFAction.ID, new ExportToLDIFAction(
				LdapBundle.message("action.ldap.ExportToLDIFAction.text"),
				LdapBundle.message("action.ldap.ExportToLDIFAction.description"),
				LdapIcons.EXPORT_TO_LDIF
		));

		ldapActions.put(ImportFromLDIFAction.ID, new ImportFromLDIFAction(
				LdapBundle.message("action.ldap.ImportFromLDIFAction.text"),
				LdapBundle.message("action.ldap.ImportFromLDIFAction.description"),
				LdapIcons.IMPORT_FROM_LDIF
		));

		ldapActions.put(ChangeObjectClassAction.ID, new ChangeObjectClassAction(
				LdapBundle.message("action.ldap.ChangeObjectClassAction.text"),
				LdapBundle.message("action.ldap.ChangeObjectClassAction.description"),
				null
		));

		ldapActions.put(SetFilterAction.ID, new SetFilterAction(
				LdapBundle.message("action.ldap.SetFilterAction.text"),
				LdapBundle.message("action.ldap.SetFilterAction.description"),
				LdapIcons.FILTER
		));

		ldapActions.put(SearchAction.ID, new SearchAction(
				LdapBundle.message("action.ldap.SearchAction.text"),
				LdapBundle.message("action.ldap.SearchAction.description"),
				LdapIcons.SEARCH
		));

		ldapActions.put(CopyDnAction.ID, new CopyDnAction(
				LdapBundle.message("action.ldap.CopyDnAction.text"),
				LdapBundle.message("action.ldap.CopyDnAction.description"),
				LdapIcons.COPY
		));

		ldapActions.put(AddEntryAction.ID, new AddEntryAction(
				LdapBundle.message("action.ldap.AddEntryAction.text"),
				LdapBundle.message("action.ldap.AddEntryAction.description"),
				LdapIcons.ADD
		));

		ldapActions.put(RenameEntryAction.ID, new RenameEntryAction(
				LdapBundle.message("action.ldap.RenameEntryAction.text"),
				LdapBundle.message("action.ldap.RenameEntryAction.description"),
				LdapIcons.RENAME
		));

		ldapActions.put(MoveEntryAction.ID, new MoveEntryAction(
				LdapBundle.message("action.ldap.MoveEntryAction.text"),
				LdapBundle.message("action.ldap.MoveEntryAction.description"),
				null
		));

		ldapActions.put(DeleteEntryAction.ID, new DeleteEntryAction(
				LdapBundle.message("action.ldap.DeleteEntryAction.text"),
				LdapBundle.message("action.ldap.DeleteEntryAction.description"),
				LdapIcons.DELETE
		));

		// LDAP Schema actions

		ldapActions.put(ExportSchemaAction.ID, new ExportSchemaAction(
				LdapBundle.message("action.ldap.ExportSchemaAction.text"),
				LdapBundle.message("action.ldap.ExportSchemaAction.description"),
				LdapIcons.EXPORT_SCHEMA
		));

		ldapActions.put(SynchronizeSchemaAction.ID, new SynchronizeSchemaAction(
				LdapBundle.message("action.ldap.SynchronizeSchemaAction.text"),
				LdapBundle.message("action.ldap.SynchronizeSchemaAction.description"),
				LdapIcons.SYNCHRONIZE_SCHEMA
		));

		ldapActions.put(BrowseSchemaAction.ID, new BrowseSchemaAction(
				LdapBundle.message("action.ldap.BrowseSchemaAction.text"),
				LdapBundle.message("action.ldap.BrowseSchemaAction.description"),
				LdapIcons.BROWSE_SCHEMA
		));

		ldapActions.put(EditEntryAction.ID, new EditEntryAction(
				LdapBundle.message("action.ldap.EditEntryAction.text"),
				LdapBundle.message("action.ldap.EditEntryAction.description"),
				LdapIcons.EDIT
		));

		// LDAP Entry Editor actions

		ldapActions.put(CollapseAllValuesAction.ID, new CollapseAllValuesAction(
				LdapBundle.message("action.ldap.CollapseAllValuesAction.text"),
				LdapBundle.message("action.ldap.CollapseAllValuesAction.description"),
				LdapIcons.COLLAPSE_ALL
		));
		ldapActions.put(ExpandAllValuesAction.ID, new ExpandAllValuesAction(
				LdapBundle.message("action.ldap.ExpandAllValuesAction.text"),
				LdapBundle.message("action.ldap.ExpandAllValuesAction.description"),
				LdapIcons.EXPAND_ALL
		));
		ldapActions.put(RefreshEditorAction.ID, new RefreshEditorAction(
				LdapBundle.message("action.ldap.RefreshEditorAction.text"),
				LdapBundle.message("action.ldap.RefreshEditorAction.description"),
				LdapIcons.REFRESH
		));
		ldapActions.put(EditAttributeValueAction.ID, new EditAttributeValueAction(
				LdapBundle.message("action.ldap.EditAttributeValueAction.text"),
				LdapBundle.message("action.ldap.EditAttributeValueAction.description"),
				LdapIcons.EDIT
		));
		ldapActions.put(AddAttributeValueAction.ID, new AddAttributeValueAction(
				LdapBundle.message("action.ldap.AddAttributeValueAction.text"),
				LdapBundle.message("action.ldap.AddAttributeValueAction.description"),
				LdapIcons.ADD
		));
		ldapActions.put(AddAttributeAction.ID, new AddAttributeAction(
				LdapBundle.message("action.ldap.AddAttributeAction.text"),
				LdapBundle.message("action.ldap.AddAttributeAction.description"),
				LdapIcons.ADD_ATTRIBUTE
		));
		ldapActions.put(RemoveAttributeValueAction.ID, new RemoveAttributeValueAction(
				LdapBundle.message("action.ldap.RemoveAttributeValueAction.text"),
				LdapBundle.message("action.ldap.RemoveAttributeValueAction.description"),
				LdapIcons.REMOVE
		));
		ldapActions.put(RemoveAttributeAction.ID, new RemoveAttributeAction(
				LdapBundle.message("action.ldap.RemoveAttributeAction.text"),
				LdapBundle.message("action.ldap.RemoveAttributeAction.description"),
				LdapIcons.REMOVE_ATTRIBUTE
		));
		ldapActions.put(CopyAttributeValueAction.ID, new CopyAttributeValueAction(
				LdapBundle.message("action.ldap.CopyAttributeValueAction.text"),
				LdapBundle.message("action.ldap.CopyAttributeValueAction.description"),
				LdapIcons.COPY
		));
		ldapActions.put(SaveEditorChangesAction.ID, new SaveEditorChangesAction(
				LdapBundle.message("action.ldap.SaveEditorChangesAction.text"),
				LdapBundle.message("action.ldap.SaveEditorChangesAction.description"),
				LdapIcons.SAVE_CHANGES));
		ldapActions.put(DiscardEditorChangesAction.ID, new DiscardEditorChangesAction(
				LdapBundle.message("action.ldap.DiscardEditorChangesAction.text"),
				LdapBundle.message("action.ldap.DiscardEditorChangesAction.description"),
				LdapIcons.DISCARD_CHANGES));


		// OTHER

		ldapActions.put(LocateEntryInTreeAction.ID, new LocateEntryInTreeAction(
				LdapBundle.message("action.ldap.LocateEntryInTree.text"),
				LdapBundle.message("action.ldap.LocateEntryInTree.description"),
				LdapIcons.LOCATE
		));

	}

}
