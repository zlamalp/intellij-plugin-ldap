package io.gitlab.zlamalp.intellijplugin.ldap.actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.LdapTreePanel;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditor;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

/**
 * Change objectClass attribute values of LDAP Entry.
 * This action is expected to lead to changes in MUST/MAY attributes for Entry.
 *
 * @see LdapTreePanel
 * @see LdapEntryEditor
 *
 * @author Pavel Zlámal
 */
public class ChangeObjectClassAction extends AnAction {

	public static final String ID = "io.gitlab.zlamalp.intellijplugin.ldap.changeObjectClass";

	public ChangeObjectClassAction() {
	}

	public ChangeObjectClassAction(@Nls(capitalization = Nls.Capitalization.Title) @Nullable String text, @Nls(capitalization = Nls.Capitalization.Sentence) @Nullable String description, @Nullable Icon icon) {
		super(text, description, icon);
	}

	@Override
	public void actionPerformed(@NotNull AnActionEvent e) {
		// TODO implement action
	}

	@Override
	public void update(AnActionEvent e) {
		e.getPresentation().setEnabled(false);
	}

}
