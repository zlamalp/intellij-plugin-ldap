package io.gitlab.zlamalp.intellijplugin.ldap.schema.ui;

import com.intellij.icons.AllIcons;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.util.SystemInfo;
import com.intellij.openapi.util.registry.Registry;
import com.intellij.ui.OnePixelSplitter;
import com.intellij.ui.RelativeFont;
import com.intellij.ui.ScrollPaneFactory;
import com.intellij.ui.SearchTextField;
import com.intellij.ui.TreeSpeedSearch;
import com.intellij.ui.components.JBLabel;
import com.intellij.ui.components.JBPanelWithEmptyText;
import com.intellij.ui.components.labels.BoldLabel;
import com.intellij.ui.treeStructure.Tree;
import com.intellij.util.ui.JBDimension;
import com.intellij.util.ui.UIUtil;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapIcons;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapServer;
import io.gitlab.zlamalp.intellijplugin.ldap.schema.SchemaUtils;
import org.apache.directory.api.ldap.model.schema.AttributeType;
import org.apache.directory.api.ldap.model.schema.ObjectClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreeSelectionModel;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

public class SchemaBrowserDialog extends DialogWrapper {

	private JPanel wrapper;
	private OnePixelSplitter splitter;
	private JBPanelWithEmptyText rightPanel;
	private Tree tree;
	private Component activeComponent;

	private boolean initialized = false;
	private LdapServer ldapServer;

	public SchemaBrowserDialog(@NotNull Project project, @NotNull Component parentComponent, LdapServer ldapServer) {
		// force IDE modality since we are managing also IDE wide ldap connections
		super(project, parentComponent, true, IdeModalityType.IDE);
		this.ldapServer = ldapServer;
		init();
	}


	private void initialize() {

		setTitle("Schema Browser for "+ ldapServer.getName());

		if (!ldapServer.isConnected()) {
			ldapServer.connect();
		}

		splitter.setProportion(0.2f);
		splitter.setAndLoadSplitterProportionKey("io.gitlab.zlamalp.intellijplugin.ldap.SchemaBrowserDialogSplitterProportion");

		rightPanel = new JBPanelWithEmptyText();
		rightPanel.withEmptyText("No schema information available / selected.");
		rightPanel.setPreferredSize(new JBDimension(550, 600));
		rightPanel.setMinimumSize(new JBDimension(550, 600));

		DefaultMutableTreeNode root = new DefaultMutableTreeNode("schemaItems");
		Tree tree = new Tree(new DefaultTreeModel(root));

		// set fixed row height -> create fake label to get height right !!
		BoldLabel fakeLabel = new BoldLabel("ASDASD");
		fakeLabel.setIcon(LdapIcons.ADD);
		fakeLabel.setBorder(BorderFactory.createEmptyBorder(1, 2, 2, 0));
		fakeLabel.setVerticalTextPosition(SwingConstants.CENTER);
		tree.setRowHeight(fakeLabel.getUI().getPreferredSize(fakeLabel).height);

		tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		tree.setBackground(UIUtil.SIDE_PANEL_BACKGROUND);
		tree.setBorder(javax.swing.BorderFactory.createEmptyBorder());

		tree.setCellRenderer(new TreeCellRenderer() {

			@Override
			public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {

				JBLabel label = new JBLabel();
				label.setIconTextGap(5);
				label.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
				label.setText(value.toString());

				if (value instanceof MyCategoryNode) {
					label.setIcon(AllIcons.Nodes.PpLib);
				} else if (value instanceof DefaultMutableTreeNode) {
					label.setIcon(AllIcons.FileTypes.Text);
				}
/*
				if (selected) {
					label.setOpaque(true);
					label.setBackground(tree.getSelectionBackground());
					label.setForeground(jList.getSelectionForeground());
				} else {
					label.setBackground(jList.getBackground());
					label.setForeground(jList.getForeground());
				}
*/
				return label;

			}
		});

		tree.addTreeSelectionListener(new TreeSelectionListener() {
			@Override
			public void valueChanged(TreeSelectionEvent e) {

				if (e.getNewLeadSelectionPath() != null) {

					if (e.getNewLeadSelectionPath().getLastPathComponent() instanceof ObjectClassNode) {

						JScrollPane myScroller = ScrollPaneFactory.createScrollPane(
								new ObjectClassPanel(((ObjectClassNode) e.getNewLeadSelectionPath().getLastPathComponent()).getObjectClass()).getComponent(), true);
						myScroller.setViewportBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
						myScroller.setWheelScrollingEnabled(true);
						myScroller.setMinimumSize(new JBDimension(550, 600));
						myScroller.setPreferredSize(new JBDimension(550, 600));
						splitter.setSecondComponent(myScroller);

					} else if (e.getNewLeadSelectionPath().getLastPathComponent() instanceof AttributeTypeNode) {

						JScrollPane myScroller = ScrollPaneFactory.createScrollPane(
								new AttributeTypePanel(((AttributeTypeNode) e.getNewLeadSelectionPath().getLastPathComponent()).getAttributeType()).getComponent(), true);

						myScroller.setViewportBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
						myScroller.setWheelScrollingEnabled(true);
						myScroller.setMinimumSize(new JBDimension(550, 600));
						myScroller.setPreferredSize(new JBDimension(550, 600));
						splitter.setSecondComponent(myScroller);

					} else {
						splitter.setSecondComponent(rightPanel);
					}

				} else {
					splitter.setSecondComponent(rightPanel);
				}

			}
		});

		tree.setRootVisible(false);

		new TreeSpeedSearch(tree);

		DefaultMutableTreeNode schemas = new MyCategoryNode("Schemas");
		root.add(schemas);

		// TODO - other schemas
		List<String> schemaList = new ArrayList<>();
		if (ldapServer.getLdapType().equals(LdapServer.LdapType.OPEN_LDAP)) {
			schemaList.addAll(SchemaUtils.openLDAPdefaultSchemas);
		} else if (ldapServer.getLdapType().equals(LdapServer.LdapType.ACTIVE_DIRECTORY)) {
			schemaList.addAll(SchemaUtils.msADdefaultSchemas);
		}
		Collections.sort(schemaList);
		for (String schema : schemaList) {
			schemas.add(new DefaultMutableTreeNode(schema));
		}

		// TODO - pre category filling

		DefaultMutableTreeNode objectClasses = new MyCategoryNode("Object Classes");
		root.add(objectClasses);
		List<ObjectClass> objectClassList = new ArrayList<>(new HashSet<>(ldapServer.getSchema().getObjectClasses().values()));
		objectClassList.sort(new Comparator<ObjectClass>() {
			@Override
			public int compare(ObjectClass o1, ObjectClass o2) {
				return o1.getName().toLowerCase().compareTo(o2.getName().toLowerCase());
			}
		});
		for (ObjectClass obj : objectClassList) {
			objectClasses.add(new ObjectClassNode(obj));
		}

		DefaultMutableTreeNode attributeTypes = new MyCategoryNode("Attribute Types");
		root.add(attributeTypes);

		List<AttributeType> attributeTypesList = new ArrayList<>(new HashSet<>(ldapServer.getSchema().getAttributeTypes().values()));
		attributeTypesList.sort(new Comparator<AttributeType>() {
			@Override
			public int compare(AttributeType o1, AttributeType o2) {
				return o1.getName().toLowerCase().compareTo(o2.getName().toLowerCase());
			}
		});
		for (AttributeType obj : attributeTypesList) {
			attributeTypes.add(new AttributeTypeNode(obj));
		}

		root.add(new MyCategoryNode("Ldap Syntax"));
		root.add(new MyCategoryNode("Matching Rules"));
		root.add(new MyCategoryNode("Matching Rule Use"));
		root.add(new MyCategoryNode("dIT Structure Rules (MS AD)"));
		root.add(new MyCategoryNode("dIT Content Rules (MS AD)"));
		root.add(new MyCategoryNode("Name Forms (MS AD)"));

		JComponent left = new JPanel(new BorderLayout());
		left.setPreferredSize(new JBDimension(250, 600));
		left.setMinimumSize(new JBDimension(250, 600));

		Box toolbarBox = Box.createHorizontalBox();

		SearchTextField search = new SearchTextField();
		search.setBackground(UIUtil.SIDE_PANEL_BACKGROUND);

		toolbarBox.add(search);
		toolbarBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		toolbarBox.setBackground(UIUtil.SIDE_PANEL_BACKGROUND);

		tree.setOpaque(true);
		RelativeFont.BOLD.install(tree);

		JScrollPane myScroller = ScrollPaneFactory.createScrollPane(tree, true);
		myScroller.setViewportBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0));
		myScroller.setWheelScrollingEnabled(true);

		if (SystemInfo.isMac) {
			left.add(BorderLayout.PAGE_END, toolbarBox);
			left.add(BorderLayout.CENTER, myScroller);
		} else {
			left.add(BorderLayout.NORTH, toolbarBox);
			left.add(BorderLayout.CENTER, myScroller);

		}

		if (!Registry.is("ide.scroll.background.auto")) {
			myScroller.setBackground(UIUtil.SIDE_PANEL_BACKGROUND);
			myScroller.getViewport().setBackground(UIUtil.SIDE_PANEL_BACKGROUND);
			myScroller.getVerticalScrollBar().setBackground(UIUtil.SIDE_PANEL_BACKGROUND);
		}

		splitter.setFirstComponent(left);
		splitter.setSecondComponent(rightPanel);
		splitter.setHonorComponentsMinimumSize(true);
		splitter.setPreferredSize(new JBDimension(800, 600));
		splitter.setMinimumSize(new JBDimension(800, 600));

		wrapper.setPreferredSize(new JBDimension(800, 600));

		((DefaultTreeModel)tree.getModel()).reload();
		tree.repaint();

		initialized = true;

	}


		@Nullable
	@Override
	protected JComponent createCenterPanel() {
		if (!initialized) {
			initialize();
		}
		return wrapper;
	}

	@NotNull
	@Override
	protected DialogStyle getStyle() {
		return DialogStyle.COMPACT;
	}

	private class MyCategoryNode extends DefaultMutableTreeNode {

		public MyCategoryNode(String userObject) {
			super(userObject);
		}

	}

	private class ObjectClassNode extends DefaultMutableTreeNode {

		public ObjectClassNode(ObjectClass userObject) {
			super(userObject);
		}

		public ObjectClass getObjectClass() {
			return (ObjectClass)getUserObject();
		}

		@Override
		public String toString() {
			return (getObjectClass() != null) ? getObjectClass().getName() : "Empty ObjectClass node";
		}

	}

	private class AttributeTypeNode extends DefaultMutableTreeNode {

		public AttributeTypeNode(AttributeType userObject) {
			super(userObject);
		}

		public AttributeType getAttributeType() {
			return (AttributeType)getUserObject();
		}

		@Override
		public String toString() {
			return (getAttributeType() != null) ? getAttributeType().getName() : "Empty AttributeType node";
		}

	}


	@NotNull
	@Override
	protected Action[] createActions() {

		ArrayList<Action> actions = new ArrayList<>();
		actions.add(getOKAction());
		return actions.toArray(new Action[0]);

	}
}
