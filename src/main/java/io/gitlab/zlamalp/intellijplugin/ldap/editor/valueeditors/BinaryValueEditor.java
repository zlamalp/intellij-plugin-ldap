package io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors;

import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers.BinaryValueProvider;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;

/**
 * Attribute Value Editor for binary attributes. It is fallback editor, where raw binary data are
 * encoded to base64 string and can be edited by user as such.
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class BinaryValueEditor extends DefaultValueEditor {

	private static Logger log = LoggerFactory.getLogger(BinaryValueEditor.class);

	/**
	 * Create editor for binary attributes.
	 *
	 * @param parent Parent Component for modal dialog
	 * @param valueProvider Value provider for edited Attribute and its Value
	 * @param isAddingNewValue TRUE when we want add new value to attribute / FALSE if we are editing current value
	 */
	public BinaryValueEditor(@NotNull Component parent, @NotNull BinaryValueProvider valueProvider, boolean isAddingNewValue) {
		super(parent, valueProvider, isAddingNewValue);
		init();
	}

	@Nullable
	@Override
	protected String getDimensionServiceKey() {
		return DefaultValueEditor.class.getCanonicalName();
	}

}
