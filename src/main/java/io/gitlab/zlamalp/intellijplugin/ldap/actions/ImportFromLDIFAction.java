package io.gitlab.zlamalp.intellijplugin.ldap.actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.LdapTreePanel;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditor;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

/**
 * Import data to LDAP from selected LDIF file or text input
 *
 * @see LdapTreePanel
 * @see LdapEntryEditor
 *
 * @author Pavel Zlámal
 */
public class ImportFromLDIFAction extends AnAction {

	public static final String ID = "io.gitlab.zlamalp.intellijplugin.ldap.importFromLDIF";

	public ImportFromLDIFAction() {
	}

	public ImportFromLDIFAction(@Nls(capitalization = Nls.Capitalization.Title) @Nullable String text, @Nls(capitalization = Nls.Capitalization.Sentence) @Nullable String description, @Nullable Icon icon) {
		super(text, description, icon);
	}

	@Override
	public void actionPerformed(@NotNull AnActionEvent e) {
		// TODO implement action
	}

	@Override
	public void update(AnActionEvent e) {
		e.getPresentation().setEnabled(false);
	}

}
