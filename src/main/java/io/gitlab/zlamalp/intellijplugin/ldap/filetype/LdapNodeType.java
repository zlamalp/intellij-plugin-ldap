package io.gitlab.zlamalp.intellijplugin.ldap.filetype;

/**
 * Enumeration of LdapTreeNode "types" internally used to distinguish whether its ROOT_DSE, BASE or standard NODE.
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public enum LdapNodeType {

	/**
	 * Represents server root node - allow getting RootDSE attributes and get available bases or create dummy base if not accessible.
	 */
	ROOT_DSE,

	/**
	 * Represents base search - allow search for child nodes. Such node is either specified in connection settings or
	 * list of bases (namingContexts) is retrieved from LDAP itself.
	 */
	BASE,

	/**
	 * Represents standard LDAP Entry with Attributes - allows search for child nodes etc.
	 */
	NODE

}
