package io.gitlab.zlamalp.intellijplugin.ldap.tree.node;

import com.intellij.util.PlatformIcons;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapServer;
import io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeVirtualFile;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Wrapper for LdapTreeNodes. To increase performance of the Tree in LdapTreePanel we wrap child nodes by counts of "X",
 * where X is configurable.
 *
 * @see LdapEntryTreeNode
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdapTreeNodeWrapper extends DefaultMutableTreeNode implements LdapTreeNode, Comparable<LdapTreeNodeWrapper> {

	private static Logger log = LoggerFactory.getLogger(LdapTreeNodeWrapper.class);

	// LDAP server ldapServer associated with this node
	private LdapServer ldapServer;

	private int fromIndex = 0;
	private int toIndex = 0;

	/**
	 * Create new TreeNode for LdapTreePanel matching single LDAP Entry.
	 *
	 * @param ldapServer LDAP server ldapServer
	 * @param fromIndex displayed from index of original children collection
	 * @param toIndex displayed to index of original children collection
	 */
	public LdapTreeNodeWrapper(@NotNull LdapServer ldapServer, int fromIndex, int toIndex) {

		super("["+fromIndex+".."+toIndex+"]");

		this.ldapServer = ldapServer;
		this.fromIndex = fromIndex;
		this.toIndex = toIndex;

	}

	@Override
	public boolean getAllowsChildren() {
		return true;
	}

	@Override
	public boolean isLeaf() {
		return false;
	}

	/**
	 * Get all child LdapTreeNodes for this parent (LdapTreeNodeWrapper) or empty list if no child is present.
	 * Value is taken from local tree structure, no actual ldap search is performed.
	 *
	 * If child nodes are not of type LdapTreeNode, they are ignored. This wrapper node is expected to
	 * contain only them !!
	 *
	 * @return list of child LdapTreeNodes
	 */
	public @NotNull List<LdapEntryTreeNode> getChildren() {
		List<LdapEntryTreeNode> children = new ArrayList<>();
		for (int i=0; i<getChildCount(); i++) {
			TreeNode node = getChildAt(i);
			if (node instanceof LdapEntryTreeNode) {
				children.add(((LdapEntryTreeNode) node));
			}
		}
		return children;
	}

	/**
	 * Get LdapServerConnection related to this TreeNode
	 *
	 * @return LdapServerConnection related to this TreeNode
	 */
	public @NotNull
	LdapServer getLdapServer() {
		return ldapServer;
	}

	@NotNull
	@Override
	public String getTreeNodePresentation() {
		return toString();
	}

	@Nullable
	public Icon getIcon() {
		return PlatformIcons.FOLDER_ICON;
	}

	@Nullable
	@Override
	public LdapNodeVirtualFile getVirtualFile() {
		return null;
	}

	@Override
	public String toString() {

		return "["+fromIndex+".."+toIndex+"]";

	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		LdapTreeNodeWrapper that = (LdapTreeNodeWrapper) o;
		return Objects.equals(ldapServer, that.ldapServer) &&
				Objects.equals(fromIndex, that.fromIndex) &&
				Objects.equals(toIndex, that.toIndex);
	}

	@Override
	public int hashCode() {
		return Objects.hash(ldapServer, fromIndex, toIndex);
	}

	@Override
	public int compareTo(@NotNull LdapTreeNodeWrapper o) {

		if (toString() == null && o.toString() != null) return -1;
		else if (o.toString() == null && toString() != null) return 1;
		else if (toString() == null && o.toString() == null) return 0;
		else return toString().compareToIgnoreCase(o.toString());

	}

}
