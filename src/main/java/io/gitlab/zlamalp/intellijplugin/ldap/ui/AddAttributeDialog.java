package io.gitlab.zlamalp.intellijplugin.ldap.ui;

import com.intellij.openapi.ui.ComponentValidator;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.ValidationInfo;
import com.intellij.ui.ComboboxSpeedSearch;
import com.intellij.ui.DocumentAdapter;
import com.intellij.ui.MutableCollectionComboBoxModel;
import com.intellij.ui.components.JBCheckBox;
import com.intellij.ui.components.JBTextField;
import com.intellij.util.ui.UIUtil;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.AttributeValueEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers.AttributeValueProvider;
import io.gitlab.zlamalp.intellijplugin.ldap.schema.SchemaUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.LdapSettings;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.schema.SchemaSettings;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Supplier;
import java.util.regex.Pattern;

/**
 * Dialog wrapper for adding new Attribute to the Entry. User can choose AttributeType and Options.
 *
 * By default we offer attributes allowed by Entry objectClasses which are not set with any value.
 * We can add new value to existing Attributes too.
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class AddAttributeDialog extends DialogWrapper {

	private JComboBox<String> attributeTypeComboBox;
	private JBCheckBox hideExistingAttributesCheckBox;
	private JPanel content;
	private JBTextField optionsTextField;
	private JBTextField previewTextField;
	private JBCheckBox showOnlySubschemaAttrsCheckBox;
	private JBCheckBox binaryOption;
	private JLabel optionsHelpText;
	private JLabel attributeType2Label;
	private JBTextField attributeType2;
	private JLabel attributeTypeLabel;
	private boolean initialized = false;
	private LdapEntryTreeNode node;
	private Component parent;

	Set<String> allAttrsSet = new HashSet<>();
	Set<String> subSchemaSet = new HashSet<>();
	Set<String> notPresentSet = new HashSet<>();
	MutableCollectionComboBoxModel<String> model = new MutableCollectionComboBoxModel<String>(new ArrayList<>());

	private static Pattern optionsPattern = Pattern.compile("([-a-zA-Z0-9])*");
	private static Pattern attributeTypePattern = Pattern.compile("[a-zA-Z][a-zA-Z0-9]*");
	private SchemaSettings.SchemaProvider schemaProvider;

	public AddAttributeDialog(@NotNull Component parent, LdapEntryTreeNode node) {

		super(parent, true);

		this.parent = parent;
		this.node = node;

		LdapSettings settings = LdapUtils.getPreferredLdapSettings(node.getLdapServer().getSettings());
		this.schemaProvider = settings.getSchemaSettings().getSchemaProvider();

		setTitle("Add Attribute");
		this.setOKButtonText("Add");

		init();
		initValidation();

	}

	private void initialize() {

		if (Objects.equals(SchemaSettings.SchemaProvider.NONE, schemaProvider)) {

			attributeTypeComboBox.setVisible(false);
			attributeTypeLabel.setVisible(false);
			hideExistingAttributesCheckBox.setVisible(false);
			showOnlySubschemaAttrsCheckBox.setVisible(false);

			new ComponentValidator(getDisposable()).withValidator(new Supplier<ValidationInfo>() {
				@Override
				public ValidationInfo get() {
					previewTextField.setText(generatePreview());
					if (attributeTypePattern.matcher(attributeType2.getText()).matches()) {
						return null;
					} else {
						return new ValidationInfo("Invalid syntax of AttributeType name.", attributeType2);
					}
				}
			}).andStartOnFocusLost().andRegisterOnDocumentListener(attributeType2).installOn(attributeType2);

		} else {

			attributeType2.setVisible(false);
			attributeType2Label.setVisible(false);

			// TODO - we must support Apache vs. Plugin mode
			allAttrsSet.addAll(node.getLdapServer().getSchema().getAttributeTypeNames());
			subSchemaSet.addAll(SchemaUtils.getMustAttributes(node.getLdapServer().getSchema(), node.getObjectClasses()));
			subSchemaSet.addAll(SchemaUtils.getMayAttributes(node.getLdapServer().getSchema(), node.getObjectClasses()));

			attributeTypeComboBox.setModel(model);
			ComboboxSpeedSearch.installOn(attributeTypeComboBox);

			properlyFillComboBox();

			attributeTypeComboBox.addActionListener(e -> {
				previewTextField.setText(generatePreview());
			});

		}

		// FIXME - this means attribute requires binary transfer, not that value itself is binary
		binaryOption.addChangeListener(e -> {
			previewTextField.setText(generatePreview());
		});

		optionsTextField.getDocument().addDocumentListener(new DocumentAdapter() {
			@Override
			protected void textChanged(@NotNull DocumentEvent documentEvent) {
				previewTextField.setText(generatePreview());
			}
		});
		new ComponentValidator(getDisposable()).withValidator(new Supplier<ValidationInfo>() {
			@Override
			public ValidationInfo get() {
				if (optionsPattern.matcher(optionsTextField.getText()).matches()) {
					return null;
				} else {
					return new ValidationInfo("Invalid syntax of AttributeType option.", optionsTextField);
				}
			}
		}).installOn(optionsTextField).andRegisterOnDocumentListener(optionsTextField);
		previewTextField.setText(generatePreview());

		optionsHelpText.setFont(UIUtil.getLabelFont(UIUtil.FontSize.SMALL));
		optionsHelpText.setForeground(UIUtil.getContextHelpForeground());
		optionsHelpText.setBorder(BorderFactory.createEmptyBorder(0, 2, 0,0));

		hideExistingAttributesCheckBox.addActionListener(e -> {
			properlyFillComboBox();
		});
		hideExistingAttributesCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 2, 0,0));

		showOnlySubschemaAttrsCheckBox.addActionListener(e -> {
			properlyFillComboBox();
		});
		showOnlySubschemaAttrsCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 2, 0,0));

		initialized = true;

	}

	private void properlyFillComboBox() {

		if (hideExistingAttributesCheckBox.isSelected()) {
			notPresentSet.clear();
			if (showOnlySubschemaAttrsCheckBox.isSelected()) {
				notPresentSet.addAll(subSchemaSet);
			} else {
				notPresentSet.addAll(allAttrsSet);
			}
			notPresentSet.removeIf(attrName->node.getEntry().containsAttribute(attrName));
			fillComboBox(notPresentSet);
		} else {
			if (showOnlySubschemaAttrsCheckBox.isSelected()) {
				fillComboBox(subSchemaSet);
			} else {
				fillComboBox(allAttrsSet);
			}
		}
	}

	public JPanel getContent() {
		return content;
	}

	private void fillComboBox(Set<String> attributeTypes) {

		String wasSelected = model.getSelected();

		List<String> sorted = new ArrayList<String>(attributeTypes);
		Collections.sort(sorted);
		model.replaceAll(sorted);

		if (wasSelected != null) {
			model.setSelectedItem(wasSelected);
		} else {
			attributeTypeComboBox.setSelectedIndex(0);
		}

	}

	@Nullable
	@Override
	protected JComponent createCenterPanel() {
		initialize();
		return content;
	}

	@Nullable
	@Override
	public JComponent getPreferredFocusedComponent() {
		if (Objects.equals(SchemaSettings.SchemaProvider.NONE, schemaProvider)) {
			return attributeType2;
		} else {
			return attributeTypeComboBox;
		}
	}

	@NotNull
	@Override
	protected List<ValidationInfo> doValidateAll() {

		List<ValidationInfo> list = new ArrayList<>();

		ComponentValidator.getInstance(optionsTextField).ifPresent(r-> {
			if (r.getValidationInfo() != null) {
				list.add(r.getValidationInfo());
			}
		});
		ComponentValidator.getInstance(attributeType2).ifPresent(r-> {
			if (r.getValidationInfo() != null) {
				list.add(r.getValidationInfo());
			}
		});

		return list;

	}

	@Nullable
	@Override
	protected String getDimensionServiceKey() {
		return this.getClass().getCanonicalName();
	}

	public AttributeValueEditor getAttributeEditorDialog() {
		AttributeValueProvider provider = LdapUtils.getLdapValueProvider(node, previewTextField.getText());
		return provider.getValueEditor(parent, true);
	}

	private String generatePreview() {

		String preview = "";
		if (Objects.equals(SchemaSettings.SchemaProvider.NONE, schemaProvider)) {
			if (optionsPattern.matcher(attributeType2.getText()).matches()) {
				preview = StringUtils.trimToEmpty(attributeType2.getText());
			}
		} else {
			preview = model.getSelected();
		}
		if (binaryOption.isSelected()) {
			preview += ";binary";
		}
		if (optionsTextField.getText() != null && !optionsTextField.getText().isEmpty()) {
			preview += ";" + optionsTextField.getText();
		}
		return preview;

	}

}
