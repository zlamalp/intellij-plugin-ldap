package io.gitlab.zlamalp.intellijplugin.ldap.actions;

import com.intellij.LdapBundle;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.ui.Messages;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapActionUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapActions;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.node.SaveEditorChangesAction;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.LdapTreePanel;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapServerTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

/**
 * Refresh selected LdapTreeNode in LdapTreePanel (including all its children - subtree).
 *
 * @see LdapTreePanel
 * @see LdapEntryTreeNode
 * @see LdapServerTreeNode
 *
 * @author Attila Majoros
 * @author Pavel Zlámal
 */
public class RefreshEntryAction extends AnAction {

	public static final String ID = "io.gitlab.zlamalp.intellijplugin.ldap.refresh";

	public RefreshEntryAction() {
	}

	public RefreshEntryAction(@Nls(capitalization = Nls.Capitalization.Title) @Nullable String text, @Nls(capitalization = Nls.Capitalization.Sentence) @Nullable String description, @Nullable Icon icon) {
		super(text, description, icon);
	}

	@Override
	public void actionPerformed(@NotNull AnActionEvent e) {

		if (!isActionActive(e)) return;

		LdapTreePanel treePanel = LdapActionUtils.getLdapTreePanel(e);
		LdapEntryTreeNode[] selectedTreeNodes = treePanel.getTree().getSelectedNodes(LdapEntryTreeNode.class, null);
		if (selectedTreeNodes.length > 0) {
			for (LdapEntryTreeNode selectedNode : selectedTreeNodes) {
				boolean done = performAction(selectedNode, e);
				if (done) treePanel.getModel().nodeStructureChanged(selectedNode);
			}
		}

	}

	@Override
	public void update(AnActionEvent e) {
		e.getPresentation().setEnabled(isActionActive(e));
	}

	/**
	 * Return TRUE if RefreshEntry action should be active.
	 *
	 * @param e triggered event
	 * @return TRUE if action should be active
	 */
	private boolean isActionActive(AnActionEvent e) {

		if (!LdapActionUtils.isProjectActive(e)) return false;
		if (LdapActionUtils.isFromTree(e)) {
			LdapTreePanel treePanel = LdapActionUtils.getLdapTreePanel(e);
			LdapEntryTreeNode[] selectedTreeNodes = treePanel.getTree().getSelectedNodes(LdapEntryTreeNode.class, null);
			return selectedTreeNodes.length > 0;
		}
		return false;

	}

	/**
	 * Perform Reload Entry action on selected node
	 *
	 * @param node Node to refresh
	 * @param e sourcing event
	 * @return TRUE if action was done / FALSE if was canceled by asked dialog window
	 */
	private boolean performAction(@NotNull LdapEntryTreeNode node, AnActionEvent e) {

		LdapEntryEditor editor = node.getEditor();
		if (editor != null && editor.isModified() && !editor.isDirectEdit()) {
			int result = Messages.showYesNoCancelDialog(editor.getComponent(),
					LdapBundle.message("action.ldap.RefreshEditorAction.askOnChanges.text"),
					LdapBundle.message("action.ldap.RefreshEditorAction.askOnChanges.title"),
					LdapBundle.message("action.ldap.RefreshEditorAction.askOnChanges.save"),
					LdapBundle.message("action.ldap.RefreshEditorAction.askOnChanges.discard"),
					LdapBundle.message("action.ldap.RefreshEditorAction.askOnChanges.cancel"),
					Messages.getQuestionIcon());
			if (result == Messages.OK) {
				LdapActions.getWrappedAction(SaveEditorChangesAction.ID).actionPerformed(e);
			} else if (result == Messages.CANCEL) {
				return false;
			}
		}

		LdapUtils.refresh(node, LdapUtils.RefreshType.WITH_CHILDREN);
		return true;

	}

}
