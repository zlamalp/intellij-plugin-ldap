package io.gitlab.zlamalp.intellijplugin.ldap.actions.node;

import com.intellij.LdapBundle;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.ui.Messages;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapActionUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapIcons;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapUIUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.AttributeModelItem;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.AttributeTable;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.AttributeTableModel;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeVirtualFile;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.ui.LdapNotificationHandler;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.DefaultModification;
import org.apache.directory.api.ldap.model.entry.Entry;
import org.apache.directory.api.ldap.model.entry.Modification;
import org.apache.directory.api.ldap.model.entry.ModificationOperation;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;

/**
 * Action which removes attribute (all values).
 * Attribute is selected in table of opened LdapNode file (representing single LDAP entry).
 *
 * @see LdapNodeVirtualFile
 * @see LdapEntryEditor
 * @see AttributeTable
 * @see AttributeTableModel
 *
 * @author Attila Majoros
 * @author Pavel Zlámal
 */
public class RemoveAttributeAction extends AnAction {

	public static final String ID = "io.gitlab.zlamalp.intellijplugin.ldap.removeAttribute";
	private static Logger log = LoggerFactory.getLogger(RemoveAttributeAction.class);

	public RemoveAttributeAction() {
	}

	public RemoveAttributeAction(@Nls(capitalization = Nls.Capitalization.Title) @Nullable String text, @Nls(capitalization = Nls.Capitalization.Sentence) @Nullable String description, @Nullable Icon icon) {
		super(text, description, icon);
	}

	@Override
	public void actionPerformed(@NotNull AnActionEvent e) {

		// Cancel if attribute can't be removed !!
		if (!isActionActive(e)) return;

		LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
		LdapEntryTreeNode node = editor.getLdapNode();
		AttributeModelItem selectedItem = editor.getModel().getItems().get(editor.getTable().convertRowIndexToModel(editor.getTable().getSelectedRow()));

		// re-check again, since we normally allow removable on MUST attributes with > 1 values
		// but this action removes whole attribute (all values)
		if (selectedItem.isMustAttribute()) return;

		// TODO - we might want to ask, if user wants to remove attribute with specific options (default) or whole attribute (all possible x-ns-vals)
		int result = Messages.showOkCancelDialog(editor.getComponent(),
				LdapBundle.message("action.ldap.RemoveAttributeAction.modal.text", selectedItem.getAttribute().getUpId()),
				LdapBundle.message("action.ldap.RemoveAttributeAction.modal.title" ),
				LdapBundle.message("action.ldap.RemoveAttributeAction.modal.ok" ),
				LdapBundle.message("action.ldap.RemoveAttributeAction.modal.cancel" ),
				LdapIcons.REMOVE_ATTRIBUTE);

		if (result == Messages.OK) {

			Attribute attribute = selectedItem.getAttribute();
			log.debug("Removing attribute: {}", attribute);

			// check if source have such attribute
			if (!checkAndRemoveAttribute(node, node.getEntry().clone(), attribute.clone())) return;

			// we can remove

			if (editor.isDirectEdit()) {

				Task task = new Task.Modal(e.getProject(), LdapBundle.message("ldap.update.modal.modifyEntry", node.getDn()), true) {

					Exception exception = null;
					LdapConnection connection = null;

					@Override
					public void run(@NotNull ProgressIndicator indicator) {

						indicator.setIndeterminate(false);
						try {

							indicator.setText(LdapBundle.message("ldap.update.modal.connecting"));
							indicator.setFraction(0.1);
							LdapUIUtils.waitIfUIDebug();
							indicator.checkCanceled();
							connection = node.getLdapServer().getConnection();

							indicator.setFraction(0.5);
							indicator.setText(LdapBundle.message("ldap.update.modal.removingAttribute", attribute.getUpId()));
							LdapUIUtils.waitIfUIDebug();

							Modification removeMod = new DefaultModification(ModificationOperation.REMOVE_ATTRIBUTE, attribute.getUpId());
							indicator.checkCanceled();
							log.info("Removing attribute: {}, (all values)", attribute.getUpId());
							connection.modify(node.getDn(), removeMod);

							indicator.setFraction(1.0);

						} catch (LdapException ex) {
							exception = ex;
						}

					}


					@Override
					public void onSuccess() {

						if (exception != null) {

							// local model was not modified, so just show error
							LdapNotificationHandler.handleError(exception, node.getLdapServer().getName(), "Unable to remove Attribute '" + attribute.getUpId() + "'.");

						} else {

							// TODO - what is better - locally modify model or re-fetch whole entry from LDAP ?
							// LdapUtils.refresh(ldapNode);
							// update local model representation
							checkAndRemoveAttribute(node, node.getEntry(), attribute);
							// we modified LDAP, make current state the last known
							editor.setLastState(node.getEntry());

						}

						editor.getModel().refresh();

					}

					@Override
					public void onFinished() {

						// close connection even if user canceled operation
						if (connection != null) {
							try {
								node.getLdapServer().releaseConnection(connection);
							} catch (LdapException e) {
								LdapNotificationHandler.handleError(e, node.getLdapServer().getName(), "Can't release connection.");
							}
						}

					}

				};
				ProgressManager.getInstance().run(task);

			} else {

				// MODIFY MODEL AND REFRESH VIEW
				// we had to modify local model before actually deciding "directEdit", so now just refresh
				checkAndRemoveAttribute(node, node.getEntry(), attribute);
				editor.getModel().refresh();

			}

		}
	}

	/**
	 * Check if Entry contains relevant attribute and if not, false is returned. Otherwise attribute is removed and TRUE returned
	 *
	 * @param node
	 * @param entry
	 * @param attribute
	 * @return
	 */
	private boolean checkAndRemoveAttribute(@NotNull LdapEntryTreeNode node, @NotNull Entry entry, @NotNull Attribute attribute) {

		// TODO - check on MUST attributes ? Currently done in isActionActive()

		// modify local Attribute model first -> client side value validation if possible
		if (entry.get(attribute.getId()) != null) {
			try {
				if (entry.remove(attribute) == null) {
					log.error("Local representation of '{}' doesn't have Attribute we want to remove: {}", node.getDn(), attribute);
					LdapNotificationHandler.handleError(null, node.getLdapServer().getName(), "Unable to remove Attribute '"+attribute.getUpId()+"'.");
					return false;
				}
			} catch (LdapException e1) {
				LdapNotificationHandler.handleError(e1, node.getLdapServer().getName(), "Unable to remove Attribute '"+attribute.getUpId()+"'.");
				return false;
			}
		} else {
			log.error("Local representation of '{}' doesn't have Attribute we want to remove: {}", node.getDn(), attribute);
			LdapNotificationHandler.handleError(null, node.getLdapServer().getName(), "Unable to remove Attribute '"+attribute.getUpId()+"'.");
			return false;
		}
		return true;

	}

	@Override
	public void update(AnActionEvent e) {
		e.getPresentation().setEnabled(isActionActive(e));
	}

	/**
	 * Return TRUE if RemoveAttribute action is allowed for selected Attribute.
	 *
	 * @param e Event triggering this
	 * @return TRUE if Action is active or FALSE
	 */
	private boolean isActionActive(AnActionEvent e) {

		if (!LdapActionUtils.isProjectActive(e)) return false;
		if (LdapActionUtils.isFromEditor(e)) {
			LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
			if (editor != null && !editor.isLoading()) {
				int selectedRow = editor.getTable().getSelectedRow();
				if (selectedRow != -1) {
					if (editor.getModel().isRowRemovable(editor.getTable().convertRowIndexToModel(selectedRow))) {

						// FIXME - below is probably not true anymore, above condition is enough

						// re-check if it's not MUST attribute !! since above condition normally allows removal of value
						// from MUST attributes with >1 values.
						AttributeModelItem selectedItem = editor.getModel().getItems().get(editor.getTable().convertRowIndexToModel(selectedRow));

						// selected editable attribute MAY
						return !selectedItem.isMustAttribute();

					} else {
						// row (attribute) not editable
						return false;
					}
				} else {
					// no row selected
					return false;
				}
			}
		}
		return false;

	}

}
