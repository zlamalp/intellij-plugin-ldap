package io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers;

import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.AttributeValueEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.OctetStringValueEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.Value;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;

/**
 * Basic Binary value editor for LDAP Entry attribute values.
 * Only binary attributes can be used with this provider.
 *
 * Since we can't edit binary value directly, it's encoded to base64 string and expect same on input to save value.
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class OctetStringValueProvider extends BinaryValueProvider {

	private static Logger log = LoggerFactory.getLogger(OctetStringValueProvider.class);

	public OctetStringValueProvider(@NotNull LdapEntryTreeNode node, @NotNull Attribute attribute, Value value) {
		super(node, attribute, value);
		// FIXME - attribute can be binary and human-readable ?? -> eg. cert DNs
		/*
		if (attribute.isHumanReadable() || (value != null && value.isHumanReadable())) {
			throw new IllegalArgumentException(attribute.getUpId() + " is HumanReadable. You can't use BinaryValueEditor to edit its value.");
		}
		*/
	}

	@Override
	public AttributeValueEditor getValueEditor(Component component, boolean addingNewValue) {
		return new OctetStringValueEditor(component, this, addingNewValue);
	}

}
