package io.gitlab.zlamalp.intellijplugin.ldap.settings.schema;

import com.intellij.util.messages.Topic;

import java.util.EventListener;

/**
 * A listener for SchemaSettings changes.
 */
@FunctionalInterface
public interface SchemaSettingsListener extends EventListener {

	Topic<SchemaSettingsListener> TOPIC = Topic.create("Schema settings", SchemaSettingsListener.class);

	void schemaSettingsChanged(SchemaSettings schemaSettings);

}
