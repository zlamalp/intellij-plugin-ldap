package io.gitlab.zlamalp.intellijplugin.ldap;

import com.intellij.LdapBundle;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.application.ModalityState;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.fileEditor.impl.text.PsiAwareTextEditorImpl;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.popup.JBPopup;
import com.intellij.openapi.ui.popup.JBPopupFactory;
import com.intellij.openapi.ui.popup.JBPopupListener;
import com.intellij.openapi.ui.popup.LightweightWindowEvent;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.ui.EditorNotificationPanel;
import com.intellij.ui.JBColor;
import com.intellij.ui.components.JBScrollPane;
import com.intellij.ui.treeStructure.Tree;
import com.intellij.util.Alarm;
import com.intellij.util.ReflectionUtil;
import com.intellij.util.ui.JBDimension;
import com.intellij.util.ui.JBUI;
import com.intellij.util.ui.UIUtil;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.AttributeModelItem;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeVirtualFile;
import io.gitlab.zlamalp.intellijplugin.ldap.schema.SchemaUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.ui.ConnectDialog;
import org.apache.commons.lang.StringUtils;
import org.apache.directory.api.ldap.model.schema.AttributeType;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.plaf.basic.BasicTreeUI;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;
import java.lang.reflect.Method;
import java.util.Objects;

/**
 * Utility methods class for LDAP plugin UI
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdapUIUtils {

	private static Logger log = LoggerFactory.getLogger(LdapUIUtils.class);

	public static void waitIfUIDebug() {
		try {
			String timeoutString = System.getProperty("UIDebugTimeout", "0");
			int timeout = 0;
			try {
				timeout = Integer.parseInt(timeoutString);
			} catch (NumberFormatException ex) {
				log.error("Couldn't parse UI DEBUG", ex);
			}
			if (timeout > 0) {
				Thread.sleep(timeout);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Display notification about LDIF export files (not saved on file system).
	 * Called on file opening.
	 *
	 * @param project Project associated with file
	 * @param virtualFile VirtualFile with content (must be already opened)
	 */
	public static void showEditorNotificationPanelForExport(@NotNull Project project,
	                                                        @NotNull VirtualFile virtualFile) {

		PsiAwareTextEditorImpl fileEditor = (PsiAwareTextEditorImpl) FileEditorManager.getInstance(project).getSelectedEditor(virtualFile);
		if (fileEditor != null) {

			final Editor editor = fileEditor.getEditor();
			EditorNotificationPanel panel = new EditorNotificationPanel();
			panel.setText(LdapBundle.message("ldif.export"));

			// TODO - Add "Save as..." action ?

			panel.createActionLabel(LdapBundle.message("ldif.export.close"), new Runnable() {
				@Override
				public void run() {
					editor.setHeaderComponent(null);
				}
			});

			// show and scroll to top
			editor.setHeaderComponent(panel);
			ApplicationManager.getApplication().invokeLater(new Runnable() {
				@Override
				public void run() {
					editor.getScrollingModel().scrollVertically(0);
				}
			});

		}

	}

	public static String getAttributeTypeHTML(AttributeType type) {

		if (type == null) return "<html>No attribute type information available</html>";

		String description = (type.getDescription() != null) ? type.getDescription() : "<i>none</i>";
		String oid = (type.getOid() != null) ? type.getOid() : "-";

		String syntax = "<i>none</i>";
		if (type.getSyntax() != null && type.getSyntax().getDescription() != null) {
			syntax = type.getSyntax().getDescription();
		}
		if (type.getSyntaxName() != null && StringUtils.isBlank(syntax)) {
			syntax = type.getSyntaxName();
		}
		if (!Objects.equals(syntax, type.getSyntaxOid()) && type.getSyntaxOid() != null) {
			syntax = syntax + " (" + type.getSyntaxOid() + ")";
		}

		String equality = (type.getEqualityName() != null) ? type.getEqualityName() : "<i>none</i>";
		if (!Objects.equals(equality, type.getEqualityOid()) && type.getEqualityOid() != null) {
			equality = equality + " (" + type.getEqualityOid() + ")";
		}

		String ordering = (type.getOrderingName() != null) ? type.getOrderingName() : "<i>none</i>";
		if (!Objects.equals(ordering, type.getOrderingOid()) && type.getOrderingOid() != null) {
			ordering = ordering + " (" + type.getOrderingOid() + ")";
		}

		String substring = (type.getSubstringName() != null) ? type.getSubstringName() : "<i>none</i>";
		if (!Objects.equals(substring, type.getSubstringOid()) && type.getSubstringOid() != null) {
			substring = substring + " (" + type.getSubstringOid() + ")";
		}

		return "<html><b>Description: </b>" + description +
				"<br/><b>OID: </b>" + oid +
				"<br/><b>Syntax: </b>" + syntax +
				"<br/><b>Equality: </b>" + equality +
				"<br/><b>Ordering: </b>" + ordering +
				"<br/><b>Substring: </b>" + substring +
				"<br/><b>Single value: </b>" + ((type.isSingleValued()) ? "yes" : "no") + "</html>";

	}

	public static MouseMotionListener createAttributeTypeMouseMotionListener(@NotNull LdapEntryEditor editor, @NotNull Alarm myAlarm) {

		return new MouseMotionAdapter() {

			JBPopup popup = null;
			JTextPane pane =  null;
			AttributeModelItem lastModelItem = null;

			@Override
			public void mouseMoved(MouseEvent e) {

				if (editor.isLoading()) {
					myAlarm.cancelAllRequests();
					return;
				}

				final int row = editor.getTable().rowAtPoint(e.getPoint());
				final int col = editor.getTable().columnAtPoint(e.getPoint());
				if (row != -1 && col != -1) {

					myAlarm.addRequest(new Runnable() {
						@Override
						public void run() {

							if (editor.isLoading()) {
								myAlarm.cancelAllRequests();
								if (popup != null) {
									popup.closeOk(null);
								}
								return;
							}

							AttributeModelItem modelItem = editor.getTable().getItem(row, col);
							int modelColumn = -1;
							try {
								modelColumn = editor.getTable().convertColumnIndexToModel(col);
							} catch (IndexOutOfBoundsException ex) {
								// ignore
							}

							if (modelItem != null && modelColumn == 1) {

								if (popup == null) {

									lastModelItem = modelItem;

									Point newPoint = editor.getTable().getMousePosition();
									if (!isSameAsOldItem(newPoint, lastModelItem)) {
										return;
									}

									AttributeType type = SchemaUtils.getAttributeType(editor.getLdapNode(), modelItem.getAttribute());
									pane = new JTextPane();
									pane.setEditable(true);
									pane.setBackground(UIUtil.getToolTipBackground());
									pane.setBorder(null);
									pane.setContentType("text/html");
									pane.setFont(UIUtil.getToolTipFont().deriveFont((float)UIUtil.getToolTipFont().getSize()-2));
									pane.putClientProperty(JEditorPane.HONOR_DISPLAY_PROPERTIES, true);
									pane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
									pane.setText(LdapUIUtils.getAttributeTypeHTML(type));
									JBScrollPane scroll = new JBScrollPane(pane, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
									scroll.setBorder(JBUI.Borders.customLine(JBColor.border(), 1, 0, 0, 0));

									popup = JBPopupFactory.getInstance().createComponentPopupBuilder(scroll, pane)
											.setCancelOnClickOutside(true)
											//.setAdText("Footer?")
											.setDimensionServiceKey(editor.getProject(), "io.gitlab.zlamalp.intellijplugin.ldap.attributeType.popup", false)
											.setCancelOnOtherWindowOpen(false)
											.setMovable(true)
											.setTitle(modelItem.getAttribute().getUpId())
											.setResizable(true)
											.setCancelKeyEnabled(true)
											.addListener(new JBPopupListener() {
												@Override
												public void onClosed(@NotNull LightweightWindowEvent event) {
													pane = null;
													popup = null;
													lastModelItem = null;
												}
											})
											.createPopup();

									popup.setMinimumSize(new JBDimension(300, 200));
									popup.showInScreenCoordinates(editor.getTable(), MouseInfo.getPointerInfo().getLocation());

								} else {

									// we moved mouse to the different item
									if (lastModelItem != null && !Objects.equals(lastModelItem, modelItem)) {
										popup.closeOk(null);
										return;
									}

									// update content since we hover above same item
									AttributeType type = SchemaUtils.getAttributeType(editor.getLdapNode(), modelItem.getAttribute());
									if (pane != null) {

										popup.setCaption(modelItem.getAttribute().getUpId());
										pane.setText(LdapUIUtils.getAttributeTypeHTML(type));

									}

									// move to current position FIXME - might hide popup content below bottom border
									//popup.setLocation(MouseInfo.getPointerInfo().getLocation());

								}
							} else {
								// hide popup if not attribute name column
								myAlarm.cancelRequest(this);
								if (popup != null) {
									popup.closeOk(null);
								}
							}
						}
					}, 1000);

				} else {
					//myAlarm.cancelAllRequests();
					// hide popup if not attribute name column
					if (popup != null) {
						popup.closeOk(null);
					}
				}

			}

			private boolean isSameAsOldItem(Point newPoint, AttributeModelItem oldItem) {

				if (newPoint == null) return false;

				int row = editor.getTable().rowAtPoint(newPoint);
				int col = editor.getTable().columnAtPoint(newPoint);
				if (row != -1 && col != -1) {

					AttributeModelItem modelItem = editor.getTable().getItem(row, col);
					int modelColumn = -1;
					try {
						modelColumn = editor.getTable().convertColumnIndexToModel(col);
					} catch(IndexOutOfBoundsException ex) {
						// ignore
					}
					if (modelItem != null && modelColumn == 1) {
						return (Objects.equals(oldItem, modelItem));
					}
				}

				return false;

			}

		};

	}

	/**
	 * Should close all opened editors/files for the children of specified parent.
	 * Search is DN based - all files of subtree are closed.
	 * If includeSelf is TRUE, then also file for parent node is closed.
	 *
	 * @param project project it belongs to
	 * @param parent where to start
	 * @param includeSelf TRUE - close also file for parent node / FALSE - close all subtree files
	 */
	public static void closeAllFilesForSubtree(@NotNull Project project, @NotNull LdapEntryTreeNode parent, boolean includeSelf) {

		// we iterate over opened files, since there should be less of such items
		VirtualFile[] openedFiles = FileEditorManager.getInstance(project).getOpenFiles();
		for (VirtualFile vf : openedFiles) {
			if (vf instanceof LdapNodeVirtualFile) {

				LdapTreeNode node = ((LdapNodeVirtualFile)vf).getLdapTreeNode();
				if (node instanceof LdapEntryTreeNode) {

					String openedFileDN = ((LdapEntryTreeNode) node).getDn().toString();

					if (includeSelf &&
							(openedFileDN.endsWith(parent.getDn().toString()) ||
									openedFileDN.equalsIgnoreCase(parent.getDn().toString()))) {
						FileEditorManager.getInstance(project).closeFile(vf);
					}

					if (!includeSelf && openedFileDN.endsWith(parent.getDn().toString()) && !openedFileDN.equalsIgnoreCase(parent.getDn().toString())) {
						FileEditorManager.getInstance(project).closeFile(vf);
					}

				}

			}
		}

	}

	/**
	 * Perform toggle expand state on specified tree path. It works only for such Tree, where
	 * UI is an instance of BasicTreeUI. Using this makes sure that as many as possible child nodes
	 * are visible on parent expansion - respecting default behavior of "scrollOnExpand" property.
	 *
	 * @see BasicTreeUI
	 *
	 * @param tree Tree to expand nodes for
	 * @param path TreePath to expand
	 */
	public static void toggleExpandStateOnTree(Tree tree, TreePath path) {

		try {
			Class aClass = tree.getUI().getClass();
			while (BasicTreeUI.class.isAssignableFrom(aClass) && !BasicTreeUI.class.equals(aClass)) {
				aClass = aClass.getSuperclass();
			}
			Method method = ReflectionUtil.getDeclaredMethod(aClass, "toggleExpandState", TreePath.class);
			if (method != null) {
				method.invoke(tree.getUI(), path);
			}
		}
		catch (Throwable ignore) {
		}

	}

	/**
	 * Opens dialog window and asks for password for passed connection.
	 * Once provided, it tries to connect and return result boolean.
	 * If canceled FALSE is returned.
	 *
	 * @param ldapServer Connection to show askForPassword used
	 * @param timeout how long to wait on success
	 * @param reason reason of previous failure if any
	 * @return TRUE if connections is OK, FALSE otherwise.
	 */
	public static boolean askForPasswordLoop(LdapServer ldapServer, int timeout, String reason) {

		if (!ApplicationManager.getApplication().isDispatchThread()) {

			// fake class to overcome necessity of "final" requirement on objects used in following call invokeAndWait()
			UserCanceledDialog userCanceled = new UserCanceledDialog();

			// execute UI in EventDispatcher thread and wait for the response
			// started in ANY ModalityState to make sure the window will appear whenever we want to ask for password
			ApplicationManager.getApplication().invokeAndWait(new Runnable() {
				@Override
				public void run() {
					// display the dialog and update connection based on input
					ConnectDialog connectDialog = new ConnectDialog(true, ldapServer, reason);
					if (connectDialog.showAndGet()) {
						ldapServer.setPassword(connectDialog.getPassword());
						ldapServer.setRememberPassword(connectDialog.getRememberPassword());
						ldapServer.setUserName(connectDialog.getUserName());
					} else {
						userCanceled.setUserCanceled(true);
					}
				}
			}, ModalityState.defaultModalityState());

			// user canceled dialog, break the loop
			if (userCanceled.isUserCanceled()) return false;
			// user provided data, try to connect in loop
			return ldapServer.connect(timeout);

		} else {

			// works in UI thread and might block it for specified timeout !!
			ConnectDialog connectDialog = new ConnectDialog(true, ldapServer, reason);
			if (connectDialog.showAndGet()) {
				ldapServer.setPassword(connectDialog.getPassword());
				ldapServer.setRememberPassword(connectDialog.getRememberPassword());
				ldapServer.setUserName(connectDialog.getUserName());
				return ldapServer.connect(timeout);
			} else {
				// stop authz loop if user canceled !!
				return false;
			}

		}

	}

	/**
	 * Fake class used just to determine if user canceled "askForPassword" dialog window
	 * and hence we should break the loop.
	 *
	 * This allows us to display dialog window in UI dispatcher thread,
	 * but determine result in a background thread (not blocking UI).
	 *
	 * @see #askForPasswordLoop(LdapServer, int, String)
	 *
	 * @author Pavel Zlámal <zlamal@cesnet.cz>
	 */
	public static class UserCanceledDialog {

		private boolean userCanceled = false;

		public boolean isUserCanceled() {
			return userCanceled;
		}

		public void setUserCanceled(boolean userCanceled) {
			this.userCanceled = userCanceled;
		}

	}

}
