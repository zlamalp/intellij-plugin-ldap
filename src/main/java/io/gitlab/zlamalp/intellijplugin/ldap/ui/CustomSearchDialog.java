package io.gitlab.zlamalp.intellijplugin.ldap.ui;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.application.ModalityState;
import com.intellij.openapi.progress.PerformInBackgroundOption;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.ui.CollectionComboBoxModel;
import com.intellij.ui.components.JBLabel;
import com.intellij.ui.components.JBTextField;
import com.intellij.ui.components.fields.ExpandableTextField;
import com.intellij.util.ui.JBDimension;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapServer;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.LdapTreePanel;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapUIUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeType;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapSearchTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapServerTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapSearchResultTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapTreeNode;
import org.apache.commons.lang.StringUtils;
import org.apache.directory.api.ldap.model.constants.LdapConstants;
import org.apache.directory.api.ldap.model.constants.SchemaConstants;
import org.apache.directory.api.ldap.model.entry.Entry;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.api.ldap.model.message.SearchScope;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Dialog for Custom LDAP searches. Results are appended below Custom Searches filetype in tree in TreePanel.
 * It can be used also for editing existing searches.
 *
 * @see LdapTreePanel
 * @see LdapEntryTreeNode
 * @see LdapSearchTreeNode
 * @see LdapSearchResultTreeNode
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class CustomSearchDialog extends DialogWrapper {

	private static Logger log = LoggerFactory.getLogger(CustomSearchDialog.class);

	private JPanel wrapper;
	private JBTextField baseDN;
	private JComboBox<SearchScope> scope;
	private JBTextField filter;
	private ExpandableTextField attributes;
	private JBTextField searchName;
	private boolean initialized = false;
	private LdapServer ldapServer;
	private String uuid;
	private boolean isEdit = false;
	private LdapSearchResultTreeNode lastResult;
	private static AtomicInteger searchCount = new AtomicInteger(0);
	private LdapTreeNode source = null;
	private Project project = null;
	private Action removeSearchAction = new AbstractAction("Remove search"){
		@Override
		public void actionPerformed(ActionEvent event) {
			if (lastResult != null) {
				// FIXME - we can get project from dialog itself
				LdapTreePanel.getInstance(project).getModel().removeNodeFromParent(lastResult);
				close(0);
			}
		}
	};

	/**
	 * Creates default dialog window for new custom search
	 *
	 * @param project Project
	 * @param ldapServer Connection to perform search on
	 * @param node node which asks for search
	 */
	public CustomSearchDialog(@NotNull Project project, @NotNull LdapServer ldapServer, LdapTreeNode node) {
		super(project, true);
		this.project = project;
		this.ldapServer = ldapServer;
		this.uuid = Long.toHexString(Double.doubleToLongBits(Math.random()));
		// clear remove action
		this.removeSearchAction = null;

		if (node instanceof LdapEntryTreeNode) {
			// we have entry selected
			this.baseDN.setText(((LdapEntryTreeNode) node).getDn().toString());
		}

		if (node instanceof LdapServerTreeNode || node instanceof LdapSearchTreeNode ||
				// or ROOT_DSE
				(node instanceof LdapEntryTreeNode && Objects.equals(LdapNodeType.ROOT_DSE, ((LdapEntryTreeNode) node).getNodeType()))) {
			// we query ldap
			try {
				List<String> bases = LdapUtils.queryRootDSEForBase(ldapServer, null);
				if (bases.size() > 0) this.baseDN.setText(bases.get(0));
			} catch (LdapException e) {
				// TODO
			}
		}
		init();
	}

	/**
	 * Creates dialog window for editing existing custom search
	 *
	 * @param project Project
	 * @param ldapServer Connection to perform search on
	 * @param result Previous result to be edited
	 */
	public CustomSearchDialog(@NotNull Project project, @NotNull LdapServer ldapServer, LdapSearchResultTreeNode result) {
		super(project, true);
		this.project = project;
		this.ldapServer = ldapServer;
		this.uuid = result.getUuid();
		this.isEdit = true;
		this.lastResult = result;

		wrapper.setPreferredSize(new JBDimension(400, 200));
		setOKButtonText("Search");
		setTitle("Edit Custom Search");

		this.scope.setModel(new CollectionComboBoxModel<>(Arrays.asList(SearchScope.values())));
		this.scope.setRenderer((list, value, index, isSelected, cellHasFocus) -> {

			JBLabel label = new JBLabel();
			if (isSelected) {
				label.setOpaque(true);
				label.setForeground(list.getSelectionForeground());
				label.setBackground(list.getSelectionBackground());
			} else {
				label.setForeground(list.getForeground());
				label.setBackground(list.getBackground());
			}

			if (SearchScope.OBJECT.equals(value)) label.setText("base");
			if (SearchScope.ONELEVEL.equals(value)) label.setText("one");
			if (SearchScope.SUBTREE.equals(value)) label.setText("sub");

			return label;
		});

		this.searchName.setText(result.getName());
		this.baseDN.setText(result.getBase());
		this.scope.setSelectedItem(result.getScope());
		this.filter.setText(result.getFilter());
		this.attributes.setText(StringUtils.join(result.getRequestedAttributes(), ", "));

		initialized = true;
		init();

	}

	private void initialize() {

		wrapper.setPreferredSize(new JBDimension(400, 200));
		setOKButtonText("Search");
		setTitle("Custom Search");
		searchName.setText("Custom search #"+searchCount.incrementAndGet());

		filter.setText(LdapConstants.OBJECT_CLASS_STAR);

		scope.setModel(new CollectionComboBoxModel<>(Arrays.asList(SearchScope.values())));
		scope.setRenderer((list, value, index, isSelected, cellHasFocus) -> {

			JBLabel label = new JBLabel();
			if (isSelected) {
				label.setOpaque(true);
				label.setForeground(list.getSelectionForeground());
				label.setBackground(list.getSelectionBackground());
			} else {
				label.setForeground(list.getForeground());
				label.setBackground(list.getBackground());
			}

			if (SearchScope.OBJECT.equals(value)) label.setText("base");
			if (SearchScope.ONELEVEL.equals(value)) label.setText("one");
			if (SearchScope.SUBTREE.equals(value)) label.setText("sub");

			return label;
		});

		initialized = true;
	}

	@Nullable
	@Override
	protected JComponent createCenterPanel() {
		if (!initialized) {
			initialize();
		}
		return wrapper;
	}

	@Nullable
	@Override
	public JComponent getPreferredFocusedComponent() {
		return baseDN;
	}

	@NotNull
	protected Action[] createActions() {

		ArrayList<Action> actions = new ArrayList<>();
		actions.add(getOKAction());

		Action removeAction = getRemoveSearchAction();
		if (removeAction != null) {
			actions.add(getRemoveSearchAction());
		}

		actions.add(getCancelAction());

		return actions.toArray(new Action[0]);

	}

	private Action getRemoveSearchAction() {
		return this.removeSearchAction;
	}

	@Override
	protected void doOKAction() {

		Task task = new Task.Backgroundable(project, "Custom Search", true, PerformInBackgroundOption.ALWAYS_BACKGROUND) {
			@Override
			public void run(@NotNull ProgressIndicator indicator) {

				try {

					List<Entry> cursor = LdapUtils.search(ldapServer, null, baseDN.getText(), filter.getText(), (SearchScope)scope.getSelectedItem(), attributes.getText().split(" "));

					if (cursor == null || cursor.isEmpty()) {
						LdapNotificationHandler.notify(ldapServer.getName(), "Search '"+searchName.getText()+"' returned empty result.");
					}

					List<LdapEntryTreeNode> result = new ArrayList<>();
					for (Entry entry : cursor) {
						result.add(new LdapEntryTreeNode(ldapServer, (entry.getDn().toString().equalsIgnoreCase(baseDN.getText())) ? LdapNodeType.BASE : LdapNodeType.NODE, entry.getDn(), entry));
					}
					Collections.sort(result);

					LdapTreePanel panel = LdapTreePanel.getInstance(project);

					LdapServerTreeNode searchNodeRoot = null;

					Enumeration<TreeNode> e = ((DefaultMutableTreeNode)panel.getModel().getRoot()).breadthFirstEnumeration();
					while (e.hasMoreElements()) {
						TreeNode node = e.nextElement();
						if (node instanceof LdapServerTreeNode && ((LdapServerTreeNode) node).getLdapServer().equals(ldapServer)) { // && "Custom searches".equals(node.getUserObject())) {
							searchNodeRoot = (LdapServerTreeNode) node;
							break;
						}
					}

					if (searchNodeRoot != null) {

						Set<String> fetchOnlyAttributesSet = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
						if (StringUtils.isNotBlank(attributes.getText())) {
							fetchOnlyAttributesSet.addAll(Arrays.asList(attributes.getText().split("\\s*,\\s*")));
						} else {
							fetchOnlyAttributesSet.add(SchemaConstants.ALL_USER_ATTRIBUTES);
						}

						LdapSearchResultTreeNode searchNode = new LdapSearchResultTreeNode(ldapServer, uuid, searchName.getText(), baseDN.getText(), filter.getText(), (SearchScope)scope.getSelectedItem(), fetchOnlyAttributesSet);

						for (LdapSearchResultTreeNode node : searchNodeRoot.getCustomSearches().getChildren()) {
							if (node.equals(searchNode)) {
								// remove old and replace it with new
								panel.getModel().removeNodeFromParent(node);
								break;
							}
						}

						List<LdapSearchResultTreeNode> searches = searchNodeRoot.getCustomSearches().getChildren();
						searches.add(searchNode);
						Collections.sort(searches);

						searchNodeRoot.getCustomSearches().removeAllChildren();
						for (LdapSearchResultTreeNode search : searches) {
							searchNodeRoot.getCustomSearches().add(search);
						}

						for (LdapEntryTreeNode node : result) {
							searchNode.add(node);
						}

						// Not sure why, but nodeStructureChanged and toggleExpandStateOnTree must be called from EDT
						// and this background task is not EDT
						final LdapServerTreeNode searchNodeRootFinal = searchNodeRoot;
						ApplicationManager.getApplication().invokeLater(new Runnable() {
							@Override
							public void run() {

								panel.getModel().nodeStructureChanged(searchNodeRootFinal.getCustomSearches());

								if (panel.getTree().isExpanded(new TreePath(searchNodeRootFinal.getCustomSearches().getPath()))) {
									panel.getTree().collapsePath(new TreePath(searchNodeRootFinal.getCustomSearches().getPath()));
								}
								LdapUIUtils.toggleExpandStateOnTree(panel.getTree(), new TreePath(searchNodeRootFinal.getCustomSearches().getPath()));

							}
						}, ModalityState.stateForComponent(wrapper));

					}

				} catch (LdapException e) {
					log.error("Failed to search LDAP.", e);
				}

			}
		};
		ProgressManager.getInstance().run(task);

		super.doOKAction();

	}

	@Override
	public void doCancelAction() {
		if (!isEdit) {
			searchCount.decrementAndGet();
		}
		super.doCancelAction();
	}

}
