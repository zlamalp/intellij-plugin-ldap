package io.gitlab.zlamalp.intellijplugin.ldap.filetype;

import com.intellij.openapi.fileTypes.FileType;
import com.intellij.openapi.vfs.VirtualFile;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapIcons;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.LdapTreePanel;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

/**
 * Definition of IDE {@link FileType} for our {@link LdapNodeVirtualFile} which represents
 * single LDAP Entry and is associated with {@link LdapTreeNode}.
 *
 * @see LdapTreePanel
 *
 * @author Attila Majoros
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdapNodeFileType implements FileType {

	public static final String EXTENSION = "ldapnode";

	@NotNull
	@Override
	public String getName() {
		return "LDAP Node";
	}

	@NotNull
	@Override
	public String getDescription() {
		return "Displays the LDAP entry attributes and values";
	}

	@NotNull
	@Override
	public String getDefaultExtension() {
		return EXTENSION;
	}

	@Nullable
	@Override
	public Icon getIcon() {
		return LdapIcons.ENTRY;
	}

	@Override
	public boolean isBinary() {
		return true;
	}

	@Override
	public boolean isReadOnly() {
		return false;
	}

	@Nullable
	@Override
	public String getCharset(@NotNull VirtualFile virtualFile, @NotNull byte[] bytes) {
		return null;
	}

}
