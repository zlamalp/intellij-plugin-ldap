package io.gitlab.zlamalp.intellijplugin.ldap.filetype;

import com.intellij.lang.Language;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.FileViewProvider;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiManager;
import com.intellij.psi.SingleRootFileViewProvider;
import com.intellij.psi.impl.PsiManagerImpl;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Implementation of IDE {@link FileViewProvider} for our {@link LdapNodeVirtualFile}. Its purpose is to
 * return proper {@link PsiFile} implementation (in this case {@link LdapNodePsiFile}) for give {@link VirtualFile}.
 *
 * @see LdapNodeFileViewProviderFactory
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdapNodeFileViewProvider extends SingleRootFileViewProvider {

	public LdapNodeFileViewProvider(@NotNull PsiManager manager, @NotNull VirtualFile file) {
		super(manager, file);
	}

	public LdapNodeFileViewProvider(@NotNull PsiManager manager, @NotNull VirtualFile virtualFile, boolean eventSystemEnabled) {
		super(manager, virtualFile, eventSystemEnabled);
	}

	public LdapNodeFileViewProvider(@NotNull PsiManager manager, @NotNull VirtualFile virtualFile, boolean eventSystemEnabled, @NotNull FileType fileType) {
		super(manager, virtualFile, eventSystemEnabled, fileType);
	}

	@Nullable
	@Override
	protected PsiFile createFile(@NotNull Project project, @NotNull VirtualFile file, @NotNull FileType fileType) {
		return new LdapNodePsiFile((PsiManagerImpl)getManager(), this);
	}

	@NotNull
	@Override
	protected PsiFile createFile(@NotNull VirtualFile file, @NotNull FileType fileType, @NotNull Language language) {
		return new LdapNodePsiFile((PsiManagerImpl)getManager(), this);
	}

	@Nullable
	@Override
	protected PsiFile createFile(@NotNull Language lang) {
		return new LdapNodePsiFile((PsiManagerImpl)getManager(), this);
	}

	@Override
	public boolean hasLanguage(@NotNull Language language) {
		return false;
	}

}
