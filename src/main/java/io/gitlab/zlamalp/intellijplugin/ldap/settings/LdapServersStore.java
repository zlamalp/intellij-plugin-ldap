package io.gitlab.zlamalp.intellijplugin.ldap.settings;

import io.gitlab.zlamalp.intellijplugin.ldap.LdapServer;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.LdapTreePanel;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.plugin.LdapPluginSettings;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.beans.Transient;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Shared implementation for persistent storage of LdapServers. This should be used mostly internally.
 *
 * @see LdapPluginSettings
 * @see LdapPluginSettingsService
 * @see LdapProjectSettingsService
 * @see LdapTreePanel
 *
 * To get list of all available LdapServers stored within IDE and Project
 *
 * @see LdapTreePanel#getLdapServers()
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdapServersStore implements Serializable {

	/**
	 * Storage for LDAP server LdapServers
	 */
	private Map<String, LdapServer> allLdapServers = new HashMap<>();

	/**
	 * Return all stored LdapServers as map of <UUID,Connection>
	 *
	 * @return all LdapServers
	 */
	public Map<String, LdapServer> getAllLdapServers() {
		return allLdapServers;
	}

	/**
	 * Store all LdapServers as map of <UUID,Connection> replacing any previous state !!
	 *
	 * @param ldapServers store new LdapServers replacing previous state
	 */
	public void setAllLdapServers(@NotNull Map<String, LdapServer> ldapServers) {
		this.allLdapServers = ldapServers;
	}

	/**
	 * Return LdapServerConnection by its UUID or NULL if such connection is not present.
	 *
	 * @param uuid UUID to get Connection by
	 * @return Found connection by UUID or NULL
	 */
	@Transient
	@Nullable
	public LdapServer getLdapServerByUUID(@NotNull String uuid) {
		return this.allLdapServers.get(uuid);
	}

	/**
	 * Add or update any specific LdapServerConnection by its UUID.
	 *
	 * @param ldapServer Connection to add or update
	 */
	@Transient
	public void setLdapServerByUUID(@NotNull LdapServer ldapServer) {
		this.allLdapServers.put(ldapServer.getUuid(), ldapServer);
	}

	/**
	 * Removes specific LdapServerConnection by its UUID.
	 *
	 * @param ldapServer Connection to remove
	 */
	@Transient
	public void removeLdapServerByUUID(@NotNull LdapServer ldapServer) {
		this.allLdapServers.remove(ldapServer.getUuid());
	}

}
