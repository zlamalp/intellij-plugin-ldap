package io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers;

import com.intellij.ui.ColoredTableCellRenderer;
import com.intellij.ui.SimpleTextAttributes;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.AttributeValueEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.StringValueEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import org.apache.directory.api.ldap.model.constants.SchemaConstants;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.Value;
import org.apache.directory.api.ldap.model.schema.ObjectClass;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;

/**
 * Basic ObjectClass value editor for LDAP Entry attribute values.
 * Only HumanReadable (non-binary) attributes can be used with this editor.
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class ObjectClassValueProvider extends AbstractValueProvider {

	private static Logger log = LoggerFactory.getLogger(ObjectClassValueProvider.class);

	public ObjectClassValueProvider(@NotNull LdapEntryTreeNode node, @NotNull Attribute attribute, Value value) {

		super(node, attribute, value);

		if (!SchemaConstants.OBJECT_CLASS_AT.equalsIgnoreCase(attribute.getUpId())) {
			throw new IllegalArgumentException("You can't use ObjectClassValueProvider to edit value of "+attribute.getUpId()+" attribute.");
		}

	}

	@Override
	public void renderDisplayValue(ColoredTableCellRenderer renderer, SimpleTextAttributes style, boolean raw) {

		renderer.append(getStringValue(), style, true);

		if (!raw) {
			// if possible, append object class type
			SimpleTextAttributes localStyle = style.derive(SimpleTextAttributes.REGULAR_ITALIC_ATTRIBUTES.getStyle() | style.getStyle(), null, null, null);
			if (node.getLdapServer().getSchema() != null) {
				ObjectClass objClass = node.getLdapServer().getSchema().getObjectClasses().get(getStringValue());
				if (objClass != null) {
					renderer.append(" (" + objClass.getType().name() + ")", localStyle, false);
				}
			}
		}

	}

	@Override
	public AttributeValueEditor getValueEditor(Component component, boolean addingNewValue) {
		return new StringValueEditor(component, this, addingNewValue);
	}

}
