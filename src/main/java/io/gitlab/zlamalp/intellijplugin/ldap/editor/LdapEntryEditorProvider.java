package io.gitlab.zlamalp.intellijplugin.ldap.editor;

import com.intellij.openapi.fileEditor.FileEditor;
import com.intellij.openapi.fileEditor.FileEditorPolicy;
import com.intellij.openapi.fileEditor.FileEditorProvider;
import com.intellij.openapi.fileEditor.impl.EditorTabColorProvider;
import com.intellij.openapi.project.DumbAware;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapServer;
import io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeVirtualFile;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.awt.*;

/**
 * Provider which connects {@link LdapNodeVirtualFile} with its {@link LdapEntryEditor}.
 * It also provides color for editor tabs.
 *
 * @author Attila Majoros
 */
public class LdapEntryEditorProvider implements FileEditorProvider, EditorTabColorProvider, DumbAware {

	private static final String TYPE = "LDAP";

	@Override
	public boolean accept(@NotNull Project project, @NotNull VirtualFile virtualFile) {
		return virtualFile instanceof LdapNodeVirtualFile;
	}

	@NotNull
	@Override
	public FileEditor createEditor(@NotNull Project project, @NotNull VirtualFile virtualFile) {
		return new LdapEntryEditor(project, (LdapNodeVirtualFile) virtualFile);
	}

	@NotNull
	@Override
	public String getEditorTypeId() {
		return TYPE;
	}

	@NotNull
	@Override
	public FileEditorPolicy getPolicy() {
		return FileEditorPolicy.HIDE_DEFAULT_EDITOR;
	}

	@Nullable
	@Override
	public Color getEditorTabColor(@NotNull Project project, @NotNull VirtualFile file) {
		if (file instanceof LdapNodeVirtualFile) {
			LdapServer ldap = ((LdapNodeVirtualFile) file).getLdapTreeNode().getLdapServer();
			if (ldap.getColor().getName().equals("No color")) return null;
			return ldap.getColor().getJBColor();
		}
		return null;
	}

}
