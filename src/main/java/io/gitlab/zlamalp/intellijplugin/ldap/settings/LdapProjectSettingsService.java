package io.gitlab.zlamalp.intellijplugin.ldap.settings;

import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.State;
import com.intellij.openapi.components.Storage;
import com.intellij.openapi.project.Project;
import org.jetbrains.annotations.NotNull;

/**
 * LdapProjectSettingsService service is used to store IDE wide settings for LDAP plugin
 * as well as IDE wide LDAP server connections.
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
@State(name = "LdapProjectSettings", storages = {@Storage("LdapProjectSettings.xml")})
public class LdapProjectSettingsService  implements PersistentStateComponent<LdapServersStore> {

	private LdapServersStore ldapServersStore;

	/**
	 * Return instance of LdapPluginSettingsService IDE Component
	 *
	 * @return LdapPluginSettingsService
	 */
	public static LdapProjectSettingsService getInstance(Project project) {
		return project.getService(LdapProjectSettingsService.class);
	}

	@Override
	public void initializeComponent() {
		if (ldapServersStore == null) {
			ldapServersStore = new LdapServersStore();
		}
	}

	@NotNull
	@Override
	public LdapServersStore getState() {
		return this.ldapServersStore;
	}

	@Override
	public void loadState(@NotNull LdapServersStore state) {
		this.ldapServersStore = state;
	}

}
