package io.gitlab.zlamalp.intellijplugin.ldap.tree.node;

import io.gitlab.zlamalp.intellijplugin.ldap.LdapIcons;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapServer;
import io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeVirtualFile;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Parent filetype for Custom searches. Its expected to reside below LdapServerTreeNode.
 * Children are expected to be instances of LdapTreeNodeSearchResults.
 *
 * @see LdapServerTreeNode
 * @see LdapSearchResultTreeNode
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdapSearchTreeNode extends DefaultMutableTreeNode implements LdapTreeNode {

	private static Logger log = LoggerFactory.getLogger(LdapSearchTreeNode.class);

	// LDAP server ldapServer associated with this node
	private LdapServer ldapServer;

	/**
	 * Create new LdapTreeNodeSearch as single parent for all custom searches for associated ldapServer
	 *
	 * @param ldapServer LDAP server ldapServer
	 */
	public LdapSearchTreeNode(@NotNull LdapServer ldapServer) {
		super("Custom searches");
		this.ldapServer = ldapServer;
	}

	@Override
	public boolean getAllowsChildren() {
		return true;
	}

	/**
	 * Get all children LdapTreeNodeSearchResult.
	 *
	 * If child nodes are not of type LdapTreeNode, they are ignored. This node is expected to
	 * contain only them !!
	 *
	 * @return list of children LdapTreeNodeSearchResult
	 */
	public @NotNull List<LdapSearchResultTreeNode> getChildren() {
		List<LdapSearchResultTreeNode> children = new ArrayList<>();
		for (int i=0; i<getChildCount(); i++) {
			TreeNode node = getChildAt(i);
			if (node instanceof LdapSearchResultTreeNode) {
				children.add(((LdapSearchResultTreeNode) node));
			}
		}
		return children;
	}

	/**
	 * Get LdapServerConnection related to this TreeNode
	 *
	 * @return LdapServerConnection related to this TreeNode
	 */
	public @NotNull
	LdapServer getLdapServer() {
		return ldapServer;
	}

	@NotNull
	@Override
	public String getTreeNodePresentation() {
		return toString();
	}

	@Nullable
	public Icon getIcon() {
		return LdapIcons.SEARCH;
	}

	@Nullable
	@Override
	public LdapNodeVirtualFile getVirtualFile() {
		return null;
	}

	@Override
	public String toString() {
		return getUserObject().toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		LdapSearchTreeNode that = (LdapSearchTreeNode) o;
		return Objects.equals(ldapServer, that.ldapServer) && Objects.equals(getUserObject(), that.getUserObject());
	}

	@Override
	public int hashCode() {
		return Objects.hash(ldapServer, getUserObject());
	}

}
