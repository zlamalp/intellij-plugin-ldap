package io.gitlab.zlamalp.intellijplugin.ldap.settings.plugin;

import com.intellij.ui.CollectionComboBoxModel;
import com.intellij.ui.SimpleListCellRenderer;
import com.intellij.ui.components.JBCheckBox;
import com.intellij.ui.components.JBLabel;
import com.intellij.ui.components.fields.ExpandableTextField;
import com.intellij.ui.components.fields.IntegerField;
import com.intellij.util.ui.UIUtil;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.LdapPluginSettingsService;
import org.apache.commons.lang.StringUtils;
import org.apache.directory.api.ldap.model.message.AliasDerefMode;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

/**
 * UI form used to manage plugin IDE/Project wide settings. It's loaded by Configurable, which
 * is managed by IDEA global Settings dialog. It stores all data in LdapPluginSettings object
 * and it's persisted by LdapPluginSettingsService component.
 *
 * @see LdapPluginConfigurable
 * @see LdapPluginSettings
 * @see LdapPluginSettingsService
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdapPluginSettingsForm {

	private JPanel wrapper;

	// fetching entries and attributes / tree-view
	private IntegerField requestTimeout;
	private IntegerField pageSize;
	private JBCheckBox fetchUserAttributes;
	private JBCheckBox fetchOperationAttributes;
	private ExpandableTextField fetchOnlyAttributes;
	private IntegerField wrapChildrenBy;

	// Entry / Value editor
	private JBCheckBox updateLdap;
	private JBCheckBox compactTable;
	private JBCheckBox showDecoratedValues;
	private JComboBox<String> sortBy;
	private JRadioButton ascending;
	private JRadioButton descending;
	private JBCheckBox sortObjectClassFirst;
	private JBCheckBox sortMUSTFirst;

	// LDIF export
	private IntegerField wrapLdifLines;
	private JBCheckBox exportPassword;
	private JBCheckBox sortLdifAttributes;
	private IntegerField wrapAttributesBy;
	private JBCheckBox sortOperationalAttributesLast;
	private IntegerField responseLimit;
	private JComboBox<AliasDerefMode> aliasDerefComboBox;
	private IntegerField connectionTimeout;
	private IntegerField maxConnections;
	private IntegerField idleConnections;
	private JBLabel comment1;
	private JBLabel comment2;
	private JBLabel comment3;
	private JBLabel comment4;
	private JBLabel comment5;
	private JBLabel comment6;


	public LdapPluginSettingsForm(@NotNull LdapPluginSettings settings) {

		// set constraints
		connectionTimeout.setMaxValue(60);
		connectionTimeout.setMinValue(2);
		connectionTimeout.setDefaultValue(5);
		connectionTimeout.getEmptyText().setText("5");

		maxConnections.setMaxValue(10);
		maxConnections.setMinValue(1);
		maxConnections.setDefaultValue(3);
		maxConnections.getEmptyText().setText("3");

		idleConnections.setMaxValue(10);
		idleConnections.setMinValue(0);
		idleConnections.setDefaultValue(1);
		idleConnections.getEmptyText().setText("1");

		requestTimeout.setDefaultValue(60);
		requestTimeout.setMinValue(10);
		requestTimeout.setMaxValue(3600);
		requestTimeout.getEmptyText().setText("60");

		pageSize.setDefaultValue(1000);
		pageSize.setMinValue(0);
		pageSize.setMaxValue(100000);
		pageSize.getEmptyText().setText("1000");

		responseLimit.setDefaultValue(0);
		responseLimit.setMinValue(0);
		responseLimit.setMaxValue(100000);
		responseLimit.getEmptyText().setText("0");

		wrapLdifLines.setMinValue(0);
		wrapLdifLines.setMaxValue(100000);
		wrapLdifLines.setDefaultValue(80);
		wrapLdifLines.getEmptyText().setText("80");

		wrapAttributesBy.setDefaultValue(10);
		wrapAttributesBy.setMinValue(2);
		wrapAttributesBy.getEmptyText().setText("10");

		wrapChildrenBy.setDefaultValue(1000);
		wrapChildrenBy.setMaxValue(10000);
		wrapChildrenBy.setMinValue(0);
		wrapChildrenBy.getEmptyText().setText("1000");

		sortBy.setModel(new CollectionComboBoxModel<>(Arrays.asList("Attribute type","Attribute value")));

		aliasDerefComboBox.setModel(new CollectionComboBoxModel<>(Arrays.asList(AliasDerefMode.values())));
		aliasDerefComboBox.setRenderer(new SimpleListCellRenderer<AliasDerefMode>() {
			@Override
			public void customize(@NotNull JList<? extends AliasDerefMode> list, AliasDerefMode value, int index, boolean selected, boolean hasFocus) {
				if (AliasDerefMode.DEREF_ALWAYS.equals(value)) setText("Always");
				if (AliasDerefMode.DEREF_FINDING_BASE_OBJ.equals(value)) setText("Finding base");
				if (AliasDerefMode.DEREF_IN_SEARCHING.equals(value)) setText("In searching");
				if (AliasDerefMode.NEVER_DEREF_ALIASES.equals(value)) setText("Never");
			}
		});

		updateCommentLabels(comment1);
		updateCommentLabels(comment2);
		updateCommentLabels(comment3);
		updateCommentLabels(comment4);
		updateCommentLabels(comment5);
		updateCommentLabels(comment6);

	}

	private void updateCommentLabels(JBLabel label) {
		label.setFont(UIUtil.getLabelFont(UIUtil.FontSize.SMALL));
		label.setForeground(UIUtil.getContextHelpForeground());
	}

	public JComponent getContent() {
		return wrapper;
	}

	public void loadSettings(LdapPluginSettings settings) {

		maxConnections.setValue(settings.getMaxConnections());
		idleConnections.setValue(settings.getMinIdleConnections());
		connectionTimeout.setValue(settings.getConnectionTimeout());

		requestTimeout.setValue(settings.getRequestTimeout());
		pageSize.setValue(settings.getPageSize());
		responseLimit.setValue(settings.getRequestLimit());
		aliasDerefComboBox.setSelectedItem(settings.getAliasDerefMode());

		fetchUserAttributes.setSelected(settings.isFetchUserAttributes());
		fetchOperationAttributes.setSelected(settings.isFetchOperationAttributes());
		fetchOnlyAttributes.setText(StringUtils.join(settings.getFetchOnlyAttributes(),","));

		updateLdap.setSelected(settings.isUpdateLdap());
		compactTable.setSelected(settings.isUseCompactTable());
		showDecoratedValues.setSelected(settings.isShowDecoratedValues());

		sortLdifAttributes.setSelected(settings.isSortLdifAttributes());
		exportPassword.setSelected(settings.isAllowExportUserPassword());
		wrapLdifLines.setValue(settings.getWrapLdifLines());

		ascending.setSelected(settings.isSortAscending());
		descending.setSelected(!settings.isSortAscending());

		sortMUSTFirst.setSelected(settings.isSortMustAttributeFirst());
		sortObjectClassFirst.setSelected(settings.isSortObjectClassFirst());
		sortOperationalAttributesLast.setSelected(settings.isSortOperationalAttributesLast());

		wrapChildrenBy.setValue(settings.getWrapChildrenBy());
		wrapAttributesBy.setValue(settings.getWrapAttributesBy());

	}

	public void saveSettings(LdapPluginSettings settings) {

		// connection
		settings.setMaxConnections(maxConnections.getValue());
		settings.setMinIdleConnections(idleConnections.getValue());
		settings.setConnectionTimeout(connectionTimeout.getValue());

		// fetching entries and attributes / tree-view
		settings.setRequestTimeout(requestTimeout.getValue());
		settings.setRequestLimit(responseLimit.getValue());
		settings.setPageSize(pageSize.getValue());
		settings.setAliasDerefMode((AliasDerefMode) aliasDerefComboBox.getSelectedItem());
		settings.setFetchUserAttributes(fetchUserAttributes.isSelected());
		settings.setFetchOperationAttributes(fetchOperationAttributes.isSelected());

		Set<String> fetchOnlyAttributesSet = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
		if (!fetchOnlyAttributes.getText().trim().isEmpty()) {
			fetchOnlyAttributesSet.clear();
			fetchOnlyAttributesSet.addAll(Arrays.asList(fetchOnlyAttributes.getText().split("\\s*,\\s*")));
		} else {
			fetchOnlyAttributesSet.clear();
		}
		settings.setFetchOnlyAttributes(fetchOnlyAttributesSet);
		settings.setWrapChildrenBy(wrapChildrenBy.getValue());

		// Entry / Value editor
		settings.setUpdateLdap(updateLdap.isSelected());
		settings.setUseCompactTable(compactTable.isSelected());
		settings.setShowDecoratedValues(showDecoratedValues.isSelected());

		String sortBy = (String) this.sortBy.getSelectedItem();
		settings.setSortByAttributeType(Objects.equals(sortBy, "Attribute type"));
		settings.setSortAscending(ascending.isSelected());

		settings.setSortMustAttributeFirst(sortMUSTFirst.isSelected());
		settings.setSortObjectClassFirst(sortObjectClassFirst.isSelected());
		settings.setSortOperationalAttributesLast(sortOperationalAttributesLast.isSelected());

		settings.setWrapAttributesBy(wrapAttributesBy.getValue());

		// LDIF export
		settings.setSortLdifAttributes(sortLdifAttributes.isSelected());
		settings.setWrapLdifLines(wrapLdifLines.getValue());
		settings.setAllowExportUserPassword(exportPassword.isSelected());

	}

	public boolean isModified(LdapPluginSettings settings) {

		// fetching entries and attributes / tree-view
		if (settings.getRequestTimeout() != requestTimeout.getValue()) return true;
		if (settings.getPageSize() != pageSize.getValue()) return true;
		if (settings.getRequestLimit() != responseLimit.getValue()) return true;
		if (!Objects.equals(settings.getAliasDerefMode(), aliasDerefComboBox.getSelectedItem())) return true;
		if (settings.isFetchUserAttributes() != fetchUserAttributes.isSelected()) return true;
		if (settings.isFetchOperationAttributes() != fetchOperationAttributes.isSelected()) return true;
		if (settings.getWrapChildrenBy() != wrapChildrenBy.getValue()) return true;

		Set<String> fetchOnlyAttributesSet = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
		if (!fetchOnlyAttributes.getText().trim().isEmpty()) {
			fetchOnlyAttributesSet.clear();
			fetchOnlyAttributesSet.addAll(Arrays.asList(fetchOnlyAttributes.getText().split("\\s*,\\s*")));
		} else {
			fetchOnlyAttributesSet.clear();
		}
		if (!Objects.equals(settings.getFetchOnlyAttributes(), fetchOnlyAttributesSet)) return true;

		// Entry / Value editor
		if (settings.isUpdateLdap() != updateLdap.isSelected()) return true;
		if (settings.isUseCompactTable() != compactTable.isSelected()) return true;
		if (settings.isShowDecoratedValues() != showDecoratedValues.isSelected()) return true;

		String sortBy = (String) this.sortBy.getSelectedItem();
		if (settings.isSortByAttributeType() != Objects.equals(sortBy, "Attribute type")) return true;
		if (settings.isSortAscending() != ascending.isSelected()) return true;

		if (settings.isSortMustAttributeFirst() != sortMUSTFirst.isSelected()) return true;
		if (settings.isSortObjectClassFirst() != sortObjectClassFirst.isSelected()) return true;
		if (settings.isSortOperationalAttributesLast() != sortOperationalAttributesLast.isSelected()) return true;

		if (settings.getWrapAttributesBy() != wrapAttributesBy.getValue()) return true;

		// LDIF export
		if (settings.isSortLdifAttributes() != sortLdifAttributes.isSelected()) return true;
		if (settings.getWrapLdifLines() != wrapLdifLines.getValue()) return true;
		if (settings.isAllowExportUserPassword() != exportPassword.isSelected()) return true;

		return false;

	}

}
