package io.gitlab.zlamalp.intellijplugin.ldap.settings.connection;

import com.intellij.openapi.ui.ValidationInfo;
import com.intellij.ui.CollectionComboBoxModel;
import com.intellij.ui.DocumentAdapter;
import com.intellij.ui.SimpleListCellRenderer;
import com.intellij.ui.components.JBCheckBox;
import com.intellij.ui.components.JBLabel;
import com.intellij.ui.components.fields.ExpandableTextField;
import com.intellij.ui.components.fields.IntegerField;
import com.intellij.util.EventDispatcher;
import com.intellij.util.ui.UIUtil;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapServer;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapServerSettings;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.LdapPluginSettingsService;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.LdapSettings;
import org.apache.commons.lang.StringUtils;
import org.apache.directory.api.ldap.model.message.AliasDerefMode;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

/**
 * This class represents form for viewing and modifying LdapServerConnection advanced settings.
 * It's used inside LdapConnectionTabbedPane with all connections.
 *
 * @see LdapServerConfigTabbedPane (outer wrapper)
 * @see ManageLdapServersDialog (outer dialog)
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdapServerConfigSettingsForm {

	private LdapServerConfigTabbedPane parent;
	private LdapServer ldapServer;
	private JPanel wrapper;
	private JBCheckBox fetchUserAttributes;
	private JBCheckBox fetchOperationAttributes;
	private ExpandableTextField fetchOnlyAttributes;
	private IntegerField wrapChildrenBy;
	private JComboBox<AliasDerefMode> aliasDerefComboBox;
	private com.intellij.ui.components.labels.LinkLabel<LdapServer> resetToDefaultLabel;
	private IntegerField requestTimeout;
	private IntegerField responseLimit;
	private IntegerField pageSize;
	private JBCheckBox overrideIDESettings;
	private JBCheckBox updateLdap;
	private JBCheckBox compactTable;
	private JBCheckBox showDecoratedValues;
	private JComboBox<String> sortBy;
	private JRadioButton ascending;
	private JRadioButton descending;
	private JBCheckBox sortObjectClassFirst;
	private JBCheckBox sortMUSTFirst;
	private JBCheckBox sortOperationalAttributesLast;
	private IntegerField wrapAttributesBy;
	private JBCheckBox sortLdifAttributes;
	private JBCheckBox exportPassword;
	private IntegerField wrapLdifLines;
	private IntegerField connectionTimeout;
	private IntegerField maxConnections;
	private IntegerField idleConnections;
	private JBLabel comment1;
	private JBLabel comment2;
	private JBLabel comment3;
	private JBLabel comment4;
	private JBLabel comment5;
	private JBLabel comment6;

	private final EventDispatcher<ChangeListener> myDispatcher = EventDispatcher.create(ChangeListener.class);
	private boolean loaded = false;
	private boolean isNew = false;

	/**
	 * Creates form for editing LDAP server settings
	 *
	 * @param parent Parent tabbed pane form
	 */
	public LdapServerConfigSettingsForm(@NotNull final LdapServerConfigTabbedPane parent) {

		this.parent = parent;
		this.ldapServer = parent.getLdapServer();

		aliasDerefComboBox.setModel(new CollectionComboBoxModel<>(Arrays.asList(AliasDerefMode.values())));
		addActionListener(aliasDerefComboBox);
		aliasDerefComboBox.setRenderer(new SimpleListCellRenderer<AliasDerefMode>() {
			@Override
			public void customize(@NotNull JList<? extends AliasDerefMode> list, AliasDerefMode value, int index, boolean selected, boolean hasFocus) {
				if (AliasDerefMode.DEREF_ALWAYS.equals(value)) setText("Always");
				if (AliasDerefMode.DEREF_FINDING_BASE_OBJ.equals(value)) setText("Finding base");
				if (AliasDerefMode.DEREF_IN_SEARCHING.equals(value)) setText("In searching");
				if (AliasDerefMode.NEVER_DEREF_ALIASES.equals(value)) setText("Never");
			}
		});

		sortBy.setModel(new CollectionComboBoxModel<>(Arrays.asList("Attribute type","Attribute value")));
		addActionListener(sortBy);

		resetToDefaultLabel.setListener((linkLabel, clickedLdapServer) -> {
			fillFromSettings(LdapPluginSettingsService.getInstance().getState());
			if (loaded) setModified();
		}, ldapServer);
		resetToDefaultLabel.setIcon(null);
		resetToDefaultLabel.setVisible(false);

		overrideIDESettings.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				switchEnabledStateToAll(overrideIDESettings.isSelected());
				if (loaded) setModified();
			}
		});

		connectionTimeout.setMaxValue(60);
		connectionTimeout.setMinValue(2);
		connectionTimeout.setDefaultValue(5);
		connectionTimeout.getEmptyText().setText("5");

		maxConnections.setMaxValue(10);
		maxConnections.setMinValue(1);
		maxConnections.setDefaultValue(3);
		maxConnections.getEmptyText().setText("3");

		idleConnections.setMaxValue(10);
		idleConnections.setMinValue(0);
		idleConnections.setDefaultValue(1);
		idleConnections.getEmptyText().setText("1");

		requestTimeout.setDefaultValue(60);
		requestTimeout.setMinValue(10);
		requestTimeout.setMaxValue(3600);
		requestTimeout.getEmptyText().setText("60");

		pageSize.setDefaultValue(1000);
		pageSize.setMinValue(0);
		pageSize.setMaxValue(100000);
		pageSize.getEmptyText().setText("1000");

		responseLimit.setDefaultValue(0);
		responseLimit.setMinValue(0);
		responseLimit.setMaxValue(100000);
		responseLimit.getEmptyText().setText("0");

		wrapAttributesBy.setDefaultValue(10);
		wrapAttributesBy.setMinValue(2);
		wrapAttributesBy.getEmptyText().setText("10");

		wrapChildrenBy.setDefaultValue(1000);
		wrapChildrenBy.setMaxValue(10000);
		wrapChildrenBy.setMinValue(0);
		wrapChildrenBy.getEmptyText().setText("1000");

		addTextChangedDocumentAdapter(connectionTimeout);
		addTextChangedDocumentAdapter(maxConnections);
		addTextChangedDocumentAdapter(idleConnections);
		addTextChangedDocumentAdapter(pageSize);
		addTextChangedDocumentAdapter(requestTimeout);
		addTextChangedDocumentAdapter(responseLimit);
		addTextChangedDocumentAdapter(fetchOnlyAttributes);
		addTextChangedDocumentAdapter(wrapChildrenBy);

		addActionListener(fetchUserAttributes);
		addActionListener(fetchOperationAttributes);
		addActionListener(updateLdap);
		addActionListener(compactTable);
		addActionListener(showDecoratedValues);

		addActionListener(ascending);
		addActionListener(descending);

		addActionListener(sortMUSTFirst);
		addActionListener(sortObjectClassFirst);
		addActionListener(sortOperationalAttributesLast);

		addTextChangedDocumentAdapter(wrapAttributesBy);

		addActionListener(sortLdifAttributes);
		addActionListener(exportPassword);
		addTextChangedDocumentAdapter(wrapLdifLines);

		updateCommentLabels(comment1);
		updateCommentLabels(comment2);
		updateCommentLabels(comment3);
		updateCommentLabels(comment4);
		updateCommentLabels(comment5);
		updateCommentLabels(comment6);

	}

	private void updateCommentLabels(JBLabel label) {
		label.setFont(UIUtil.getLabelFont(UIUtil.FontSize.SMALL));
		label.setForeground(UIUtil.getContextHelpForeground());
	}


	/**
	 * Load LdapServerConnection details into form
	 *
	 * @param ldapServer Connection to load
	 */
	public void loadConnection(LdapServer ldapServer) {

		this.loaded = false;

		LdapServerSettings settings = ldapServer.getSettings();

		if (isNew || settings == null) {
			// settings were not initialized -> create from plugin config
			ldapServer.setSettings(new LdapServerSettings(LdapPluginSettingsService.getInstance().getState()));
			fillFromSettings(ldapServer.getSettings());
			overrideIDESettings.setSelected(false);
		} else {
			fillFromSettings(settings);
			overrideIDESettings.setSelected(settings.isOverrideIDESettings());
		}

		this.loaded = true;

		// update UI
		setShowResetToIDEdefaults(); // change visibility based on form settings
		switchEnabledStateToAll(overrideIDESettings.isSelected());

	}

	/**
	 * Save details from form into LdapServerConnection
	 *
	 * @param ldapServer Connection to save
	 */
	public void saveConnection(LdapServer ldapServer) {

		LdapServerSettings settings;
		// backup to init ldapServer settings if it was not initialized
		if (ldapServer.getSettings() == null) {
			ldapServer.setSettings(new LdapServerSettings(LdapPluginSettingsService.getInstance().getState()));
		}
		settings = ldapServer.getSettings();

		settings.setOverrideIDESettings(overrideIDESettings.isSelected());

		// ldapServer
		settings.setMaxConnections(maxConnections.getValue());
		settings.setMinIdleConnections(idleConnections.getValue());
		settings.setConnectionTimeout(connectionTimeout.getValue());

		settings.setPageSize(pageSize.getValue());
		settings.setRequestTimeout(requestTimeout.getValue());
		settings.setRequestLimit(responseLimit.getValue());
		settings.setAliasDerefMode((AliasDerefMode) aliasDerefComboBox.getSelectedItem());
		settings.setFetchUserAttributes(fetchUserAttributes.isSelected());
		settings.setFetchOperationAttributes(fetchOperationAttributes.isSelected());

		Set<String> fetchOnlyAttributesSet = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
		if (!fetchOnlyAttributes.getText().trim().isEmpty()) {
			fetchOnlyAttributesSet.clear();
			fetchOnlyAttributesSet.addAll(Arrays.asList(fetchOnlyAttributes.getText().split("\\s*,\\s*")));
		} else {
			fetchOnlyAttributesSet.clear();
		}
		settings.setFetchOnlyAttributes(fetchOnlyAttributesSet);

		settings.setWrapChildrenBy(wrapChildrenBy.getValue());

		// Entry / Value editor
		settings.setUpdateLdap(updateLdap.isSelected());
		settings.setUseCompactTable(compactTable.isSelected());
		settings.setShowDecoratedValues(showDecoratedValues.isSelected());

		String sortBy = (String) this.sortBy.getSelectedItem();
		settings.setSortByAttributeType(Objects.equals(sortBy, "Attribute type"));
		settings.setSortAscending(ascending.isSelected());

		settings.setSortMustAttributeFirst(sortMUSTFirst.isSelected());
		settings.setSortObjectClassFirst(sortObjectClassFirst.isSelected());
		settings.setSortOperationalAttributesLast(sortOperationalAttributesLast.isSelected());

		settings.setWrapAttributesBy(wrapAttributesBy.getValue());

		// LDIF export
		settings.setSortLdifAttributes(sortLdifAttributes.isSelected());
		settings.setWrapLdifLines(wrapLdifLines.getValue());
		settings.setAllowExportUserPassword(exportPassword.isSelected());

		// FIXME - add rest of config options

	}

	/**
	 * Return TRUE if form is modified against original provided settings
	 *
	 * @param settings
	 * @return
	 */
	public boolean isModified(LdapSettings settings) {

		if (!loaded) return false;

		// handled by upper dialog wrapper
		//if (isNew) return true;

		if (settings instanceof LdapServerSettings) {
			if (!Objects.equals(overrideIDESettings.isSelected(), ((LdapServerSettings) settings).isOverrideIDESettings()))
				return true;
		}

		// ldapServer
		if (!Objects.equals(maxConnections.getValue(), settings.getMaxConnections())) return true;
		if (!Objects.equals(idleConnections.getValue(), settings.getMinIdleConnections())) return true;
		if (!Objects.equals(connectionTimeout.getValue(), settings.getConnectionTimeout())) return true;

		// fetching entries and attributes
		if (!Objects.equals(pageSize.getValue(), settings.getPageSize())) return true;
		if (!Objects.equals(requestTimeout.getValue(), settings.getRequestTimeout())) return true;
		if (!Objects.equals(responseLimit.getValue(), settings.getRequestLimit())) return true;
		if (!Objects.equals(aliasDerefComboBox.getSelectedItem(), settings.getAliasDerefMode())) return true;
		if (!Objects.equals(fetchUserAttributes.isSelected(), settings.isFetchUserAttributes())) return true;
		if (!Objects.equals(fetchOperationAttributes.isSelected(), settings.isFetchOperationAttributes())) return true;
		if (!Objects.equals(wrapChildrenBy.getValue(), settings.getWrapChildrenBy())) return true;

		// Entry / Value editor
		if (!Objects.equals(settings.isUpdateLdap(), updateLdap.isSelected())) return true;
		if (!Objects.equals(settings.isUseCompactTable(), compactTable.isSelected())) return true;
		if (!Objects.equals(settings.isShowDecoratedValues(), showDecoratedValues.isSelected())) return true;

		String sortBy = (String) this.sortBy.getSelectedItem();
		if (!Objects.equals(settings.isSortByAttributeType(), Objects.equals(sortBy, "Attribute type"))) return true;
		if (!Objects.equals(settings.isSortAscending(), ascending.isSelected())) return true;

		if (!Objects.equals(settings.isSortMustAttributeFirst(), sortMUSTFirst.isSelected())) return true;
		if (!Objects.equals(settings.isSortObjectClassFirst(), sortObjectClassFirst.isSelected())) return true;
		if (!Objects.equals(settings.isSortOperationalAttributesLast(), sortOperationalAttributesLast.isSelected())) return true;

		if (!Objects.equals(settings.getWrapAttributesBy(), wrapAttributesBy.getValue())) return true;

		// LDIF export
		if (!Objects.equals(settings.isSortLdifAttributes(), sortLdifAttributes.isSelected())) return true;
		if (!Objects.equals(settings.getWrapLdifLines(), wrapLdifLines.getValue())) return true;
		if (!Objects.equals(settings.isAllowExportUserPassword(), exportPassword.isSelected())) return true;

		// FIXME - add rest of config options

		return false;

	}

	public boolean isNew() {
		return isNew;
	}

	public void setNew(boolean aNew) {
		isNew = aNew;
	}

	/**
	 * Returns Component representing form
	 *
	 * @return form component
	 */
	public JComponent getContent() {
		return wrapper;
	}

	public void setModified() {
		fireStateChanged();
	}

	public void addChangeListener(ChangeListener listener) {
		myDispatcher.addListener(listener);
	}

	public void removeChangeListener(ChangeListener listener) {
		myDispatcher.removeListener(listener);
	}

	public void fireStateChanged() {
		myDispatcher.getMulticaster().stateChanged(new ChangeEvent(parent));
	}

	/**
	 * Creates text changed listener for form field expecting string value
	 *
	 * @param textField text field to attach listener
	 */
	private void addTextChangedDocumentAdapter(@NotNull JTextField textField) {
		textField.getDocument().addDocumentListener(new DocumentAdapter() {
			@Override
			protected void textChanged(DocumentEvent documentEvent) {
				if (loaded) setModified();
			}
		});
	}

	/**
	 * Creates action listener for checkbox
	 *
	 * @param checkbox checkbox to attach listener
	 */
	private void addActionListener(@NotNull JBCheckBox checkbox) {
		checkbox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (loaded) setModified();
			}
		});
	}

	/**
	 * Creates action listener for combobox
	 *
	 * @param combobox combobox to attach listener
	 */
	private void addActionListener(@NotNull JComboBox combobox) {
		combobox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (loaded) setModified();
			}
		});
	}

	/**
	 * Creates action listener for radio button
	 *
	 * @param rbutton radio button to attach listener
	 */
	private void addActionListener(@NotNull JRadioButton rbutton) {
		rbutton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (loaded) setModified();
			}
		});
	}

	/**
	 * Fills form with "settings" properties
	 */
	private void fillFromSettings(LdapSettings settings) {

		// ldapServer
		maxConnections.setValue(settings.getMaxConnections());
		idleConnections.setValue(settings.getMinIdleConnections());
		connectionTimeout.setValue(settings.getConnectionTimeout());

		// fetching
		pageSize.setValue(settings.getPageSize());
		requestTimeout.setValue(settings.getRequestTimeout());
		responseLimit.setValue(settings.getRequestLimit());

		aliasDerefComboBox.setSelectedItem(settings.getAliasDerefMode());
		fetchUserAttributes.setSelected(settings.isFetchUserAttributes());
		fetchOperationAttributes.setSelected(settings.isFetchOperationAttributes());
		fetchOnlyAttributes.setText(StringUtils.join(settings.getFetchOnlyAttributes(),","));
		wrapChildrenBy.setValue(settings.getWrapChildrenBy());

		// editor
		updateLdap.setSelected(settings.isUpdateLdap());
		compactTable.setSelected(settings.isUseCompactTable());
		showDecoratedValues.setSelected(settings.isShowDecoratedValues());

		ascending.setSelected(settings.isSortAscending());
		descending.setSelected(!settings.isSortAscending());

		sortMUSTFirst.setSelected(settings.isSortMustAttributeFirst());
		sortObjectClassFirst.setSelected(settings.isSortObjectClassFirst());
		sortOperationalAttributesLast.setSelected(settings.isSortOperationalAttributesLast());

		wrapAttributesBy.setValue(settings.getWrapAttributesBy());

		// ldif
		exportPassword.setSelected(settings.isAllowExportUserPassword());
		wrapLdifLines.setValue(settings.getWrapLdifLines());
		sortLdifAttributes.setSelected(settings.isSortLdifAttributes());

		// FIXME - get rest of values from ldapServer

	}

	/**
	 * Trigger decision, if "Reset to IDE default" should be visible or not
	 */
	public void setShowResetToIDEdefaults() {
		resetToDefaultLabel.setVisible(loaded && !isSameAsIDEdefaults() && overrideIDESettings.isSelected());
	}

	/**
	 * Return TRUE if LDAP server ldapServer settings reflect current IDE defaults
	 *
	 * @return TRUE if config is same as IDE default
	 */
	public boolean isSameAsIDEdefaults() {
		return !isModified(LdapPluginSettingsService.getInstance().getState());
	}

	/**
	 * Switch enabled/disabled state of all options in configuration
	 *
	 * @param enable TRUE to enable
	 */
	private void switchEnabledStateToAll(boolean enable) {

		for (Component c : wrapper.getComponents()) {
			c.setEnabled(enable);
		}
		overrideIDESettings.setEnabled(true);

	}

	public List<ValidationInfo> doValidateAll() {
		// TODO - implement
		List<ValidationInfo> result = new ArrayList<>();
		return result;
	}

}
