package io.gitlab.zlamalp.intellijplugin.ldap.ui.components;

import com.intellij.ui.ColorUtil;
import com.intellij.ui.JBColor;
import com.intellij.util.xmlb.annotations.Transient;
import com.sun.istack.NotNull;
import org.apache.commons.lang.StringUtils;

import javax.swing.*;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Our color settings storage element. It holds colors name and value.
 *
 * @see io.gitlab.zlamalp.intellijplugin.ldap.LdapServer
 * @see io.gitlab.zlamalp.intellijplugin.ldap.settings.connection.LdapServerConfigTabbedPane
 *
 * @author Pavel Zlámal
 */
public class ColorNamePair implements Serializable {

	@Transient
	public final static Map<String, JBColor> defaultColors = new HashMap<String,JBColor>();

	static {
		defaultColors.put("Blue", JBColor.namedColor("FileColor.Blue", new JBColor(0xeaf6ff, 0x4f556b)));
		defaultColors.put("Green", JBColor.namedColor("FileColor.Green", new JBColor(0xeffae7, 0x49544a)));
		defaultColors.put("Orange", JBColor.namedColor("FileColor.Orange", new JBColor(0xf6e9dc, 0x806052)));
		defaultColors.put("Rose", JBColor.namedColor("FileColor.Rose", new JBColor(0xf2dcda, 0x6e535b)));
		defaultColors.put("Violet", JBColor.namedColor("FileColor.Violet", new JBColor(0xe6e0f1, 0x534a57)));
		defaultColors.put("Yellow", JBColor.namedColor("FileColor.Yellow", new JBColor(0xffffe4, 0x4f4b41)));
		defaultColors.put("Gray", JBColor.namedColor("FileColor.Gray", new JBColor(0xf5f5f5, 0x45484a)));
	}

	private String name;
	private String color;

	public ColorNamePair() {
	}

	public ColorNamePair(@NotNull String name, JBColor color) {
		setJBColor(name, color);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getColor() {
		return this.color;
	}

	@Transient
	public void setJBColor(@NotNull String name, JBColor color) {
		this.name = name;
		if ("No color".equals(name)) {
			this.color = null;
		} else if ("Custom...".equals(name) && color != null) {
			this.color = ColorUtil.toHex(color);
		} else {
			this.color = null;
		}
	}

	@Transient
	public JBColor getJBColor() {

		if ("No color".equals(name)) {
			return JBColor.WHITE;
		} else if ("Custom...".equals(name)) {
			if (StringUtils.isNotBlank(color)) {
				return new JBColor(ColorUtil.fromHex(color), ColorUtil.fromHex(color));
			} else {
				return JBColor.WHITE;
			}
		} else {
			return defaultColors.get(this.name);
		}

	}

	@Transient
	public Icon getIcon() {
		return new CircleColorIcon(16, getJBColor(), true);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		ColorNamePair that = (ColorNamePair) o;
		return Objects.equals(name, that.name) &&
				Objects.equals(color, that.color);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, color);
	}

}
