package io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers;

import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.AttributeValueEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.BinaryValueEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.Value;
import org.apache.directory.api.ldap.model.exception.LdapInvalidAttributeValueException;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.util.Objects;

/**
 * Value provider for objectGUID attribute of MS AD.
 *
 * Value is Binary, but we are able to convert it to human readable string and again valid input parse to Binary form.
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class ObjectGuidValueProvider extends BinaryValueProvider {

	private static Logger log = LoggerFactory.getLogger(ObjectGuidValueProvider.class);

	public ObjectGuidValueProvider(@NotNull LdapEntryTreeNode node, @NotNull Attribute attribute, Value value) {
		super(node, attribute, value);
		if (!Objects.equals(attribute.getId(), "objectguid")) {
			throw new IllegalArgumentException("You can't use ObjectGuidValueProvider to edit value of "+attribute.getUpId()+" attribute.");
		}
		if (value != null && value.isHumanReadable()) {
			throw new IllegalArgumentException("Value passed to ObjectGuidValueProvider must be binary.");
		}
	}

	@Override
	public String getStringValue() {

		if (getRawValue() == null) return null;
		if (getRawValue().isNull()) return null;

		return convertToString(getRawValue().getBytes());

	}

	@Override
	public Value constructNewValue(String stringValue) throws LdapInvalidAttributeValueException {
		return constructNewValue(convertToBytes(stringValue));
	}

	@Override
	public String convertToString(byte[] bytes) {

		return prefixZeros((int) bytes[3] & 0xFF) +
				prefixZeros((int) bytes[2] & 0xFF) +
				prefixZeros((int) bytes[1] & 0xFF) +
				prefixZeros((int) bytes[0] & 0xFF) +
				"-" +
				prefixZeros((int) bytes[5] & 0xFF) +
				prefixZeros((int) bytes[4] & 0xFF) +
				"-" +
				prefixZeros((int) bytes[7] & 0xFF) +
				prefixZeros((int) bytes[6] & 0xFF) +
				"-" +
				prefixZeros((int) bytes[8] & 0xFF) +
				prefixZeros((int) bytes[9] & 0xFF) +
				"-" +
				prefixZeros((int) bytes[10] & 0xFF) +
				prefixZeros((int) bytes[11] & 0xFF) +
				prefixZeros((int) bytes[12] & 0xFF) +
				prefixZeros((int) bytes[13] & 0xFF) +
				prefixZeros((int) bytes[14] & 0xFF) +
				prefixZeros((int) bytes[15] & 0xFF);

	}

	@Override
	public byte[] convertToBytes(String string) {
		throw new UnsupportedOperationException("We can't store objectGUID value yet!");
	}

	private static String prefixZeros(int value) {
		if (value <= 0xF) {
			StringBuilder sb = new StringBuilder("0");
			sb.append(Integer.toHexString(value));

			return sb.toString();

		} else {
			return Integer.toHexString(value);
		}
	}

	@Override
	public AttributeValueEditor getValueEditor(Component component, boolean addingNewValue) {
		return new BinaryValueEditor(component, this, addingNewValue);
	}

}
