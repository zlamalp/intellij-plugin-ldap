package io.gitlab.zlamalp.intellijplugin.ldap.actions;

import com.intellij.LdapBundle;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.vfs.VirtualFile;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapActionUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.LdapTreePanel;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapUIUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeType;
import io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeVirtualFile;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapServerTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.ui.LdapNotificationHandler;
import io.gitlab.zlamalp.intellijplugin.ldap.ui.RenameEntryDialog;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.api.ldap.model.name.Dn;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.io.IOException;

/**
 * Rename selected LdapTreeNode in LdapTreePanel or Entry directly from opened editor.
 *
 * @see LdapTreePanel
 * @see LdapEntryTreeNode
 * @see LdapServerTreeNode
 *
 * @author Pavel Zlámal
 */
public class RenameEntryAction extends AnAction {

	public static final String ID = "io.gitlab.zlamalp.intellijplugin.ldap.rename";
	private static Logger log = LoggerFactory.getLogger(RenameEntryAction.class);

	public RenameEntryAction() {
	}

	public RenameEntryAction(@Nls(capitalization = Nls.Capitalization.Title) @Nullable String text, @Nls(capitalization = Nls.Capitalization.Sentence) @Nullable String description, @Nullable Icon icon) {
		super(text, description, icon);
	}

	@Override
	public void actionPerformed(@NotNull AnActionEvent e) {

		if (!isActionActive(e)) return;

		LdapTreePanel treePanel = LdapActionUtils.getLdapTreePanel(e);

		if (LdapActionUtils.isFromTree(e)) {

			LdapEntryTreeNode[] selectedTreeNodes = treePanel.getTree().getSelectedNodes(LdapEntryTreeNode.class,
					node -> node.getNodeType().equals(LdapNodeType.NODE) || node.getNodeType().equals(LdapNodeType.BASE));
			if (selectedTreeNodes.length == 1) {
				performAction(treePanel, selectedTreeNodes[0]);
			}

		} else if (LdapActionUtils.isFromEditor(e)) {

			LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
			if (editor != null) {
				performAction(treePanel, editor.getLdapNode());
			}

		}

	}

	@Override
	public void update(AnActionEvent e) {
		e.getPresentation().setEnabled(isActionActive(e));
	}

	/**
	 * Return TRUE if RenameEntry action should be active.
	 *
	 * @param e triggered event
	 * @return TRUE if action should be active
	 */
	private boolean isActionActive(AnActionEvent e) {

		if (!LdapActionUtils.isProjectActive(e)) return false;
		if (LdapActionUtils.isFromTree(e)) {
			LdapTreePanel treePanel = LdapActionUtils.getLdapTreePanel(e);
			LdapEntryTreeNode[] selectedTreeNodes = treePanel.getTree().getSelectedNodes(LdapEntryTreeNode.class,
					node -> node.getNodeType().equals(LdapNodeType.NODE) || node.getNodeType().equals(LdapNodeType.BASE));
			return selectedTreeNodes.length == 1 &&
					((selectedTreeNodes[0].getEditor() != null) ?
					!selectedTreeNodes[0].getEditor().isLocallyReadOnly() :
					!selectedTreeNodes[0].getLdapServer().isReadOnly());
		} else if (LdapActionUtils.isFromEditor(e)) {
			LdapEntryEditor editor = LdapActionUtils.getEntryEditor(e);
			if (editor != null && !editor.isLoading()) {
				LdapEntryTreeNode node = editor.getLdapNode();
				if (node.getNodeType().equals(LdapNodeType.NODE) || node.getNodeType().equals(LdapNodeType.BASE)) {
					return (node.getEditor() != null) ? !node.getEditor().isLocallyReadOnly() : !node.getLdapServer().isReadOnly();
				}
			}
		}
		return false;

	}

	/**
	 * Perform Rename Entry action on selected node
	 *
	 * @param treePanel LdapTreePanel to refresh nodes in
	 * @param node Node to refresh
	 */
	private void performAction(@NotNull LdapTreePanel treePanel, @NotNull LdapEntryTreeNode node) {

		RenameEntryDialog dialog = new RenameEntryDialog(treePanel.getProject(), true, node);
		if (dialog.showAndGet()) {

			// change to new DN
			Dn newDN = dialog.getNewDn();
			if (newDN != null) {

				Task task = new Task.Modal(treePanel.getProject(),
						LdapBundle.message("ldap.update.modal.renameEntry", node.getDn()),
						true) {

					LdapConnection connection = null;
					Exception exception = null;

					@Override
					public void run(@NotNull ProgressIndicator indicator) {

						try {

							indicator.setText(LdapBundle.message("ldap.update.modal.connecting"));
							indicator.setFraction(0.1);
							LdapUIUtils.waitIfUIDebug();
							indicator.checkCanceled();
							connection = node.getLdapServer().getConnection();

							indicator.setFraction(0.5);
							indicator.setText(LdapBundle.message("ldap.update.modal.renamingEntry", newDN.getRdn()));
							LdapUIUtils.waitIfUIDebug();
							indicator.checkCanceled();
							log.info("Renaming: {} , new DN: {} , deleteOld: {}", node.getDn(), newDN, dialog.isDeleteOld());
							connection.rename(node.getDn(), newDN.getRdn(), dialog.isDeleteOld());
							indicator.setFraction(1.0);

						} catch (LdapException ex) {

							exception = ex;

						}

					}

					@Override
					public void onFinished() {

						// close connection even if user canceled operation
						if (connection != null) {
							try {
								node.getLdapServer().releaseConnection(connection);
							} catch (LdapException e) {
								LdapNotificationHandler.handleError(e, node.getLdapServer().getName(), "Can't release connection.");
							}
						}

					}

					@Override
					public void onSuccess() {

						if (exception != null) {
							LdapNotificationHandler.handleError(exception, node.getLdapServer().getName(), "Entry "+ node.getDn() +" was not renamed.");
						} else {

							// update and refresh local entry
							node.setDn(newDN);
							LdapUtils.refresh(node);

							// rename any opened editor
							VirtualFile[] openedFiles = FileEditorManager.getInstance(treePanel.getProject()).getOpenFiles();
							for (VirtualFile vf : openedFiles) {
								if (vf instanceof LdapNodeVirtualFile) {
									if (vf.equals(node.getVirtualFile())) {
										try {
											vf.rename(node, newDN.getRdn().toString());
											break;
										} catch (IOException e) {
											// we are on virtual file system only - shouldn't happen
										}
									}
								}
							}

						}

					}
				};
				ProgressManager.getInstance().run(task);

			}

		}

	}

}
