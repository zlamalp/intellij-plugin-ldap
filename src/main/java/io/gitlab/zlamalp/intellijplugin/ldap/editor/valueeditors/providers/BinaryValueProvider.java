package io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.providers;

import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.AttributeValueEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.valueeditors.BinaryValueEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.Value;
import org.apache.directory.api.ldap.model.exception.LdapInvalidAttributeValueException;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.directory.api.util.Base64;

import java.awt.*;

/**
 * Basic Binary value editor for LDAP Entry attribute values.
 * Only binary attributes can be used with this provider.
 *
 * Since we can't edit binary value directly, it's encoded to base64 string and expect same on input to save value.
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class BinaryValueProvider extends AbstractValueProvider {

	private static Logger log = LoggerFactory.getLogger(BinaryValueProvider.class);

	public BinaryValueProvider(@NotNull LdapEntryTreeNode node, @NotNull Attribute attribute, Value value) {
		super(node, attribute, value);
		// FIXME - attribute can be binary and human-readable ?? -> eg. cert DNs
		/*
		if (attribute.isHumanReadable() || (value != null && value.isHumanReadable())) {
			throw new IllegalArgumentException(attribute.getUpId() + " is HumanReadable. You can't use BinaryValueEditor to edit its value.");
		}
		*/
	}

	@Override
	public String getStringValue() {

		// FIXME - we override it, since by default values are HR

		// FIXME - we might want to construct simple string if attribute is binary but value is human readable.
		// --> TODO override getDisplayValue() to construct string from bytes if its HR value.

		if (getRawValue() == null) return null;
		if (getRawValue().isNull()) return null;

		return convertToString(getRawValue().getBytes());

	}

	@Override
	public Value constructNewValue(String stringValue) throws LdapInvalidAttributeValueException {
		return constructNewValue(convertToBytes(stringValue));
	}

	@Override
	public String convertToString(byte[] bytes) {
		return new String(Base64.encode(bytes));
	}

	@Override
	public byte[] convertToBytes(String string) {
		return Base64.decode(string.toCharArray());
	}

	@Override
	public AttributeValueEditor getValueEditor(Component component, boolean addingNewValue) {
		return new BinaryValueEditor(component, this, addingNewValue);
	}

}
