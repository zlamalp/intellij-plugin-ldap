package io.gitlab.zlamalp.intellijplugin.ldap.editor.struct;

import com.intellij.navigation.ItemPresentation;
import com.intellij.util.PlatformIcons;
import io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeType;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

/**
 * StructureView presentation of single LDAP Entry attribute or its root if attribute is null.
 *
 * @see LdapNodeStructureViewModel
 * @see LdapNodeStructureViewTreeElement
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdapNodeItemPresentation implements ItemPresentation {

	private LdapEntryTreeNode node;
	private Attribute attribute;

	public LdapNodeItemPresentation(@NotNull LdapEntryTreeNode node, Attribute attribute) {
		this.node = node;
		this.attribute = attribute;
	}

	@Nullable
	@Override
	public String getPresentableText() {
		if (node.getNodeType().equals(LdapNodeType.ROOT_DSE) && attribute == null) return "Root DSE ("+node.getLdapServer().getName()+")";
		return (attribute != null) ? attribute.getUpId() : node.getDn().toString();
	}

	@Nullable
	@Override
	public String getLocationString() {
		// if Entry has attributes, use this to show number of values
		if (attribute != null) {
			return "("+String.format("%,d", attribute.size())+")";
		}
		return null;
	}

	@Nullable
	@Override
	public Icon getIcon(boolean unused) {
		return (attribute != null) ? PlatformIcons.CLASS_INITIALIZER : PlatformIcons.CUSTOM_FILE_ICON;
	}

}
