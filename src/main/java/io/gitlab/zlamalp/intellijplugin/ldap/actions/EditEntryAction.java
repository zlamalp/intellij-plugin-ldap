package io.gitlab.zlamalp.intellijplugin.ldap.actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.project.Project;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapActionUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.LdapTreePanel;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapUtils;
import io.gitlab.zlamalp.intellijplugin.ldap.filetype.LdapNodeVirtualFile;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

/**
 * Edit selected LdapTreeNode from LdapTreePanel in LdapEntryEditor
 * (means opening LdapNodeVirtualFile associated with selected LdapNode).
 *
 * LdapTreeNode attributes are fetched from LDAP
 *
 * @see LdapTreePanel
 * @see LdapEntryTreeNode
 * @see io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditor
 * @see LdapNodeVirtualFile
 *
 * @author Pavel Zlámal
 */
public class EditEntryAction extends AnAction {

	public static final String ID = "io.gitlab.zlamalp.intellijplugin.ldap.edit";

	public EditEntryAction() {
	}

	public EditEntryAction(@Nls(capitalization = Nls.Capitalization.Title) @Nullable String text, @Nls(capitalization = Nls.Capitalization.Sentence) @Nullable String description, @Nullable Icon icon) {
		super(text, description, icon);
	}

	@Override
	public void actionPerformed(@NotNull AnActionEvent e) {

		if (!isActionActive(e)) return;

		LdapTreePanel treePanel = LdapActionUtils.getLdapTreePanel(e);
		LdapEntryTreeNode[] selectedTreeNodes = treePanel.getTree().getSelectedNodes(LdapEntryTreeNode.class, null);
		for (LdapEntryTreeNode node : selectedTreeNodes) {
			performAction(treePanel.getProject(), node);
		}

	}

	@Override
	public void update(AnActionEvent e) {
		e.getPresentation().setEnabled(isActionActive(e));
	}

	/**
	 * Return TRUE if EditEntry action is allowed for selected LdapNode.
	 *
	 * @param e Event triggering this action
	 * @return TRUE if Action is active or FALSE
	 */
	private boolean isActionActive(AnActionEvent e) {

		if (e.getProject() == null || e.getProject().isDisposed()) return false;
		if (LdapActionUtils.isFromTree(e)) {
			LdapTreePanel treePanel = LdapActionUtils.getLdapTreePanel(e);
			LdapEntryTreeNode[] selectedTreeNodes = treePanel.getTree().getSelectedNodes(LdapEntryTreeNode.class, null);
			return selectedTreeNodes.length > 0;
		}
		return false;

	}

	/**
	 * Opens LdapEntryEditor for selected entry
	 *
	 * @param project source project
	 * @param node node to open LdapEntryEditor for
	 */
	private void performAction(@NotNull Project project, @NotNull LdapEntryTreeNode node) {

		// reloading all attributes in entry
		LdapUtils.refresh(node);

		// edit single entry in file
		FileEditorManager.getInstance(project).openFile(node.getVirtualFile(), true, true);

	}

}
