package io.gitlab.zlamalp.intellijplugin.ldap.schema;

import org.apache.directory.api.ldap.model.constants.SchemaConstants;
import org.apache.directory.api.ldap.model.entry.Attribute;
import org.apache.directory.api.ldap.model.entry.Entry;
import org.apache.directory.api.ldap.model.entry.Value;
import org.apache.directory.api.ldap.model.schema.AttributeType;
import org.apache.directory.api.ldap.model.schema.LdapComparator;
import org.apache.directory.api.ldap.model.schema.LdapSyntax;
import org.apache.directory.api.ldap.model.schema.MatchingRule;
import org.apache.directory.api.ldap.model.schema.MatchingRuleUse;
import org.apache.directory.api.ldap.model.schema.Normalizer;
import org.apache.directory.api.ldap.model.schema.ObjectClass;
import org.apache.directory.api.ldap.model.schema.ObjectClassTypeEnum;
import org.apache.directory.api.ldap.model.schema.SyntaxChecker;
import org.apache.directory.api.ldap.model.schema.UsageEnum;
import org.apache.directory.api.ldap.model.schema.comparators.BitStringComparator;
import org.apache.directory.api.ldap.model.schema.comparators.BooleanComparator;
import org.apache.directory.api.ldap.model.schema.comparators.ByteArrayComparator;
import org.apache.directory.api.ldap.model.schema.comparators.CertificateComparator;
import org.apache.directory.api.ldap.model.schema.comparators.ComparableComparator;
import org.apache.directory.api.ldap.model.schema.comparators.CsnComparator;
import org.apache.directory.api.ldap.model.schema.comparators.CsnSidComparator;
import org.apache.directory.api.ldap.model.schema.comparators.DeepTrimComparator;
import org.apache.directory.api.ldap.model.schema.comparators.DeepTrimToLowerComparator;
import org.apache.directory.api.ldap.model.schema.comparators.DnComparator;
import org.apache.directory.api.ldap.model.schema.comparators.GeneralizedTimeComparator;
import org.apache.directory.api.ldap.model.schema.comparators.IntegerComparator;
import org.apache.directory.api.ldap.model.schema.comparators.LongComparator;
import org.apache.directory.api.ldap.model.schema.comparators.NumericStringComparator;
import org.apache.directory.api.ldap.model.schema.comparators.ObjectClassTypeComparator;
import org.apache.directory.api.ldap.model.schema.comparators.ObjectIdentifierComparator;
import org.apache.directory.api.ldap.model.schema.comparators.ObjectIdentifierFirstComponentComparator;
import org.apache.directory.api.ldap.model.schema.comparators.StringComparator;
import org.apache.directory.api.ldap.model.schema.comparators.TelephoneNumberComparator;
import org.apache.directory.api.ldap.model.schema.comparators.UniqueMemberComparator;
import org.apache.directory.api.ldap.model.schema.comparators.UuidComparator;
import org.apache.directory.api.ldap.model.schema.comparators.WordComparator;
import org.apache.directory.api.ldap.model.schema.normalizers.BooleanNormalizer;
import org.apache.directory.api.ldap.model.schema.normalizers.DeepTrimNormalizer;
import org.apache.directory.api.ldap.model.schema.normalizers.DeepTrimToLowerNormalizer;
import org.apache.directory.api.ldap.model.schema.normalizers.DnNormalizer;
import org.apache.directory.api.ldap.model.schema.normalizers.GeneralizedTimeNormalizer;
import org.apache.directory.api.ldap.model.schema.normalizers.NameOrNumericIdNormalizer;
import org.apache.directory.api.ldap.model.schema.normalizers.NoOpNormalizer;
import org.apache.directory.api.ldap.model.schema.normalizers.NumericNormalizer;
import org.apache.directory.api.ldap.model.schema.normalizers.ObjectIdentifierNormalizer;
import org.apache.directory.api.ldap.model.schema.normalizers.TelephoneNumberNormalizer;
import org.apache.directory.api.ldap.model.schema.normalizers.UniqueMemberNormalizer;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.AccessPointSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.AttributeTypeDescriptionSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.AttributeTypeUsageSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.AudioSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.BinarySyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.BitStringSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.BooleanSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.CertificateListSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.CertificatePairSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.CertificateSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.ComparatorSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.CountrySyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.CsnSidSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.CsnSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.DataQualitySyntaxSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.DeliveryMethodSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.DerefAliasSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.DirectoryStringSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.DitContentRuleDescriptionSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.DitStructureRuleDescriptionSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.DlSubmitPermissionSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.DnSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.DsaQualitySyntaxSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.DseTypeSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.EnhancedGuideSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.FacsimileTelephoneNumberSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.FaxSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.GeneralizedTimeSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.GuideSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.Ia5StringSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.IntegerSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.JavaByteSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.JavaIntegerSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.JavaLongSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.JavaShortSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.JpegSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.LdapSyntaxDescriptionSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.MailPreferenceSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.MasterAndShadowAccessPointSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.MatchingRuleDescriptionSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.MatchingRuleUseDescriptionSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.MhsOrAddressSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.NameAndOptionalUIDSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.NameFormDescriptionSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.NormalizerSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.NumberSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.NumericOidSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.NumericStringSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.ObjectClassDescriptionSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.ObjectClassTypeSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.ObjectNameSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.OctetStringSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.OidLenSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.OtherMailboxSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.PostalAddressSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.PresentationAddressSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.PrintableStringSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.ProtocolInformationSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.SearchScopeSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.SubstringAssertionSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.SubtreeSpecificationSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.SupplierAndConsumerSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.SupplierInformationSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.SupplierOrConsumerSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.SupportedAlgorithmSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.SyntaxCheckerSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.TelephoneNumberSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.TeletexTerminalIdentifierSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.TelexNumberSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.UtcTimeSyntaxChecker;
import org.apache.directory.api.ldap.model.schema.syntaxCheckers.UuidSyntaxChecker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

/**
 * Best effort schema parser to manually construct Apache Directory API schema objects to ease
 * schema validation resolving on client side.
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdapSchemaParser {

	/**
	 * Parse "ldapSyntaxes" attribute values from Entry containing schema.
	 *
	 * @param entry Entry containing schema information
	 * @return List of found LdapSyntaxes
	 */
	public static List<LdapSyntax> parseLdapSyntaxes(Entry entry) {

		List<LdapSyntax> syntaxList = new ArrayList<>();
		if (entry == null) return syntaxList;

		for (String ldapSyntaxSourceValue : getValuesOfSchemaAttribute(entry.get(SchemaConstants.LDAP_SYNTAXES_AT))) {

			if (ldapSyntaxSourceValue == null || ldapSyntaxSourceValue.trim().isEmpty())
				throw new IllegalArgumentException("LdapSyntaxes definition must not be NULL or empty.");
			if (!ldapSyntaxSourceValue.startsWith("(") && !ldapSyntaxSourceValue.endsWith(")"))
				throw new IllegalArgumentException("LdapSyntaxes definition must be enclosed in '(' ').");

			List<String> lineParams = Arrays.asList(ldapSyntaxSourceValue.split("\\s+|(?=\\W'|'\\W)|(?<=\\W'|'\\W})"));

			LdapSyntax syntax = new LdapSyntax(lineParams.get(1));

			int descIndex = lineParams.indexOf("DESC");
			if (descIndex != -1) {

				if (Objects.equals("'", lineParams.get(descIndex+1))) {

					// multi value
					List<String> values = new ArrayList<>();
					int index = descIndex+2;
					while (!Objects.equals("'", lineParams.get(index))) {
						values.add(lineParams.get(index));
						index++;
					}
					syntax.setDescription(printListWithWhitespace(values));

				}

			}

			int binaryTransfer = lineParams.indexOf("X-BINARY-TRANSFER-REQUIRED");
			if (binaryTransfer != -1 && binaryTransfer > descIndex) {
				if (Objects.equals("'", lineParams.get(binaryTransfer+1))) {
					// binary transfer
					syntax.getExtensions().putIfAbsent("X-BINARY-TRANSFER-REQUIRED", toList(lineParams.get(binaryTransfer+2)));
				}
			}

			int humanReadable = lineParams.indexOf("X-NOT-HUMAN-READABLE");
			if (humanReadable != -1 && humanReadable > descIndex) {
				if (Objects.equals("'", lineParams.get(humanReadable+1))) {
					// human readable
					syntax.getExtensions().putIfAbsent("X-NOT-HUMAN-READABLE", toList(lineParams.get(humanReadable+2)));
				}
			}

			syntax.setSyntaxChecker(getSyntaxCheckerByOid(syntax.getOid()));

			syntaxList.add(syntax);

		}

		return syntaxList;

	}


	public static List<MatchingRule> parseMatchingRule(Entry entry) {

		List<MatchingRule> matchingRuleList = new ArrayList<>();
		if (entry == null) return matchingRuleList;

		for (String matchingRuleSourceValue : getValuesOfSchemaAttribute(entry.get(SchemaConstants.MATCHING_RULES_AT))) {

			if (matchingRuleSourceValue == null || matchingRuleSourceValue.trim().isEmpty())
				throw new IllegalArgumentException("MatchingRules definition must not be NULL or empty.");
			if (!matchingRuleSourceValue.startsWith("(") && !matchingRuleSourceValue.endsWith(")"))
				throw new IllegalArgumentException("MatchingRules definition must be enclosed in '(' ').");

			List<String> lineParams = Arrays.asList(matchingRuleSourceValue.split("\\s+|(?=\\W'|'\\W)|(?<=\\W'|'\\W})"));

			MatchingRule rule = new MatchingRule(lineParams.get(1));

			int nameIndex = lineParams.indexOf("NAME");
			if (nameIndex != -1) {

				if (Objects.equals("'", lineParams.get(nameIndex+1))) {

					// multi value
					List<String> values = new ArrayList<>();
					int index = nameIndex+2;
					while (!Objects.equals("'", lineParams.get(index))) {
						values.add(lineParams.get(index));
						index++;
					}
					rule.setNames(printListWithWhitespace(values));

				}

			}

			int syntax = lineParams.indexOf("SYNTAX");
			if (syntax != -1 && syntax > nameIndex) {
				rule.setSyntaxOid(lineParams.get(syntax+1));
			}

			rule.setLdapComparator(getComparatorByOid(rule.getOid()));
			rule.setNormalizer(getNormalizerByOid(rule.getOid()));

			matchingRuleList.add(rule);

		}

		return matchingRuleList;

	}


	public static List<MatchingRuleUse> parseMatchingRuleUse(Entry entry) {

		List<MatchingRuleUse> matchingRuleUseList = new ArrayList<>();
		if (entry == null) return matchingRuleUseList;

		for (String matchingRuleSourceValue : getValuesOfSchemaAttribute(entry.get(SchemaConstants.MATCHING_RULE_USE_AT))) {

			if (matchingRuleSourceValue == null || matchingRuleSourceValue.trim().isEmpty())
				throw new IllegalArgumentException("MatchingRuleUse definition must not be NULL or empty.");
			if (!matchingRuleSourceValue.startsWith("(") && !matchingRuleSourceValue.endsWith(")"))
				throw new IllegalArgumentException("MatchingRuleUse definition must be enclosed in '(' ').");

			List<String> lineParams = Arrays.asList(matchingRuleSourceValue.split("\\s+|(?=\\W'|'\\W)|(?<=\\W'|'\\W})"));

			MatchingRuleUse ruleUse = new MatchingRuleUse(lineParams.get(1));

			int descIndex = lineParams.indexOf("NAME");
			if (descIndex != -1) {

				if (Objects.equals("'", lineParams.get(descIndex+1))) {

					// multi value
					List<String> values = new ArrayList<>();
					int index = descIndex+2;
					while (!Objects.equals("'", lineParams.get(index))) {
						values.add(lineParams.get(index));
						index++;
					}
					ruleUse.setNames(printListWithWhitespace(values));

				}

			}

			int appliesIndex = lineParams.indexOf("APPLIES");
			if (appliesIndex != -1 && (appliesIndex > descIndex)) {

				if (lineParams.get(appliesIndex+1).startsWith("(")) {
					// multi value applies param
					List<String> values = new ArrayList<>();
					int index = appliesIndex+1;
					if (Objects.equals("(", lineParams.get(appliesIndex+1))) {
						index++;
					} else {
						// crop first value, since AD schema doesn't have leading whitespace
						values.add(lineParams.get(appliesIndex+1).substring(1));
						index++;
					}

					while (!Objects.equals(")", lineParams.get(index))) {
						if (!Objects.equals("$", lineParams.get(index))) {
							// skip value separator $
							values.add(lineParams.get(index));
						}
						index++;
					}
					ruleUse.setApplicableAttributeOids(values);

				} else {

					// single applies attribute
					ruleUse.setApplicableAttributeOids(toList(lineParams.get(appliesIndex+1)));

				}

			}

			matchingRuleUseList.add(ruleUse);

		}

		return matchingRuleUseList;

	}

	public static List<AttributeType> parseAttributeTypes(Entry entry) {

		List<AttributeType> attributeTypeList = new ArrayList<>();
		if (entry == null) return attributeTypeList;

		for (String matchingRuleSourceValue : getValuesOfSchemaAttribute(entry.get(SchemaConstants.ATTRIBUTE_TYPES_AT))) {

			if (matchingRuleSourceValue == null || matchingRuleSourceValue.trim().isEmpty())
				throw new IllegalArgumentException("AttributeTypes definition must not be NULL or empty.");
			if (!matchingRuleSourceValue.startsWith("(") && !matchingRuleSourceValue.endsWith(")"))
				throw new IllegalArgumentException("AttributeTypes definition must be enclosed in '(' ').");

			List<String> lineParams = Arrays.asList(matchingRuleSourceValue.split("\\s+|(?=\\W'|'\\W)|(?<=\\W'|'\\W})"));

			AttributeType attributeType = new AttributeType(lineParams.get(1));

			int nameIndex = lineParams.indexOf("NAME");
			if (nameIndex != -1) {


				if (Objects.equals("'", lineParams.get(nameIndex+1))) {

					// single value
					attributeType.setNames(toList(lineParams.get(nameIndex+2)));

				} else if (Objects.equals("(", lineParams.get(nameIndex+1))) {
					// multi value
					List<String> values = new ArrayList<>();
					int index = nameIndex+2;
					while (!Objects.equals(")", lineParams.get(index))) {
						if (!Objects.equals("'", lineParams.get(index))) {
							// skip value enclosing appostroph
							values.add(lineParams.get(index));
						}
						index++;
					}
					attributeType.setNames(values);

				}

			}

			int descIndex = lineParams.indexOf("DESC");
			if (descIndex != -1 && descIndex > nameIndex) {

				if (Objects.equals("'", lineParams.get(descIndex+1))) {

					// multi value
					List<String> values = new ArrayList<>();
					int index = descIndex+2;
					while (!Objects.equals("'", lineParams.get(index))) {
						values.add(lineParams.get(index));
						index++;
					}
					attributeType.setDescription(printListWithWhitespace(values));

				}

			}

			int supIndex = lineParams.indexOf("SUP");
			if (supIndex != -1 && supIndex > descIndex) {
				attributeType.setSuperior(lineParams.get(supIndex+1));
			}

			int equalityIndex = lineParams.indexOf("EQUALITY");
			if (equalityIndex != -1 && equalityIndex > supIndex) {
				attributeType.setEqualityOid(lineParams.get(equalityIndex+1));
			}

			int orderingIndex = lineParams.indexOf("ORDERING");
			if (orderingIndex != -1 && orderingIndex > equalityIndex) {
				if (Objects.equals("'", lineParams.get(orderingIndex+1))) {
					// single value
					attributeType.setOrderingOid(lineParams.get(orderingIndex+2));
				}
			}

			int substrIndex = lineParams.indexOf("SUBSTR");
			if (substrIndex != -1 && substrIndex > orderingIndex) {
				attributeType.setSubstringOid(lineParams.get(substrIndex+1));
			}

			int syntaxIndex = lineParams.indexOf("SYNTAX");
			if (syntaxIndex != -1 && syntaxIndex > substrIndex) {

				String syntaxSource = lineParams.get(syntaxIndex+1);
				if ("'".equals(lineParams.get(syntaxIndex+1))) {
					// MS AD encloses syntax OID in ''
					syntaxSource = lineParams.get(syntaxIndex+2);
				}
				// check if contains length
				int lengthIndex = syntaxSource.indexOf("{");
				if (lengthIndex != -1) {
					attributeType.setSyntaxOid(syntaxSource.substring(0, lengthIndex));
					attributeType.setSyntaxLength(Integer.parseInt(syntaxSource.substring(lengthIndex+1, syntaxSource.length()-1)));
				} else {
					attributeType.setSyntaxOid(syntaxSource);
				}
			}

			int singleIndex = lineParams.indexOf("SINGLE-VALUE");
			if (singleIndex != -1 && singleIndex > syntaxIndex) {
				attributeType.setSingleValued(true);
			}

			int unmodIndex = lineParams.indexOf("NO-USER-MODIFICATION");
			if (unmodIndex != -1 && unmodIndex > singleIndex) {
				attributeType.setUserModifiable(false);
			}

			int ordered = lineParams.indexOf("X-ORDERED");
			if (ordered != -1 && ordered > unmodIndex) {
				if (Objects.equals("'", lineParams.get(ordered+1))) {
					List<String> values = new ArrayList<>();
					values.add(lineParams.get(ordered+2));
					attributeType.getExtensions().putIfAbsent("X-ORDERED", values);
				}
			}

			int usageIndex = lineParams.indexOf("USAGE");
			if (usageIndex != -1 && usageIndex > ordered) {
				attributeType.setUsage(UsageEnum.getUsage(lineParams.get(usageIndex+1)));
			}

			attributeTypeList.add(attributeType);

		}

		return attributeTypeList;

	}

	public static List<ObjectClass> parseObjectClasses(Entry entry) {

		List<ObjectClass> objectClassesList = new ArrayList<>();
		if (entry == null) return objectClassesList;

		for (String matchingRuleSourceValue : getValuesOfSchemaAttribute(entry.get(SchemaConstants.OBJECT_CLASSES_AT))) {

			if (matchingRuleSourceValue == null || matchingRuleSourceValue.trim().isEmpty())
				throw new IllegalArgumentException("ObjectClasses definition must not be NULL or empty.");
			if (!matchingRuleSourceValue.startsWith("(") && !matchingRuleSourceValue.endsWith(")"))
				throw new IllegalArgumentException("ObjectClasses definition must be enclosed in '(' ').");

			List<String> lineParams = Arrays.asList(matchingRuleSourceValue.split("\\s+|(?=\\W'|'\\W)|(?<=\\W'|'\\W})"));


			ObjectClass objectClass = new ObjectClass(lineParams.get(1));

			int nameIndex = lineParams.indexOf("NAME");
			if (nameIndex != -1) {

				if (Objects.equals("'", lineParams.get(nameIndex+1))) {

					// single value
					objectClass.setNames(toList(lineParams.get(nameIndex+2)));

				} else if (Objects.equals("(", lineParams.get(nameIndex+1))) {
					// multi value
					List<String> values = new ArrayList<>();
					int index = nameIndex+2;
					while (!Objects.equals(")", lineParams.get(index))) {
						if (!Objects.equals("'", lineParams.get(index))) {
							// skip value enclosing appostroph
							values.add(lineParams.get(index));
						}
						index++;
					}
					objectClass.setNames(values);

				}

			}

			int descIndex = lineParams.indexOf("DESC");
			if (descIndex != -1 && descIndex > nameIndex) {

				if (Objects.equals("'", lineParams.get(descIndex+1))) {

					// multi value
					List<String> values = new ArrayList<>();
					int index = descIndex+2;
					while (!Objects.equals("'", lineParams.get(index))) {
						values.add(lineParams.get(index));
						index++;
					}
					objectClass.setDescription(printListWithWhitespace(values));

				}

			}

			int supIndex = lineParams.indexOf("SUP");
			if (supIndex != -1 && supIndex > descIndex) {

				if (Objects.equals("(", lineParams.get(supIndex+1))) {
					// multi value superior object class
					List<String> values = new ArrayList<>();
					int index = supIndex+2;
					while (!Objects.equals(")", lineParams.get(index))) {
						if (!Objects.equals("$", lineParams.get(index))) {
							// skip value separator $
							values.add(lineParams.get(index));
						}
						index++;
					}
					objectClass.setSuperiorOids(values);

				} else {

					// single superior object class
					objectClass.setSuperiorOids(toList(lineParams.get(supIndex+1)));

				}

			}

			int abstractIndex = lineParams.indexOf("ABSTRACT");
			if (abstractIndex != - 1 && abstractIndex>supIndex) {
				objectClass.setType(ObjectClassTypeEnum.ABSTRACT);
			}
			int structuralIndex= lineParams.indexOf("STRUCTURAL");
			if (structuralIndex != - 1 && structuralIndex>supIndex) {
				objectClass.setType(ObjectClassTypeEnum.STRUCTURAL);
			}
			int auxiliaryIndex = lineParams.indexOf("AUXILIARY");
			if (auxiliaryIndex != - 1 && auxiliaryIndex>supIndex) {
				objectClass.setType(ObjectClassTypeEnum.AUXILIARY);
			}

			int mustIndex = lineParams.indexOf("MUST");
			if (mustIndex != -1 && ((mustIndex > abstractIndex) && (mustIndex > structuralIndex) && (mustIndex > auxiliaryIndex))) {

				if (lineParams.get(mustIndex+1).startsWith("(")) {

					// multi value superior object class
					List<String> values = new ArrayList<>();
					int index = mustIndex+1;
					if (Objects.equals("(", lineParams.get(mustIndex+1))) {
						index++;
					} else {
						// crop first value, since AD schema doesn't have leading whitespace
						values.add(lineParams.get(mustIndex+1).substring(1));
						index++;
					}

					while (!Objects.equals(")", lineParams.get(index))) {
						if (!Objects.equals("$", lineParams.get(index))) {
							// skip value separator $
							values.add(lineParams.get(index));
						}
						index++;
					}
					objectClass.setMustAttributeTypeOids(values);

				} else {

					// single must attribute
					objectClass.setMustAttributeTypeOids(toList(lineParams.get(mustIndex+1)));

				}

			}

			int mayIndex = lineParams.indexOf("MAY");
			if (mayIndex != -1 && ((mayIndex > mustIndex) && (mayIndex > abstractIndex) && (mayIndex > structuralIndex) && (mayIndex > auxiliaryIndex))) {

				if (lineParams.get(mayIndex+1).startsWith("(")) {

					// multi value superior object class
					List<String> values = new ArrayList<>();
					int index = mayIndex+1;
					if (Objects.equals("(", lineParams.get(mayIndex+1))) {
						index++;
					} else {
						// crop first value, since AD schema doesn't have leading whitespace
						values.add(lineParams.get(mayIndex+1).substring(1));
						index++;
					}

					while (!Objects.equals(")", lineParams.get(index))) {
						if (!Objects.equals("$", lineParams.get(index))) {
							// skip value separator $
							values.add(lineParams.get(index));
						}
						index++;
					}
					objectClass.setMayAttributeTypeOids(values);

				} else {

					// single may attribute
					objectClass.setMayAttributeTypeOids(toList(lineParams.get(mayIndex+1)));

				}

			}

			objectClassesList.add(objectClass);

		}

		return objectClassesList;

	}


	private static List<String> getValuesOfSchemaAttribute(Attribute attribute) {

		List<String> result = new ArrayList<>();
		if (attribute == null) return result;
		for (Value val :attribute) {
			result.add(val.getString());
		}
		return result;

	}

	private static String printListWithWhitespace(List<String> list) {

		if (list == null) return "";
		StringJoiner joiner = new StringJoiner(" ");
		for (String item : list) {
			joiner.add(item);
		}
		return joiner.toString();

	}

	private static List<String> toList(String item) {
		List<String> arrayList = new ArrayList<>();
		arrayList.add(item);
		return arrayList;
	}

	/**
	 * Return instance of SyntaxChecker associated with LdapSyntax specified by its OID
	 *
	 * @param oid OID of LdapSyntax to get SyntaxChecker for
	 * @return SyntaxChecker or NULL if OID is unknown
	 */
	private static SyntaxChecker getSyntaxCheckerByOid(String oid) {

		switch (oid) {
			case SchemaConstants.ACCESS_POINT_SYNTAX: return AccessPointSyntaxChecker.INSTANCE;
			case SchemaConstants.ATTRIBUTE_TYPE_DESCRIPTION_SYNTAX: return AttributeTypeDescriptionSyntaxChecker.INSTANCE;
			case SchemaConstants.ATTRIBUTE_TYPE_USAGE_SYNTAX: return AttributeTypeUsageSyntaxChecker.INSTANCE;
			case SchemaConstants.AUDIO_SYNTAX: return AudioSyntaxChecker.INSTANCE;
			case SchemaConstants.BINARY_SYNTAX: return BinarySyntaxChecker.INSTANCE;
			case SchemaConstants.BIT_STRING_SYNTAX: return BitStringSyntaxChecker.INSTANCE;
			case SchemaConstants.BOOLEAN_SYNTAX: return BooleanSyntaxChecker.INSTANCE;
			case SchemaConstants.CERTIFICATE_LIST_SYNTAX: return CertificateListSyntaxChecker.INSTANCE;
			case SchemaConstants.CERTIFICATE_PAIR_SYNTAX: return CertificatePairSyntaxChecker.INSTANCE;
			case SchemaConstants.CERTIFICATE_SYNTAX: return CertificateSyntaxChecker.INSTANCE;
			case SchemaConstants.COMPARATOR_SYNTAX: return ComparatorSyntaxChecker.INSTANCE;
			case SchemaConstants.COUNTRY_STRING_SYNTAX: return CountrySyntaxChecker.INSTANCE;
			case SchemaConstants.CSN_SID_SYNTAX: return CsnSidSyntaxChecker.INSTANCE;
			case SchemaConstants.CSN_SYNTAX: return CsnSyntaxChecker.INSTANCE;
			case SchemaConstants.DATA_QUALITY_SYNTAX: return DataQualitySyntaxSyntaxChecker.INSTANCE;
			case SchemaConstants.DELIVERY_METHOD_SYNTAX: return DeliveryMethodSyntaxChecker.INSTANCE;
			case SchemaConstants.DEREF_ALIAS_SYNTAX: return DerefAliasSyntaxChecker.INSTANCE;
			case SchemaConstants.DIRECTORY_STRING_SYNTAX: return DirectoryStringSyntaxChecker.INSTANCE;
			case SchemaConstants.DIT_CONTENT_RULE_SYNTAX: return DitContentRuleDescriptionSyntaxChecker.INSTANCE;
			case SchemaConstants.DIT_STRUCTURE_RULE_SYNTAX: return DitStructureRuleDescriptionSyntaxChecker.INSTANCE;
			case SchemaConstants.DL_SUBMIT_PERMISSION_SYNTAX: return DlSubmitPermissionSyntaxChecker.INSTANCE;
			case SchemaConstants.DN_SYNTAX: return DnSyntaxChecker.INSTANCE;
			case SchemaConstants.DSA_QUALITY_SYNTAX: return DsaQualitySyntaxSyntaxChecker.INSTANCE;
			case SchemaConstants.DSE_TYPE_SYNTAX: return DseTypeSyntaxChecker.INSTANCE;
			case SchemaConstants.ENHANCED_GUIDE_SYNTAX: return EnhancedGuideSyntaxChecker.INSTANCE;
			case SchemaConstants.FACSIMILE_TELEPHONE_NUMBER_SYNTAX: return FacsimileTelephoneNumberSyntaxChecker.INSTANCE;
			case SchemaConstants.FAX_SYNTAX: return FaxSyntaxChecker.INSTANCE;
			case SchemaConstants.GENERALIZED_TIME_SYNTAX: return GeneralizedTimeSyntaxChecker.INSTANCE;
			case SchemaConstants.GUIDE_SYNTAX: return GuideSyntaxChecker.INSTANCE;
			case SchemaConstants.IA5_STRING_SYNTAX: return Ia5StringSyntaxChecker.INSTANCE;
			case SchemaConstants.INTEGER_SYNTAX: return IntegerSyntaxChecker.INSTANCE;
			case SchemaConstants.JAVA_BYTE_SYNTAX: return JavaByteSyntaxChecker.INSTANCE;
			case SchemaConstants.JAVA_INT_SYNTAX: return JavaIntegerSyntaxChecker.INSTANCE;
			case SchemaConstants.JAVA_LONG_SYNTAX: return JavaLongSyntaxChecker.INSTANCE;
			case SchemaConstants.JAVA_SHORT_SYNTAX: return JavaShortSyntaxChecker.INSTANCE;
			case SchemaConstants.JPEG_SYNTAX: return JpegSyntaxChecker.INSTANCE;
			case SchemaConstants.LDAP_SYNTAX_DESCRIPTION_SYNTAX: return LdapSyntaxDescriptionSyntaxChecker.INSTANCE;
			case SchemaConstants.MAIL_PREFERENCE_SYNTAX: return MailPreferenceSyntaxChecker.INSTANCE;
			case SchemaConstants.MASTER_AND_SHADOW_ACCESS_POINTS_SYNTAX: return MasterAndShadowAccessPointSyntaxChecker.INSTANCE;
			case SchemaConstants.MATCHING_RULE_DESCRIPTION_SYNTAX: return MatchingRuleDescriptionSyntaxChecker.INSTANCE;
			case SchemaConstants.MATCHING_RULE_USE_DESCRIPTION_SYNTAX: return MatchingRuleUseDescriptionSyntaxChecker.INSTANCE;
			case SchemaConstants.MHS_OR_ADDRESS_SYNTAX: return MhsOrAddressSyntaxChecker.INSTANCE;
			case SchemaConstants.NAME_AND_OPTIONAL_UID_SYNTAX: return NameAndOptionalUIDSyntaxChecker.INSTANCE;
			case SchemaConstants.NAME_FORM_DESCRIPTION_SYNTAX: return NameFormDescriptionSyntaxChecker.INSTANCE;
			case SchemaConstants.NORMALIZER_SYNTAX: return NormalizerSyntaxChecker.INSTANCE;
			case SchemaConstants.NUMBER_SYNTAX: return NumberSyntaxChecker.INSTANCE;
			case SchemaConstants.NUMERIC_OID_SYNTAX: return NumericOidSyntaxChecker.INSTANCE;
			case SchemaConstants.NUMERIC_STRING_SYNTAX: return NumericStringSyntaxChecker.INSTANCE;
			case SchemaConstants.OBJECT_CLASS_DESCRIPTION_SYNTAX: return ObjectClassDescriptionSyntaxChecker.INSTANCE;
			case SchemaConstants.OBJECT_CLASS_TYPE_SYNTAX: return ObjectClassTypeSyntaxChecker.INSTANCE;
			case SchemaConstants.OBJECT_NAME_SYNTAX: return ObjectNameSyntaxChecker.INSTANCE;
			case SchemaConstants.OCTET_STRING_SYNTAX: return OctetStringSyntaxChecker.INSTANCE;
			case SchemaConstants.OID_LEN_SYNTAX: return OidLenSyntaxChecker.INSTANCE;
			// TODO - OpenLdapObjectIdentifierMacro ?
			case SchemaConstants.OTHER_MAILBOX_SYNTAX: return OtherMailboxSyntaxChecker.INSTANCE;
			case SchemaConstants.POSTAL_ADDRESS_SYNTAX: return PostalAddressSyntaxChecker.INSTANCE;
			case SchemaConstants.PRESENTATION_ADDRESS_SYNTAX: return PresentationAddressSyntaxChecker.INSTANCE;
			case SchemaConstants.PRINTABLE_STRING_SYNTAX: return PrintableStringSyntaxChecker.INSTANCE;
			case SchemaConstants.PROTOCOL_INFORMATION_SYNTAX: return ProtocolInformationSyntaxChecker.INSTANCE;
			// TODO - RegexSyntaxChecker
			case SchemaConstants.SEARCH_SCOPE_SYNTAX: return SearchScopeSyntaxChecker.INSTANCE;
			case SchemaConstants.SUBSTRING_ASSERTION_SYNTAX: return SubstringAssertionSyntaxChecker.INSTANCE;
			case SchemaConstants.SUBTREE_SPECIFICATION_SYNTAX: return SubtreeSpecificationSyntaxChecker.INSTANCE;
			case SchemaConstants.SUPPLIER_AND_CONSUMER_SYNTAX: return SupplierAndConsumerSyntaxChecker.INSTANCE;
			case SchemaConstants.SUPPLIER_INFORMATION_SYNTAX: return SupplierInformationSyntaxChecker.INSTANCE;
			case SchemaConstants.SUPPLIER_OR_CONSUMER_SYNTAX: return SupplierOrConsumerSyntaxChecker.INSTANCE;
			case SchemaConstants.SUPPORTED_ALGORITHM_SYNTAX: return SupportedAlgorithmSyntaxChecker.INSTANCE;
			case SchemaConstants.SYNTAX_CHECKER_SYNTAX: return SyntaxCheckerSyntaxChecker.INSTANCE;
			case SchemaConstants.TELEPHONE_NUMBER_SYNTAX: return TelephoneNumberSyntaxChecker.INSTANCE;
			case SchemaConstants.TELETEX_TERMINAL_IDENTIFIER_SYNTAX: return TeletexTerminalIdentifierSyntaxChecker.INSTANCE;
			case SchemaConstants.TELEX_NUMBER_SYNTAX: return TelexNumberSyntaxChecker.INSTANCE;
			case SchemaConstants.UTC_TIME_SYNTAX: return UtcTimeSyntaxChecker.INSTANCE;
			case SchemaConstants.UUID_SYNTAX: return UuidSyntaxChecker.INSTANCE;
			default:
				// TODO - maybe we should fallback to something like DirectoryString syntax
				return null;
		}

	}

	/**
	 * Return instance of value Normalizer associated with MatchingRule specified by OID.
	 *
	 * @param oid OID of MatchingRule to get Normalizer for
	 * @return Normalizer or NoOpNormalizer if OID is unknown
	 */
	private static Normalizer getNormalizerByOid(String oid) {

		switch (oid) {

			case SchemaConstants.BOOLEAN_MATCH_MR_OID:
				return new BooleanNormalizer();

			// TODO - ConcreteNameComponentNormalizer ??

			// deep trim normalizer
			case SchemaConstants.CASE_EXACT_IA5_MATCH_MR_OID:
			case SchemaConstants.PROTOCOL_INFORMATION_MATCH_MR_OID:
			case SchemaConstants.CASE_EXACT_MATCH_MR_OID:
			case SchemaConstants.CASE_EXACT_SUBSTRING_MATCH_MR_OID:
				return new DeepTrimNormalizer(oid);

			// deep trim to lower normalizer
			case SchemaConstants.SUP_DIT_STRUCTURE_RULE_MATCH_OID:
			case SchemaConstants.RULE_ID_MATCH_OID:
			case SchemaConstants.CASE_IGNORE_IA5_MATCH_MR_OID:
			case SchemaConstants.CASE_IGNORE_IA5_SUBSTRINGS_MATCH_MR_OID:
			case SchemaConstants.CASE_IGNORE_LIST_MATCH_MR_OID:
			case SchemaConstants.CASE_IGNORE_LIST_SUBSTRINGS_MATCH_MR_OID:
			case SchemaConstants.CASE_IGNORE_MATCH_MR_OID:
			case SchemaConstants.CASE_IGNORE_ORDERING_MATCH_MR_OID:
			case SchemaConstants.CASE_IGNORE_SUBSTRING_MATCH_MR_OID:
				return new DeepTrimToLowerNormalizer(oid);

			case SchemaConstants.DISTINGUISHED_NAME_MATCH_MR_OID:
				return new DnNormalizer();

			case SchemaConstants.GENERALIZED_TIME_MATCH_MR_OID:
			case SchemaConstants.GENERALIZED_TIME_ORDERING_MATCH_MR_OID:
				return new GeneralizedTimeNormalizer();

			case SchemaConstants.NAME_OR_NUMERIC_ID_MATCH_OID:
				return new NameOrNumericIdNormalizer();

			case SchemaConstants.NUMERIC_STRING_MATCH_MR_OID:
			case SchemaConstants.NUMERIC_STRING_ORDERING_MATCH_MR_OID:
			case SchemaConstants.NUMERIC_STRING_SUBSTRINGS_MATCH_MR_OID:
			case SchemaConstants.INTEGER_MATCH_MR_OID:
			case SchemaConstants.INTEGER_ORDERING_MATCH_MR_OID:
				return new NumericNormalizer();

			case SchemaConstants.OBJECT_IDENTIFIER_MATCH_MR_OID:
				return new ObjectIdentifierNormalizer();

			// TODO - OidNormalizer ??
			// TODO - RegexNormalizer ??

			case SchemaConstants.TELEPHONE_NUMBER_MATCH_MR_OID:
			case SchemaConstants.TELEPHONE_NUMBER_SUBSTRINGS_MATCH_MR_OID:
				return new TelephoneNumberNormalizer();

			case SchemaConstants.UNIQUE_MEMBER_MATCH_MR_OID:
				return new UniqueMemberNormalizer();

			default:
				return new NoOpNormalizer(oid);
		}

	}

	/**
	 * Return instance of LdapComparator associated with MatchingRule specified by OID.
	 *
	 * @param oid OID of MatchingRule to get LdapComparator for
	 * @return Specific LdapComparator instance or ComparableComparator if OID is unknown
	 */
	private static LdapComparator getComparatorByOid(String oid) {

		switch (oid) {

			case SchemaConstants.BIT_STRING_MATCH_MR_OID:
				return new BitStringComparator(oid);

			case SchemaConstants.BOOLEAN_MATCH_MR_OID:
				return new BooleanComparator(oid);

			case SchemaConstants.OCTET_STRING_MATCH_MR_OID:
			case SchemaConstants.OCTET_STRING_ORDERING_MATCH_MR_OID:
			case SchemaConstants.OCTET_STRING_SUBSTRINGS_MATCH_MR_OID:
				return new ByteArrayComparator(oid);

			// TODO - missing in SchemaConstants - certificateExactMatch matching rule
			case "2.5.13.34":
				return new CertificateComparator(oid);

			case SchemaConstants.CSN_MATCH_MR_OID:
			case SchemaConstants.CSN_ORDERING_MATCH_MR_OID:
				return new CsnComparator(oid);

			case SchemaConstants.CSN_SID_MATCH_MR_OID:
				return new CsnSidComparator(oid);

			// TODO - missing SchemaConstants - caseExactIA5SubstringsMatch matching rule
			case "1.3.6.1.4.1.4203.1.2.1":
			case SchemaConstants.CASE_EXACT_IA5_MATCH_MR_OID:
			case SchemaConstants.PROTOCOL_INFORMATION_MATCH_MR_OID:
			case SchemaConstants.CASE_EXACT_MATCH_MR_OID:
			case SchemaConstants.CASE_EXACT_SUBSTRING_MATCH_MR_OID:
				return new DeepTrimComparator(oid);

			case SchemaConstants.SUP_DIT_STRUCTURE_RULE_MATCH_OID:
			case SchemaConstants.RULE_ID_MATCH_OID:
			case SchemaConstants.CASE_IGNORE_IA5_MATCH_MR_OID:
			case SchemaConstants.CASE_IGNORE_IA5_SUBSTRINGS_MATCH_MR_OID:
			case SchemaConstants.CASE_IGNORE_MATCH_MR_OID:
			case SchemaConstants.CASE_IGNORE_ORDERING_MATCH_MR_OID:
			case SchemaConstants.CASE_IGNORE_SUBSTRING_MATCH_MR_OID:
			case SchemaConstants.CASE_IGNORE_LIST_MATCH_MR_OID:
			case SchemaConstants.CASE_IGNORE_LIST_SUBSTRINGS_MATCH_MR_OID:
				return new DeepTrimToLowerComparator(oid);

			case SchemaConstants.DISTINGUISHED_NAME_MATCH_MR_OID:
				return new DnComparator(oid);

			case SchemaConstants.GENERALIZED_TIME_MATCH_MR_OID:
			case SchemaConstants.GENERALIZED_TIME_ORDERING_MATCH_MR_OID:
				return new GeneralizedTimeComparator(oid);

			case SchemaConstants.INTEGER_MATCH_MR_OID:
			case SchemaConstants.INTEGER_ORDERING_MATCH_MR_OID:
				return new IntegerComparator(oid);

			case SchemaConstants.BIG_INTEGER_MATCH_MR_OID:
				return new LongComparator(oid);

			// TODO - NormalizingComparator

			case SchemaConstants.NUMERIC_STRING_MATCH_MR_OID:
			case SchemaConstants.NUMERIC_STRING_ORDERING_MATCH_MR_OID:
			case SchemaConstants.NUMERIC_STRING_SUBSTRINGS_MATCH_MR_OID:
				return new NumericStringComparator(oid);

			case SchemaConstants.OBJECT_CLASS_TYPE_MATCH_OID:
				return new ObjectClassTypeComparator(oid);

			case SchemaConstants.OBJECT_IDENTIFIER_MATCH_MR_OID:
				return new ObjectIdentifierComparator(oid);

			// TODO - this comparator is not used in DirectoryAPI schemas, but Comparator and MR exists with documented OID
			case SchemaConstants.OBJECT_IDENTIFIER_FIRST_COMPONENT_MATCH_MR_OID:
				return new ObjectIdentifierFirstComponentComparator(oid);

			// TODO - Parsed DN Comparator ?
			// TODO - SerializableComparator

			case SchemaConstants.NUMERIC_OID_MATCH_OID:
			case SchemaConstants.JDBM_STRING_MATCH_MR_OID:
				return new StringComparator(oid);

			case SchemaConstants.TELEPHONE_NUMBER_MATCH_MR_OID:
			case SchemaConstants.TELEPHONE_NUMBER_SUBSTRINGS_MATCH_MR_OID:
				return new TelephoneNumberComparator(oid);

			case SchemaConstants.NAME_OR_NUMERIC_ID_MATCH_OID:
			case SchemaConstants.UNIQUE_MEMBER_MATCH_MR_OID:
				return new UniqueMemberComparator(oid);

			case SchemaConstants.UUID_MATCH_MR_OID:
			case SchemaConstants.UUID_ORDERING_MATCH_MR_OID:
				return new UuidComparator(oid);

			case SchemaConstants.WORD_MATCH_MR_OID:
			case SchemaConstants.KEYWORD_MATCH_MR_OID:
				return new WordComparator(oid);

			default: return new ComparableComparator(oid);

		}

	}

}
