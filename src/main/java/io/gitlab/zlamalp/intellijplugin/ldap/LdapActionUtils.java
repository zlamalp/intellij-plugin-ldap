package io.gitlab.zlamalp.intellijplugin.ldap;

import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.fileEditor.FileEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.LdapPluginActionPlaces;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.AttributeTable;
import io.gitlab.zlamalp.intellijplugin.ldap.editor.LdapEntryEditor;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.LdapTree;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.LdapTreePanel;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.awt.*;
import java.util.Objects;

import static com.intellij.openapi.actionSystem.PlatformDataKeys.CONTEXT_COMPONENT;

/**
 * Utility methods related to LDAP Actions
 *
 * @see com.intellij.openapi.actionSystem.AnAction
 *
 * @author Pavel Zlámal
 */
public class LdapActionUtils {

	/**
	 * Return TRUE if project related to the action is not null and not disposed.
	 *
	 * @param event event to check
	 * @return TRUE if project is present and active
	 */
	public static boolean isProjectActive(@NotNull AnActionEvent event) {
		return event.getProject() != null && !event.getProject().isDisposed();
	}

	/**
	 * Return TRUE if action originated from our known place and we can safely extract event data.
	 *
	 * @param event event to check
	 * @return TRUE if originated from known place
	 */
	public static boolean isFromKnownPlace(@NotNull AnActionEvent event) {
		return LdapPluginActionPlaces.LDAP_TREE_TOOLBAR.equals(event.getPlace()) ||
				LdapPluginActionPlaces.LDAP_TREE_POPUP.equals(event.getPlace()) ||
				LdapPluginActionPlaces.LDAP_EDITOR_TOOLBAR.equals(event.getPlace()) ||
				LdapPluginActionPlaces.LDAP_EDITOR_POPUP.equals(event.getPlace()) ||
				LdapPluginActionPlaces.LDAP_KEYBOARD_SHORTCUT.equals(event.getPlace());
	}

	/**
	 * Return TRUE if action originated from our TREE and we can safely extract event data.
	 *
	 * @param event event to check
	 * @return TRUE if originated from TREE place
	 */
	public static boolean isFromTree(@NotNull AnActionEvent event) {
		return LdapPluginActionPlaces.LDAP_TREE_TOOLBAR.equals(event.getPlace()) ||
				LdapPluginActionPlaces.LDAP_TREE_POPUP.equals(event.getPlace()) ||
				(LdapPluginActionPlaces.LDAP_KEYBOARD_SHORTCUT.equals(event.getPlace()) &&
								event.getData(CONTEXT_COMPONENT) instanceof LdapTree);
	}

	/**
	 * Return TRUE if action originated from our EDITOR and we can safely extract event data.
	 *
	 * @param event event to check
	 * @return TRUE if originated from EDITOR place
	 */
	public static boolean isFromEditor(@NotNull AnActionEvent event) {
		return LdapPluginActionPlaces.LDAP_EDITOR_TOOLBAR.equals(event.getPlace()) ||
				LdapPluginActionPlaces.LDAP_EDITOR_POPUP.equals(event.getPlace()) ||
				(LdapPluginActionPlaces.LDAP_KEYBOARD_SHORTCUT.equals(event.getPlace()) &&
						event.getData(CONTEXT_COMPONENT) instanceof AttributeTable);
	}

	/**
	 * Do the best effort to get Editor from action based on its source or NULL
	 *
	 * @param event event to get editor from
	 * @return LdapEntryEditor related to the action
	 */
	public static @Nullable LdapEntryEditor getEntryEditor(@NotNull AnActionEvent event) {

		FileEditor fileEditor = PlatformDataKeys.FILE_EDITOR.getData(event.getDataContext());

		if (fileEditor instanceof LdapEntryEditor) {
			return (LdapEntryEditor) fileEditor;
		}

		Component comp = event.getData(CONTEXT_COMPONENT);
		if (comp instanceof AttributeTable) {
			return ((AttributeTable) comp).getEditor();
		}

		return null;

	}

	/**
	 * Do the best effort to get LdapTreePanel from action based on its source or NULL
	 *
	 * @param event event to get editor from
	 * @return LdapTreePanel related to the action
	 */
	public static LdapTreePanel getLdapTreePanel(@NotNull AnActionEvent event) {

		if (isProjectActive(event)) {
			return LdapTreePanel.getInstance(Objects.requireNonNull(event.getProject()));
		} else {
			return LdapTreePanel.getInstance();
		}

	}

}
