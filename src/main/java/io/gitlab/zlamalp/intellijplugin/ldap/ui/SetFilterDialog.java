package io.gitlab.zlamalp.intellijplugin.ldap.ui;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogEarthquakeShaker;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.ValidationInfo;
import com.intellij.openapi.util.registry.Registry;
import com.intellij.openapi.wm.IdeFocusManager;
import com.intellij.ui.DocumentAdapter;
import com.intellij.ui.components.JBTextField;
import io.gitlab.zlamalp.intellijplugin.ldap.actions.SetFilterAction;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import org.apache.directory.api.ldap.model.filter.ExprNode;
import org.apache.directory.api.ldap.model.filter.FilterParser;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import java.awt.event.ActionEvent;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Dialog to set custom filter on Entry for getting its children.
 * Refresh of the entry must be called from outside.
 *
 * @see SetFilterAction
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class SetFilterDialog extends DialogWrapper {

	private boolean initialized = false;
	LdapEntryTreeNode ldapNode;
	private JPanel wrapper;
	private JBTextField filterField;
	private Action removeFilterAction = new AbstractAction("Remove filter"){
		@Override
		public void actionPerformed(ActionEvent event) {
			doRemoveFilterAction();
		}
	};

	public SetFilterDialog(@Nullable Project project, boolean canBeParent, @NotNull LdapEntryTreeNode ldapNode) {
		super(project, canBeParent);
		this.ldapNode = ldapNode;
		init();
	}

	private void initialize() {

		setTitle("Set filter for '"+ldapNode.getDn()+"'");
		setOKButtonText("Set filter");

		if (ldapNode.getFilter() != null) {
			filterField.setText(ldapNode.getFilter());
		}

		filterField.getDocument().addDocumentListener(new DocumentAdapter() {
			@Override
			protected void textChanged(DocumentEvent documentEvent) {
				triggerValidation();
			}
		});

		initialized = true;

	}

	@Nullable
	@Override
	public JComponent getPreferredFocusedComponent() {
		return filterField;
	}

	@Nullable
	@Override
	protected JComponent createCenterPanel() {
		if (!initialized) {
			initialize();
		}
		return wrapper;
	}

	@Override
	protected void doOKAction() {
		doAction(filterField.getText());
	}

	private void doRemoveFilterAction() {
		doAction(null);
	}

	@NotNull
	protected Action[] createActions() {

		ArrayList<Action> actions = new ArrayList<>();
		actions.add(getOKAction());

		Action removeFilter = getRemoveFilterAction();
		if (removeFilter != null) {
			actions.add(getRemoveFilterAction());
		}

		actions.add(getCancelAction());

		return actions.toArray(new Action[0]);

	}

	private Action getRemoveFilterAction() {
		return this.removeFilterAction;
	}

	private void doAction(String filter) {

		if (filter == null || filter.isEmpty()) {
			this.ldapNode.setFilter(null);
		} else {
			this.ldapNode.setFilter(filter);
		}
		close(0);

	}

	@Nullable
	@Override
	protected ValidationInfo doValidate() {

		if (filterField.getText() == null || filterField.getText().isEmpty()) {
			return new ValidationInfo("Invalid LDAP filter!", filterField);
		}
		try {
			ExprNode node = FilterParser.parse(filterField.getText());
		} catch (ParseException ex) {
			return new ValidationInfo("Invalid LDAP filter!", filterField);
		}
		return super.doValidate();

	}

	private void triggerValidation() {

		List<ValidationInfo> infoList = doValidateAll();
		if (!infoList.isEmpty()) {
			ValidationInfo info = infoList.get(0);
			if (info.component != null && info.component.isVisible()) {
				IdeFocusManager.getInstance(null).requestFocus(info.component, true);
			}

			if (!Registry.is("ide.inplace.validation.tooltip")) {
				DialogEarthquakeShaker.shake(getPeer().getWindow());
			}

			startTrackingValidation();
			if(infoList.stream().anyMatch(info1 -> !info1.okEnabled)) return;

		}

	}

}
