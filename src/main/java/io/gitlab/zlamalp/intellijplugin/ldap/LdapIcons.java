package io.gitlab.zlamalp.intellijplugin.ldap;

import com.intellij.icons.AllIcons;
import com.intellij.openapi.util.IconLoader;
import com.intellij.util.PlatformIcons;

import javax.swing.*;

/**
 * Provider of all icons used in UI of the LDAP plugin.
 *
 * Icons are sourced either from Intellij IDEA stock icons or from local resources.
 *
 * @author Pavel Zlámal
 */
public class LdapIcons {

	// LDAP
	public static final Icon LDAP_TOOL_WINDOW = IconLoader.getIcon("/icons/ldapToolWindow.svg", LdapIcons.class);
	public static final Icon LDAP = AllIcons.Actions.ShowAsTree;
	public static final Icon LDAP_MSAD = AllIcons.Providers.Microsoft;
	public static final Icon LDAP_APACHE = AllIcons.Providers.Apache;

	// LDAP TREE
	public static final Icon ADD_CONNECTION = PlatformIcons.ADD_ICON;
	public static final Icon LOCATE = AllIcons.General.Locate;

	public static final Icon ROOT_DSE = AllIcons.General.ProjectStructure;
	public static final Icon BASE = AllIcons.Nodes.Module;
	public static final Icon ENTRY = PlatformIcons.CUSTOM_FILE_ICON;
	public static final Icon ENTRY_WITH_CHILDREN = PlatformIcons.PACKAGE_ICON;

	// GENERIC ENTRY/EDITOR ACTIONS
	public static final Icon CONNECT = AllIcons.Actions.Execute;
	public static final Icon DISCONNECT = AllIcons.Actions.Suspend;
	public static final Icon CONNECTION_SETTINGS = PlatformIcons.SHOW_SETTINGS_ICON;
	public static final Icon OPEN_CONSOLE = AllIcons.Debugger.Console;

	public static final Icon REFRESH = AllIcons.Actions.Refresh;
	public static final Icon FILTER = AllIcons.General.Filter;
	public static final Icon SEARCH = AllIcons.Actions.Search;
	public static final Icon COPY = AllIcons.Actions.Copy;
	public static final Icon ADD = PlatformIcons.ADD_ICON;
	public static final Icon REMOVE = AllIcons.General.Remove;
	public static final Icon DELETE = AllIcons.Actions.Cancel;
	public static final Icon EDIT = AllIcons.Actions.Edit;
	public static final Icon RENAME = AllIcons.Actions.EditSource;
	public static final Icon ADD_ATTRIBUTE = IconLoader.getIcon("/icons/addAttribute.png", LdapIcons.class);
	public static final Icon REMOVE_ATTRIBUTE = IconLoader.getIcon("/icons/removeAttribute.png", LdapIcons.class);
	public static final Icon EXPORT_TO_LDIF = AllIcons.ToolbarDecorator.Export;
	public static final Icon IMPORT_FROM_LDIF = AllIcons.ToolbarDecorator.Import;

	public static final Icon SAVE_CHANGES = AllIcons.Actions.Commit;
	public static final Icon DISCARD_CHANGES = AllIcons.Actions.Rollback;

	public static final Icon EXPAND_ALL = AllIcons.Actions.Expandall;
	public static final Icon COLLAPSE_ALL = AllIcons.Actions.Collapseall;
	public static final Icon TOGGLE_VISIBILITY = AllIcons.Actions.ToggleVisibility;
	public static final Icon SETTINGS = AllIcons.General.GearPlain;

	// schema
	public static final Icon SYNCHRONIZE_SCHEMA = IconLoader.getIcon("/icons/synchronizeSchema.png", LdapIcons.class);
	public static final Icon EXPORT_SCHEMA = IconLoader.getIcon("/icons/exportSchema.png", LdapIcons.class);
	public static final Icon BROWSE_SCHEMA = AllIcons.Nodes.DataSchema;

	public static final Icon ABSTRACT_SCHEMA = IconLoader.getIcon("/icons/abstractSchema.svg", LdapIcons.class);
	public static final Icon AUXILIARY_SCHEMA = AllIcons.Nodes.ObjectTypeAttribute;
	public static final Icon STRUCTURAL_SCHEMA = AllIcons.Nodes.Static;

}
