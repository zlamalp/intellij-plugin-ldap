package io.gitlab.zlamalp.intellijplugin.ldap.settings;

import io.gitlab.zlamalp.intellijplugin.ldap.LdapServerSettings;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.plugin.LdapPluginSettings;
import io.gitlab.zlamalp.intellijplugin.ldap.settings.schema.SchemaSettings;
import org.apache.directory.api.ldap.model.message.AliasDerefMode;

import java.util.Set;

/**
 * Interface defining available LDAP connection settings/preferences.
 * Its implemented by IDE wide plugins configuration and each LDAP connection  in LdapServerConnectionSettings.
 *
 * @see LdapPluginSettings
 * @see LdapServerSettings
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public interface LdapSettings {

	int getConnectionTimeout();

	void setConnectionTimeout(int timeout);

	int getMaxConnections();

	void setMaxConnections(int maxConnections);

	int getMinIdleConnections();

	void setMinIdleConnections(int minIdle);

	int getWrapLdifLines();

	void setWrapLdifLines(int wrapLinesOnColumn);

	boolean isAllowExportUserPassword();

	void setAllowExportUserPassword(boolean allowExportUserPassword);

	boolean isSortLdifAttributes();

	void setSortLdifAttributes(boolean sortAttributes);

	int getPageSize();

	void setPageSize(int pageSize);

	int getWrapChildrenBy();

	void setWrapChildrenBy(int wrapChildrenBy);

	int getRequestTimeout();

	void setRequestTimeout(int requestTimeout);

	boolean isFetchUserAttributes();

	void setFetchUserAttributes(boolean fetchUserAttributes);

	boolean isFetchOperationAttributes();

	void setFetchOperationAttributes(boolean fetchOperationAttributes);

	Set<String> getFetchOnlyAttributes();

	void setFetchOnlyAttributes(Set<String> fetchOnlyAttributes);

	boolean isFetchAllAttributeValues();

	void setFetchAllAttributeValues(boolean fetchAllAttributeValues);

	boolean isUpdateLdap();

	void setUpdateLdap(boolean updateLdap);

	boolean isUseCompactTable();

	void setUseCompactTable(boolean compactTable);

	boolean isShowDecoratedValues();

	void setShowDecoratedValues(boolean showDecoratedValues);

	boolean isSortObjectClassFirst();

	void setSortObjectClassFirst(boolean sortObjectClassFirst);

	boolean isSortMustAttributeFirst();

	void setSortMustAttributeFirst(boolean sortMustAttributeFirst);

	boolean isSortByAttributeType();

	void setSortByAttributeType(boolean sortByAttributeType);

	boolean isSortAscending();

	void setSortAscending(boolean sortAscending);

	int getWrapAttributesBy();

	void setWrapAttributesBy(int wrapAttributesBy);

	boolean isSortOperationalAttributesLast();

	void setSortOperationalAttributesLast(boolean sortOperationalAttributesLast);

	int getRequestLimit();

	void setRequestLimit(int requestLimit);

	AliasDerefMode getAliasDerefMode();

	void setAliasDerefMode(AliasDerefMode aliasDerefMode);

	SchemaSettings getSchemaSettings();

	void setSchemaSettings(SchemaSettings schemaSettings);

}
