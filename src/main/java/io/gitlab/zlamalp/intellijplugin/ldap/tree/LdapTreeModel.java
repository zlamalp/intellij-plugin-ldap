package io.gitlab.zlamalp.intellijplugin.ldap.tree;

import com.intellij.openapi.project.Project;
import com.intellij.ui.tree.BaseTreeModel;
import com.intellij.util.ui.tree.TreeUtil;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapEntryTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapSearchTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapTreeNode;
import io.gitlab.zlamalp.intellijplugin.ldap.tree.node.LdapTreeRootNode;
import org.jetbrains.annotations.ApiStatus;
import org.jetbrains.annotations.NotNull;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Custom {@link TreeModel} implementation used by {@link LdapTree}.
 *
 * Some methods were backported from {@link DefaultTreeModel} in order to fire
 * proper tree change events based on modified nodes. We might reconsider this in a future.
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdapTreeModel extends BaseTreeModel<LdapTreeNode> {

	private LdapTreeRootNode rootNode;
	private Project project;

	public LdapTreeModel(@NotNull Project project, @NotNull LdapTreeRootNode root) {
		this.rootNode = root;
		this.project = project;
	}

	@Override
	public List<? extends LdapTreeNode> getChildren(Object parent) {
		if (parent == null) return new ArrayList<>();

		if (parent instanceof LdapEntryTreeNode) {
			return ((LdapEntryTreeNode) parent).getChildren();
		} else if (parent instanceof LdapSearchTreeNode) {
			return ((LdapSearchTreeNode) parent).getChildren();
		} else if (parent instanceof DefaultMutableTreeNode) {
			List<LdapTreeNode> list = new ArrayList<>();
			for (int i = 0; i< ((DefaultMutableTreeNode) parent).getChildCount(); i++) {
				LdapTreeNode node = (LdapTreeNode)((DefaultMutableTreeNode) parent).getChildAt(i);
				list.add(node);
			}
			return list;
		}
		return new ArrayList<>();

	}

	@Override
	public Object getRoot() {
		return rootNode;
	}

	/**
	 * Returns whether the specified node is a leaf node.
	 *
	 * @param node the node to check
	 * @return true if the node is a leaf node
	 *
	 * @see TreeModel#isLeaf
	 */
	@Override
	public boolean isLeaf(Object node) {
		return !((TreeNode)node).getAllowsChildren() || ((TreeNode) node).isLeaf();
	}

	// FIXME / TODO - following is backported from swing DefaultTreeModel

	public void add(MutableTreeNode parent, MutableTreeNode newChild) {
		if(newChild != null && newChild.getParent() == parent)
			insertNodeInto(newChild, parent, parent.getChildCount() - 1);
		else
			insertNodeInto(newChild, parent, parent.getChildCount());
	}

	/**
	 * Invoked this to insert newChild at location index in parents children.
	 * This will then message nodesWereInserted to create the appropriate
	 * event. This is the preferred way to add children as it will create
	 * the appropriate event.
	 */
	public void insertNodeInto(MutableTreeNode newChild,
	                           MutableTreeNode parent, int index){
		parent.insert(newChild, index);

		int[]           newIndexs = new int[1];
		Object[]          newChildren = new Object[1];

		newIndexs[0] = index;
		newChildren[0] = parent.getChildAt(newIndexs[0]);

		treeNodesInserted(TreeUtil.getPath(rootNode, parent), newIndexs, newChildren);
	}

	/**
	 * Invoke this method if you've totally changed the children of
	 * node and its children's children...  This will post a
	 * treeStructureChanged event.
	 */
	public void nodeStructureChanged(TreeNode node) {
		if(node != null) {
			treeStructureChanged(TreeUtil.getPath(rootNode, node), null, null);
		}
	}

	/**
	 * Message this to remove node from its parent. This will message
	 * nodesWereRemoved to create the appropriate event. This is the
	 * preferred way to remove a node as it handles the event creation
	 * for you.
	 */
	public void removeNodeFromParent(MutableTreeNode node) {
		MutableTreeNode         parent = (MutableTreeNode)node.getParent();

		if(parent == null)
			throw new IllegalArgumentException("node does not have a parent.");

		int[]            childIndex = new int[1];
		Object[]         removedArray = new Object[1];

		childIndex[0] = parent.getIndex(node);
		parent.remove(childIndex[0]);
		removedArray[0] = node;
		treeNodesRemoved(TreeUtil.getPath(rootNode, parent), childIndex, removedArray);
	}

	/**
	 * Invoke this method after you've changed how node is to be
	 * represented in the tree.
	 */
	public void nodeChanged(TreeNode node) {
		if(listeners != null && node != null) {
			TreeNode         parent = node.getParent();

			if(parent != null) {
				int        anIndex = parent.getIndex(node);
				if(anIndex != -1) {
					int[]        cIndexs = new int[1];

					cIndexs[0] = anIndex;
					nodesChanged(parent, cIndexs);
				}
			}
			else if (node == getRoot()) {
				nodesChanged(node, null);
			}
		}
	}

	/**
	 * Invoke this method after you've changed how the children identified by
	 * childIndicies are to be represented in the tree.
	 */
	public void nodesChanged(TreeNode node, int[] childIndices) {
		if(node != null) {
			if (childIndices != null) {
				int            cCount = childIndices.length;

				if(cCount > 0) {
					Object[]       cChildren = new Object[cCount];

					for(int counter = 0; counter < cCount; counter++)
						cChildren[counter] = node.getChildAt
								(childIndices[counter]);
					treeNodesChanged(TreeUtil.getPath(rootNode, node), childIndices, cChildren);
				}
			}
			else if (node == getRoot()) {
				treeNodesChanged(TreeUtil.getPath(rootNode, node), null, null);
			}
		}
	}

	/**
	 * This sets the user object of the TreeNode identified by path
	 * and posts a node changed.  If you use custom user objects in
	 * the TreeModel you're going to need to subclass this and
	 * set the user object of the changed node to something meaningful.
	 */
	@Override
	public void valueForPathChanged(TreePath path, Object newValue) {
		MutableTreeNode   aNode = (MutableTreeNode)path.getLastPathComponent();

		aNode.setUserObject(newValue);
		nodeChanged(aNode);
	}

	/**
	 * Sort node children. Should be called only once!
	 *
	 * @param parent
	 * @param comparator
	 */
	@ApiStatus.Experimental
	public void sortChildren(MutableTreeNode parent, Comparator comparator) {

		if (parent instanceof LdapTreeRootNode) {
			((LdapTreeRootNode) parent).sort(comparator);
			nodeStructureChanged(parent);
			// FIXME - use less destructive event, but updating children failed :-(
			/*
			if (parent.getChildCount() > 0) {
				int[] childIndicies = new int[parent.getChildCount()];
				for(int i = 1; i <= parent.getChildCount(); i++){
					childIndicies[i] = i;
				}
				nodesChanged(parent, childIndicies);
			} else {
				nodesChanged(parent, null);
			}
			*/
		}

	}

}
