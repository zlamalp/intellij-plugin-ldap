package io.gitlab.zlamalp.intellijplugin.ldap.settings.connection;

import com.intellij.LdapBundle;
import com.intellij.openapi.progress.PerformInBackgroundOption;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.ui.ComponentValidator;
import com.intellij.openapi.ui.ValidationInfo;
import com.intellij.ui.DocumentAdapter;
import com.intellij.ui.JBColor;
import com.intellij.ui.SimpleListCellRenderer;
import com.intellij.ui.TitledSeparator;
import com.intellij.ui.components.JBCheckBox;
import com.intellij.ui.components.JBLabel;
import com.intellij.ui.components.JBPasswordField;
import com.intellij.ui.components.JBTextField;
import com.intellij.ui.components.fields.IntegerField;
import com.intellij.util.EventDispatcher;
import io.gitlab.zlamalp.intellijplugin.ldap.LdapServer;
import org.apache.commons.lang.StringUtils;
import org.apache.directory.api.ldap.model.exception.LdapInvalidDnException;
import org.apache.directory.api.ldap.model.filter.ExprNode;
import org.apache.directory.api.ldap.model.filter.FilterParser;
import org.apache.directory.api.ldap.model.name.Dn;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.List;
import java.util.function.Supplier;

/**
 * This class represents form for viewing and modifying LdapServerConnection.
 * It's used inside LdapConnectionTabbedPane with all connections.
 *
 * @see LdapServerConfigTabbedPane (outer wrapper)
 * @see ManageLdapServersDialog (outer dialog)
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdapServerConfigForm {

	private JPanel wrapper;
	private JBTextField filterField;
	private JBCheckBox readOnlyField;
	private JBTextField baseDNField;
	private IntegerField portField;
	private JBTextField hostnameField;
	private JComboBox<String> protocolField;
	private TitledSeparator authenticationSeparator;
	private JBCheckBox authenticateField;
	private JBTextField bindDNField;
	private JBPasswordField passwordField;
	private JBCheckBox rememberPasswordField;
	private JButton testConnectionButton;
	private JBLabel connectedLabel;
	private JBLabel bindDNLabel;
	private JBLabel passwordLabel;

	private final EventDispatcher<ChangeListener> myDispatcher = EventDispatcher.create(ChangeListener.class);
	private final LdapServer ldapServer;
	private boolean loaded = false;
	private boolean isNew = false;

	private LdapServerConfigTabbedPane parent;

	/**
	 * Creates form for editing LDAP server configuration
	 *
	 * @param parent Parent tabbed pane form
	 */
	public LdapServerConfigForm(@NotNull final LdapServerConfigTabbedPane parent) {

		this.parent = parent;
		this.ldapServer = parent.getLdapServer();

		authenticationSeparator.setText("Authentication");

		protocolField.addItem("ldap://");
		protocolField.addItem("ldaps://");

		protocolField.addActionListener (new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (Objects.equals("ldaps://", (String)protocolField.getSelectedItem())) {
					if (portField.getText() != null && !portField.getText().trim().isEmpty()) {
						if (portField.getText().equals("389")) {
							portField.setText("636");
						}
						// keep custom port
					} else {
						portField.setText("636");
					}
				} else {
					if (portField.getText() != null && !portField.getText().trim().isEmpty()) {
						if (portField.getText().equals("636")) {
							portField.setText("389"); }
						// keep custom port
					} else {
						portField.setText("389");
					}
				}
				if (loaded) setModified();

			}
		});

		protocolField.setRenderer(new SimpleListCellRenderer<String>() {
			@Override
			public void customize(@NotNull JList<? extends String> list, String value, int index, boolean selected, boolean hasFocus) {
				setText(value);
			}
		});

		rememberPasswordField.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				if (loaded) setModified();
			}
		});

		portField.setMinValue(0);
		portField.setMaxValue(65535);
		portField.setDefaultValue(389);
		portField.setCanBeEmpty(false);
		portField.getEmptyText().setText("389");

		authenticateField.addActionListener(e->updateAuthenticationPanelByCheckboxValue());

		addTextChangedDocumentAdapter(hostnameField);
		addTextChangedDocumentAdapter(portField);
		addTextChangedDocumentAdapter(baseDNField);
		addTextChangedDocumentAdapter(filterField);
		addTextChangedDocumentAdapter(bindDNField);
		addTextChangedDocumentAdapter(passwordField);
		addActionListener(readOnlyField);

		/* TODO - by design we shouldn fill default values to empty text
		hostnameField.getEmptyText().setText("localhost");
		filterField.getEmptyText().setText(LdapConstants.OBJECT_CLASS_STAR);
		baseDNField.getEmptyText().setText("dc=example,dc=com");
		bindDNField.getEmptyText().setText("cn=john,ou=people,dc=example,dc=com");
		*/

		testConnectionButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				// TODO - validate form ?
				connectedLabel.setText("");
				LdapServer testingLdapServer = new LdapServer();

				testingLdapServer.setName(ldapServer.getName());
				testingLdapServer.setLdapType(ldapServer.getLdapType());
				testingLdapServer.setDescription(ldapServer.getDescription());
				testingLdapServer.setGlobal(ldapServer.isGlobal());

				testingLdapServer.setHostname(getValueOrNull(hostnameField));
				testingLdapServer.setProtocol((String)protocolField.getSelectedItem());
				testingLdapServer.setPort(portField.getValue());
				testingLdapServer.setBase(baseDNField.getText());
				testingLdapServer.setFilter(getValueOrNull(filterField));
				testingLdapServer.setRequireAuthz(authenticateField.isSelected());
				testingLdapServer.setUserName(getValueOrNull(bindDNField));
				testingLdapServer.setPassword(getValueOrNull(passwordField));
				testingLdapServer.setReadOnly(readOnlyField.isSelected());
				testingLdapServer.setRememberPassword(rememberPasswordField.isSelected());

				testConnectionButton.setEnabled(false);
				clearConnectedLabel();
				connectedLabel.setForeground(JBColor.foreground());
				connectedLabel.setText("");

				Task task = new Task.Backgroundable(parent.getProject(),
						"Testing connection to "+ ldapServer.getName(),
						false,
						PerformInBackgroundOption.ALWAYS_BACKGROUND) {

					@Override
					public void run(@NotNull ProgressIndicator indicator) {

						indicator.setIndeterminate(false);
						indicator.setFraction(0.1);
						indicator.setText(LdapBundle.message("ldap.update.modal.connecting"));
						// re-init pool
						if (testingLdapServer.connect(5000)) {

							EventQueue.invokeLater(new Runnable() {
								@Override
								public void run() {
									// if user changed mind, put back password and remembering status
									rememberPasswordField.setSelected(testingLdapServer.isRememberPassword());
									passwordField.setText(testingLdapServer.getPassword());
									setModified();
									connectedLabel.setForeground(JBColor.GREEN.darker());
									connectedLabel.setText(LdapBundle.message("ldap.settings.dialog.connectionTest.result.ok"));
									testConnectionButton.setEnabled(true);
								}
							});

						} else {

							EventQueue.invokeLater(new Runnable() {
								@Override
								public void run() {
									connectedLabel.setForeground(JBColor.RED);
									connectedLabel.setText(LdapBundle.message("ldap.settings.dialog.connectionTest.result.nok"));
									testConnectionButton.setEnabled(true);
								}
							});

						}

						indicator.setFraction(1.0);

					}

				};

				ProgressManager.getInstance().run(task);

			}
		});

		// init validation

		new ComponentValidator(parent.getDisposable()).withValidator(new Supplier<ValidationInfo>() {
			@Override
			public ValidationInfo get() {
				if (StringUtils.isBlank(hostnameField.getText())) {
					return new ValidationInfo(LdapBundle.message("ldap.settings.dialog.validator.cantBeEmpty"), hostnameField);
				}
				return null;
			}
		}).andStartOnFocusLost().installOn(hostnameField);

		new ComponentValidator(parent.getDisposable()).withValidator(new Supplier<ValidationInfo>() {
			@Override
			public ValidationInfo get() {
				if (StringUtils.isBlank(portField.getText())) {
					return new ValidationInfo(LdapBundle.message("ldap.settings.dialog.validator.cantBeEmpty"), portField);
				} else if (!StringUtils.isNumeric(portField.getText())) {
					return new ValidationInfo(LdapBundle.message("ldap.settings.dialog.validator.mustBeNumber"), portField);
				} else if (Integer.parseInt(portField.getText()) < 0 || Integer.parseInt(portField.getText()) > 65535) {
					return new ValidationInfo(LdapBundle.message("ldap.settings.dialog.validator.mustBeBetween", 0, 65535), portField);
				}
				return null;
			}
		}).andStartOnFocusLost().installOn(portField);

		new ComponentValidator(parent.getDisposable()).withValidator(new Supplier<ValidationInfo>() {
			@Override
			public ValidationInfo get() {
				try {
					if (StringUtils.isNotBlank(filterField.getText())) {
						ExprNode node = FilterParser.parse(filterField.getText());
					}
				} catch (ParseException ex) {
					return new ValidationInfo(LdapBundle.message("ldap.settings.dialog.validator.invalidFilter"), filterField);
				}
				return null;
			}
		}).andStartOnFocusLost().installOn(filterField);

		new ComponentValidator(parent.getDisposable()).withValidator(new Supplier<ValidationInfo>() {
			@Override
			public ValidationInfo get() {
				try {
					if (StringUtils.isNotEmpty(baseDNField.getText())) {
						new Dn(baseDNField.getText());
					}
				} catch (LdapInvalidDnException e) {
					return new ValidationInfo(e.getMessage(), baseDNField);
				}
				return null;
			}
		}).andRegisterOnDocumentListener(baseDNField).installOn(baseDNField);

		new ComponentValidator(parent.getDisposable()).withValidator(new Supplier<ValidationInfo>() {
			@Override
			public ValidationInfo get() {
				try {
					if (StringUtils.isNotEmpty(bindDNField.getText()) && bindDNField.isEnabled()) {
						new Dn(bindDNField.getText());
					}
				} catch (LdapInvalidDnException e) {
					return new ValidationInfo(e.getMessage(), bindDNField);
				}
				return null;
			}
		}).andRegisterOnDocumentListener(bindDNField).installOn(bindDNField);

	}

	/**
	 * Return backing ldapServer associated with this form
	 *
	 * @return LDAP server
	 */
	public LdapServer getLdapServer() {
		return ldapServer;
	}

	/**
	 * Creates text changed listener for form field expecting string value
	 *
	 * @param textField text field to attach listener
	 */
	private void addTextChangedDocumentAdapter(@NotNull JTextField textField) {
		textField.getDocument().addDocumentListener(new DocumentAdapter() {
			@Override
			protected void textChanged(DocumentEvent documentEvent) {
				connectedLabel.setText("");
				if (loaded) setModified();
			}
		});
	}

	/**
	 * Creates action listener for checkbox
	 *
	 * @param checkbox checkbox to attach listener
	 */
	private void addActionListener(@NotNull JBCheckBox checkbox) {
		checkbox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (loaded) setModified();
			}
		});
	}

	private String getValueOrNull(@NotNull JTextField textField) {
		if (textField instanceof JBPasswordField) {
			if (((JBPasswordField) textField).getPassword().length != 0) {
				return new String(((JBPasswordField) textField).getPassword());
			}
		} else {
			return (textField.getText().trim().isEmpty()) ? null : textField.getText();
		}
		return null;
	}

	/**
	 * Switch requireAuthz flag on LDAP ldapServer and switch "enabled" state of input form for DN/password etc.
	 */
	private void updateAuthenticationPanelByCheckboxValue() {

		boolean selected = authenticateField.isSelected();

		bindDNLabel.setEnabled(selected);
		bindDNField.setEnabled(selected);
		passwordField.setEnabled(selected);
		passwordLabel.setEnabled(selected);
		connectedLabel.setText("");
		rememberPasswordField.setEnabled(selected);

		if (loaded) setModified();

	}

	/**
	 * Returns Component representing form
	 *
	 * @return form component
	 */
	public JComponent getContent() {
		return wrapper;
	}

	/**
	 * Load LdapServerConnection details into form
	 *
	 * @param ldapServer Connection to load
	 */
	public void loadConnection(LdapServer ldapServer) {

		this.loaded = false;

		protocolField.setSelectedItem(ldapServer.getProtocol());
		hostnameField.setText(ldapServer.getHostname());
		portField.setValue(ldapServer.getPort());
		baseDNField.setText(ldapServer.getBase());
		filterField.setText(ldapServer.getFilter());
		readOnlyField.setSelected(ldapServer.isReadOnly());

		authenticateField.setSelected(ldapServer.isRequireAuthz());
		bindDNField.setText(ldapServer.getUserName());
		rememberPasswordField.setSelected(ldapServer.isRememberPassword());
		passwordField.setText(ldapServer.getPassword());

		// set UI to current authentication state
		updateAuthenticationPanelByCheckboxValue();

		this.loaded = true;

	}

	/**
	 * Save details from form into LdapServerConnection
	 *
	 * @param ldapServer Connection to save
	 */
	public void saveConnection(LdapServer ldapServer) {

		ldapServer.setProtocol((String)protocolField.getSelectedItem());
		ldapServer.setHostname(getValueOrNull(hostnameField));
		ldapServer.setPort(portField.getValue());
		ldapServer.setBase(baseDNField.getText());
		ldapServer.setFilter(getValueOrNull(filterField));
		ldapServer.setReadOnly(readOnlyField.isSelected());

		ldapServer.setRequireAuthz(authenticateField.isSelected());
		ldapServer.setUserName(getValueOrNull(bindDNField));
		ldapServer.setPassword(getValueOrNull(passwordField));
		ldapServer.setRememberPassword(rememberPasswordField.isSelected());

	}

	/**
	 * Return TRUE if form is modified against original LdapServerConnection
	 *
	 * @param ldapServer
	 * @return
	 */
	public boolean isModified(LdapServer ldapServer) {

		if (!loaded) return false;
		if (isNew) return true;
		if (!Objects.equals(ldapServer.getProtocol(), (String)protocolField.getSelectedItem())) return true;
		if (!Objects.equals(ldapServer.getHostname(), getValueOrNull(hostnameField))) return true;
		if (!Objects.equals(ldapServer.getPort(), portField.getValue())) return true;
		if (!Objects.equals(ldapServer.getBase(), baseDNField.getText())) return true;
		if (!Objects.equals(ldapServer.getFilter(), getValueOrNull(filterField))) return true;
		if (!Objects.equals(ldapServer.isReadOnly(), readOnlyField.isSelected())) return true;
		if (!Objects.equals(ldapServer.isRequireAuthz(), authenticateField.isSelected())) return true;
		if (!Objects.equals(ldapServer.getUserName(), getValueOrNull(bindDNField))) return true;
		if (!Objects.equals(ldapServer.isRememberPassword(), rememberPasswordField.isSelected())) return true;
		if (!Objects.equals(ldapServer.getPassword(), getValueOrNull(passwordField))) return true;

		return false;

	}

	public boolean isNew() {
		return isNew;
	}

	public void setNew(boolean aNew) {
		isNew = aNew;
	}

	public void setModified() {
		fireStateChanged();
	}

	public void addChangeListener(ChangeListener listener) {
		myDispatcher.addListener(listener);
	}

	public void removeChangeListener(ChangeListener listener) {
		myDispatcher.removeListener(listener);
	}

	public void fireStateChanged() {
		myDispatcher.getMulticaster().stateChanged(new ChangeEvent(parent));
	}

	public void clearConnectedLabel() {
		connectedLabel.setText("");
	}

	/**
	 * Validate all form items present in this part of dialog window.
	 *
	 * @return List with validation results
	 */
	public List<ValidationInfo> doValidateAll() {
		List<ValidationInfo> result = new ArrayList<>();
		revalidate(result, hostnameField);
		revalidate(result, portField);
		revalidate(result, filterField);
		revalidate(result, baseDNField);
		revalidate(result, bindDNField);
		return result;
	}

	/**
	 * Validate passed input field and store results in result list.
	 *
	 * @param resultList list with all gathered validation results
	 * @param field Input field to validate
	 */
	private void revalidate(@NotNull List<ValidationInfo> resultList, @NotNull JBTextField field) {

		ComponentValidator.getInstance(field).ifPresent(componentValidator -> {
			componentValidator.revalidate();
			ValidationInfo info = componentValidator.getValidationInfo();
			if (info != null) {
				resultList.add(info);
			}
		});

	}

}
