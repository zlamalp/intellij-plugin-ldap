package io.gitlab.zlamalp.intellijplugin.ldif;

import com.intellij.openapi.fileTypes.*;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;

/**
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdifSyntaxHighlighterFactory extends SyntaxHighlighterFactory {

	@NotNull
	@Override
	public SyntaxHighlighter getSyntaxHighlighter(Project project, VirtualFile virtualFile) {
		return new LdifSyntaxHighlighter();
	}

}
