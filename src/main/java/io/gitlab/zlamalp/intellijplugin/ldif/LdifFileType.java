package io.gitlab.zlamalp.intellijplugin.ldif;

import com.intellij.openapi.fileTypes.LanguageFileType;
import org.jetbrains.annotations.*;

import javax.swing.*;

/**
 * Representation of LDIF file type registered by LdifFileTypeFactory
 *
 * @see LdifFileTypeFactory
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdifFileType extends LanguageFileType {

	public static final String EXTENSION = "ldif";
	public static final LdifFileType INSTANCE = new LdifFileType();

	private LdifFileType() {
		super(LdifLanguage.INSTANCE);
	}

	@NotNull
	@Override
	public String getName() {
		return "Ldif";
	}

	@NotNull
	@Override
	public String getDescription() {
		return "LDIF file";
	}

	@NotNull
	@Override
	public String getDefaultExtension() {
		return EXTENSION;
	}

	@Nullable
	@Override
	public Icon getIcon() {
		return LdifIcons.FILE;
	}

}
