package io.gitlab.zlamalp.intellijplugin.ldif.psi;

import com.intellij.openapi.project.Project;
import com.intellij.psi.*;
import io.gitlab.zlamalp.intellijplugin.ldif.LdifFileType;

/**
 * Class with factory methods to generate all LDIF file PSI elements.
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdifElementFactory {

	/**
	 * Create dummy file in a project with specified text content.
	 * Resulting file can be parsed and used to return any PSI element.
	 *
	 * @param project Current project
	 * @param text Textual content of the file
	 * @return LDIF file with specified content as PSI element.
	 */
	public static LdifFile createFile(Project project, String text) {
		String name = "dummy.ldif";
		return (LdifFile) PsiFileFactory.getInstance(project).
				createFileFromText(name, LdifFileType.INSTANCE, text);
	}

	/**
	 * Create heading of LDIF entry.
	 * @see LdifDn
	 *
	 * @param project Current project
	 * @param dn DN value of heading
	 * @return LDIF Heading element
	 */
	public static LdifDn createLdifHeading(Project project, String dn) {
		final LdifFile file = createFile(project, "dn: " + dn);
		System.out.println(file.getFirstChild().getClass().getCanonicalName());
		return (LdifDn) file.getFirstChild();
	}

}
