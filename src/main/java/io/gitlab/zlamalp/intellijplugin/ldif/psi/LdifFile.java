package io.gitlab.zlamalp.intellijplugin.ldif.psi;

import com.intellij.extapi.psi.PsiFileBase;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.psi.FileViewProvider;
import io.gitlab.zlamalp.intellijplugin.ldif.*;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

/**
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdifFile extends PsiFileBase {

	public LdifFile(@NotNull FileViewProvider viewProvider) {
		super(viewProvider, LdifLanguage.INSTANCE);
	}

	@NotNull
	@Override
	public FileType getFileType() {
		return LdifFileType.INSTANCE;
	}

	@Override
	public String toString() {
		return "Ldif File";
	}

	@Override
	public Icon getIcon(int flags) {
		return super.getIcon(flags);
	}

}
