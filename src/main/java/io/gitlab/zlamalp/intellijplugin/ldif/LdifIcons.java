package io.gitlab.zlamalp.intellijplugin.ldif;

import com.intellij.openapi.util.IconLoader;
import com.intellij.util.PlatformIcons;

import javax.swing.*;

/**
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdifIcons {
	public static final Icon FILE = PlatformIcons.CUSTOM_FILE_ICON; // IconLoader.getIcon("/icons/ldif_file.png");
	public static final Icon ENTRY = IconLoader.getIcon("/icons/ldif_entry.png", LdifIcons.class);
	public static final Icon ENTRY_ADD = IconLoader.getIcon("/icons/ldif_entry_add.png", LdifIcons.class);
	public static final Icon ENTRY_DEL = IconLoader.getIcon("/icons/ldif_entry_delete.png", LdifIcons.class);
	public static final Icon ENTRY_MOD = IconLoader.getIcon("/icons/ldif_entry_modify.png", LdifIcons.class);
	public static final Icon ENTRY_RENAME = IconLoader.getIcon("/icons/ldif_entry_rename.png", LdifIcons.class);
	public static final Icon ENTRY_UNUSED = IconLoader.getIcon("/icons/ldif_entry_unused.png", LdifIcons.class);

	public static final Icon LDAP = IconLoader.getIcon("/icons/ldap.png", LdifIcons.class);
	public static final Icon LDAP_DC = IconLoader.getIcon("/icons/ldif_file.png", LdifIcons.class);
	public static final Icon LDAP_OU = IconLoader.getIcon("/icons/ldap_ou.png", LdifIcons.class);
	public static final Icon LDAP_ENTRY = IconLoader.getIcon("/icons/ldif_entry.png", LdifIcons.class);

}
