package io.gitlab.zlamalp.intellijplugin.ldif;

import com.intellij.lang.annotation.*;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.*;
import com.intellij.psi.util.PsiTreeUtil;
import java.util.Base64;

import io.gitlab.zlamalp.intellijplugin.ldap.LdapUtils;
import io.gitlab.zlamalp.intellijplugin.ldif.psi.LdifAttribute;
import io.gitlab.zlamalp.intellijplugin.ldif.psi.LdifDn;
import io.gitlab.zlamalp.intellijplugin.ldif.psi.LdifEntry;
import io.gitlab.zlamalp.intellijplugin.ldif.psi.LdifValueBase64;
import io.gitlab.zlamalp.intellijplugin.ldif.psi.LdifValueNormal;
import io.gitlab.zlamalp.intellijplugin.ldif.psi.LdifValueUrl;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Annotator for LDIF files.
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdifAnnotator implements Annotator {

	private static Map<String,Integer> dnRefAttributes = new HashMap<>();

	static {
		dnRefAttributes.put("member", 1);
		dnRefAttributes.put("memberOf", 1);
	}

	@Override
	public void annotate(@NotNull final PsiElement element, @NotNull AnnotationHolder holder) {

		// From example file...
		//TextRange range = new TextRange(element.getTextRange().getStartOffset(), element.getTextRange().getEndOffset());
		//holder.createErrorAnnotation(range, "Unresolved property").registerFix(new CreatePropertyQuickFix(key));

		/*
		if (element instanceof LdifControl) {
			for (ASTNode node : element.getNode().getChildren(TokenSet.create(LdifTypes.CRITICALITY))) {
			}
		}
		 */

		if (element instanceof LdifDn) {

			LdifDn currentDn = (LdifDn) element;
			LdifValueNormal val = currentDn.getValueNormal();
			LdifValueBase64 val64 = currentDn.getValueBase64();

			// annotates base64 values
			if (val64 != null) {

				TextRange range = new TextRange(val64.getTextRange().getStartOffset(), val64.getTextRange().getEndOffset());
				try {
					String value = new String(Base64.getDecoder().decode(val64.getText().replaceAll("\n ","")));
					holder.createInfoAnnotation(range, value);
				} catch (java.lang.IllegalArgumentException ex) {
					holder.createErrorAnnotation(range, "Invalid base64 value: "+ex.getMessage());
				} catch (Exception ex) {
					//holder.createInfoAnnotation(range, "Can't decode base64 value");
				}

			}

		}

		if (element instanceof LdifAttribute) {

			LdifAttribute currentAttribute = (LdifAttribute) element;
			LdifValueNormal val = currentAttribute.getValueNormal();
			LdifValueBase64 val64 = currentAttribute.getValueBase64();
			LdifValueUrl valUrl = currentAttribute.getValueUrl();

			// annotate plain values
			if (val != null && val.getText().endsWith(" ")) {
				TextRange range = new TextRange(val.getTextRange().getStartOffset(), val.getTextRange().getEndOffset());
				Annotation annotation = holder.createWarningAnnotation(range, "Values with trailing whitespace should be base64 encoded.");
				annotation.setNeedsUpdateOnTyping(true);
			}

			// annotates base64 values
			if (val64 != null) {

				TextRange range = new TextRange(val64.getTextRange().getStartOffset(), val64.getTextRange().getEndOffset());
				try {
					String value = new String(Base64.getDecoder().decode(val64.getText().replaceAll("\n ","")));
					if (!LdapUtils.binaryAttributeDetector.isBinary(currentAttribute.getAttributeKey().getText())) {
						holder.createInfoAnnotation(range, value);
					} else {
						holder.createInfoAnnotation(range, "Binary attributes are not decoded.");
					}
				} catch (java.lang.IllegalArgumentException ex) {
					holder.createErrorAnnotation(range, "Invalid base64 value: "+ex.getMessage());
				} catch (Exception ex) {
					//holder.createInfoAnnotation(range, "Can't decode base64 value");
				}

			}

		}

		if (element instanceof LdifValueNormal && dnRefAttributes.containsKey(element.getNode().getTreePrev().getTreePrev().getText())) {

			// value is type of DN, but no entry with such DN exists in a file - mark Undefined

			String value = ((LdifValueNormal) element).getText();
			value = value.replaceAll("\n ", "");
			//System.out.println("val: '"+value+"'");
			if (value.startsWith("cn=") || value.startsWith("uid=")) {
				List<LdifEntry> entries = LdifUtil.findEntriesInFile(element.getContainingFile(), value);
				if (entries.size() == 0) {
					PsiElement val = ((LdifValueNormal) element);
					TextRange range = new TextRange(val.getTextRange().getStartOffset(), val.getTextRange().getEndOffset());
					Annotation annotation = holder.createWarningAnnotation(range, "Referenced entry '"+value+"' not found in the file.");
					annotation.setNeedsUpdateOnTyping(true);
					//annotation.setTextAttributes(DefaultLanguageHighlighterColors.LINE_COMMENT);
				} else {

					LdifEntry currentEntry = PsiTreeUtil.getParentOfType(element, LdifEntry.class);
					List<LdifEntry> allEntries = LdifUtil.findEntriesInFile(element.getContainingFile());
					for (LdifEntry entry : allEntries) {
						if (entries.contains(entry)) {
							// found entry with DN before current entry (reference)
							break;
						}
						if (entry.equals(currentEntry)) {
							// defining entry is not before first reference within file
							PsiElement val = ((LdifValueNormal) element);
							TextRange range = new TextRange(val.getTextRange().getStartOffset(), val.getTextRange().getEndOffset());
							Annotation annotation = holder.createErrorAnnotation(range, "Reference to '"+value+"' used before entry definition.");
							annotation.setNeedsUpdateOnTyping(true);
							break;
						}
					}


				}
			}

		} else if (element instanceof LdifValueBase64 && dnRefAttributes.containsKey(element.getNode().getTreePrev().getTreePrev().getText())) {

			// value is type of DN, but no entry with such DN exists in a file - mark Undefined

			String value = new String(Base64.getDecoder().decode(((LdifValueBase64)element).getText().replaceAll("\n ","")));

			if (value.startsWith("cn=") || value.startsWith("uid=")) {
				List<LdifEntry> entries = LdifUtil.findEntriesInFile(element.getContainingFile(), value);
				if (entries.size() == 0) {
					PsiElement val = ((LdifValueBase64)element);
					TextRange range = new TextRange(val.getTextRange().getStartOffset(), val.getTextRange().getEndOffset());
					Annotation annotation = holder.createWarningAnnotation(range, "Referenced entry '"+value+"' not found in the file.");
					annotation.setNeedsUpdateOnTyping(true);
					//annotation.setTextAttributes(DefaultLanguageHighlighterColors.LINE_COMMENT);
				} else {

					LdifEntry currentEntry = PsiTreeUtil.getParentOfType(element, LdifEntry.class);
					List<LdifEntry> allEntries = LdifUtil.findEntriesInFile(element.getContainingFile());
					for (LdifEntry entry : allEntries) {
						if (entries.contains(entry)) {
							// found entry with DN before current entry (reference)
							break;
						}
						if (entry.equals(currentEntry)) {
							// defining entry is not before first reference within file
							PsiElement val = ((LdifValueBase64)element);
							TextRange range = new TextRange(val.getTextRange().getStartOffset(), val.getTextRange().getEndOffset());
							Annotation annotation = holder.createErrorAnnotation(range, "Reference to '"+value+"' used before entry definition.");
							annotation.setNeedsUpdateOnTyping(true);
							break;
						}
					}

				}
			}

		}

	}

}
