package io.gitlab.zlamalp.intellijplugin.ldif;

import com.intellij.lang.Commenter;
import org.jetbrains.annotations.Nullable;

/**
 * Allow use IDE shortcut to "comment" current line.
 * Block comments are not supported.
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdifCommenter implements Commenter {
	@Nullable
	@Override
	public String getLineCommentPrefix() {
		return "#";
	}

	@Nullable
	@Override
	public String getBlockCommentPrefix() {
		return null;
	}

	@Nullable
	@Override
	public String getBlockCommentSuffix() {
		return null;
	}

	@Nullable
	@Override
	public String getCommentedBlockCommentPrefix() {
		return null;
	}

	@Nullable
	@Override
	public String getCommentedBlockCommentSuffix() {
		return null;
	}
}
