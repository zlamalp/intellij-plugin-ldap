package io.gitlab.zlamalp.intellijplugin.ldif.psi;

import com.intellij.navigation.ItemPresentation;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import java.util.Base64;
import io.gitlab.zlamalp.intellijplugin.ldif.LdifIcons;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.util.Objects;

/**
 * All utility methods for PSI elements
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdifPsiUtil {

	/**
	 * Return name of LdifHeading element.
	 * In this case its a value part from "dn: value"
	 *
	 * @param element Element to get name for
	 * @return Name of LdifHeading element
	 */
	public static String getName(LdifDn element) {
		if (element.getValue() != null) {
			return element.getValue().getLastChild().getText();
		}
		return null;
		/*
		ASTNode valueNode = element.getNode().findChildByType(LdifTypes.VALUE);
		if (valueNode != null) {
			return valueNode.getText();
		} else {
			valueNode = element.getNode().findChildByType(LdifTypes.VALUE_64);
			if (valueNode != null) {
				return valueNode.getText();
			}
			return null;
		}
		*/
	}

	/**
	 * Set new name to LdifHeading element.
	 * In this case its a value part from "dn: value"
	 *
	 * @param element Element to set name for
	 * @param newName New name
	 * @return Updated LdifHeading element
	 */
	public static PsiElement setName(LdifDn element, String newName) {

		PsiElement value = element.getValue(); // normal or base64 value
		if (value != null) {
			//ASTNode keyNode = value.getNode();
			LdifDn property = LdifElementFactory.createLdifHeading(element.getProject(), newName);
			//ASTNode newKeyNode = property.getValue().getLastChild().getNode();
			element.getValue().replace(property.getValue());
			//element.getNode().replaceChild(keyNode, newKeyNode);
		}
		/*
		ASTNode keyNode = element.getNode().findChildByType(LdifTypes.VALUE);
		if (keyNode != null) {
			LdifHeading property = LdifElementFactory.createLdifHeading(element.getProject(), newName);
			ASTNode newKeyNode = property.getLastChild().getNode();
			element.getNode().replaceChild(keyNode, newKeyNode);
		}
		keyNode = element.getNode().findChildByType(LdifTypes.VALUE_64);
		if (keyNode != null) {
			LdifHeading property = LdifElementFactory.createLdifHeading(element.getProject(), newName);
			ASTNode newKeyNode = property.getLastChild().getNode();
			element.getNode().replaceChild(keyNode, newKeyNode);
		}
		*/
		return element;
	}

	public static PsiElement getNameIdentifier(LdifDn element) {
		return element.getValue();
		/*
		ASTNode keyNode = element.getNode().findChildByType(LdifTypes.VALUE);
		if (keyNode != null) {
			return keyNode.getPsi();
		} else {
			keyNode = element.getNode().findChildByType(LdifTypes.VALUE_64);
			if (keyNode != null) {
				return keyNode.getPsi();
			}
			return null;
		}
		*/
	}

	/**
	 * Return value of DN from element (normal or base64)
	 *
	 * @param element LdifHeading to get value from
	 * @return DN value of LdifHeading
	 */
	public static PsiElement getValue(LdifDn element) {

		if (element.getValueNormal() != null) {
			return element.getValueNormal();
		} else if (element.getValueBase64() != null) {
			return element.getValueBase64();
		}
		return null;
	}

	/**
	 * Return presentation of LdifDN element for a StructureView component.
	 *
	 * @param element LdifDn to process
	 * @return Expected item presentation
	 */
	public static ItemPresentation getPresentation(final LdifDn element) {
		return new ItemPresentation() {
			@Nullable
			@Override
			public String getPresentableText() {
				// textual representation of DN value
				if (element.getValueBase64() != null) {
					return new String(Base64.getDecoder().decode(element.getValue().getLastChild().getText()));
				} else {
					return element.getValue().getLastChild().getText();
				}

			}

			@Nullable
			@Override
			public String getLocationString() {
				PsiFile containingFile = element.getContainingFile();
				return containingFile == null ? null : containingFile.getName();
			}

			@Nullable
			@Override
			public Icon getIcon(boolean unused) {
				PsiElement rightSibling = element.getNextSibling();
				if (rightSibling instanceof LdifAttribute) {
					return LdifIcons.ENTRY;
				} else if (Objects.equals(rightSibling.getNode().getElementType(),LdifTypes.KEY_CHANGE)) {
					LdifEntry entry = (LdifEntry)element.getParent();
					if (entry.getChangetype() != null && entry.getChangetype().getChangeAdd() != null) return LdifIcons.ENTRY_ADD;
					if (entry.getChangetype() != null && entry.getChangetype().getChangeModify() != null) return LdifIcons.ENTRY_MOD;
					if (entry.getChangetype() != null && entry.getChangetype().getChangeDelete() != null) return LdifIcons.ENTRY_DEL;
					if (entry.getChangetype() != null && entry.getChangetype().getChangeModdn() != null) return LdifIcons.ENTRY_RENAME;
				}
				// unknown or partial entry
				return LdifIcons.ENTRY;
			}
		};
	}

	/*

	LDIF VALUE NORMAL UTILITY METHODS

	 */

}
