package io.gitlab.zlamalp.intellijplugin.ldif.psi;

import com.intellij.psi.PsiNameIdentifierOwner;

/**
 * Interface defining PSI elements, which can be named (renamed/referenced) etc.
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public interface LdifNamedElement extends PsiNameIdentifierOwner {

}
