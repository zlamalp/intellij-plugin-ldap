package io.gitlab.zlamalp.intellijplugin.ldif.psi;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import org.jetbrains.annotations.NotNull;

/**
 * Abstract class for all LDIF elements, which can be named/referenced.
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public abstract class LdifNamedElementImpl extends ASTWrapperPsiElement implements LdifNamedElement {

	public LdifNamedElementImpl(@NotNull ASTNode node) {
		super(node);
	}

}
