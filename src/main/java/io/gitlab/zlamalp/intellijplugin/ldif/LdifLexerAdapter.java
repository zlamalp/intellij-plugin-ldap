package io.gitlab.zlamalp.intellijplugin.ldif;

import com.intellij.lexer.FlexAdapter;

import java.io.Reader;

/**
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdifLexerAdapter extends FlexAdapter {

	public LdifLexerAdapter() {
		super(new LdifLexer((Reader) null));
	}

}
