package io.gitlab.zlamalp.intellijplugin.ldif;

import com.intellij.lang.Language;

/**
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdifLanguage extends Language {

	public static final LdifLanguage INSTANCE = new LdifLanguage();

	private LdifLanguage() {
		super("Ldif");
	}

}
