package io.gitlab.zlamalp.intellijplugin.ldif;

import com.intellij.LdapBundle;
import com.intellij.codeInsight.daemon.RelatedItemLineMarkerInfo;
import com.intellij.codeInsight.daemon.RelatedItemLineMarkerProvider;
import com.intellij.codeInsight.navigation.NavigationGutterIconBuilder;
import com.intellij.psi.PsiElement;
import io.gitlab.zlamalp.intellijplugin.ldif.psi.LdifAttribute;
import io.gitlab.zlamalp.intellijplugin.ldif.psi.LdifAttributeKey;
import io.gitlab.zlamalp.intellijplugin.ldif.psi.LdifEntry;
import org.apache.directory.api.ldap.model.constants.SchemaConstants;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.util.Base64;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Provide line marker in editor gutter.
 *
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdifLineMarkerProvider extends RelatedItemLineMarkerProvider {

	private static transient Logger log = LoggerFactory.getLogger(LdifLineMarkerProvider.class);

	private static Set<String> dnRefAttributes = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);

	static {
		dnRefAttributes.add(SchemaConstants.MEMBER_AT);
		dnRefAttributes.add(SchemaConstants.MEMBER_AT_OID);
		dnRefAttributes.add(SchemaConstants.UNIQUE_MEMBER_AT);
		dnRefAttributes.add(SchemaConstants.UNIQUE_MEMBER_AT_OID);
		// MS AD memberOf
		dnRefAttributes.add("memberOf");
		dnRefAttributes.add("1.2.840.113556.1.2.102");
	}

	@Override
	protected void collectNavigationMarkers(@NotNull PsiElement element, @NotNull Collection<? super RelatedItemLineMarkerInfo<?>> result) {

		if (element instanceof LdifAttributeKey) {
			if (dnRefAttributes.contains(element.getText())) {

				LdifAttribute attribute = (LdifAttribute) element.getParent();

				String value = "";
				if (attribute.getValueNormal() != null) {
					value = attribute.getValueNormal().getText().replaceAll("\n ", "");
				}
				if (attribute.getValueBase64() != null) {
					try {
						value = new String(Base64.getDecoder().decode(attribute.getText().replaceAll("\n ", "")));
					} catch (IllegalArgumentException ex) {
						log.warn("Couldn't decode base64 value of {} attribute: {}", element.getText(), ex);
					}
				}

				// element is a reference to LDIF Entry
				List<LdifEntry> entries = LdifUtil.findEntriesInFile(element.getContainingFile(), value);
				if (entries.size() > 0) {

					// FIXME - consider allow navigation to multiple usages/definitions
					// FIXME - for now use first entry in a file (from the top)

					LdifEntry entry = entries.get(0);
					Icon resultingIcon = LdifIcons.ENTRY;
					if (entry.getChangetype() != null && entry.getChangetype().getChangeAdd() != null)
						resultingIcon = LdifIcons.ENTRY_ADD;
					if (entry.getChangetype() != null && entry.getChangetype().getChangeModify() != null)
						resultingIcon = LdifIcons.ENTRY_MOD;
					if (entry.getChangetype() != null && entry.getChangetype().getChangeDelete() != null)
						resultingIcon = LdifIcons.ENTRY_DEL;
					if (entry.getChangetype() != null && entry.getChangetype().getChangeModdn() != null)
						resultingIcon = LdifIcons.ENTRY_RENAME;

					NavigationGutterIconBuilder<PsiElement> builder =
							NavigationGutterIconBuilder.create(resultingIcon).
									setTarget(entries.get(0)).
									setTooltipText(LdapBundle.message("ldif.linemarker.navigateToEntry.tooltip.text"));
					// put marker on first child PsiElement which is LdifTokenType.KEY
					result.add(builder.createLineMarkerInfo(element.getFirstChild()));
				}

			}
		}
	}

}
