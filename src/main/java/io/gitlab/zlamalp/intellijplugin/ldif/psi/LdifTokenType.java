package io.gitlab.zlamalp.intellijplugin.ldif.psi;

import com.intellij.psi.tree.IElementType;
import io.gitlab.zlamalp.intellijplugin.ldif.LdifLanguage;
import org.jetbrains.annotations.*;

/**
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdifTokenType extends IElementType {

	public LdifTokenType(@NotNull @NonNls String debugName) {
		super(debugName, LdifLanguage.INSTANCE);
	}

	@Override
	public String toString() {
		return "LdifTokenType." + super.toString();
	}

}
