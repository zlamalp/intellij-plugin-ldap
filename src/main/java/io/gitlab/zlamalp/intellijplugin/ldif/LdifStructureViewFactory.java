package io.gitlab.zlamalp.intellijplugin.ldif;

import com.intellij.ide.structureView.*;
import com.intellij.lang.PsiStructureViewFactory;
import com.intellij.openapi.editor.Editor;
import com.intellij.psi.PsiFile;
import org.jetbrains.annotations.*;

/**
 * @author Pavel Zlámal <zlamal@cesnet.cz>
 */
public class LdifStructureViewFactory implements PsiStructureViewFactory {

	@Nullable
	@Override
	public StructureViewBuilder getStructureViewBuilder(@NotNull final PsiFile psiFile) {
		return new TreeBasedStructureViewBuilder() {
			@NotNull
			@Override
			public StructureViewModel createStructureViewModel(@Nullable Editor editor) {
				return new LdifStructureViewModel(psiFile);
			}
		};
	}

}
