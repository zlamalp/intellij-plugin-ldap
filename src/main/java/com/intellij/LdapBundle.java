package com.intellij;

import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.PropertyKey;

import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.util.ResourceBundle;

public class LdapBundle {

	private static final String BUNDLE = "io.gitlab.zlamalp.intellijplugin.ldap.messages";
	private static Reference<ResourceBundle> ourBundle;

	public LdapBundle() {
	}

	@Nls
	@NotNull
	public static String message(@NotNull @PropertyKey(resourceBundle = BUNDLE) String key, @NotNull Object... params) {
		return message(getLdapBundle(), key, params);
	}

	@NotNull
	private static ResourceBundle getLdapBundle() {
		ResourceBundle bundle = com.intellij.reference.SoftReference.dereference(ourBundle);
		if (bundle == null) {
			bundle = ResourceBundle.getBundle(BUNDLE);
			ourBundle = new SoftReference<>(bundle);
		}
		return bundle;
	}

	public static String messageOrDefault(@Nullable ResourceBundle bundle, @NotNull String key, @Nullable String defaultValue, @NotNull Object... params) {
		if (bundle == null) return defaultValue;
		if (!bundle.containsKey(key)) {
			return BundleBase.postprocessValue(bundle, BundleBase.useDefaultValue(bundle, key, defaultValue), params);
		}
		return BundleBase.messageOrDefault(bundle, key, defaultValue, params);
	}

	@Nls
	@NotNull
	public static String message(@NotNull ResourceBundle bundle, @NotNull String key, @NotNull Object... params) {
		return BundleBase.message(bundle, key, params);
	}

	@Nullable
	public static String messageOfNull(@NotNull ResourceBundle bundle, @NotNull String key, @NotNull Object... params) {
		String value = messageOrDefault(bundle, key, key, params);
		if (key.equals(value)) return null;
		return value;
	}

}
