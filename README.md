# 'LDAP & LDIF Support' plugin for Intellij IDEA

* **Best-effort schema aware LDAP client with LDIF file suport**
* Should work in all JetBrains IDE products, not just Intellij IDEA. 
* Minimum required version is **2023.2**

> Originally based on a code and ideas from **LDAP Browser** plugin by *Attila Majoros* and **Apache Directory Studio** by *The Apache Software Foundation*

### Features

#### LDAP

* [x] Store LDAP connection settings within IDE or Project, wide range of configuration options.
* [x] Browse LDAP tree like DB plugins do
* [x] Add/Remove/Modify Entry attributes (using both add/remove or replace)
* [ ] Add new Entry
* [x] Rename and Move Entry
* [x] Delete Entry
* [ ] Change objectClass of Entry
* [x] Filtering and Custom search
* [x] Support R and R/W connections
* [x] Export Attribute/Entry/Branch/Schema to LDIF
* [x] Support for userPassword Check/Bind/Set/Change
* [ ] Support for common binary attributes (images, certs)
* [ ] Allow local changes to the Entry (change values of multiple Attributes) with update to LDAP at once
* [x] Collapsible rows for multivalued Attributes in Entry editor
* [x] Show decorated values in Entry editor / allow switching to see raw values
* [ ] Cache state of LDAP tree nodes in a TreeView/Panel
* [ ] Browse schema definition in GUI
* [x] (*partial support*) Parse Schema outside of Directory API library to provide best effort support for client-side Schema validation
* [ ] LDIF console to run ldapadd/ldapmodify actions against LDAP

#### LDIF

* [x] Support for LDIF file format (lexer, parser) according to RFC 2849
  * Format of DNs is not checked yet, now it's just a value/base64 value/url value
* [x] Support for syntax highlighting and color settings
* [x] (*partial support*) Support for file *Structure* view
* [ ] Support schema change LDIF syntax
* [ ] Treat/check syntax of DN values differently (by its own RFC)
* [ ] Understand and implement values references to support stuff like rename, find usages, auto-completion suggestions (especially cross-check between DN, memberOf, member properties)
* [ ] Better support for Structure view (display also ldif entry params)
* [ ] Implement default code completion
* [ ] Allow transformations between normal and base64 values as quick-fix (or display base64->normal on mouse hower - documentation/help widget?)
* [ ] Implement annotator to mark places where syntax is incorrect with proper message
* [ ] Consider checks on a whole ldif file, like ordering of related/structured entries

### How to build / run / install plugin

If you open the project in Intellij IDEA, it will be recognized as IDE plugin. **Gradle (>=8.3)** is used to build the plugin and manage its dependencies. If necessary, you can build plugin from sources using command:
```shell script
gradle buildPlugin
``` 
It will produce packaged zip file at:
```
build/distributions/intellij-plugin-ldap-{version}.zip
```
and it can be later manually installed into Intellij IDEA using its *Settings... -> Plugins -> Cog icon on top -> Install Plugin from Disk...* option.

You can run plugin within sand-boxed IDE instance. IDEA will provide run configuration by-default, but you can also use command:
```shell script
gradle runIde
```

#### LDIF part of plugin

Generated classes for LDIF are kept in sync for each commit, so you should have no problem building and using the plugin. If you make changes to the LDIF part of the plugin, you might need to regenerate them: 

* Install **Grammar-Kit** plugin
* Delete all files in *src/main/gen/* folders, with exception of *LdifParserUtil*
* Make any necessary changes to the sources
* Right-click on a grammar (*Ldif.bnf*) and select *Generate Parser Code*
* Right-click on a lexer (*Ldif.flex*) and select *Run JFlex Generator*
